<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">
<title><?php echo $this->titulo?></title>
<meta name="description" content="description">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="robots" content="all" />
<link rel="shorcut icon" href="favicon.png" />
          
<meta property="og:title" content="OG:TITULO"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="https://www.OG:URL.com/"/>
<meta property="og:image" content="OG:IMAGE"/>
<meta property="og:site_name" content="OG:SITE NAME"/>
<meta property="og:description" content="OG:DESCRIPTION"/>       

<!-- <link href="<?php echo $_params['ruta_css']?>bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo $_params['ruta_css']?>bootstrap-glyphicons.css" rel="stylesheet" type="text/css"> -->
<link href="<?php echo $_params['ruta_css']?>reset.css" rel="stylesheet">
<link href="<?php echo $_params['ruta_css']?>css.css" rel="stylesheet">
<link href="<?php echo $_params['ruta_css']?>owl.carousel.css" rel="stylesheet">
<link href="<?php echo $_params['ruta_css']?>font-awesome.min.css" rel="stylesheet">
<link href="<?php echo $_params['ruta_font']?>fonts.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Playfair+Display:700&display=swap" rel="stylesheet">

<?php if(isset($_params['css_plugin']) && count($_params['css_plugin'])):?>
<?php for($i=0;$i<count($_params['css_plugin']);$i++):?>
<link href="<?php echo $_params['css_plugin'][$i]?>" rel="stylesheet" type="text/css">
<?php endfor?>
<?php endif?> 

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_params['ruta_js']?>jquery.flexslider-min.js"></script>
<script src="<?php echo $_params['ruta_js']?>owl.carousel.min.js"></script>
<script src="<?php echo $_params['ruta_js']?>funciones.js"></script>
<script src="<?php echo $_params['ruta_js']?>masonry.js"></script>
<!-- <script src="https://unpkg.com/packery@2/dist/packery.pkgd.min.js"></script> -->


<!-- <script src="<?php echo $_params['ruta_js']?>bootstrap.min.js"></script> -->
<script src="<?php echo $_params['ruta_js']?>jquery.maskedinput.js"></script>
<script src="<?php echo $_params['ruta_js']?>dropzone.js"></script>

<?php if(isset($_params['js']) && count($_params['js'])):?>
<?php for($i=0;$i<count($_params['js']);$i++):?>
<script src="<?php echo $_params['js'][$i]?>" type="text/javascript"></script>
<?php endfor?>
<?php endif?>

<?php if(isset($_params['js_plugin']) && count($_params['js_plugin'])):?>
<?php for($i=0;$i<count($_params['js_plugin']);$i++):?>
<script src="<?php echo $_params['js_plugin'][$i]?>" type="text/javascript"></script>
<?php endfor?>
<?php endif?>

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script>

function cat_prev(_val){
      
       //var _val = _val;
        var valor = _val.split('_');
        
        var dataString = 'id='+valor[1]+'&tabla='+valor[0]+'&_csrf=<?=$this->_sess->get('_csrf')?>';
        $.ajax({            
                type: 'POST',
                url: _root_+"home/traerNotasCatPreview", 
                data: dataString,
                beforeSend: function(){
                  $('#contenido_cat_preview').fadeOut('fast').html('');
                  //$('.pagination').hide();
                  /*$('#contenedor').fadeOut('fast', function(){
                    $('.loader').fadeIn('fast');        
                  });*/
                },
                success: function(data){
                  $('#contenido_cat_preview').html(data).fadeIn('slow');
                  // $('#contenido_cat_preview').css('opacity',1)
                  //$('.pagination').hide();
                  /*$('.loader').fadeOut('fast', function(){
                    $('#contenedor').fadeIn('fast');  
                    $('.loader').fadeOut('fast');     
                  });*/
            
                },
                error: function(){
                     alert('error!!');
                }
        });
}
$(document).ready(function() {

    /*$('.sub_nav').on('click', function(e){
        e.preventDefault();
        $('.sub_nav .snav').slideUp(100)
        $(this).slideUp(200);


    });*/

    $('#bt_contacto').click(function(){

        if($('#nombre').val()==''){
          // alert("Debe ingresar un email");
          $('.error').html("Debe ingresar un nombre").fadeIn('slow');
          $("#nombre").focus();
          return false;
        }

        if ($("#numero_cliente").val() == "") {
          $('.error').html("Debe completar el campo numero de cliente").fadeIn('slow');
          // $("#numero_cliente").val('');
          $('#numero_cliente').focus();     
          //$("#titulo").attr('placeholder', 'Titulo');
          return false;        
        }

        if($('#email').val()==''){
          // alert("Debe ingresar un email");
          $('.error').html("Debe ingresar un email").fadeIn('slow');
          $("#email").focus();
          return false;
        }

        if ($("#nombre_farmacia").val() == "") {
          $('.error').html("Debe completar el campo nombre de farmacia").fadeIn('slow');
          // $("#numero_cliente").val('');
          $('#nombre_farmacia').focus();     
          //$("#titulo").attr('placeholder', 'Titulo');
          return false;        
        }
        
        if ($("#consulta").val() == "") {
          $('.error').html("Debe completar el campo consulta").fadeIn('slow');
          // $("#numero_cliente").val('');
          $('#consulta').focus();     
          //$("#titulo").attr('placeholder', 'Titulo');
          return false;        
        }
        
      

          //alert(url)
          $.ajax({
              type: 'POST',
              url: _root_+"contacto",
              data: $('#formContacto').serialize(),
              beforeSend: function(){
                //$('#juego').html('<option>Cargando...</option>');
              },        
              success: function(data){
                  if(data=='ok'){
                      $('.error').html('').fadeOut('fast');
                      $('.ok').html('Los datos se enviaron exitosamente').fadeIn('slow');
                      setTimeout(function() {
                            // window.location.href = _root_ + "perfil";
                            $('.ok').html('').fadeOut('fast');
                        }, 1000);
                      
                  }else{
                      $('.error').html(data).fadeIn('slow');
                  }
                
              }
          });
              
          
      return true;
        
      
      });

    $(".bt_cat").click(function(){  
    // $(".bt_cat").mouseover(function() {

        if($(this).hasClass('open')){   
          $('.sub_nav .snav').slideUp(100)
          $('.sub_nav').slideUp(200)
          $(this).removeClass('open')
          return false;
        }else{

          $('.bt_cat').removeClass('open');
          $(this).addClass('open');
    
          var sec = $(this).data('rel');
          // console.log(sec);
          // $('.sub_nav .snav').removeClass();
          // $('.sub_nav .snav').removeClass(this.className.split(' ').pop());

          var lastClass = $('.sub_nav .snav').attr('class').split(' ').pop();
          console.log(lastClass);
          if(lastClass!='snav'){
            $('.sub_nav .snav').removeClass(lastClass);
          }


          $('.sub_nav .snav').addClass(sec);

          


          var valor = $(this).data("id");
         
          var dataString = 'valor='+valor+'&_csrf=<?=$this->_sess->get('_csrf')?>';
          $.ajax({            
                  type: 'POST',
                  url: _root_+"home/traerNotasCat", 
                  data: dataString,
                  beforeSend: function(){
                     $('#contenido_cat').fadeOut('fast').html('');
                      $('.sub_nav .snav').slideUp(100)
                      // $('.sub_nav').slideUp(200)
                  },
                  success: function(data){
                    $('#contenido_cat').html(data).fadeIn('fast');
                    $('.sub_nav').slideDown(300, function(){
                        $('.sub_nav .snav.'+sec).stop(true,true).slideDown(500);      
                     });
                    
                    /*if($(this).hasClass('open')){   
                      $('.sub_nav .snav').slideUp(100)
                      $('.sub_nav').slideUp(200)
                      // $('.categories ul.cat a').removeClass('open')
                    }else{
                      $('.categories ul.cat a').removeClass('open')
                      $('.sub_nav .snav').stop(true,true).slideUp(100)
                      $('.sub_nav').slideDown(300, function(){
                        $('.sub_nav .snav.'+sec).stop(true,true).slideDown(500);      
                      });
                      $(this).toggleClass('open')
                    }
                    $('.sub_nav .snav .mid').removeAttr('style');*/
              
                  },
                  error: function(){
                       alert('error!!');
                  }
          });

        }
    
    
    })   

  $("#btBuscador").click(function(){  
  
  
      if ($("#buscador").val() == "") {
        return false;        
      } 

      $('#formTags').submit();
      
      /*var valor = $("#buscador").val();
      
      var dataString = 'valor='+valor+'&_csrf=<?=$this->_sess->get('_csrf')?>';
      $.ajax({            
              type: 'POST',
              url: _root_+"tags", 
              data: dataString,
              beforeSend: function(){
                //$('.pagination').hide();
                $('#contenedor').fadeOut('fast', function(){
                  $('.loader').fadeIn('fast');        
                });
              },
              success: function(data){
                $('#contenedor').html(data).fadeIn('fast');
                
              },
              error: function(){
                   alert('error!!');
              }
      });*/
    
    
    })   

     $('#buscador').on('keyup', function() {
       var key = $(this).val();   
       var dataString = 'key='+key+'&_csrf=<?=$this->_sess->get('_csrf')?>';
     $.ajax({
            type: "POST",
            url: _root_+"tags/traerTags", 
            data: dataString,
            success: function(data) {
                //Escribimos las sugerencias que nos manda la consulta
                $('#suggestions').fadeIn(1000).html(data);
                //Al hacer click en algua de las sugerencias
                $('.suggest-element').on('click', function(){
                        //Obtenemos la id unica de la sugerencia pulsada
                        var id = $(this).attr('id');
                        //Editamos el valor del input con data de la sugerencia pulsada
                        $('#buscador').val($('#'+id).attr('data'));
                        $('#buscar').val(id);
                        //Hacemos desaparecer el resto de sugerencias
                         $('#suggestions').fadeOut(200);
                        // alert('Has seleccionado el '+id+' '+$('#'+id).attr('data'));
                        return false;
                });
            }
        });
    });
  

});
</script>
<style type="text/css">
#content {
    margin-bottom: 25px;
}




#suggestions {
  box-shadow: 2px 2px 8px 0 rgba(0,0,0,.2);
  height: auto;
  position: absolute;
  top: 54px;
  z-index: 9999;
  width: 200px;
  font-family: 'Roboto Condensed', sans-serif;
  font-size: 10px;
  max-height: 400px;
  overflow-y: auto;
}

#suggestions .suggest-element {
    background-color: #eaeaea;
    border-top: 1px solid #d6d4d4;
    cursor: pointer;
    padding: 8px;
    width: 100%;
    float: left;
}
</style>
</head>
<body>

<!-- echo "<pre>";print_r($_SESSION);echo "</pre>"; -->


<!-- <a href="<?= $this->_conf['base_url']?>index/salir">salir</a> 
<br>
<h4>Hola <strong><?=$this->_sess->get('usuario_front')?></strong> | <?=home::traerUserPuntosTotalesStatic($this->_sess->get('id_usuario_front'))?> pts</h4>

<a href="<?= $this->_conf['base_url']?>perfil">ver perfil</a> - <a href="<?= $this->_conf['base_url']?>perfil/puntos">ver puntos</a> 
<br><br>	 -->

<!-- <header>
    <ul>
        <li><a href="<?= $this->_conf['base_url']?>home">Home</a></li>
        <li><a href="<?= $this->_conf['base_url']?>videos">Videos</a></li>
        <li><a href="<?= $this->_conf['base_url']?>capacitaciones">Capacitaciones</a></li>
        <li><a href="<?= $this->_conf['base_url']?>lanzamientos">Lanzamientos</a></li>
        <li><a href="<?= $this->_conf['base_url']?>tendencias">Tendencias y consejos</a></li>
        <li><a href="<?= $this->_conf['base_url']?>clubaliadas">club aliadas</a></li>
        <li><a href="<?= $this->_conf['base_url']?>tags">Buscador de tags</a></li>
    </ul>		
</header>

<ul>
<?php $_categorias = home::traerCategorias();?>
<?php if($_categorias):?>
<?php foreach ($_categorias as $val):?>
<li><a class="bt_cat" data-id="<?=$val['id']?>" href="javascript:void(0);"><?=$val['nombre']?></a></li>
<?php endforeach?>
<?php endif?>
</ul>
<div id="contenido_cat"></div>

<hr> -->



<header>
  <div class="strip">
    <div class="wp">
      <?php 
        $_dest = home::traerCapacitacionDestacada();
        $_cat=array();
        $_cat = explode(',', $_dest['categorias']);       
        $_arr_cat=array();
        foreach ($_cat as $val) {
          $_arr = home::traerCategoria($val);
          $_arr_cat[] = $_arr['nombre']; 
        }
        $_cate = implode(', ', $_arr_cat);

      ?>
      <?php if($_dest):?>
        <a href="<?=$this->_conf['url_enlace'].'capacitaciones/detalle/'.home::crearUrl($_dest['id'],$_dest['titulo'])?>">
          <strong><?=strtoupper($_dest['titulo'])?></strong> - <?=strtoupper($_cate)?>
        </a>
      <?php endif?>
      
      <div class="info">
        <a href="mailto:aliadas@drogdelsud.com.ar"><img src="<?php echo $_params['ruta_img']?>icon02.svg" alt="mail"> aliadas@drogdelsud.com.ar</a>
        <a href="tel:43099100"><img src="<?php echo $_params['ruta_img']?>icon01.svg" alt="tel"> (011) 4309-9100</a>
      </div>
    </div>
  </div>
  <div class="wp mainMenu">
    <!-- <a href="<?= $this->_conf['base_url']?>"> -->
    	<img src="<?php echo $_params['ruta_img']?>logo.svg" alt="Aliadas">
    <!-- </a> -->
    <nav>
      <div class="search">
        <form id="formTags" role="form" method="post" autocomplete="off" action="<?=$this->_conf['base_url']?>tags">
         <!--  <input type="text" name="search" placeholder="Buscar"> -->
          <input type="text" id="buscador" name="buscador" placeholder="Buscar">
          <input type="hidden" id="_csrf" name="_csrf" value="<?=$this->_sess->get('_csrf')?>" />
          <input type="hidden" id="buscar" name="buscar" value="" />
          <button id="btBuscador"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
        <div id="suggestions"></div>
      </div>
      <ul class="nav">
        <li><a href="<?= $this->_conf['base_url']?>linearios">Linearios & Vademecums</a></li>
        <!-- <li><a href="<?= $this->_conf['base_url']?>eventos">Eventos</a></li> -->
        <li><a href="<?= $this->_conf['base_url']?>videos">Videos</a></li>
        <li><a href="<?= $this->_conf['base_url']?>capacitaciones">Capacitaciones</a></li>
        <li><a href="<?= $this->_conf['base_url']?>tendencias">Tendencias</a></li>
        <li><a href="<?= $this->_conf['base_url']?>lanzamientos">Lanzamientos</a></li>
        <li><a href="<?= $this->_conf['base_url']?>clubaliadas">Club Aliadas</a></li>
      </ul>
    </nav>
    <div class="user">
      <div class="points"><?=home::traerUserPuntosTotalesStatic($this->_sess->get('id_usuario_front'))?> <strong>PTS.</strong></div>
      <i></i>
      <div class="icon" style="background-image: url(<?php echo $_params['ruta_img']?>avatar.svg)"></div>
      <span><?=$this->_sess->get('usuario_front')?></span>
       <!-- <a href="<?= $this->_conf['base_url']?>index/salir">salir</a> -->
      <em class="fa fa-ellipsis-v" aria-hidden="true"></em>
    </div>
    <div id="burger"><i></i><i></i><i></i></div>

  </div>
  <div class="categories">
    <div class="wp">
      <ul class="cat">
        <?php $_categorias = home::traerCategorias();?>
        <?php if($_categorias):?>
        <?php foreach ($_categorias as $val):?>
        <li><a class="bt_cat" data-id="<?=$val['id']?>" data-rel="<?=$val['clase']?>" href="javascript:void(0);"><?=$val['nombre']?></a></li>
        <?php endforeach?>
        <?php endif?>
       <!--  <li><a href="#" data-rel="dermo">Dermo</a></li>
        <li><a href="#" data-rel="makeup">Make Up</a></li>
        <li><a href="#" data-rel="fragancias">Fragancias</a></li>
        <li><a href="#" data-rel="electro">Electrobeauty</a></li> -->
      </ul>
    </div>
    

    <div class="sub_nav">
      <div class="snav">
        <div id="contenido_cat" class="wpSnav">
         
            
            <!-- <div class="box">
              <a href="#">UMA PERFUMA</a>
              <a href="#">Fragancias: ¿Qué se espera</a>
              <a href="#">UMA BLANC</a>
              <a href="#">Claves para impulsar las vent</a>
                
              <a href="#" class="btn-mas">Ver más ></a>

            </div>
            <div class="box">
              <h4>Lanzamientos</h4>

              <a href="#">GIESSO ESENCIA HOMBRE</a>
              <a href="#">We Rock! FOR MEN</a>
              <a href="#">Fragancias Rihanna</a>
              <a href="#">Shakira Dream</a>

              <a href="#" class="btn-mas">Ver más ></a>

            </div> -->

          
         <!--  <div id="contenido_cat_preview" class="mid _preview">
            <div class="image_news" style="background-image: url(<?php echo $_params['ruta_img']?>img-menu@2x.jpg)"></div>
            <div class="copy">
              <h3>Fragancias: ¿qué se espera  para este otoño?</h3>
              <h5>TENDENCIA fragancia</h5>
              <p>Todavía hay muchas mujeres que permanecen fieles a uno o dos perfumes durante todo el año y pasan por alto el cambio estacional. Un buen asesoramiento puede</p>
              <a href="#" class="btn-mas">Continuar leyendo ></a>
            </div> 
          </div>-->

        </div>
      </div>
      

      <!-- <div class="snav makeup">
        <div class="wpSnav">
          <div class="box">
            <a href="#">UMA PERFUMA</a>
            <a href="#">Fragancias: ¿Qué se espera</a>
            <a href="#">UMA BLANC</a>
            <a href="#">Claves para impulsar las vent</a>
              
              <a href="#" class="btn-mas">Ver más ></a>

          </div>
          <div class="box">
            <h4>Lanzamientos</h4>

            <a href="#">GIESSO ESENCIA HOMBRE</a>
            <a href="#">We Rock! FOR MEN</a>
            <a href="#">Fragancias Rihanna</a>
            <a href="#">Shakira Dream</a>

          </div>
          <div class="mid">
            <div class="image_news" style="background-image: url(img/img-menu@2x.jpg)"></div>
            <div class="copy">
              <h3>Fragancias: ¿qué se espera  para este otoño?</h3>
              <h5>TENDENCIA fragancia</h5>
              <p>Todavía hay muchas mujeres que permanecen fieles a uno o dos perfumes durante todo el año y pasan por alto el cambio estacional. Un buen asesoramiento puede  </p>

                <a href="#" class="btn-mas">Continuar leyendo ></a>
            </div>
          </div>
        </div>
      </div>
      <div class="snav fragancias">
        <div class="wpSnav">
          <div class="box">
            <a href="#">UMA PERFUMA</a>
            <a href="#">Fragancias: ¿Qué se espera</a>
            <a href="#">UMA BLANC</a>
            <a href="#">Claves para impulsar las vent</a>
              
              <a href="#" class="btn-mas">Ver más ></a>

          </div>
          <div class="box">
            <h4>Lanzamientos</h4>

            <a href="#">GIESSO ESENCIA HOMBRE</a>
            <a href="#">We Rock! FOR MEN</a>
            <a href="#">Fragancias Rihanna</a>
            <a href="#">Shakira Dream</a>

          </div>
          <div class="mid">
            <div class="image_news" style="background-image: url(img/img-menu@2x.jpg)"></div>
            <div class="copy">
              <h3>Fragancias: ¿qué se espera  para este otoño?</h3>
              <h5>TENDENCIA fragancia</h5>
              <p>Todavía hay muchas mujeres que permanecen fieles a uno o dos perfumes durante todo el año y pasan por alto el cambio estacional. Un buen asesoramiento puede  </p>

                <a href="#" class="btn-mas">Continuar leyendo ></a>
            </div>
          </div>
        </div>
      </div>
      <div class="snav electro">
        <div class="wpSnav">
          <div class="box">
            <a href="#">UMA PERFUMA</a>
            <a href="#">Fragancias: ¿Qué se espera</a>
            <a href="#">UMA BLANC</a>
            <a href="#">Claves para impulsar las vent</a>
              
              <a href="#" class="btn-mas">Ver más ></a>

          </div>
          <div class="box">
            <h4>Lanzamientos</h4>

            <a href="#">GIESSO ESENCIA HOMBRE</a>
            <a href="#">We Rock! FOR MEN</a>
            <a href="#">Fragancias Rihanna</a>
            <a href="#">Shakira Dream</a>

          </div>
          <div class="mid">
            <div class="image_news" style="background-image: url(img/img-menu@2x.jpg)"></div>
            <div class="copy">
              <h3>Fragancias: ¿qué se espera  para este otoño?</h3>
              <h5>TENDENCIA fragancia</h5>
              <p>Todavía hay muchas mujeres que permanecen fieles a uno o dos perfumes durante todo el año y pasan por alto el cambio estacional. Un buen asesoramiento puede  </p>

                <a href="#" class="btn-mas">Continuar leyendo ></a>
            </div>
          </div>
        </div>
      </div> -->
    </div>



  </div>
</header>