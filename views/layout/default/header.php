<!doctype html>
<html>
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0, maximum-scale=1.0, user-scalable=0">

<?php 
  $url = $_SERVER['REQUEST_URI'];

  switch ($url) {
    case '/home':
        $titulo = 'Aliadas, de Droguería del Sud';
        $descrip = 'La comunidad de especialistas en belleza que empodera y capacita dermoconsultoras para potenciar las ventas de las farmacias.';
        break;
    case '/login':
        $titulo = 'Iniciar Sesión';
        $descrip = 'La pasión es eso que nos mueve ¡Sumate vos también! ';
        break;
    case '/capacitaciones':
        $titulo = '¡Capacitate online!';
        $descrip = 'Todo para asesorar los clientes de tu farmacia como una experta.';
        break;
    case '/tendencias':
        $titulo = 'Tendencias';
        $descrip = 'Descubrí antes que nadie lo que se viene en belleza.';
        break;
    case '/videos':
        $titulo = 'Videos';
        $descrip = '¡Tutoriales de las principales marcas! ';
        break;
    case '/linearios':
        $titulo = 'Linearios y vademécums';
        $descrip = 'Optimizá la exhibición en tu farmacia para potenciar las ventas.';
        break;
    case '/lanzamientos':
        $titulo = 'Lanzamientos';
        $descrip = '¡Conocé los últimos productos de belleza y sumalos a tu farmacia!';
        break;
    default:
        $titulo = 'Aliadas, de Droguería del Sud';
        $descrip = 'La comunidad de especialistas en belleza que empodera y capacita dermoconsultoras para potenciar las ventas de las farmacias.';
        break;
}
echo '<!--';
echo $url;
echo $titulo;
echo '-->';
?>


<title><?= $titulo?></title>
<meta name="description" content="<?=$descrip?>">
<meta http-equiv="Content-Type" content="text/html; charset=ISO-8859-1" />
<meta name="robots" content="all" />
<link rel="shorcut icon" href="<?= $this->_conf['base_url']?>favicon.png" />
          
<meta property="og:title" content="Aliadas, de Droguería del Sud"/>
<meta property="og:type" content="website"/>
<meta property="og:url" content="https://www.aliadas.com.ar/"/>
<meta property="og:image" content="https://aliadas.com.ar/views/layout/default/img/img-share.jpg"/>
<meta property="og:site_name" content="Aliadas"/>
<meta property="og:description" content="La comunidad de especialistas en belleza que empodera y capacita dermoconsultoras para potenciar las ventas de las farmacias."/>         

<!-- <link href="<?php echo $_params['ruta_css']?>bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="<?php echo $_params['ruta_css']?>bootstrap-glyphicons.css" rel="stylesheet" type="text/css"> -->
<link href="<?php echo $_params['ruta_css']?>reset.css" rel="stylesheet">
<link href="<?php echo $_params['ruta_css']?>css.css" rel="stylesheet">
<link href="<?php echo $_params['ruta_css']?>css-2.css" rel="stylesheet">
<link href="<?php echo $_params['ruta_css']?>owl.carousel.css" rel="stylesheet">
<link href="<?php echo $_params['ruta_css']?>font-awesome.min.css" rel="stylesheet">
<link href="<?php echo $_params['ruta_font']?>fonts.css" rel="stylesheet">
<!-- <link href="https://fonts.googleapis.com/css?family=Montserrat:300,400,500,700|Playfair+Display:700&display=swap" rel="stylesheet"> -->
<link href="https://fonts.googleapis.com/css2?family=Barlow:wght@300;400;500&family=DM+Serif+Display:ital@0;1&display=swap" rel="stylesheet">

<?php if(isset($_params['css_plugin']) && count($_params['css_plugin'])):?>
<?php for($i=0;$i<count($_params['css_plugin']);$i++):?>
<link href="<?php echo $_params['css_plugin'][$i]?>" rel="stylesheet" type="text/css">
<?php endfor?>
<?php endif?> 

<script src="https://code.jquery.com/jquery-3.1.1.min.js"></script>
<script src="<?php echo $_params['ruta_js']?>jquery.flexslider-min.js"></script>
<script src="<?php echo $_params['ruta_js']?>owl.carousel.min.js"></script>
<script src="<?php echo $_params['ruta_js']?>funciones.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery.isotope/3.0.6/isotope.pkgd.min.js"></script>
<script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
<!-- <script src="https://unpkg.com/packery@2/dist/packery.pkgd.min.js"></script> -->


<!-- <script src="<?php echo $_params['ruta_js']?>bootstrap.min.js"></script> -->
<script src="<?php echo $_params['ruta_js']?>jquery.maskedinput.js"></script>
<script src="<?php echo $_params['ruta_js']?>dropzone.js"></script>

<?php if(isset($_params['js']) && count($_params['js'])):?>
<?php for($i=0;$i<count($_params['js']);$i++):?>
<script src="<?php echo $_params['js'][$i]?>" type="text/javascript"></script>
<?php endfor?>
<?php endif?>

<?php if(isset($_params['js_plugin']) && count($_params['js_plugin'])):?>
<?php for($i=0;$i<count($_params['js_plugin']);$i++):?>
<script src="<?php echo $_params['js_plugin'][$i]?>" type="text/javascript"></script>
<?php endfor?>
<?php endif?>

<!-- Just for debugging purposes. Don't actually copy this line! -->
<!--[if lt IE 9]><script src="../../docs-assets/js/ie8-responsive-file-warning.js"></script><![endif]-->

<!-- HTML5 shim and Respond.js IE8 support of HTML5 elements and media queries -->
<!--[if lt IE 9]>
  <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
  <script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

<script>

function cat_prev(_val){
      
       //var _val = _val;
        var valor = _val.split('_');
        
        var dataString = 'id='+valor[1]+'&tabla='+valor[0]+'&_csrf=<?=$this->_sess->get('_csrf')?>';
        $.ajax({            
                type: 'POST',
                url: _root_+"home/traerNotasCatPreview", 
                data: dataString,
                beforeSend: function(){
                  $('#contenido_cat_preview').fadeOut('fast').html('');
                  //$('.pagination').hide();
                  /*$('#contenedor').fadeOut('fast', function(){
                    $('.loader').fadeIn('fast');        
                  });*/
                },
                success: function(data){
                  $('#contenido_cat_preview').html(data).fadeIn('slow');
                  // $('#contenido_cat_preview').css('opacity',1)
                  //$('.pagination').hide();
                  /*$('.loader').fadeOut('fast', function(){
                    $('#contenedor').fadeIn('fast');  
                    $('.loader').fadeOut('fast');     
                  });*/
            
                },
                error: function(){
                     alert('error!!');
                }
        });
}
$(document).ready(function() {

    /*$('.sub_nav').on('click', function(e){
        e.preventDefault();
        $('.sub_nav .snav').slideUp(100)
        $(this).slideUp(200);


    });*/

    $('#bt_contacto').click(function(){

        if($('#nombre').val()==''){
          // alert("Debe ingresar un email");
          $('.error').html("Debe ingresar un nombre").fadeIn('slow');
          $("#nombre").focus();
          return false;
        }

        if ($("#numero_cliente").val() == "") {
          $('.error').html("Debe completar el campo numero de cliente").fadeIn('slow');
          // $("#numero_cliente").val('');
          $('#numero_cliente').focus();     
          //$("#titulo").attr('placeholder', 'Titulo');
          return false;        
        }

        if($('#email').val()==''){
          // alert("Debe ingresar un email");
          $('.error').html("Debe ingresar un email").fadeIn('slow');
          $("#email").focus();
          return false;
        }

        if ($("#nombre_farmacia").val() == "") {
          $('.error').html("Debe completar el campo nombre de farmacia").fadeIn('slow');
          // $("#numero_cliente").val('');
          $('#nombre_farmacia').focus();     
          //$("#titulo").attr('placeholder', 'Titulo');
          return false;        
        }
        
        if ($("#consulta").val() == "") {
          $('.error').html("Debe completar el campo consulta").fadeIn('slow');
          // $("#numero_cliente").val('');
          $('#consulta').focus();     
          //$("#titulo").attr('placeholder', 'Titulo');
          return false;        
        }
        
      

          //alert(url)
          $.ajax({
              type: 'POST',
              url: _root_+"contacto",
              data: $('#formContacto').serialize(),
              beforeSend: function(){
                //$('#juego').html('<option>Cargando...</option>');
              },        
              success: function(data){
                  if(data=='ok'){
                      $('.error').html('').fadeOut('fast');
                      $('.ok').html('Los datos se enviaron exitosamente').fadeIn('slow');
                      setTimeout(function() {
                            // window.location.href = _root_ + "perfil";
                            $('.ok').html('').fadeOut('fast');
                        }, 1000);
                      
                  }else{
                      $('.error').html(data).fadeIn('slow');
                  }
                
              }
          });
              
          
      return true;
        
      
      });

    $(".bt_cat").click(function(){  
    // $(".bt_cat").mouseover(function() {

        if($(this).hasClass('open')){   
          $('.sub_nav .snav').slideUp(100)
          $('.sub_nav').slideUp(200)
          $(this).removeClass('open')
          return false;
        }else{

          $('.bt_cat').removeClass('open');
          $(this).addClass('open');
    
          var sec = $(this).data('rel');
          // console.log(sec);
          // $('.sub_nav .snav').removeClass();
          // $('.sub_nav .snav').removeClass(this.className.split(' ').pop());

          var lastClass = $('.sub_nav .snav').attr('class').split(' ').pop();
          console.log(lastClass);
          if(lastClass!='snav'){
            $('.sub_nav .snav').removeClass(lastClass);
          }


          $('.sub_nav .snav').addClass(sec);

          


          var valor = $(this).data("id");
         
          var dataString = 'valor='+valor+'&_csrf=<?=$this->_sess->get('_csrf')?>';
          $.ajax({            
                  type: 'POST',
                  url: _root_+"home/traerNotasCat", 
                  data: dataString,
                  beforeSend: function(){
                     $('#contenido_cat').fadeOut('fast').html('');
                      $('.sub_nav .snav').slideUp(100)
                      // $('.sub_nav').slideUp(200)
                  },
                  success: function(data){
                    $('#contenido_cat').html(data).fadeIn('fast');
                    $('.sub_nav').slideDown(300, function(){
                        $('.sub_nav .snav.'+sec).stop(true,true).slideDown(500);      
                     });
                    
                    /*if($(this).hasClass('open')){   
                      $('.sub_nav .snav').slideUp(100)
                      $('.sub_nav').slideUp(200)
                      // $('.categories ul.cat a').removeClass('open')
                    }else{
                      $('.categories ul.cat a').removeClass('open')
                      $('.sub_nav .snav').stop(true,true).slideUp(100)
                      $('.sub_nav').slideDown(300, function(){
                        $('.sub_nav .snav.'+sec).stop(true,true).slideDown(500);      
                      });
                      $(this).toggleClass('open')
                    }
                    $('.sub_nav .snav .mid').removeAttr('style');*/
              
                  },
                  error: function(){
                       alert('error!!');
                  }
          });

        }
    
    
    })   

  $("#btBuscador").click(function(){  
  
  
      if ($("#buscador").val() == "") {
        return false;        
      } 

      $('#formTags').submit();
      
      /*var valor = $("#buscador").val();
      
      var dataString = 'valor='+valor+'&_csrf=<?=$this->_sess->get('_csrf')?>';
      $.ajax({            
              type: 'POST',
              url: _root_+"tags", 
              data: dataString,
              beforeSend: function(){
                //$('.pagination').hide();
                $('#contenedor').fadeOut('fast', function(){
                  $('.loader').fadeIn('fast');        
                });
              },
              success: function(data){
                $('#contenedor').html(data).fadeIn('fast');
                
              },
              error: function(){
                   alert('error!!');
              }
      });*/
    
    
    })   

    $('#buscador').on('keyup', function() {
       var key = $(this).val();   
       var dataString = 'key='+key+'&_csrf=<?=$this->_sess->get('_csrf')?>';
     $.ajax({
            type: "POST",
            url: _root_+"tags/traerTags", 
            data: dataString,
            success: function(data) {
                //Escribimos las sugerencias que nos manda la consulta
                $('#suggestions').fadeIn('fast').html(data);
                //Al hacer click en algua de las sugerencias
                $('.suggest-element').on('click', function(){
                        //Obtenemos la id unica de la sugerencia pulsada
                        var id = $(this).attr('id');
                        //Editamos el valor del input con data de la sugerencia pulsada
                        $('#buscador').val($('#'+id).attr('data'));
                        $('#buscar').val(id);
                        //Hacemos desaparecer el resto de sugerencias
                         $('#suggestions').fadeOut(200);
                        // alert('Has seleccionado el '+id+' '+$('#'+id).attr('data'));
                        return false;
                });
            }
        });
    });

    $('#orden_clubalidas').on('change', function() {
        // alert( this.value );
        window.location.href = _root_+"clubaliadas/ordenar/"+this.value;
    });






    //HEader    
    $(window).on('scroll', function(){
      var st = $(window).scrollTop();
      if(st > 60){
        $('header').addClass('st')
      }else{        
        $('header').removeClass('st')
      }
    })

});
</script>
<style type="text/css">
#content {
    margin-bottom: 25px;
}




#suggestions {
  box-shadow: 2px 2px 8px 0 rgba(0,0,0,.2);
  height: auto;
  position: absolute;
  top: 54px;
  z-index: 9999;
  width: 200px;
  font-family: 'Roboto Condensed', sans-serif;
  font-size: 10px;
  max-height: 400px;
  overflow-y: auto;
}

#suggestions .suggest-element {
    background-color: #eaeaea;
    border-top: 1px solid #d6d4d4;
    cursor: pointer;
    padding: 8px;
    width: 100%;
    float: left;
}
</style>
<!-- Global site tag (gtag.js) - Google Analytics -->
<!-- <script async src="https://www.googletagmanager.com/gtag/js?id=UA-136097121-1"></script>
<script>
  window.dataLayer = window.dataLayer || [];
  function gtag(){dataLayer.push(arguments);}
  gtag('js', new Date());

  gtag('config', 'UA-136097121-1');
</script> -->
<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-WLMKCSF');</script>

<!-- End Google Tag Manager -->
<!-- Facebook Pixel Code -->
<script>
  !function(f,b,e,v,n,t,s)
  {if(f.fbq)return;n=f.fbq=function(){n.callMethod?
  n.callMethod.apply(n,arguments):n.queue.push(arguments)};
  if(!f._fbq)f._fbq=n;n.push=n;n.loaded=!0;n.version='2.0';
  n.queue=[];t=b.createElement(e);t.async=!0;
  t.src=v;s=b.getElementsByTagName(e)[0];
  s.parentNode.insertBefore(t,s)}(window, document,'script',
  'https://connect.facebook.net/en_US/fbevents.js');
  fbq('init', '896333877487539');
  fbq('track', 'PageView');
</script>
</head>
<body>
<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-WLMKCSF"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->
<noscript><img height="1" width="1" style="display:none" src="https://www.facebook.com/tr?id=896333877487539&ev=PageView&noscript=1"/></noscript>
<!-- End Facebook Pixel Code -->
<header>
  <div class="strip">
    <div class="wp">
      <?php 
        $_dest = home::traerCapacitacionDestacada();
        $_cat=array();
        $_cat = explode(',', $_dest['categorias']);       
        $_arr_cat=array();
        foreach ($_cat as $val) {
          $_arr = home::traerCategoria($val);
          $_arr_cat[] = $_arr['nombre']; 
        }
        $_cate = implode(', ', $_arr_cat);

      ?>
      <?php if($_dest):?>
        <a href="<?=$this->_conf['url_enlace'].'capacitaciones/detalle/'.home::crearUrl($_dest['id'],$_dest['titulo'])?>">
          <strong><?=$_dest['titulo']?></strong> - <?=strtoupper($_cate)?>
        </a>
      <?php endif?>
      
      
    </div>
  </div>
  <div class="mainMenu">
    <a href="<?= $this->_conf['base_url']?>">
    	<img src="<?php echo $_params['ruta_img']?>logo.svg" alt="Aliadas" class="logo">
    </a>
    <nav>
      <div class="search">
        <form id="formTags" role="form" method="post" autocomplete="off" action="<?=$this->_conf['base_url']?>tags">
         <!--  <input type="text" name="search" placeholder="Buscar"> -->
          <input type="text" id="buscador" name="buscador" placeholder="Buscar">
          <input type="hidden" id="_csrf" name="_csrf" value="<?=$this->_sess->get('_csrf')?>" />
          <input type="hidden" id="buscar" name="buscar" value="" />
          <button id="btBuscador"><i class="fa fa-search" aria-hidden="true"></i></button>
        </form>
        <div id="suggestions"></div>
      </div>
      <ul class="nav">
        <li><a href="<?= $this->_conf['base_url']?>especialdetemporada" class="btnInv">Especial de temporada</a></li>
        <li><a href="<?= $this->_conf['base_url']?>capacitaciones">Capacitaciones</a></li>
        <li><a href="<?= $this->_conf['base_url']?>tendencias">Tendencias</a></li>
        <li><a href="<?= $this->_conf['base_url']?>videos">Videos</a></li>
        <li><a href="<?= $this->_conf['base_url']?>lanzamientos">Lanzamientos</a></li>
        <li><a href="<?= $this->_conf['base_url']?>linearios">Linearios & Vademecums</a></li>
        <li><a href="<?= $this->_conf['base_url']?>clubaliadas">Club Aliadas</a></li>
      </ul>
      <style>
        .btnInv{
          color: #fff;
          background-color: #e35660;
          display: inline-block;
          padding: 3px 9px 5px;
          border-radius: 11px;
        }
      </style>
    </nav>
    <div class="user">
    <?php $usr = $this->_sess->get('usuario_front'); ?>
    <?php if(isset($usr)): ?>

      <?php $_data_user = home::traerDataUsers($this->_sess->get('id_usuario_front'));?>
      <?php $_img_user = home::traerDataImagenPorIdentificador($_data_user['identificador'],'user');?>
      <?php if($_img_user !=''):?>
        <a href="<?= $this->_conf['base_url']?>perfil" class="icon" style="background-image: url(<?= $this->_conf['base_url'] . "public/img/subidas/user/". $_img_user->path ?>)"></a>
      <?php else:?>
      <a href="<?= $this->_conf['base_url']?>perfil" class="icon" style="background-image: url(<?php echo $_params['ruta_img']?>avatar.svg)"></a>
      <?php endif?>       
      
      <span><a href="<?= $this->_conf['base_url']?>perfil"><?=$this->_sess->get('usuario_front')?></a></span>
      <i></i>
      <a href="<?= $this->_conf['base_url']?>clubaliadas" class="points"><?= ( home::traerUserPuntosTotalesStatic($this->_sess->get('id_usuario_front'))!='') ?  home::traerUserPuntosTotalesStatic($this->_sess->get('id_usuario_front')) : 0?> <strong>PTS.</strong></a>
     
      <em class="fa fa-ellipsis-v" aria-hidden="true">
        
        <div class="sbnav">
          <a href="<?= $this->_conf['base_url']?>perfil">Perfil</a>
           <a href="<?= $this->_conf['base_url']?>index/salir">salir</a>
        </div>
      </em>
    <?php else: ?>
      <a href="<?= $this->_conf['base_url']?>login"  class="icon" style="background-image: url(<?php echo $_params['ruta_img']?>avatar.svg)"></a>
      <span><a href="<?= $this->_conf['base_url']?>login">Iniciar sesión</a></span>
    <?php endif; ?>
    </div>
    <div id="burger"><i></i><i></i><i></i></div>
    <div class="info">
      <a href="mailto:aliadas@delsud.com.ar"><img src="<?php echo $_params['ruta_img']?>icon02.svg" alt="mail"> aliadas@delsud.com.ar</a>
      <a href="tel:43099100"><img src="<?php echo $_params['ruta_img']?>icon01.svg" alt="tel"> (011) 4309-9100 int. 705</a>
    </div>
  </div>
  <div class="categories">
    <div class="wp">
      <?php if($this->_item !='clubaliadas'):?>
      <div id="filter">
        <span>Filtros:</span>
        <?php $_categorias = home::traerCategorias();?>
        <?php if($_categorias):?>
        <?php foreach ($_categorias as $val):?>
        <a data-id="<?=$val['id']?>" data-rel="<?=$val['clase']?>" href="javascript:void(0);"><?=$val['nombre']?></a>
        <?php endforeach?>
        <?php endif?>
      </div>
      <?php endif?>

      <?php if($this->_item=='clubaliadas'):?>
      <div id="filter">
        <span>Ordenar por puntos:</span>        
        <select name="orden_clubalidas" id="orden_clubalidas">
          <option value="desc">De mayor a menor</option>
          <option value="asc">De menor a mayor</option>
        </select>        
      </div>
      <div id="legal">¿Cómo sumo puntos?</div>
      <?php endif?>

    </div>
  </div>
</header>
<div id="popUplegal">
  <div class="content">
    <h3>¿Cómo sumo y utilizo mis puntos en Club Aliadas?</h3> 

    <p>Participando de las CAPACITACIONES. Por cada respuesta acertada del multiple choice de la capacitación, el participante ganará determinada cantidad de puntos. Los mismos irán de 5 a 10 puntos, dependiendo de la dificultad de la capacitación, y se acreditarán al finalizarla.</p>

    <p>Los puntos podrán ser canjeados por los productos incluidos en el catálogo de premios de la sección CLUB ALIADAS de nuestra web, únicamente por dueños y/o empleados de farmacias que sean clientes de Droguería del Sud S.A. Al realizar el canje, un correo automático se entregará al equipo de Aliadas, quienes se podrán en contacto dentro de las 72 hs hábiles vía mail con el ganador para gestionar la entrega. Para más información, escribir a <a href="mailto:aliadas@delsud.com.ar">aliadas@delsud.com.ar</a></p>
  </div>
</div>