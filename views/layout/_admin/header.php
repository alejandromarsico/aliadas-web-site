<!DOCTYPE html>
<html lang="es">
<head>
    <title><?php echo $this->titulo?></title>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link href="<?php echo $_params['ruta_css']?>bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $_params['ruta_css']?>bootstrap-glyphicons.css" rel="stylesheet" type="text/css">
    <link href="<?php echo $_params['ruta_css']?>jquery-ui.css" rel="stylesheet" type="text/css">
	
    <?php if(isset($_params['css']) && count($_params['css'])):?>
	<?php for($i=0;$i<count($_params['css']);$i++):?>
    <link href="<?=$_params['css'][$i]?>" rel="stylesheet" type="text/css">
    <?php endfor?>
    <?php endif?>
       
    <?php if(isset($_params['css_plugin']) && count($_params['css_plugin'])):?>
    <?php for($i=0;$i<count($_params['css_plugin']);$i++):?>
    <link href="<?php echo $_params['css_plugin'][$i]?>" rel="stylesheet" type="text/css">
    <?php endfor?>
    <?php endif?>
    
    <script src="<?php echo $_params['ruta_js']?>jquery-3.2.1.min.js"></script> 
    <script src="<?php echo $_params['ruta_js']?>jquery-ui.min.js"></script>        
    
    <?php if(isset($_params['js']) && count($_params['js'])):?>
    <?php for($i=0;$i<count($_params['js']);$i++):?>
    <script src="<?php echo $_params['js'][$i]?>" type="text/javascript"></script>
    <?php endfor?>
    <?php endif?>
    
    <?php if(isset($_params['js_plugin']) && count($_params['js_plugin'])):?>
    <?php for($i=0;$i<count($_params['js_plugin']);$i++):?>
    <script src="<?php echo $_params['js_plugin'][$i]?>" type="text/javascript"></script>
    <?php endfor?>
    <?php endif?>
    
    <?php if($this->_sess->get('_supermercado')==1):?>
    <link href="<?php echo $_params['ruta_css']?>estilos-verde.css" rel="stylesheet" type="text/css">          
    <?php else:?>
    <link href="<?php echo $_params['ruta_css']?>estilos.css" rel="stylesheet" type="text/css">
    <?php endif?>
</head>
    
<body>
    <?php //echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;?>
   <!--  <div class="navbar navbar-default navbar-fixed-top pie" role="navigation">
        <div class="container">
            
            <?php if($this->_sess->get('_supermercado')):?>
            <?php //if(isset($widgets['menu_administrador']))echo $widgets['menu_administrador'];?>
            <?php endif?>
           
           <?php if($this->_sess->get('autenticado')):?>
            
            <a href="<?php echo $this->_conf['url_enlace']?>usuarios/login/cerrar" class="navbar-brand pull-right">Salir</a>
            
            <?php else:?>
            
            <form class="navbar-form pull-right" method="post" action="<?php echo $this->_conf['base_url']?>usuarios/login">
                <input type="hidden" value="1" name="enviar">
                <input class="form-control" name="usuario" type="text" placeholder="Usuario" style="width:120px;">
                <input class="form-control" name="pass" type="password" placeholder="Password" style="width:120px;">
                <button type="submit" class="btn btn-default">Entrar</button>
            </form>
            
            <?php endif?>
        </div>
    </div> -->
            
    
    <div class="container-general">
        <?php # Menu Administrador - Usuarios # ?>
        <?php //if(isset($widgets['menu_administracion'])) echo $widgets['menu_administracion'];?>
        
        <?php //if($this->_sess->get('_supermercado')):?>
        <div class="nav-pill">
			<?php //if($this->_sess->get('level')!= 2):?>
            
            <div class="boxUser">
                <div class="wpUs">
                    <a href="<?php echo $this->_conf['base_url']?>administrador">
                        <div <?= ($this->_sess->get('_supermercado')) ? 'class="circle"' : ''?>></div>
                    </a>
                    <h4><?=$this->_sess->get('usuario')?></h4>                    
                </div>
            </div>
          

            <?php if($this->_sess->get('level')==1):?>
            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/usuarios" <?php echo ($this->_item=='usuarios') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-usuario.png"/>
                    Control de Usuarios         
                </a>
            </div>
            <?php endif?> 

          
            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/lanzamientos" <?php echo ($this->_item=='lanzamientos') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-conten.png"/>
                    Lanzamientos         
                </a>
            </div>

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/tendencias" <?php echo ($this->_item=='tendencias') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-conten.png"/>
                    Tendecias & Consejos         
                </a>
            </div>

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/clubaliadas" <?php echo ($this->_item=='clubaliadas') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-conten.png"/>
                    Club Aliadas        
                </a>
            </div>

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/capacitaciones" <?php echo ($this->_item=='capacitaciones') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-conten.png"/>
                    Capacitaciones       
                </a>
            </div>

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/users" <?php echo ($this->_item=='users') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-conten.png"/>
                    Usuarios       
                </a>
            </div>

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/banners" <?php echo ($this->_item=='banners') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-conten.png"/>
                    Banners       
                </a>
            </div>

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/comentarios" <?php echo ($this->_item=='comentarios') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-conten.png"/>
                    Comentarios       
                </a>
            </div>

            <!-- <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/contenidos" <?php echo ($this->_item=='contenidos') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-conten.png"/>
                    Ver Contenidos           
                </a>
            </div>

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/asignacion" <?php echo ($this->_item=='asignacion') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-pencil.png"/>
                    Asignar Contenido          
                </a>
            </div> 

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/visualizacion" <?php echo ($this->_item=='visualizacion') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-calendar.png"/>
                    Visualización General        
                </a>
            </div>

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/controlpantalla" <?php echo ($this->_item=='controlpantalla') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-monitor.png"/>
                    Control de pantallas         
                </a>
            </div>

            <div class="btns">
                <a href="<?php echo $this->_conf['base_url']?>administrador/totems" <?php echo ($this->_item=='totems') ? 'class="active"' : ''?>>
                    <img src="<?=$this->_conf['base_url']?>public/img/ico-totem.png"/>
                    Totems          
                </a>
            </div> -->
            
           
            
           

           <div class="btns exit">
                <a href="<?php echo $this->_conf['url_enlace']?>usuarios/login/cerrar">Salir</a>
            </div>
                        
             
            <?php //endif?>
        </div>
        <?php //endif?>
        <!--  <div class="col-lg-12">
            <noscript><p>Para el correcto funcionamiento debe tener el soporte para javascript habilitado</p></noscript>
            
            <?php if(isset($this->_error)):?>
            <div id="_errl" class="alert alert-danger">
                <a class="close" data-dismiss="alert">x</a>
                <?php echo $this->_error?>
            </div>
            <?php endif?>
            
            <?php if(isset($this->_mensaje)):?>
            <div id="_errl" class="alert alert-success">
                <a class="close" data-dismiss="alert">x</a>
                <?php echo $this->_mensaje?>
            </div>
            <?php endif?>
        </div> --> 
            