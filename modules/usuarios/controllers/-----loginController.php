<?php

use controllers\usuariosController\usuariosController;

class loginController extends usuariosController
{   
	
    public function __construct()
    {
        parent::__construct();
		$this->getLibrary('class.validador');	
		
		
    }
    
    public function index()
    {
        if($this->_sess->get('autenticado')){
            $this->redireccionar('administrador');
        }
        
        $this->_view->titulo = 'Iniciar Sesion';
        //echo "<pre>";print_r($_SESSION);echo "</pre>";
		
		if($_POST){
			
			
			if(validador::getInt('enviar') == 1){
				
				
				$this->_view->datos = $_POST;
				
				if(!validador::getAlphaNum('usuario')){
					$this->_view->_error = 'Debe introducir su nombre de usuario';
					$this->_view->renderizar('index','login');
					exit;
				}
				
				//if(!validador::getSql('pass',$this->_conf['baseDatos'])){
				if(!validador::getAlphaNum('pass')){
					$this->_view->_error = 'Debe introducir su contraseña';
					$this->_view->renderizar('index','login');
					exit;
				}
								
				

				$adServer = "ldap://cencosud.corp";
	
			    $ldap = ldap_connect($adServer);
			    $username = $_POST['usuario'];
			    $password = $_POST['pass'];

			    $ldaprdn = 'cencosud' . "\\" . $username;

			    ldap_set_option($ldap, LDAP_OPT_PROTOCOL_VERSION, 3);
			    ldap_set_option($ldap, LDAP_OPT_REFERRALS, 0);

			    $bind = @ldap_bind($ldap, $ldaprdn, $password);


			    if ($bind) {
			        $filter="(sAMAccountName=$username)";
			        $result = ldap_search($ldap,"dc=CENCOSUD,dc=CORP",$filter);
			        ldap_sort($ldap,$result,"sn");
			        $info = ldap_get_entries($ldap, $result);
			        for ($i=0; $i<$info["count"]; $i++){
			            if($info['count'] > 1)
			                break;
			            
			            /*echo "<p>You are accessing <strong> ". $info[$i]["sn"][0] .", " . $info[$i]["givenname"][0] ."</strong><br /> (" . $info[$i]["samaccountname"][0] .")</p>\n";
			            echo '<pre>'; print_r($info);echo '</pre>';
			            exit;*/
			            
			            $userDn = $info[$i]["distinguishedname"][0]; 
			            $_user_name = $info[$i]["samaccountname"][0];
			            $_user_mail = $info[$i]["mail"][0];			            
			            $_rol = explode(',', $info[$i]["memberof"][0]);
			            $_user_rol = explode('_', $_rol[0]);
			            $_grupos = $info[$i]["memberof"];
			        }
			        @ldap_close($ldap);
			    } else {
			        /*$msg = "Usuario y/o password incorrectos";
			        echo $msg;*/
			        $this->_view->_error = 'Usuario y/o contraseña incorrectos';
					$this->_view->renderizar('index','login');
					exit;
			    }


			    switch ($_user_rol[2]) {
			    	case 'SADM':
			    		$rol = 1;
			    		break;
			    	case 'ADM':
			    		$rol = 2;
			    		break;
			    	case 'VIS':
			    		$rol = 3;
			    		break;
			    	
			    	default:
			    		$rol = 1;
			    		break;
			    }


			    if($rol!=1){
					// $_prov = usuarios::traerProvGestion($row->id)->provincias;
					// $_prov = explode(',', $_prov);
					$_prov = '';

					foreach ($_grupos as $key => $val) {
						$_arr = explode('_', $val);
						/*$_array[$key]['super'] = $_arr[3];
						$_array[$key]['region'] = $_arr[4];

						$_super[] = $_arr[3];
						$_region[] = $_arr[4];*/
										

						if($_arr[0] == 'CN=GGG100' && $_arr[1] == 'CARTELERIA'){

							if($_arr[4] == 'NAC'){
								$this->_view->_error = 'La región no corresponde al rol asignado';
								$this->_view->renderizar('index','login');
								exit;
							}


							if($_arr[3] == 'J'){
								$_reg_jumbo[] =$_arr[4];
							}else{
								$_reg_disco[] =$_arr[4];
							}
						}

						
					}


					/*$_jumbo = array_search('J', $_super);
					$_disco = array_search('s', $_super);

					echo $_disco;*/


					if(isset($_reg_jumbo)){
						// echo '<pre>'; print_r($_reg_jumbo);echo '</pre>';						
						foreach ($_reg_jumbo as $val) {
							$_arr_prov_jumbo[]= admin::traerProvPorRegion($val);
						}
						$_prov_jumbo = array_reduce($_arr_prov_jumbo, 'array_merge', array());
						// $_prov_jumbo = implode(',', $result);
						$this->_sess->set('_super_jumbo', 1);
						$this->_sess->set('_prov_jumbo', $_prov_jumbo);
					}

					if(isset($_reg_disco)){
						// echo '<pre>'; print_r($_reg_disco);echo '</pre>';						
						foreach ($_reg_disco as $val) {
							$_arr_prov_disco[]= admin::traerProvPorRegion($val);
						}
						$_prov_disco = array_reduce($_arr_prov_disco, 'array_merge', array());
						// $_prov_disco = implode(',', $result);
						$this->_sess->set('_super_disco', 2);
						$this->_sess->set('_prov_disco', $_prov_jumbo);
					}



				}else{
					$_prov = '';
				}

				
				// 'find' si se busca un solo registro, 'all' si se busca solo 1
				/*$row = usuario::find(array(
										'conditions' => array(
														'usuario = ? AND pass = ?', 
														validador::getAlphaNum('usuario'), 
														Hash::getHash('sha512', validador::getPostParam('pass'), $this->_conf['hash_key'])
														)
											)
									);
				
				
				if(!$row){
					$this->_view->_error = 'Usuario y/o password incorrectos';
					$this->_view->renderizar('index','login');
					exit;
				}
				
				if($row->estado != 1){
					$this->_view->_error = 'Este usuario no esta habilitado';
					$this->_view->renderizar('index','login');
					exit;
				}
				
				if($row->role!=1){
					$_prov = usuarios::traerProvGestion($row->id)->provincias;
					$_prov = explode(',', $_prov);
				}else{
					$_prov = '';
				}*/
				
				

				$this->_sess->set('autenticado', true);
				$this->_sess->set('sesion_en_curso', true);
				$this->_sess->set('level', $rol);
				$this->_sess->set('usuario', $_user_name);
				$this->_sess->set('id_usuario', 1);
				$this->_sess->set('user_mail', $_user_mail);
				$this->_sess->set('tiempo', time());
				$this->_sess->set('_provincias', $_prov);
				
				$this->redireccionar('administrador');
			}
		}
        $this->_view->renderizar('index','login');
    }
    
	
	public function userData($carpeta)
	{
		foreach(glob($carpeta . "/*") as $archivos_carpeta){
			if (is_dir($archivos_carpeta)){
				userData($archivos_carpeta);
			}else{
				unlink($archivos_carpeta);
			}
		} 
		//rmdir($carpeta);
	}
	
	
    public function cerrar()
    {
        $this->_sess->destroy();
        $this->redireccionar();
    }
}