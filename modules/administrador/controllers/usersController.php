<?php

use controllers\administradorController\administradorController;

class usersController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		$this->getLibrary('class.admin');		
		$this->_trabajosGestion = new admin();
		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		
		
    }
    
    public function index()
    {	
		
		$this->redireccionar('administrador/users/listar');	
    }
	
	public function listar($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		// $this->_view->setCss(array('sweetalert'));
       //  $this->_view->setJs(array('sweetalert.min'));
		
		 $datos = $this->_trabajosGestion->traerUsers();

		 // echo "<pre>";print_r($datos);echo "</pre>";exit;
		
		$this->_view->datos = $paginador->paginar($datos, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/users/listar');
		
		 
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'users');	
    }
	
	
	
	
	/*
	
	public function vista_previa_catalogos($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('basic'));
		$this->_view->setJs(array('modernizr.2.5.3.min', 'turn'));
		
		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);
		$this->_view->imagenes = $this->_trabajosGestion->traerGaleriaPorIdentificador($this->_view->trabajo->identificador, $this->_view->trabajo->imagenes_orientacion);
		$this->_view->size = getimagesize($this->_conf['base_url']. 'public/img/subidas/catalogos/cat_'.$this->_view->trabajo->identificador.'/'.$this->_view->imagenes[0]->path);
		
		//echo $this->_view->size;
		//echo "<pre>";print_r($this->_view->size);exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('vista_previa_catalogos', 'tendencias');	
    }
	*/
	
	
	
	public function editar($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
			
		
		// $this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('jquery.maskedinput'));

		$this->_view->trabajo = $this->_trabajosGestion->traerUser($_id);
		$this->_view->cliente = admin::traerClientePorUsers($this->_view->trabajo['id_cliente']);
				
		// $this->_sess->set('edicion_actual', $this->_view->trabajo['identificador']);
		
		  // echo "<pre>";print_r($this->_view->_cliente);exit;		
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					// echo "<pre>";print_r($this->_view->data);exit;

					/*if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'capacitaciones');
						exit;
					} 

					if(!validador::getInt('codigo')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					}*/
					
					
				
					
						
								
					
					$cat = contenidos_cliente::find($this->_view->trabajo['id']);
					$cat->numero_cliente = $this->_view->data['numero_cliente'];
					$cat->razon_social = $this->_xss->xss_clean(validador::getTexto('razon_social'));
					$cat->ciudad = $this->_xss->xss_clean(validador::getTexto('ciudad'));
					$cat->provincia = $this->_xss->xss_clean(validador::getTexto('provincia'));
					// $cat->email = $this->_view->data['email'];
					// $cat->password = $this->_view->data['password'];
					// $cat->nombre = $this->_xss->xss_clean(validador::getTexto('nombre'));
					// $cat->fecha_nacimiento = $this->_view->data['fecha_nacimiento'];					
					$cat->save();
								
					

					// $this->_sess->destroy('edicion_actual');					
					$this->redireccionar('administrador/users');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editar', 'users');	
    }
	
	
	
	
	
	public function cargar($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		$this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited'));

		// $this->_view->categorias = $this->_trabajosGestion->traerCategorias();
		// $this->_view->tags = $this->_trabajosGestion->traerTags();

		 // echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		// echo "<pre>";print_r($this->_view->tags);echo "</pre>";exit;

		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					// echo "<pre>";print_r($_POST);exit;



					//Leer excel
	                $_excel = contenidos_archivos_exce::find(array('conditions' => array('id = ?', 1)));
	                if($_excel){
	                	require_once dirname(__FILE__) . '/../../../libs/PHPExcel/IOFactory.php';				

						$objReader = PHPExcel_IOFactory::createReader('Excel2007');
						//$objReader->setReadDataOnly(true);
						$objPHPExcel = $objReader->load(dirname(__FILE__) ."/../../../public/files/".$_excel->path);
						$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);

	                    $_cont= array();
	                    for ($i=0; $i < count($sheetData); $i++) { 
	                        if(count(array_filter($sheetData[$i])) != 0){
	                            $_cont[] = $sheetData[$i];
	                        }
	                    }

	                    /*$_contenido = serialize($_cont);
	                    $_contenido = base64_encode($_contenido);*/

						

	                    // echo "<pre>";print_r($_cont);echo "</pre>";exit;

	                    

	                    for ($i=1; $i < count($_cont); $i++) { 

	                    	$_dat = contenidos_cliente::find(array('conditions' => array('numero_cliente = ?', $_cont[$i][0])));
	                    	if(!$_dat){
	                    		$cat = new contenidos_cliente();
								$cat->numero_cliente = $_cont[$i][0];
								$cat->razon_social = $_cont[$i][1];
								$cat->ciudad = $_cont[$i][2];
								$cat->provincia = $_cont[$i][3];				
								$cat->fecha = date('Y-m-d');
								$cat->save();
	                    	}

	                    	
	                    }


	                    $this->_sess->destroy('carga_actual');
						$this->redireccionar('administrador/users');

						
	                }else{
						$this->redireccionar('error/access/404');				
					}

					
							
								
					
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'users');	
    }


   
	
	public function exportaDatos()
    {

    	$this->_acl->acceso('encargado_access');

		$_datos = $this->_trabajosGestion->traerUsersExport();

		// echo "<pre>";print_r($_datos);echo "</pre>";exit;

		$this->_view->filename = 'usuarios_'.date('d-m-Y').'.xls';	

		$this->_view->html ='<table border="1" cellpadding="10" cellspacing="0" bordercolor="#666666" style="border-collapse:collapse;">
		<tbody>
		<tr style="font-weight: bold;">		
		<td>Nombre</td>			
		<td>Email</td>	
		<td>Fecha de nacimiento</td>	
		<td>Nº Cliente</td>
		<td>Razón Social</td>
		<td>Fecha de alta</td>	
		</tr>';

		foreach ($_datos as $data) {
			$this->_view->html .='<tr>';
			$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($data['nombre']).'</td>';
			$this->_view->html .='<td valign="top">'.$data['email'].'</td>';
			$this->_view->html .='<td valign="top">'.$data['fecha_nacimiento'].'</td>';
			$this->_view->html .='<td valign="top">'.$data['numero_cliente'].'</td>';
			$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($data['razon_social']).'</td>';
			$this->_view->html .='<td valign="top">'.$data['fecha'].'</td>';
			
			// $this->_view->html .='<td valign="top">'.$data['fecha_nacimiento'].'</td>';
			// $this->_view->html .='<td valign="top">'.$data['email'].'</td>';
			// $this->_view->html .='<td valign="top">'.$data['password'].'</td>';
	    	$this->_view->html .='</tr>';
		}
	
		
		$this->_view->html .='</tbody>
		</table>';


		$this->_view->titulo = 'Administrador - Seguimiento';
		$this->_view->renderizar('exportar', 'users','vacio');



    }

    public function buscador()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerBuscadorUsers(ucwords(strtolower($_val)));
				
				/*echo "<pre>";print_r($this->_view->users);echo"</pre>";*/
				
				$_html = '';
				foreach($this->_view->prod as $prod){ 

					$_cliente = admin::traerClientePorUsers($prod['id_cliente']);
					
	        		$_html .='<div class="forum-item">
	        					<div class="row">
	        						<div class="col-md-10">
										<a href="" class="forum-item-title">'.$prod['nombre'].'</a>
										<small>Razon Social: <b>'.$_cliente['razon_social'].'</b></small>
									</div>
									
									<div class="col-md-2 forum-info">
						                <div class="tooltip-demo pull-right">   							
											<a class="btn btn-warning btn-round" href="'.$this->_conf['url_enlace'].'administrador/users/editar/'. $prod['id'].'">
												Detalle
											</a>&nbsp;&nbsp;
										</div>
					                </div>
					            </div>
					        </div>';
					
	        
				}			
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	
	
}