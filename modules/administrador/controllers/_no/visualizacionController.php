<?php

use controllers\administradorController\administradorController;

class visualizacionController extends administradorController
{
	public $_trabajosGestion;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		$this->getLibrary('class.admin');
		$this->_trabajosGestion = new admin();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';
		$this->_view->colors = array('#fd8586','#95ef98','#98ecf7','#ffe489','#bd8dbf','#7da7d9','#faad81','#c4df9c','#8781bd','#f06eaa','#f49ac0');
		
		unset($_SESSION['editar_asignacion']);
		unset($_SESSION['asignacion']);
		unset($_SESSION['_bitacora']);
		unset($_SESSION['_bitacora_old']);

		if(!$this->_sess->get('_supermercado') || $this->_sess->get('_supermercado')==''){
			$this->redireccionar('administrador');	
		}
		
    }
    
    public function index()
    {	
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('style_contenidos','sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));

		$_fecha = date('d-m-Y');
		//$_fecha = date('15-12-2019');
		$_mes = explode('-', $_fecha);
		$this->_view->mes =$_mes[1];
		$this->_view->anio =$_mes[2];
		$this->_view->prev_anio = ($this->_view->mes-1==0) ? $this->_view->anio-1 : $this->_view->anio;
		$this->_view->next_anio = ($this->_view->mes+1==13) ? $this->_view->anio+1 : $this->_view->anio;
		$this->_view->mesAnio = $_mes[2].'-'.$_mes[1];
		$this->_view->prev_mes = ($this->_view->mes-1==0) ? 12 : $this->_view->mes-1;
		$this->_view->next_mes = ($this->_view->mes+1==13) ? 1 : $this->_view->mes+1;
		
		$this->_view->totems = $this->_trabajosGestion->traerTotemsAsig($this->_view->mesAnio);	
		
		//echo "<pre>";print_r($this->_view->totems);echo "</pre>";exit;
		
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'visualizacion');	
    }
	
	public function eliminarAsignacion()
	{
		$this->_acl->acceso('encargado_access');

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		

				// echo "<pre>";print_r($_POST);echo "</pre>";exit;
			
				$_id = (int) $_POST['_id'];
				
				$_data = $this->_trabajosGestion->eliminarAsignacion($_id);
				if($_data==true){
					echo "ok";
				}				

			}else{
				$this->redireccionar('error/access/404');
			}
		}

		/*iif ($_data==false) {
			echo "enuso";
		}else{
			echo "ok";
		}*/
		
	}

	public function traerDataMes()
	{
		
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

				if(!validador::numerico($_POST['mes'])){
					$jsondata['error']= "El mes debe ser un valor numerico";
					echo json_encode($jsondata);
					exit;
				} 

				if(!validador::numerico($_POST['anio'])){
					$jsondata['error']= "El año debe ser un valor numerico";
					echo json_encode($jsondata);
					exit;
				} 
			
				$_mes = (int) $_POST['mes'];
				$_mes = ($_mes<10) ? '0'.$_mes : $_mes;
				$_anio = (int) $_POST['anio'];
				$_prev_anio = ($_mes-1==0) ? $_anio-1 : $_anio;
				$_next_anio = ($_mes+1==13) ? $_anio+1 : $_anio;
				$_mesAnio = $_anio.'-'.$_mes;
				$_prev_mes = ($_mes-1==0) ? 12 : $_mes-1;
				$_next_mes = ($_mes+1==13) ? 1 : $_mes+1;		
				
				$jsondata['mes'] ='<h4><a onclick="$().traerMes('.$_prev_mes.','.$_prev_anio.')" href="javascript:void(0)" style="color:#858585;font-weight: 100;margin-right: 20px"><</a> '.admin::convertirMes($_mes).' <a onclick="$().traerMes('.$_next_mes.','.$_next_anio.')" href="javascript:void(0)" style="color:#858585;font-weight: 100;margin-left: 20px">></a></h4>';
	      		
	      		$_totems = $this->_trabajosGestion->traerTotemsAsigPod($_mesAnio);	

	      		// echo "<pre>";print_r($_totems);echo "</pre>";exit;
	      		
	      		$jsondata['error']='';
	      		$jsondata['asig']='';

	      		if(!empty($_totems)) {
	      		
		      		foreach ($_totems as $data) {

		      			$_tot = admin::traerTotemPorIdPod($data['id_totem']);
		      			$jsondata['asig'] .= '<div class="wpVisual">
									            <div id="contenido_totem" class="branch">
									              <h4>
									                '.$_tot['nombre'].'
									                <br>
									                <small>'.$_tot['localidad'].'</small>
									              </h4>
									            </div>';
									        
						$jsondata['asig'] .= '<div id="contenido_totem" class="timeline">';
									              $_asig = admin::traerAsigsPorTotemStaticPod($data['id_totem'], $_mesAnio);
									              // echo "<pre>";print_r($_asig);echo "</pre>";exit;

									              foreach($_asig as $dat){
									              	if($dat['daydif'] < 15){$_width = '20%';}elseif($dat['daydif'] > 15 && $dat['daydif'] < 30){$_width = '30%';}else{$_width = '40%';}
									              	$rand = $this->_view->colors[array_rand($this->_view->colors,1)];
									             	$jsondata['asig'] .= '<div class="cont_asig" style="background-color:'.$rand.';width:'.$_width.'">
														                <div>
														                  <span>'.date('j-m-Y G:i', strtotime($dat['vigencia_desde'])).'hs'.' - '.date('j-m-Y G:i', strtotime($dat['vigencia_hasta'])).'hs</span>
														                </div> 
															            <div>
															                <a href="javascript:void(0);" class="_borrar_'. $dat['id'].'" title="Borrar">
														                      <img src="'.$this->_conf['base_url'].'public/img/ico-cruz.png" alt="eliminar"/>
														                  	</a>, 
														                </div>
													                  </div>';
													$jsondata['asig'] .= '<script>
															                $(document).ready(function () {
															                      $("._borrar_'.$dat['id'].'").click(function () {
															                          swal({
															                              title: "Estas seguro de borrar este contenido?",
															                              text: "Los datos se perderán permanentemente!",
															                              type: "warning",
															                              showCancelButton: true,
															                              confirmButtonColor: "#DD6B55",
															                              confirmButtonText: "Si, que se borre!",
															                              cancelButtonText: "No, mejor no!",
															                              closeOnConfirm: false,
															                              closeOnCancel: false },
															                          function (isConfirm) {
															                              if (isConfirm) {
															                                  var url= _root_ + "administrador/visualizacion/eliminarAsignacion";
															                                  var dataString = "_id='.$dat['id'].'&_csrf='.$this->_sess->get('_csrf').'";
															                                  $.ajax({
															                                          type: "POST",
															                                          url: url,
															                                          data: dataString,
															                                          success: function(data){
															                                            if(data=="ok"){
															                                              swal("Borrado!", "El contenido se borró con exito.", "success");
															                                              setTimeout(function() {
															                                                  location.reload();
															                                              }, 200);
															                                            }else{
															                                              swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
															                                            }
															                                              
															                                              
															                                          }
															                                  });
															                              } else {
															                                  swal("Cancelado", "El contenido esta guardado", "error");
															                              }
															                          });
															                      });
															                  });
															               </script>';
									              }
						$jsondata['asig'] .= '</div>                    
									        </div>';
						

		      		}

		      	}
	      		echo json_encode($jsondata);
	    		exit();

    		}else{
				$this->redireccionar('error/access/404');
			}
		}
	}
		
	
	
	//////////////////////////////////////////////////////////////////////

/*

	public function selecTotem($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('style_contenidos'));
		
		
		if($this->_sess->get('level')==1){
			$this->_view->provincias = $this->_trabajosGestion->traerProvincias();
		}else{
			$this->_view->provincias = $this->_trabajosGestion->traerFiltroProv($this->_sess->get('_provincias'));
		}
		
		$this->_view->tipo_locales = $this->_trabajosGestion->traerTipoLocales();
		
		//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('selec_totem', 'controlpantalla');	
    }
    public function traerTotems()
    {
    	$this->_acl->acceso('encargado_access');
    	if($_POST){
			
			$_data = $this->_trabajosGestion->traerFiltroTotemsAsig($this->_sess->get('_supermercado'), $_POST['prov'], $_POST['tipo_local']);
			//$_html='';
			foreach ($_data as $data) {
				$_array[] = $data->id;
				
			}
			
			//echo $_ids;exit;
			$_datos = $this->_trabajosGestion->traerFiltroAsig($_array);
			$_html='';
			foreach ($_datos as $data) {
				
				$_html .='<div id="'.$data->id.'" class="cont_totem">
							<div class="col-lg-6">
								<h3>'.admin::traerTotemPorID($data->id_totem)->nombre.'</h3>
								Direccion:<br><strong>'.admin::convertirCaracteres(admin::traerTotemPorID($data->id_totem)->direccion).'</strong>								
							</div>
							<div class="col-lg-6">
								<a class="btn btn-danger btn-sm btn-block" href="'.$this->_conf['url_enlace'].'administrador/controlpantalla/verContenido/'.$data->id_totem.'">Ver contenido</a>
								<a class="btn btn-danger btn-sm btn-block" href="'.$this->_conf['url_enlace'].'administrador/controlpantalla/modificarContenido/'.$data->id_totem.'">Modificar contenido</a>
							</div>
						</div>';
			}
			//echo "<pre>";print_r($_datos);echo "</pre>";exit;
			echo $_html;
    	}
    }
	
    public function verContenido($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('style_contenidos'));
		$this->_view->totem = $this->_trabajosGestion->traerTotem($_id);	
		$this->_view->asignaciones = $this->_trabajosGestion->traerAsigsPorTotem($_id);
		//echo "<pre>";print_r($this->_view->asignaciones);echo "</pre>";exit;
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('ver_contenido', 'controlpantalla');
    }
    public function modificarContenido($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('style_contenidos'));
		$this->_view->totem = $this->_trabajosGestion->traerTotem($_id);	
		$this->_view->asignaciones = $this->_trabajosGestion->traerAsigsPorTotem($_id);
		//echo "<pre>";print_r($this->_view->asignaciones);echo "</pre>";exit;
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('modificar_contenido', 'controlpantalla');
    }
	
	public function ultimoCambio($_totem, $_id)
    {
		$this->_acl->acceso('encargado_access');
		$_update = $this->_trabajosGestion->desHacerUltimoCambio($_id);
		
		if($_update==true){
			$this->redireccionar('administrador/controlpantalla/modificarContenido/'.$_totem);
		}
		
    }
	public function consolidarTotems()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){
			$_SESSION['asignacion']['_totems']  = $_POST['totems'];
			//echo "<pre>";print_r($_SESSION['seccion']);exit;
			echo '1';
            exit();
				
        }
		
    }
     public function editarContenido($_id)
    {
    	$this->_acl->acceso('encargado_access');
		
		$_asig = $this->_trabajosGestion->traerAsignacion($_id);
		unset($_SESSION['asignacion']);
		//unset($_SESSION['asignacion_old']);
		$_SESSION['asignacion']['_id'] = $_id;
		$_SESSION['asignacion']['_catalogos'] = $_asig->ids_catalogos;
		$_SESSION['asignacion']['_videos'] = $_asig->ids_videos;
		$_SESSION['asignacion']['_footer'] = $_asig->ids_footers;
		$_SESSION['asignacion']['_fondos'] = $_asig->ids_fondos;
		$_SESSION['asignacion']['_fecha_inicial'] = $_asig->vigencia_desde->format('d-m-Y H:i');
		$_SESSION['asignacion']['_fecha_final'] = $_asig->vigencia_hasta->format('d-m-Y H:i');
		
		$this->redireccionar('administrador/controlpantalla/cargarHeader');
    	
    }
    public function cargarHeader()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->videos = $this->_trabajosGestion->traerVideos();
		// $data = contenidos_asignacione::find(2)->to_array();
		// echo "<pre>";print_r($data);echo "</pre>";exit;
		
		echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_header', 'controlpantalla');
    }
    public function consolidarHeader()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){
			//echo "<pre>";print_r($_POST);exit;
         	
			$_SESSION['asignacion']['_videos']  = $_POST['videos'];
			//
			//echo "<pre>";print_r($_SESSION['seccion']);exit;
			echo '1';
            exit();
				
        }
		
    }
	public function cargarCatalogos()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->catalogos = $this->_trabajosGestion->traerCatalogos();
		echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_catalogos', 'controlpantalla');
    }
    public function consolidarCatalogos()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){
			//echo "<pre>";print_r($_POST);exit;         	
			$_SESSION['asignacion']['_catalogos']  = $_POST['catalogos'];
			//
			//echo "<pre>";print_r($_SESSION['seccion']);exit;
			echo '1';
            exit();
				
        }
		
    }
    public function cargarFooter()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->footers = $this->_trabajosGestion->traerPromociones();
		echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_footers', 'controlpantalla');
    }
    public function consolidarFooter()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){
			//echo "<pre>";print_r($_POST);exit;         	
			$_SESSION['asignacion']['_footer']  = $_POST['footer'];
			//
			//echo "<pre>";print_r($_SESSION['seccion']);exit;
			echo '1';
            exit();
				
        }
		
    }
    public function cargarFondos()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->fondos = $this->_trabajosGestion->traerFondos();
		echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_fondos', 'controlpantalla');
    }
    public function consolidarFondos()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){
			//echo "<pre>";print_r($_POST);exit;         	
			$_SESSION['asignacion']['_fondos']  = $_POST['fondos'];
			//
			//echo "<pre>";print_r($_SESSION['asignacion']);exit;
			echo '1';
            exit();
				
        }
		
    }
 	public function selecVigencia()
    {
    	$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('jquery-ui-timepicker-addon'));
		$this->_view->setJs(array('jquery-ui-timepicker-addon'));
		//$this->_view->setCss(array('style_contenidos'));
		
		//$this->_view->fondos = $this->_trabajosGestion->traerFondos();
		echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		if(isset($_SESSION['asignacion']['_fecha_inicial'])){
			$time_desde = strtotime($_SESSION['asignacion']['_fecha_inicial']);
			$this->_view->fecha_inicial = date('d-m-Y H:i',$time_desde);
		}
		if(isset($_SESSION['asignacion']['_fecha_final'])){
			$time_hasta = strtotime($_SESSION['asignacion']['_fecha_final']);
			$this->_view->fecha_final = date('d-m-Y H:i',$time_hasta);
		}
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('selec_vigencia', 'controlpantalla');
    }
	public function consolidarVigencia()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){
			//echo "<pre>";print_r($_POST);exit; 
			$time_desde = strtotime($_POST['fecha_inicial']);
			$time_hasta = strtotime($_POST['fecha_final']);
			$_vigencia_desde = date('Y-m-d H:i:s',$time_desde);
			$_vigencia_hasta = date('Y-m-d H:i:s',$time_hasta);
					        	
			$_SESSION['asignacion']['_fecha_inicial']  = $_vigencia_desde;
			$_SESSION['asignacion']['_fecha_final']  = $_vigencia_hasta;
			$_editar = $this->_trabajosGestion->editarAsignacion($_SESSION['asignacion'], $this->_sess->get('usuario'));
			if($_editar == true){
				echo '1';
            	exit();
			}
			
			//echo "<pre>";print_r($_SESSION['asignacion']);exit;
			
				
        }
		
    }

*/
	
}