<?php

use controllers\administradorController\administradorController;

class asignacionController extends administradorController
{
	public $_trabajosGestion;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		$this->getLibrary('class.admin');
		$this->_trabajosGestion = new admin();
		
		$this->getLibrary('class.upload');

		$this->getLibrary('PHPMailerAutoload');
		$this->envioMail = new PHPMailer();
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		unset($_SESSION['editar_asignacion']);
		unset($_SESSION['_bitacora']);
		unset($_SESSION['_bitacora_old']);
		//unset($_SESSION['asignacion']);

		if(!$this->_sess->get('_supermercado') || $this->_sess->get('_supermercado')==''){
			$this->redireccionar('administrador');	
		}
		
		
    }
    
    public function index()
    {	
		unset($_SESSION['asignacion']);
		$this->redireccionar('administrador/asignacion/selecTotem');	
    }
	
		
	
	public function selecTotem($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('style_contenidos','fastselect.min'));
		$this->_view->setJs(array('fastsearch.min','fastselect.min'));
		
		/*$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();*/
		if($this->_sess->get('level')==1){
			$this->_view->provincias = $this->_trabajosGestion->traerProvincias();
		}else{

			/*if($this->_sess->get('_supermercado') == 1 && isset($this->_sess->get('_prov_jumbo'))){
				$this->_view->provincias = $this->_trabajosGestion->traerFiltroProv($this->_sess->get('_prov_jumbo'));
			}else if($this->_sess->get('_supermercado') == 2 && isset($this->_sess->get('_prov_disco'))){
				$this->_view->provincias = $this->_trabajosGestion->traerFiltroProv($this->_sess->get('_prov_disco'));
			}*/

			if($this->_sess->get('_prov_jumbo') && $this->_sess->get('_supermercado') == 1){
				$this->_view->provincias = $this->_trabajosGestion->traerFiltroProv($this->_sess->get('_prov_jumbo'));
			}else if($this->_sess->get('_prov_disco') && $this->_sess->get('_supermercado') == 2){
				$this->_view->provincias = $this->_trabajosGestion->traerFiltroProv($this->_sess->get('_prov_disco'));
			}
			// $this->_view->provincias = $this->_trabajosGestion->traerFiltroProv($this->_sess->get('_provincias'));
		}
		
		$this->_view->tipo_locales = $this->_trabajosGestion->traerTipoLocales();
		
		//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('selec_totem', 'asignacion');	
    }


    public function traerTotems()
    {
    	$this->_acl->acceso('encargado_access');
    	
    	if($_POST){

    		if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				// $_excluidos = (isset($_POST['excluidos'])) ? 'si' : 'no';

				foreach ($_POST['prov'] as $val) {
	    			if(!validador::numerico($val)) exit("Debe introducir un valor correcto en el campo provincias");
	    		}

	    		foreach ($_POST['tipo_local'] as $val) {
	    			if(!validador::numerico($val)) exit("Debe introducir un valor correcto en el campo tipo local");
	    		}
	    		
				if(isset($_POST['excluidos'])){
	    			if(!validador::getTexto('excluidos')) exit('Debe introducir un valor correcto en el campo excluidos');
	    			$_excluidos = 'si';
	    		}else{
	    			$_excluidos = 'no';
	    		}


				
				if($this->_sess->get('level')==1){
	    			$_data = $this->_trabajosGestion->traerFiltroTotemsPod($this->_sess->get('_supermercado'), $_POST['prov'], $_POST['tipo_local'], $_excluidos);			
				}else{

					$_prov = ($this->_sess->get('_super_jumbo')) ? $this->_sess->get('_prov_jumbo') : $this->_sess->get('_prov_disco');
					foreach ($_POST['prov'] as $val) {
						if(!in_array($val, $_prov)){
							echo '<h3>Consulta invalida</h3>';
	    					exit;
						}
					}			
					
					$_data = $this->_trabajosGestion->traerFiltroTotemsPod($this->_sess->get('_supermercado'), $_POST['prov'], $_POST['tipo_local'], $_excluidos);   			

				}			

				// echo "<pre>";print_r($_data);echo "</pre>";exit;
				
				if(!empty($_data)){
					$_html='';
					foreach ($_data as $data) {
						$_provincia = admin::traerProvinciaPorIDPod($data['id_provincia']);
						$_html .='<li id="'.$data['id'].'" class="cont_totem">
									<h3>'.$data['nombre'].'</h3>
									Direccion:<br>'.admin::convertirCaracteres($data['direccion']).'
									<p>Provincia:<br>'.$_provincia['nombre'].'</p>
								</li>';
					}
				}else{
					$_html='nada';
				}
				//echo "<pre>";print_r($_data);echo "</pre>";
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
    	}
    }
	
	public function consolidarTotems()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				//echo "<pre>";print_r($_POST);exit;
	         	
	         	// if(!validador::getTexto('_menu')) exit('<span class="text-danger">Debe seleccionar un Menu</span>');
	        	// if(!validador::getTexto('_titulo')) exit('<span class="text-danger">Debe seleccionar un Titulo</span>');
	        	//unset($_SESSION['seccion']);
				
				/*$_SESSION['seccion']['_menu'] = validador::getTexto('_menu');
				$_SESSION['seccion']['_titulo'] = validador::getTexto('_titulo');*/
				$_SESSION['asignacion']['_totems']  = $_POST['totems'];
	        	/*if($_POST['_submenu'][0]==''){
	        		$_SESSION['seccion']['_submenu'] = 'ninguno';
	        	}else{
	        		$_SESSION['seccion']['_submenu'] = $_POST['_submenu'];
	        	}*/
				
				//
				//echo "<pre>";print_r($_SESSION['seccion']);exit;
				echo '1';
	            exit();

            }else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    }
    public function cargarHeader()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->videos = $this->_trabajosGestion->traerVideos();
		//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_header', 'asignacion');
    }
    public function consolidarHeader()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
				//echo "<pre>";print_r($_POST);exit;
	         	
	         	// if(!validador::getTexto('_menu')) exit('<span class="text-danger">Debe seleccionar un Menu</span>');
	        	// if(!validador::getTexto('_titulo')) exit('<span class="text-danger">Debe seleccionar un Titulo</span>');
	        	//unset($_SESSION['seccion']);
				
				/*$_SESSION['seccion']['_menu'] = validador::getTexto('_menu');
				$_SESSION['seccion']['_titulo'] = validador::getTexto('_titulo');*/
				$_SESSION['asignacion']['_videos']  = $_POST['videos'];
	        	/*if($_POST['_submenu'][0]==''){
	        		$_SESSION['seccion']['_submenu'] = 'ninguno';
	        	}else{
	        		$_SESSION['seccion']['_submenu'] = $_POST['_submenu'];
	        	}*/
				
				//
				//echo "<pre>";print_r($_SESSION['seccion']);exit;
				echo '1';
	            exit();

            }else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    }
	public function cargarCatalogos()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->catalogos = $this->_trabajosGestion->traerCatalogos();
		//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_catalogos', 'asignacion');
    }
    public function consolidarCatalogos()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
				//echo "<pre>";print_r($_POST);exit;         	
				$_SESSION['asignacion']['_catalogos']  = $_POST['catalogos'];
				//
				//echo "<pre>";print_r($_SESSION['seccion']);exit;
				echo '1';
	            exit();

            }else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    }
    public function cargarFooter()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->footers = $this->_trabajosGestion->traerPromociones();
		//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_footers', 'asignacion');
    }
    public function consolidarFooter()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				//echo "<pre>";print_r($_POST);exit;         	
				$_SESSION['asignacion']['_footer']  = $_POST['footer'];
				//
				//echo "<pre>";print_r($_SESSION['seccion']);exit;
				echo '1';
	            exit();

            }else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    }
    public function cargarFondos()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->fondos = $this->_trabajosGestion->traerFondos();
		//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_fondos', 'asignacion');
    }
    public function consolidarFondos()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				//echo "<pre>";print_r($_POST);exit;         	
				$_SESSION['asignacion']['_fondos']  = $_POST['fondos'];
				//
				//echo "<pre>";print_r($_SESSION['asignacion']);exit;
				echo '1';
	            exit();
			
			}else{
				$this->redireccionar('error/access/404');
			}	
        }
		
    }

    public function cargarInactivas()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->inactivas = $this->_trabajosGestion->traerInactivas();
		//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_inactivas', 'asignacion');
    }

    public function consolidarInactivas()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				//echo "<pre>";print_r($_POST);exit;         	
				$_SESSION['asignacion']['_inactivas']  = $_POST['inactivas'];
				//
				//echo "<pre>";print_r($_SESSION['asignacion']);exit;
				echo '1';
	            exit();

			}else{
				$this->redireccionar('error/access/404');
			}		
        }
		
    }

 	public function selecVigencia()
    {
    	$this->_acl->acceso('encargado_access');
		
		/*$this->_view->setCss(array('jquery-ui-timepicker-addon','sweetalert'));
		$this->_view->setJs(array('jquery-ui-timepicker-addon','sweetalert.min'))*/;

		$this->_view->setCss(array('jquery-ui-timepicker-addon','sweetalert','fastselect.min','style_contenidos'));
		$this->_view->setJs(array('jquery-ui-timepicker-addon','sweetalert.min','fastsearch.min','fastselect.min'));
		//$this->_view->setCss(array('style_contenidos'));
		
		//$this->_view->fondos = $this->_trabajosGestion->traerFondos();
		//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		if(isset($_SESSION['asignacion']['_fecha_inicial'])){
			$time_desde = strtotime($_SESSION['asignacion']['_fecha_inicial']);
			$this->_view->fecha_inicial = date('d-m-Y H:i',$time_desde);
		}
		if(isset($_SESSION['asignacion']['_fecha_final'])){
			$time_hasta = strtotime($_SESSION['asignacion']['_fecha_final']);
			$this->_view->fecha_final = date('d-m-Y H:i',$time_hasta);
		}

		$this->_view->dias = array('Lunes','Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');

		if(isset($_SESSION['asignacion']['_dias_semana'])){
			$this->_view->dias_semana = explode(',', $_SESSION['asignacion']['_dias_semana']);
		}

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				//echo "<pre>";print_r($_POST);exit; 
				$time_desde = strtotime($_POST['fecha_inicial']);
				$time_hasta = strtotime($_POST['fecha_final']);
				$_vigencia_desde = date('Y-m-d H:i:s',$time_desde);
				$_vigencia_hasta = date('Y-m-d H:i:s',$time_hasta);
						        	
				$_SESSION['asignacion']['_fecha_inicial']  = $_vigencia_desde;
				$_SESSION['asignacion']['_fecha_final']  = $_vigencia_hasta;
				$_SESSION['asignacion']['_dias_semana']  = implode(',', $_POST['dias']);
				/*$_data = $this->_trabajosGestion->traerAsigAlta($_SESSION['asignacion']);
				if($_data){
					exit('<h4 class="text-danger">Ya existe uno o mas totems de los seleccionados con contenidos vigentes de alta</h4>');
				}*/

				if(!isset($_SESSION['asignacion']['_totems']) || $_SESSION['asignacion']['_totems']==''){
					echo '<h4 class="text-danger">Debe seleccionar un totem</h4>';
					exit;
				}

				if($_SESSION['asignacion']['_videos']=='' && $_SESSION['asignacion']['_catalogos']=='' && $_SESSION['asignacion']['_footer']=='' && $_SESSION['asignacion']['_fondos']=='' && $_SESSION['asignacion']['_inactivas']==''){
					echo '<h4 class="text-danger">Debe seleccionar al menos un contenido</h4>';
					exit;
				}
				
				
				echo '1';
	           	exit;

           	}else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('selec_vigencia', 'asignacion');
    }
	
	public function consolidarVigencia()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
        		
				//echo "<pre>";print_r($_POST);exit; 
				/*$time_desde = strtotime($_POST['fecha_inicial']);
				$time_hasta = strtotime($_POST['fecha_final']);
				$_vigencia_desde = date('Y-m-d H:i:s',$time_desde);
				$_vigencia_hasta = date('Y-m-d H:i:s',$time_hasta);
						        	
				$_SESSION['asignacion']['_fecha_inicial']  = $_vigencia_desde;
				$_SESSION['asignacion']['_fecha_final']  = $_vigencia_hasta;
				

				if($_SESSION['asignacion']['_videos']=='' && $_SESSION['asignacion']['_catalogos']=='' && $_SESSION['asignacion']['_footer']=='' && $_SESSION['asignacion']['_fondos']=='' && $_SESSION['asignacion']['_inactivas']==''){
					exit('<h4 class="text-danger">Debe seleccionar al menos un contenido</h4>');
				}*/

				//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;

				//cargar la asignacion en la tabla de bitacora
				$_bitacora = $this->_trabajosGestion->cargarBitacora($_SESSION['asignacion'], $this->_sess->get('usuario'),'creado');
				if($_bitacora == false){
					echo 'error carga bitacora';
	            	exit();
				}
				
				$_asignar = $this->_trabajosGestion->cargarAsignacion($_SESSION['asignacion'], $this->_sess->get('usuario'));
				if($_asignar == true){

					//mandar mail a superadmin
					// $_supermail = usuarios::traerUsuarioPorRol(1);
					/*$_body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
							<title>Cencosud</title>
							</head>					
							<body>
							<p>Se creo un paquete de contenidos para los totems: <strong>'.admin::traerTotemsPorID($_SESSION['asignacion']['_totems']).'</strong></p>													
							</body>
							</html>';
					
					$this->envioMail->IsSMTP();
					$this->envioMail->SMTPAuth = true;
					$this->envioMail->Host = "smtphub.cencosud.cl";
					$this->envioMail->Username = "_MailCarteleria"; 
					$this->envioMail->Password = "a23uj8rs"; 
					$this->envioMail->Port = 25; 
					$this->envioMail->From ='info@jumbo.com.ar';
					// $this->envioMail->From ='info@cencosud.com';
					$this->envioMail->FromName ='Cencosud';
					$this->envioMail->Subject = 'Creacion de paquete de contenidos';				
					$this->envioMail->Body = $_body;
								
					//$this->envioMail->AddAddress($_supermail);
					$this->envioMail->AddAddress($this->_sess->get('user_mail'));
					$this->envioMail->addCC('flaviosantiago.fiorentini@jumbo.com.ar');
					$this->envioMail->IsHTML(true);					
					$exito = $this->envioMail->Send();
					
					$intentos=1;
					
					while ((!$exito) && ($intentos < 3)) {
						sleep(5);			
						$exito = $this->envioMail->Send();				
						$intentos=$intentos+1;			
					}
					
					if(!$exito) {			
						echo "Problemas enviando correo electrónico a ".$this->envioMail->ErrorInfo;
						exit;				
					}*/
					
					unset($_SESSION['asignacion']);
					echo '1';
	            	exit();
				}
				
				//echo "<pre>";print_r($_SESSION['asignacion']);exit;
			
			}else{
				$this->redireccionar('error/access/404');
			}	
        }
		
    }
	
    ///////////////////////////////////////////////////////////////////////////////////////////////

/*
	public function baja($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		// $pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		// $paginador = new Paginador();
		
		$this->_view->catalogos = $this->_trabajosGestion->traerCatalogos('baja');
		
		// $this->_view->beneficios = $paginador->paginar($beneficios, $pagina, 20);
		// $this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/beneficios/listarAlta');
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listarBaja', 'catalogos');	
    }
	
	
	
	
	public function cambiarEstado($_id, $_val)
	{
		$this->_acl->acceso('encargado_access');
		$_id = (int) $_id;
		
		$this->_trabajosGestion->cambiarEstadoCatalogo($_id, $_val);
		($_val=='alta') ? $this->redireccionar('administrador/catalogos/baja') : $this->redireccionar('administrador/catalogos/alta');
		
	
	}
	
	public function activar($_id)
	{
		$this->_acl->acceso('encargado_access');
		$_id = (int) $_id;
		
		$this->_trabajosGestion->cambiarEstadoCatalogo($_id, 'alta');
		$this->redireccionar('administrador/catalogos/alta');
		
	
	}
	public function ordenarGal()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){
			$_puntos = explode(',',$_POST['puntos']);
			foreach($_puntos as $i => $pt){
				$_idPunto = explode('-', $pt);
				$this->_trabajosGestion->cambiarOrdenGaleria($_idPunto[1], $i+1);
			}
			
			echo 1;
	
		}
	}
	
	
	public function buscador()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){
			
			$_val = $_POST['valor'];
			$_est = $_POST['estado'];
			
			$this->_view->prod  = $this->_trabajosGestion->traerCatalogoBuscador(ucwords(strtolower($_val)),$_est);
			
			
			
			$_html = '<ul id="lista" class="list-group listar">';
			foreach($this->_view->prod as $prod){           
        
				$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item">					
						<p>'.$prod->titulo.'</p>
						<span class="flotar_derecha" style="margin-right:15px">				
							<a href="'.$this->_conf['url_enlace'].'administrador/catalogos/cambiarEstado/'. $prod->id.'/baja" class="btn btn-info btn-sm">
								Desactivar
							</a>							
							<a href="'.$this->_conf['url_enlace'].'administrador/catalogos/editar/'. $prod->id.'" class="btn btn-success btn-sm">
								editar
							</a>
							<a href="'.$this->_conf['url_enlace'].'administrador/catalogos/borrar/'. $prod->id.'" class="btn btn-danger btn-sm">
								Borrar
							</a>
						</span>
					</li>';
        
			}			
			$_html .='</ul>';
			
			echo $_html;
			
		}
	}
	
	public function vista_previa($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('basic'));
		$this->_view->setJs(array('modernizr.2.5.3.min', 'turn'));
		
		// $pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		// $paginador = new Paginador();
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);
		$this->_view->imagenes = $this->_trabajosGestion->traerGaleriaPorIdentificador($this->_view->trabajo->identificador, $this->_view->trabajo->imagenes_orientacion);
		$this->_view->size = getimagesize($this->_conf['base_url']. 'public/img/subidas/catalogos/cat_'.$this->_view->trabajo->identificador.'/'.$this->_view->imagenes[0]->path);
		
		//echo $this->_view->size;
		//echo "<pre>";print_r($this->_view->size);exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('vista_previa', 'catalogos');	
    }
	
	public function ver($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('basic'));
		$this->_view->setJs(array('modernizr.2.5.3.min', 'turn'));
		
		// $pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		// $paginador = new Paginador();
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);
		$this->_view->imagenes = $this->_trabajosGestion->traerGaleriaPorIdentificador($this->_view->trabajo->identificador, $this->_view->trabajo->imagenes_orientacion);
		$this->_view->size = getimagesize($this->_conf['base_url']. 'public/img/subidas/catalogos/cat_'.$this->_view->trabajo->identificador.'/'.$this->_view->imagenes[0]->path);
		
		//echo $this->_view->size;
		//echo "<pre>";print_r($this->_view->size);exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('ver', 'catalogos');	
    }
	
	
	public function editar($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);	
		
		$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();	
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo->identificador);
		
		
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
									
		
				
			
		if($_POST){
			
			
			if($_POST['envio01'] == 1){
				
				$this->_view->data = $_POST;
				
			
				//echo "<pre>";print_r($this->_view->data);exit;
				
				$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND orientacion = ?', $this->_view->trabajo->identificador, $this->_view->data['imagenes_orientacion'])));
				if(!$_images){
					$this->_view->_error ='No hay imagenes cargadas con la orientación seleccionada';
					$this->_view->titulo = 'Administrador - Seguimiento';
					$this->_view->renderizar('editar', 'catalogos');
					exit;
				}
				
				//chequear que no haya img de otra orientacion, si hay borrarlas
				if($this->_view->data['imagenes_orientacion']=='horizontal'){
					//$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND orientacion = ?', $this->_view->trabajo->identificador,'vertical'),'order' => 'orden asc'));
					$_images = contenidos_imagene::find_by_sql('SELECT * FROM contenidos_imagenes WHERE identificador = '.$this->_view->trabajo->identificador.' AND (orientacion = "vertical" OR orientacion = "cuadrada") ORDER BY id DESC');
					if($_images){
						foreach($_images as $borrar_img){
							$this->_trabajosGestion->eliminarImgGal($borrar_img->id,$this->_conf['ruta_img_cargadas'], 'catalogos/cat_'.$this->_view->trabajo->identificador);
						}
					}
				}
				
				if($this->_view->data['imagenes_orientacion']=='vertical'){
					//$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND orientacion = ?', $this->_view->trabajo->identificador,'horizontal'),'order' => 'orden asc'));
					$_images = contenidos_imagene::find_by_sql('SELECT * FROM contenidos_imagenes WHERE identificador = '.$this->_view->trabajo->identificador.' AND (orientacion = "horizontal" OR orientacion = "cuadrada") ORDER BY id DESC');
					if($_images){
						foreach($_images as $borrar_img){
							$this->_trabajosGestion->eliminarImgGal($borrar_img->id,$this->_conf['ruta_img_cargadas'], 'catalogos/cat_'.$this->_view->trabajo->identificador);
						}
					}
				}
				
				if($this->_view->data['imagenes_orientacion']=='cuadrada'){
					$_images = contenidos_imagene::find_by_sql('SELECT * FROM contenidos_imagenes WHERE identificador = '.$this->_view->trabajo->identificador.' AND (orientacion = "horizontal" OR orientacion = "vertical") ORDER BY id DESC');
					if($_images){
						foreach($_images as $borrar_img){
							$this->_trabajosGestion->eliminarImgGal($borrar_img->id,$this->_conf['ruta_img_cargadas'], 'catalogos/cat_'.$this->_view->trabajo->identificador);
						}
					}
				}
				
				//eliminar img galeria
				if(isset($this->_view->data['eliminar_gal']) && $this->_view->data['eliminar_gal'][0]!=''){
					foreach($this->_view->data['eliminar_gal'] as $_gal){
						$this->_trabajosGestion->eliminarImgGal($_gal,$this->_conf['ruta_img_cargadas'], 'catalogos/cat_'.$this->_view->trabajo->identificador);			
					}
				}
				
				
				//SETEAR FECHA Y HORA
				$_vigencia_desde = admin::formatFechaSql($_POST['vigencia_desde']);
				$_vigencia_hasta = admin::formatFechaSql($_POST['vigencia_hasta']);
				
				$time_desde = strtotime($_vigencia_desde);
				$time_hasta = strtotime($_vigencia_hasta);
				$_vigencia_desde = date('Y-m-d',$time_desde);
				$_vigencia_hasta = date('Y-m-d',$time_hasta);
						
							
				
				$_editar = contenidos_catalogo::find($this->_view->trabajo->id);
				$_editar->id_computadora = $this->_view->data['id_computadora'];
				$_editar->titulo = validador::getTexto('titulo');
				$_editar->descripcion = validador::getTexto('descripcion');
				$_editar->vigencia_desde = "$_vigencia_desde";
				$_editar->vigencia_hasta = "$_vigencia_hasta";
				$_editar->imagenes_orientacion = $this->_view->data['imagenes_orientacion'];
				$_editar->estado = 'baja';
				$_editar->save();
				
				$this->_sess->destroy('edicion_actual');
				$this->_sess->destroy('img_id');
				$this->redireccionar('administrador/catalogos/vista_previa/'.$this->_view->trabajo->id);
				
											
				
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editar', 'catalogos');	
    }
	
	
	
	
	
	public function cargar($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)9999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
		$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();
		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){
			
			if($_POST['envio01'] == 1){
				
				$this->_view->data = $_POST;
				
			
				//echo "<pre>";print_r($_POST);exit;
				
				//SETEAR FECHA Y HORA
				$_vigencia_desde = admin::formatFechaSql($_POST['vigencia_desde']);
				$_vigencia_hasta = admin::formatFechaSql($_POST['vigencia_hasta']);
				
				$time_desde = strtotime($_vigencia_desde);
				$time_hasta = strtotime($_vigencia_hasta);
				$_vigencia_desde = date('Y-m-d',$time_desde);
				$_vigencia_hasta = date('Y-m-d',$time_hasta);
				
				
			
							
				$_fechaBd = date('Y-m-d');				

				$cat = new contenidos_catalogo();
				$cat->id_computadora = $this->_view->data['id_computadora'];
				$cat->titulo = validador::getTexto('titulo');
				$cat->descripcion = validador::getTexto('descripcion');
				$cat->vigencia_desde = "$_vigencia_desde";
				$cat->vigencia_hasta = "$_vigencia_hasta";
				$cat->imagenes_orientacion = $this->_view->data['imagenes_orientacion'];
				$cat->identificador = $this->_sess->get('carga_actual');
				$cat->estado = 'baja';
				$cat->fecha = "$_fechaBd";
				$cat->save();			
				
							
				
				$this->_sess->destroy('carga_actual');
				$this->_sess->destroy('img_id');
				$this->redireccionar('administrador/catalogos/vista_previa/'.$cat->id);
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'catalogos');	
    }
	
	
	public function borrar($_id)
	{
		$this->_acl->acceso('encargado_access');
		$_id = (int) $_id;
		
		$this->_trabajosGestion->borrarCatalogo($_id, $this->_conf['ruta_img_cargadas'], 'catalogos');
		$this->redireccionar('administrador/catalogos/listar');
	}
	
	
*/	
	
}