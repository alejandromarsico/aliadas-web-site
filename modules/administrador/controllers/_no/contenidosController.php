<?php

use controllers\administradorController\administradorController;

class contenidosController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		$this->getLibrary('class.admin');		
		$this->_trabajosGestion = new admin();
		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		unset($_SESSION['editar_asignacion']);
		unset($_SESSION['asignacion']);	
		unset($_SESSION['_bitacora']);
		unset($_SESSION['_bitacora_old']);	

		/*if(!$this->_sess->get('_supermercado') || $this->_sess->get('_supermercado')==''){
			$this->redireccionar('administrador');	
		}*/
		
    }
    
    public function index()
    {	
		$this->_acl->acceso('encargado_access');
		
		//$this->redireccionar('administrador/catalogos/alta');
		//echo "<pre>";print_r($_SESSION);echo "</pre>";	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'contenidos');
    }
	
	
	/////////////////////CATALAGO//////////////////////////////////////////////////////////////
	
	public function listarCatalogo($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		/*$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();*/

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		$this->_view->catalogos = $this->_trabajosGestion->traerCatalogos();
		
		/*$this->_view->beneficios = $paginador->paginar($beneficios, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/beneficios/listarAlta');*/
		//echo "<pre>";print_r($_SESSION);echo "</pre>";
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listarCatalogo', 'contenidos');	
    }
	
	
	
	
	public function cambiarEstado($_id, $_val)
	{
		$this->_acl->acceso('encargado_access');
		$_id = (int) $_id;
		
		$this->_trabajosGestion->cambiarEstadoCatalogo($_id, $_val);
		($_val=='alta') ? $this->redireccionar('administrador/catalogos/baja') : $this->redireccionar('administrador/catalogos/alta');
		
	
	}
	
	public function activarCatalogo($_id)
	{
		$this->_acl->acceso('encargado_access');
		$_id = (int) $_id;
		
		$this->_trabajosGestion->cambiarEstadoCatalogo($_id, 'alta');
		$this->redireccionar('administrador/contenidos/listarCatalogo');
		
	
	}
	public function ordenarGal()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){
			$_puntos = explode(',',$_POST['puntos']);
			foreach($_puntos as $i => $pt){
				$_idPunto = explode('-', $pt);
				$this->_trabajosGestion->cambiarOrdenGaleria($_idPunto[1], $i+1);
			}
			
			echo 1;
	
		}
	}
	
	
	public function buscadorCatalogo()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				$_est = $_POST['estado'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerCatalogoBuscador(ucwords(strtolower($_val)),$_est);
				
				/*echo "<pre>";
				print_r($this->_view->users);
				echo"</pre>";*/
				
				$_html = '<ul id="lista" class="list-group listar">';
				foreach($this->_view->prod as $prod){ 

					$_img = admin::traerImgOrientacion($prod->identificador,'thumb');       
	        		if(isset($_img) && $_img !=''){
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-catalogo" style="background-image: url('.$this->_conf['base_url'] . "public/img/subidas/catalogos/cat_".$prod->identificador."/thumb/". $_img[0]->path.')">';
	        		}else{
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-catalogo">';
	        		}
										
					$_html .='<div class="contInfo">
							<p>'.$prod->titulo.'</p>
							<span class="flotar_derecha" style="margin-right:15px">			
															
								<a href="'.$this->_conf['url_enlace'].'administrador/contenidos/editarCatalogo/'. $prod->id.'">
									<img src="'.$this->_conf['base_url'].'public/img/ico-pencil.png" alt="editar" style="width: 27px"/>
								</a>
								
								<a href="javascript:void(0);" class="_borrar_'. $prod->id.'" title="Borrar">
		                            <img src="'.$this->_conf['url_enlace'].'public/img/ico-cruz.png" alt="eliminar"/>
		                        </a>
							</span>
							</div>
						</li>
						<script>
		                    $(document).ready(function () {
		                          $("._borrar_'.$prod->id.'").click(function () {
		                              swal({
		                                  title: "Estas seguro de borrar este contenido?",
		                                  text: "Los datos se perderán permanentemente!",
		                                  type: "warning",
		                                  showCancelButton: true,
		                                  confirmButtonColor: "#DD6B55",
		                                  confirmButtonText: "Si, que se borre!",
		                                  cancelButtonText: "No, mejor no!",
		                                  closeOnConfirm: false,
		                                  closeOnCancel: false },
		                              function (isConfirm) {
		                                  if (isConfirm) {
		                                      var url= _root_ + "administrador/contenidos/borrarCatalogo";
		                                      var dataString = "_id='.$prod->id.'&_csrf='.$this->_sess->get('_csrf').'";
		                                      $.ajax({
		                                              type: "POST",
		                                              url: url,
		                                              data: dataString,
		                                              success: function(data){
		                                                if(data=="ok"){
		                                                  swal("Borrado!", "El contenido se borró con exito.", "success");
		                                                  setTimeout(function() {
		                                                      window.location.reload()
		                                                  }, 200);
		                                                }else{
		                                                  swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
		                                                }
		                                                  
		                                                  
		                                              }
		                                      });
		                                  } else {
		                                      swal("Cancelado", "El contenido esta guardado", "error");
		                                  }
		                              });
		                          });
		                      });
		                   </script>';
	        
				}			
				$_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	public function vista_previa_catalogos($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('basic'));
		$this->_view->setJs(array('modernizr.2.5.3.min', 'turn'));
		
		/*$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();*/
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);
		$this->_view->imagenes = $this->_trabajosGestion->traerGaleriaPorIdentificador($this->_view->trabajo->identificador, $this->_view->trabajo->imagenes_orientacion);
		$this->_view->size = getimagesize($this->_conf['base_url']. 'public/img/subidas/catalogos/cat_'.$this->_view->trabajo->identificador.'/'.$this->_view->imagenes[0]->path);
		
		//echo $this->_view->size;
		//echo "<pre>";print_r($this->_view->size);exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('vista_previa_catalogos', 'contenidos');	
    }
	
	
	
	
	public function editarCatalogo($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);	
		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();	
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo->identificador);
		
		/*echo "<pre>";
		print_r($this->_view->imgs);
		echo "</pre>";*/
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
									
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					//echo "<pre>";print_r($this->_view->data);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarCatalogo', 'contenidos');
						exit;
					} 
					
					$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND orientacion = ?', $this->_view->trabajo->identificador, $this->_view->data['imagenes_orientacion'])));
					if(!$_images){
						$this->_view->_error ='No hay imagenes cargadas con la orientación seleccionada';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarCatalogo', 'contenidos');
						exit;
					}
					
					//chequear que no haya img de otra orientacion, si hay borrarlas
					if($this->_view->data['imagenes_orientacion']=='horizontal'){
						//$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND orientacion = ?', $this->_view->trabajo->identificador,'vertical'),'order' => 'orden asc'));
						$_images = contenidos_imagene::find_by_sql('SELECT * FROM contenidos_imagenes WHERE identificador = '.$this->_view->trabajo->identificador.' AND (orientacion = "vertical" OR orientacion = "cuadrada") ORDER BY id DESC');
						if($_images){
							foreach($_images as $borrar_img){
								$this->_trabajosGestion->eliminarImgGal($borrar_img->id,$this->_conf['ruta_img_cargadas'], 'catalogos/cat_'.$this->_view->trabajo->identificador);
							}
						}
					}
					
					if($this->_view->data['imagenes_orientacion']=='vertical'){
						//$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND orientacion = ?', $this->_view->trabajo->identificador,'horizontal'),'order' => 'orden asc'));
						$_images = contenidos_imagene::find_by_sql('SELECT * FROM contenidos_imagenes WHERE identificador = '.$this->_view->trabajo->identificador.' AND (orientacion = "horizontal" OR orientacion = "cuadrada") ORDER BY id DESC');
						if($_images){
							foreach($_images as $borrar_img){
								$this->_trabajosGestion->eliminarImgGal($borrar_img->id,$this->_conf['ruta_img_cargadas'], 'catalogos/cat_'.$this->_view->trabajo->identificador);
							}
						}
					}
					
					if($this->_view->data['imagenes_orientacion']=='cuadrada'){
						$_images = contenidos_imagene::find_by_sql('SELECT * FROM contenidos_imagenes WHERE identificador = '.$this->_view->trabajo->identificador.' AND (orientacion = "horizontal" OR orientacion = "vertical") ORDER BY id DESC');
						if($_images){
							foreach($_images as $borrar_img){
								$this->_trabajosGestion->eliminarImgGal($borrar_img->id,$this->_conf['ruta_img_cargadas'], 'catalogos/cat_'.$this->_view->trabajo->identificador);
							}
						}
					}
					
					//eliminar img galeria
					if(isset($this->_view->data['eliminar_gal']) && $this->_view->data['eliminar_gal'][0]!=''){
						foreach($this->_view->data['eliminar_gal'] as $_gal){
							$this->_trabajosGestion->eliminarImgGal($_gal,$this->_conf['ruta_img_cargadas'], 'catalogos/cat_'.$this->_view->trabajo->identificador);			
						}
					}
					
					
					//SETEAR FECHA Y HORA
					/*$_vigencia_desde = admin::formatFechaSql($_POST['vigencia_desde']);
					$_vigencia_hasta = admin::formatFechaSql($_POST['vigencia_hasta']);				
					$time_desde = strtotime($_vigencia_desde);
					$time_hasta = strtotime($_vigencia_hasta);
					$_vigencia_desde = date('Y-m-d',$time_desde);
					$_vigencia_hasta = date('Y-m-d',$time_hasta);*/
							
								
					
					$_editar = contenidos_catalogo::find($this->_view->trabajo->id);
					//$_editar->id_computadora = $this->_view->data['id_computadora'];
					$_editar->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					//$_editar->descripcion = validador::getTexto('descripcion');
					//$_editar->vigencia_desde = $_vigencia_desde;
					//$_editar->vigencia_hasta = $_vigencia_hasta;
					$_editar->imagenes_orientacion = $this->_view->data['imagenes_orientacion'];
					$_editar->estado = 'alta';
					$_editar->save();
								
					
					$_updateModif = $this->_trabajosGestion->traerAsigModif($this->_view->trabajo->id, 'ids_catalogos');
					if($_updateModif){
						$_dat = 'c_'.$this->_view->trabajo->id;
						foreach ($_updateModif as $val) {
							$_edit = contenidos_asignacione::find($val->id);
							if($_edit){
								if($_edit->update_contenido!=''){
									$_ar = explode(',', $_edit->update_contenido);
									$_ar[] = $_dat;
									$_ar_unique = array_unique($_ar);
									$_ar_final = implode(',', $_ar_unique);
									$_edit->update_contenido = $_ar_final;
									$_edit->save();
								}else{
									$_edit->update_contenido = $_dat;
									$_edit->save();
								}
								
								
							}
						}

					}

					//$this->redireccionar('administrador/contenidos/vista_previa_catalogos/'.$this->_view->trabajo->id);

					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('img_id');
					$this->_sess->destroy('img_lote');
					$this->redireccionar('administrador/contenidos/listarCatalogo');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editarCatalogo', 'contenidos');	
    }
	
	
	
	
	
	public function cargarCatalogo($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));

		// echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();
		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					//echo "<pre>";print_r($_POST);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargarCatalogo', 'contenidos');
						exit;
					} 
					
					//SETEAR FECHA Y HORA
					/*$_vigencia_desde = admin::formatFechaSql($_POST['vigencia_desde']);
					$_vigencia_hasta = admin::formatFechaSql($_POST['vigencia_hasta']);
					$time_desde = strtotime($_vigencia_desde);
					$time_hasta = strtotime($_vigencia_hasta);
					$_vigencia_desde = date('Y-m-d',$time_desde);
					$_vigencia_hasta = date('Y-m-d',$time_hasta);
					*/
					/*echo $_vigencia_desde;
					exit;*/			
					
					$_fechaBd = date('Y-m-d');
					
					$cat = new contenidos_catalogo();
					//$cat->id_computadora = $this->_view->data['id_computadora'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					//$cat->descripcion = validador::getTexto('descripcion');
					//$cat->vigencia_desde = $_vigencia_desde;
					//$cat->vigencia_hasta = $_vigencia_hasta;
					$cat->imagenes_orientacion = $this->_view->data['imagenes_orientacion'];
					$cat->identificador = $this->_sess->get('carga_actual');
					$cat->estado = 'alta';
					$cat->id_user = $this->_sess->get('usuario');
					$cat->fecha = "$_fechaBd";
					$cat->save();			
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('img_id');
					$this->_sess->destroy('img_lote');
					//$this->redireccionar('administrador/contenidos/vista_previa_catalogos/'.$cat->id);
					$this->redireccionar('administrador/contenidos/listarCatalogo');
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargarCatalogo', 'contenidos');	
    }
	
	
	public function borrarCatalogo()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		
				
				/*$this->_trabajosGestion->borrarCatalogo($_id, $this->_conf['ruta_img_cargadas'], 'catalogos');
				$this->redireccionar('administrador/contenidos/listarCatalogo');*/

				$_borrar = $this->_trabajosGestion->borrarCatalogo($_id, $this->_conf['ruta_img_cargadas'], 'catalogos');
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}
	
	///////////////////////////FOOTER//////////////////////////////////////////
	

	public function listarFooter($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));

        $this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');
		
		$this->_view->promociones = $this->_trabajosGestion->traerPromociones();
		
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listarFooter', 'contenidos');	
    }
	

	
	
	public function buscadorFooter()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				$_est = $_POST['estado'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerPromocionBuscador(ucwords(strtolower($_val)),$_est);
				
				/*echo "<pre>";
				print_r($this->_view->users);
				echo"</pre>";*/
				
				$_html = '<ul id="lista" class="list-group listar">';
				foreach($this->_view->prod as $prod){  

					$_img = admin::traerImgOrientacion($prod->identificador,'thumb');       
	        		if(isset($_img) && $_img !=''){
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-footer" style="background-image: url('.$this->_conf['base_url'] . "public/img/subidas/promociones/thumb/". $_img[0]->path.')">';
	        		}else{
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-footer">';
	        		}         
	        
					$_html .='<div class="contInfo">	
							<p>'.$prod->titulo.'</p>
							<span class="flotar_derecha" style="margin-right:15px">				
														
								<a href="'.$this->_conf['url_enlace'].'administrador/contenidos/editarFooter/'. $prod->id.'">
									<img src="'.$this->_conf['base_url'].'public/img/ico-pencil.png" alt="editar" style="width: 27px"/>
								</a>
								
								<a href="javascript:void(0);" class="_borrar_'. $prod->id.'" title="Borrar">
		                            <img src="'.$this->_conf['url_enlace'].'public/img/ico-cruz.png" alt="eliminar"/>
		                        </a>
							</span>
							</div>
						</li>
						<script>
		                    $(document).ready(function () {
		                          $("._borrar_'.$prod->id.'").click(function () {
		                              swal({
		                                  title: "Estas seguro de borrar este contenido?",
		                                  text: "Los datos se perderán permanentemente!",
		                                  type: "warning",
		                                  showCancelButton: true,
		                                  confirmButtonColor: "#DD6B55",
		                                  confirmButtonText: "Si, que se borre!",
		                                  cancelButtonText: "No, mejor no!",
		                                  closeOnConfirm: false,
		                                  closeOnCancel: false },
		                              function (isConfirm) {
		                                  if (isConfirm) {
		                                      var url= _root_ + "administrador/contenidos/borrarFooter";
		                                      var dataString = "_id='.$prod->id.'&_csrf='.$this->_sess->get('_csrf').'";
		                                      $.ajax({
		                                              type: "POST",
		                                              url: url,
		                                              data: dataString,
		                                              success: function(data){
		                                                if(data=="ok"){
		                                                  swal("Borrado!", "El contenido se borró con exito.", "success");
		                                                  setTimeout(function() {
		                                                      window.location.reload()
		                                                  }, 200);
		                                                }else{
		                                                  swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
		                                                }
		                                                  
		                                                  
		                                              }
		                                      });
		                                  } else {
		                                      swal("Cancelado", "El contenido esta guardado", "error");
		                                  }
		                              });
		                          });
		                      });
		                   </script>';
	        
				}			
				$_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	
	public function editarFooter($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerPromocion($_id);	
		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();	
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo->identificador);
		
		/*echo "<pre>";print_r($this->_view->imgs);echo "</pre>";*/
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
									
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					//echo "<pre>";print_r($this->_view->data);exit;
					
					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarFooter', 'contenidos');
						exit;
					} 
					
					$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $this->_view->trabajo->identificador)));
					if(!$_images){
						$this->_view->_error ='No hay imagenes cargadas';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarFooter', 'contenidos');
						exit;
					}
					
					
					//SETEAR FECHA Y HORA
					/*$_vigencia_desde = admin::formatFechaSql($_POST['vigencia_desde']);
					$_vigencia_hasta = admin::formatFechaSql($_POST['vigencia_hasta']);
					$time_desde = strtotime($_vigencia_desde);
					$time_hasta = strtotime($_vigencia_hasta);
					$_vigencia_desde = date('Y-m-d',$time_desde);
					$_vigencia_hasta = date('Y-m-d',$time_hasta);*/		
								
					
					$_editar = contenidos_promocione::find($this->_view->trabajo->id);
					//$_editar->id_computadora = $this->_view->data['id_computadora'];
					$_editar->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					//$_editar->vigencia_desde = $_vigencia_desde;
					//$_editar->vigencia_hasta = $_vigencia_hasta;
					$_editar->save();


					$_updateModif = $this->_trabajosGestion->traerAsigModif($this->_view->trabajo->id, 'ids_footers');
					if($_updateModif){
						$_dat = 'foo_'.$this->_view->trabajo->id;
						foreach ($_updateModif as $val) {
							$_edit = contenidos_asignacione::find($val->id);
							if($_edit){
								if($_edit->update_contenido!=''){
									$_ar = explode(',', $_edit->update_contenido);
									$_ar[] = $_dat;
									$_ar_unique = array_unique($_ar);
									$_ar_final = implode(',', $_ar_unique);
									$_edit->update_contenido = $_ar_final;
									$_edit->save();
								}else{
									$_edit->update_contenido = $_dat;
									$_edit->save();
								}
								
								
							}
						}

					}
					
					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('img_id');
					$this->redireccionar('administrador/contenidos/listarFooter');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editarFooter', 'contenidos');	
    }
	
	
	
	
	
	public function cargarFooter($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)9999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();
		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					//echo "<pre>";print_r($_POST);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargarFooter', 'contenidos');
						exit;
					} 
					
					$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $this->_sess->get('carga_actual'))));
					if(!$_images){
						$this->_view->_error ='No hay imagenes cargadas';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargarFooter', 'contenidos');
						exit;
					}
					
					
					//SETEAR FECHA Y HORA
				/*	$_vigencia_desde = admin::formatFechaSql($_POST['vigencia_desde']);
					$_vigencia_hasta = admin::formatFechaSql($_POST['vigencia_hasta']);
					$time_desde = strtotime($_vigencia_desde);
					$time_hasta = strtotime($_vigencia_hasta);
					$_vigencia_desde = date('Y-m-d',$time_desde);
					$_vigencia_hasta = date('Y-m-d',$time_hasta);
								*/
					$_fechaBd = date('Y-m-d');

					$promo = new contenidos_promocione();
					$promo->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					//$promo->vigencia_desde = $_vigencia_desde;
					//$promo->vigencia_hasta = $_vigencia_hasta;
					$promo->identificador = $this->_sess->get('carga_actual');
					$promo->estado = 'alta';
					$promo->id_user = $this->_sess->get('usuario');
					$promo->fecha = "$_fechaBd";
					$promo->save();
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('img_id');
					$this->redireccionar('administrador/contenidos/listarFooter');
				}	

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargarFooter', 'contenidos');	
    }
	
	
	public function borrarFooter()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		
				
				/*$this->_trabajosGestion->borrarPromocion($_id, $this->_conf['ruta_img_cargadas'], 'promociones');
				$this->redireccionar('administrador/contenidos/listarFooter');*/

				$_borrar = $this->_trabajosGestion->borrarPromocion($_id, $this->_conf['ruta_img_cargadas'], 'promociones');
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}

	}
	

	///////////////////////////FONDO PANTALLA//////////////////////////////////////////
	

	public function listarFondo($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));

        $this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		$this->_view->fondos = $this->_trabajosGestion->traerFondos();
		
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listarFondo', 'contenidos');	
    }
	
	
	
	
	public function buscadorFondo()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				$_est = $_POST['estado'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerFondoBuscador(ucwords(strtolower($_val)),$_est);
				
				/*echo "<pre>";
				print_r($this->_view->users);
				echo"</pre>";*/
				
				$_html = '<ul id="lista" class="list-group listar">';
				foreach($this->_view->prod as $prod){ 

					$_img = admin::traerImgOrientacion($prod->identificador,'thumb');       
	        		if(isset($_img) && $_img !=''){
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-fondo" style="background-image: url('.$this->_conf['base_url'] . "public/img/subidas/fondos/thumb/". $_img[0]->path.')">';
	        		}else{
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-fondo">';
	        		}           
	        
					$_html .='<div class="contInfo">				
							<p>'.$prod->titulo.'</p>
							<span class="flotar_derecha" style="margin-right:15px">				
														
								<a href="'.$this->_conf['url_enlace'].'administrador/contenidos/editarFondo/'. $prod->id.'" >
									<img src="'.$this->_conf['base_url'].'public/img/ico-pencil.png" alt="editar" style="width: 27px"/>
								</a>
								

								<a href="javascript:void(0);" class="_borrar_'. $prod->id.'" title="Borrar">
		                            <img src="'.$this->_conf['url_enlace'].'public/img/ico-cruz.png" alt="eliminar"/>
		                        </a>
							</span>
							</div>
						</li>
						<script>
		                    $(document).ready(function () {
		                          $("._borrar_'.$prod->id.'").click(function () {
		                              swal({
		                                  title: "Estas seguro de borrar este contenido?",
		                                  text: "Los datos se perderán permanentemente!",
		                                  type: "warning",
		                                  showCancelButton: true,
		                                  confirmButtonColor: "#DD6B55",
		                                  confirmButtonText: "Si, que se borre!",
		                                  cancelButtonText: "No, mejor no!",
		                                  closeOnConfirm: false,
		                                  closeOnCancel: false },
		                              function (isConfirm) {
		                                  if (isConfirm) {
		                                      var url= _root_ + "administrador/contenidos/borrarFondo";
		                                      var dataString = "_id='.$prod->id.'&_csrf='.$this->_sess->get('_csrf').'";
		                                      $.ajax({
		                                              type: "POST",
		                                              url: url,
		                                              data: dataString,
		                                              success: function(data){
		                                                if(data=="ok"){
		                                                  swal("Borrado!", "El contenido se borró con exito.", "success");
		                                                  setTimeout(function() {
		                                                      window.location.reload()
		                                                  }, 200);
		                                                }else{
		                                                  swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
		                                                }
		                                                  
		                                                  
		                                              }
		                                      });
		                                  } else {
		                                      swal("Cancelado", "El contenido esta guardado", "error");
		                                  }
		                              });
		                          });
		                      });
		                   </script>';
	        
				}			
				$_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	
	public function editarFondo($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerFondo($_id);	
		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();	
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo->identificador);
		
		/*echo "<pre>";print_r($this->_view->imgs);echo "</pre>";*/
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
									
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					//echo "<pre>";print_r($this->_view->data);exit;
					
					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarFondo', 'contenidos');
						exit;
					} 
					
					$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $this->_view->trabajo->identificador)));
					if(!$_images){
						$this->_view->_error ='No hay imagenes cargadas';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarFondo', 'contenidos');
						exit;
					}
					
					
					//SETEAR FECHA Y HORA
					/*$_vigencia_desde = admin::formatFechaSql($_POST['vigencia_desde']);
					$_vigencia_hasta = admin::formatFechaSql($_POST['vigencia_hasta']);
					$time_desde = strtotime($_vigencia_desde);
					$time_hasta = strtotime($_vigencia_hasta);
					$_vigencia_desde = date('Y-m-d',$time_desde);
					$_vigencia_hasta = date('Y-m-d',$time_hasta);*/		
								
					
					$_editar = contenidos_fondo::find($this->_view->trabajo->id);
					//$_editar->id_computadora = $this->_view->data['id_computadora'];
					$_editar->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					//$_editar->vigencia_desde = $_vigencia_desde;
					//$_editar->vigencia_hasta = $_vigencia_hasta;
					$_editar->save();


					$_updateModif = $this->_trabajosGestion->traerAsigModif($this->_view->trabajo->id, 'ids_fondos');
					if($_updateModif){
						$_dat = 'fo_'.$this->_view->trabajo->id;
						foreach ($_updateModif as $val) {
							$_edit = contenidos_asignacione::find($val->id);
							if($_edit){
								if($_edit->update_contenido!=''){
									$_ar = explode(',', $_edit->update_contenido);
									$_ar[] = $_dat;
									$_ar_unique = array_unique($_ar);
									$_ar_final = implode(',', $_ar_unique);
									$_edit->update_contenido = $_ar_final;
									$_edit->save();
								}else{
									$_edit->update_contenido = $_dat;
									$_edit->save();
								}
								
								
							}
						}

					}

					
					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('img_id');
					$this->redireccionar('administrador/contenidos/listarFondo');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editarFondo', 'contenidos');	
    }
	
	
	
	
	
	public function cargarFondo($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)9999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();
		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					//echo "<pre>";print_r($_POST);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargarFondo', 'contenidos');
						exit;
					} 
					
					$_images = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $this->_sess->get('carga_actual'))));
					if(!$_images){
						$this->_view->_error ='No hay imagenes cargadas';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargarFondo', 'contenidos');
						exit;
					}
					
					
					//SETEAR FECHA Y HORA
				/*	$_vigencia_desde = admin::formatFechaSql($_POST['vigencia_desde']);
					$_vigencia_hasta = admin::formatFechaSql($_POST['vigencia_hasta']);
					$time_desde = strtotime($_vigencia_desde);
					$time_hasta = strtotime($_vigencia_hasta);
					$_vigencia_desde = date('Y-m-d',$time_desde);
					$_vigencia_hasta = date('Y-m-d',$time_hasta);
								*/
					$_fechaBd = date('Y-m-d');

					$promo = new contenidos_fondo();
					$promo->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					//$promo->vigencia_desde = $_vigencia_desde;
					//$promo->vigencia_hasta = $_vigencia_hasta;
					$promo->identificador = $this->_sess->get('carga_actual');
					$promo->estado = 'alta';
					$promo->id_user = $this->_sess->get('usuario');
					$promo->fecha = "$_fechaBd";
					$promo->save();
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('img_id');
					$this->redireccionar('administrador/contenidos/listarFondo');
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargarFondo', 'contenidos');	
    }
	
	
	public function borrarFondo()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		
				/*$this->_trabajosGestion->borrarFondo($_id, $this->_conf['ruta_img_cargadas'], 'fondos');
				$this->redireccionar('administrador/contenidos/listarFondo');*/
				$_borrar = $this->_trabajosGestion->borrarFondo($_id, $this->_conf['ruta_img_cargadas'], 'fondos');
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		
		
	}
	

	///////////////////////////VIDEOS//////////////////////////////////////////
	

	public function listarVideo($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));

        $this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');
		
		$this->_view->videos = $this->_trabajosGestion->traerVideos();
		
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listarVideo', 'contenidos');	
    }
	
	
	
	
	public function buscadorVideo()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				$_est = $_POST['estado'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerVideoBuscador(ucwords(strtolower($_val)),$_est);
				
				/*echo "<pre>";
				print_r($this->_view->users);
				echo"</pre>";*/
				
				$_html = '<ul id="lista" class="list-group listar">';
				foreach($this->_view->prod as $prod){    

					$_img = admin::traerImgOrientacion($prod->identificador,'thumb');       
	        		if(isset($_img) && $_img !=''){
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-video" style="background-image: url('.$this->_conf['base_url'] . "public/videos/vid_".$prod->identificador."/thumb/". $_img[0]->path.')">';
	        		}else{
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-video">';
	        		}         
	        
					
					$_html .='<div class="contInfo">					
							<p>'.$prod->titulo.'</p>
							<span class="flotar_derecha" style="margin-right:15px">				
														
								<a href="'.$this->_conf['url_enlace'].'administrador/contenidos/editarVideo/'. $prod->id.'">
									<img src="'.$this->_conf['base_url'].'public/img/ico-pencil.png" alt="editar" style="width: 27px"/>
								</a>
								
								<a href="javascript:void(0);" class="_borrar_'. $prod->id.'" title="Borrar">
		                            <img src="'.$this->_conf['url_enlace'].'public/img/ico-cruz.png" alt="eliminar"/>
		                        </a>
							</span>
							</div>
						</li>
						<script>
		                    $(document).ready(function () {
		                          $("._borrar_'.$prod->id.'").click(function () {
		                              swal({
		                                  title: "Estas seguro de borrar este contenido?",
		                                  text: "Los datos se perderán permanentemente!",
		                                  type: "warning",
		                                  showCancelButton: true,
		                                  confirmButtonColor: "#DD6B55",
		                                  confirmButtonText: "Si, que se borre!",
		                                  cancelButtonText: "No, mejor no!",
		                                  closeOnConfirm: false,
		                                  closeOnCancel: false },
		                              function (isConfirm) {
		                                  if (isConfirm) {
		                                      var url= _root_ + "administrador/contenidos/borrarVideo";
		                                      var dataString = "_id='.$prod->id.'&_csrf='.$this->_sess->get('_csrf').'";
		                                      $.ajax({
		                                              type: "POST",
		                                              url: url,
		                                              data: dataString,
		                                              success: function(data){
		                                                if(data=="ok"){
		                                                  swal("Borrado!", "El contenido se borró con exito.", "success");
		                                                  setTimeout(function() {
		                                                      window.location.reload()
		                                                  }, 200);
		                                                }else{
		                                                  swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
		                                                }
		                                                  
		                                                  
		                                              }
		                                      });
		                                  } else {
		                                      swal("Cancelado", "El contenido esta guardado", "error");
		                                  }
		                              });
		                          });
		                      });
		                   </script>';
	        
				}			
				$_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	
	public function editarVideo($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerVideo($_id);	
		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();	
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo->identificador);
		
		/*echo "<pre>";print_r($this->_view->imgs);echo "</pre>";*/
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
									
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					//echo "<pre>";print_r($this->_view->data);exit;
					
					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarVideo', 'contenidos');
						exit;
					} 
					
					$_vid = contenidos_video::find('all',array('conditions' => array('identificador = ?', $this->_view->trabajo->identificador)));
					if(!$_vid){
						$this->_view->_error ='No hay videos cargados';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarVideo', 'contenidos');
						exit;
					}
					
					
							
					
					$_editar = contenidos_heade::find($this->_view->trabajo->id);
					//$_editar->id_computadora = $this->_view->data['id_computadora'];
					$_editar->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$_editar->save();

					$_updateModif = $this->_trabajosGestion->traerAsigModif($this->_view->trabajo->id, 'ids_videos');
					if($_updateModif){
						$_dat = 'v_'.$this->_view->trabajo->id;
						foreach ($_updateModif as $val) {
							$_edit = contenidos_asignacione::find($val->id);
							if($_edit){
								if($_edit->update_contenido!=''){
									$_ar = explode(',', $_edit->update_contenido);
									$_ar[] = $_dat;
									$_ar_unique = array_unique($_ar);
									$_ar_final = implode(',', $_ar_unique);
									$_edit->update_contenido = $_ar_final;
									$_edit->save();
								}else{
									$_edit->update_contenido = $_dat;
									$_edit->save();
								}
								
								
							}
						}

					}
					
					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('video_id');
					$this->redireccionar('administrador/contenidos/listarVideo');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editarVideo', 'contenidos');	
    }
	
	
	
	
	
	public function cargarVideo($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)9999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();
		//echo "<pre>";print_r($_SESSION);echo "</pre>";
		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;				
				
					//echo "<pre>";print_r($_POST);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargarVideo', 'contenidos');
						exit;
					} 
					
					$_vid = contenidos_video::find('all',array('conditions' => array('identificador = ?', $this->_sess->get('carga_actual'))));
					if(!$_vid){
						$this->_view->_error ='No hay videos cargados';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargarVideo', 'contenidos');
						exit;
					}
					
					
			
					$_fechaBd = date('Y-m-d');

					$promo = new contenidos_heade();
					$promo->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$promo->identificador = $this->_sess->get('carga_actual');
					$promo->estado = 'alta';
					$promo->id_user = $this->_sess->get('usuario');
					$promo->fecha = "$_fechaBd";
					$promo->save();
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('video_id');
					$this->redireccionar('administrador/contenidos/listarVideo');
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargarVideo', 'contenidos');	
    }
	
	
	public function borrarVideo()
	{
		$this->_acl->acceso('encargado_access');

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		
				/*$this->_trabajosGestion->borrarVideo($_id, $this->_conf['ruta_videos']);
				$this->redireccionar('administrador/contenidos/listarVideo');*/
				$_borrar = $this->_trabajosGestion->borrarVideo($_id, $this->_conf['ruta_videos']);
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}		
		
	}


	///////////////////////////PANTALLA INACTIVA//////////////////////////////////////////
	

	public function listarInactiva($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));

        $this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');
		
		$this->_view->videos = $this->_trabajosGestion->traerInactivas();
		
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listarInactiva', 'contenidos');	
    }
	
		
	
	public function buscadorInactiva()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				$_est = $_POST['estado'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerInactivaBuscador(ucwords(strtolower($_val)),$_est);
				
				/*echo "<pre>";
				print_r($this->_view->users);
				echo"</pre>";*/
				
				$_html = '<ul id="lista" class="list-group listar">';
				foreach($this->_view->prod as $prod){        

					
	        		$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-video">
	        				<div class="contInfo">				
								<p>'.$prod->titulo.'</p>
								<span class="flotar_derecha" style="margin-right:15px">				
															
									<a href="'.$this->_conf['url_enlace'].'administrador/contenidos/editarInactiva/'. $prod->id.'">
										<img src="'.$this->_conf['base_url'].'public/img/ico-pencil.png" alt="editar" style="width: 27px"/>
									</a>
									
									<a href="javascript:void(0);" class="_borrar_'. $prod->id.'" title="Borrar">
			                            <img src="'.$this->_conf['url_enlace'].'public/img/ico-cruz.png" alt="eliminar"/>
			                        </a>
								</span>
							</div>
						</li>
						<script>
		                    $(document).ready(function () {
		                          $("._borrar_'.$prod->id.'").click(function () {
		                              swal({
		                                  title: "Estas seguro de borrar este contenido?",
		                                  text: "Los datos se perderán permanentemente!",
		                                  type: "warning",
		                                  showCancelButton: true,
		                                  confirmButtonColor: "#DD6B55",
		                                  confirmButtonText: "Si, que se borre!",
		                                  cancelButtonText: "No, mejor no!",
		                                  closeOnConfirm: false,
		                                  closeOnCancel: false },
		                              function (isConfirm) {
		                                  if (isConfirm) {
		                                      var url= _root_ + "administrador/contenidos/borrarInactiva";
		                                      var dataString = "_id='.$prod->id.'&_csrf='.$this->_sess->get('_csrf').'";
		                                      $.ajax({
		                                              type: "POST",
		                                              url: url,
		                                              data: dataString,
		                                              success: function(data){
		                                                if(data=="ok"){
		                                                  swal("Borrado!", "El contenido se borró con exito.", "success");
		                                                  setTimeout(function() {
		                                                      window.location.reload()
		                                                  }, 200);
		                                                }else{
		                                                  swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
		                                                }
		                                                  
		                                                  
		                                              }
		                                      });
		                                  } else {
		                                      swal("Cancelado", "El contenido esta guardado", "error");
		                                  }
		                              });
		                          });
		                      });
		                   </script>';
	        
				}			
				$_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	
	public function editarInactiva($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerInactiva($_id);	
		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();	
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo->identificador);
		
		/*echo "<pre>";print_r($this->_view->imgs);echo "</pre>";*/
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
									
		
				
			
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){			
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					//echo "<pre>";print_r($this->_view->data);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarInactiva', 'contenidos');
						exit;
					} 
					
					
					
					$_vid = contenidos_video::find('all',array('conditions' => array('identificador = ?', $this->_view->trabajo->identificador)));
					if(!$_vid){
						$this->_view->_error ='No hay videos cargados';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editarInactiva', 'contenidos');
						exit;
					}
					
					
							
					
					$_editar = contenidos_pantalla_inactiv::find($this->_view->trabajo->id);
					//$_editar->id_computadora = $this->_view->data['id_computadora'];
					$_editar->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$_editar->save();

					$_updateModif = $this->_trabajosGestion->traerAsigModif($this->_view->trabajo->id, 'ids_inactivas');
					if($_updateModif){
						$_dat = 'i_'.$this->_view->trabajo->id;
						foreach ($_updateModif as $val) {
							$_edit = contenidos_asignacione::find($val->id);
							if($_edit){
								if($_edit->update_contenido!=''){
									$_ar = explode(',', $_edit->update_contenido);
									$_ar[] = $_dat;
									$_ar_unique = array_unique($_ar);
									$_ar_final = implode(',', $_ar_unique);
									$_edit->update_contenido = $_ar_final;
									$_edit->save();
								}else{
									$_edit->update_contenido = $_dat;
									$_edit->save();
								}
								
								
							}
						}

					}
					
					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('video_id');
					$this->redireccionar('administrador/contenidos/listarInactiva');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editarInactiva', 'contenidos');	
    }
	
	
	
	
	
	public function cargarInactiva($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)9999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));
		
		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();
		//echo "<pre>";print_r($_SESSION);echo "</pre>";
		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					//echo "<pre>";print_r($_POST);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargarInactiva', 'contenidos');
						exit;
					} 

					
					$_vid = contenidos_video::find('all',array('conditions' => array('identificador = ?', $this->_sess->get('carga_actual'))));
					if(!$_vid){
						$this->_view->_error ='No hay videos cargados';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargarInactiva', 'contenidos');
						exit;
					}
					
					
					$_fechaBd = date('Y-m-d');
					
					$promo = new contenidos_pantalla_inactiv();
					$promo->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$promo->identificador = $this->_sess->get('carga_actual');
					$promo->estado = 'alta';
					$promo->id_user = $this->_sess->get('usuario');
					$promo->fecha = "$_fechaBd";
					$promo->save();
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('video_id');
					$this->redireccionar('administrador/contenidos/listarInactiva');
				}	

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargarInactiva', 'contenidos');	
    }
	
	
	public function borrarInactiva()
	{
		$this->_acl->acceso('encargado_access');

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		
				/*$this->_trabajosGestion->borrarVideo($_id, $this->_conf['ruta_videos']);
				$this->redireccionar('administrador/contenidos/listarVideo');*/
				$_borrar = $this->_trabajosGestion->borrarInactiva($_id, $this->_conf['ruta_videos']);
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}

		
	}
	
	
}