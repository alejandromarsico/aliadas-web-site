<?php

use controllers\administradorController\administradorController;

class controlpantallaController extends administradorController
{
	public $_trabajosGestion;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		$this->getLibrary('class.usuarios');
		$this->getLibrary('class.admin');
		$this->_trabajosGestion = new admin();

		$this->getLibrary('PHPMailerAutoload');
		$this->envioMail = new PHPMailer();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';
		
		//unset($_SESSION['editar_asignacion']);
		unset($_SESSION['asignacion']);

		/*if(!$this->_sess->get('_supermercado') || $this->_sess->get('_supermercado')==''){
			$this->redireccionar('administrador');	
		}*/
    }
    
    public function index()
    {	

    	/*$_provincias = $this->_trabajosGestion->traerProvPDO();
    	echo "<pre>";print_r($_provincias);echo "</pre>";exit;*/
		unset($_SESSION['editar_asignacion']);
		unset($_SESSION['_bitacora']);
		unset($_SESSION['_bitacora_old']);
		$this->redireccionar('administrador/controlpantalla/selecTotem');	
    }
	
		
	
	public function selecTotem($pagina = false)
    {
		$this->_acl->acceso('ver_access');
		
		//$this->_view->setCss(array('style_contenidos'));
		$this->_view->setCss(array('style_contenidos','fastselect'));
		$this->_view->setJs(array('fastsearch.min','fastselect.min'));

		/*$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();*/

		//$_SESSION['level']=1;
		//echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;

		if($this->_sess->get('level')==1){
			$this->_view->provincias = $this->_trabajosGestion->traerProvincias();
		}else{

			if($this->_sess->get('_prov_jumbo') && $this->_sess->get('_supermercado') == 1){
				$this->_view->provincias = $this->_trabajosGestion->traerFiltroProv($this->_sess->get('_prov_jumbo'));
			}else if($this->_sess->get('_prov_disco') && $this->_sess->get('_supermercado') == 2){
				$this->_view->provincias = $this->_trabajosGestion->traerFiltroProv($this->_sess->get('_prov_disco'));
			}
			
			// $this->_view->provincias = $this->_trabajosGestion->traerFiltroProv($this->_sess->get('_provincias'));
		}
		
		$this->_view->tipo_locales = $this->_trabajosGestion->traerTipoLocales();
		
		
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('selec_totem', 'controlpantalla');	
    }



    public function traerTotems()
    {
    	$this->_acl->acceso('ver_access');
    	
    	if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

	    		//echo "<pre>";print_r($_POST);echo "</pre>";//exit;    		
	    		
	    		foreach ($_POST['prov'] as $val) {
	    			if(!validador::numerico($val)) exit("Debe introducir un valor correcto en el campo provincias");
	    		}

	    		foreach ($_POST['tipo_local'] as $val) {
	    			if(!validador::numerico($val)) exit("Debe introducir un valor correcto en el campo tipo local");
	    		}
	    		
				if(isset($_POST['excluidos'])){
	    			if(!validador::getTexto('excluidos')) exit('Debe introducir un valor correcto en el campo excluidos');
	    			$_excluidos = 'si';
	    		}else{
	    			$_excluidos = 'no';
	    		}

	    		

	    		if($this->_sess->get('level')==1){
	    			$_data = $this->_trabajosGestion->traerFiltroTotemsAsigPod($this->_sess->get('_supermercado'), $_POST['prov'], $_POST['tipo_local'], $_excluidos);				
				}else{

					$_prov = ($this->_sess->get('_super_jumbo')) ? $this->_sess->get('_prov_jumbo') : $this->_sess->get('_prov_disco');
					foreach ($_POST['prov'] as $val) {
						if(!in_array($val, $_prov)){
							echo '<h3>Consulta invalida</h3>';
	    					exit;
						}
					}			
					
					$_data = $this->_trabajosGestion->traerFiltroTotemsAsigPod($this->_sess->get('_supermercado'), $_POST['prov'], $_POST['tipo_local'], $_excluidos);	    			

				}

	    		
				//$_html='';

				   // echo "<pre>";print_r($_data);echo "</pre>";exit;

				if(!empty($_data)){
					foreach ($_data as $data) {
						$_array[] = $data['id'];
						// $_array[] = $data->id;
						/*$_html .='<div id="'.$data->id.'" class="cont_totem">
									<h3>'.$data->nombre.'</h3>
									Direccion:<br>'.admin::convertirCaracteres($data->direccion).'
									<p>Provincia:<br>'.admin::traerProvinciaPorID($data->id_provincia)->nombre.'</p>
								</div>';*/
					}
					
					//echo $_ids;exit;

					// echo implode(',', $_array);
					// echo "<pre>";print_r($_array);echo "</pre>";exit;
					// $_array = implode(',', $_array);
					$_datos = $this->_trabajosGestion->traerFiltroAsigPod($_array);

					// echo "<pre>";print_r($_datos);echo "</pre>";exit;
					if(!empty($_datos)){

						$_html='';
						foreach ($_datos as $data) {
							/*$_sinc = ($data->fecha_sincronizacion!='') ? $data->fecha_sincronizacion->format('d-m-Y H:i') : '';
							$_html .='<div id="'.$data->id.'" class="cont_totem">
										<div class="col-lg-6">
											<h3>'.admin::traerTotemPorID($data->id_totem)->nombre.'</h3>
											Direccion:<br><strong>'.admin::convertirCaracteres(admin::traerTotemPorID($data->id_totem)->direccion).'</strong>
											<p>Ultima sincronizacion:<br><strong>'. $_sinc.'</strong></p>
											<p>Estado:<br><strong>'.$data->estado.'</strong></p>
										</div>
										<div class="col-lg-6">
											<a class="btn btn-danger btn-sm btn-block" href="">Ver contenido</a>
											<a class="btn btn-danger btn-sm btn-block" href="">Modificar contenido</a>
										</div>
									</div>';*/
							$_tot = admin::traerTotemPorIdPod($data['id_totem']);
							$_html .='<div id="'.$data['id'].'" class="cont_totem">
										<div class="col-lg-6">
											<h3>'.$_tot['nombre'].'</h3>
											Direccion:<br><strong>'.$_tot['direccion'].'</strong>								
										</div>
										<div class="col-lg-6">
											<a class="btn btn-danger btn-sm btn-block" href="'.$this->_conf['url_enlace'].'administrador/controlpantalla/verContenido/'.$data['id_totem'].'">Ver contenido</a>';
							if($this->_sess->get('level')!=3){
								$_html .='<a class="btn btn-danger btn-sm btn-block" href="'.$this->_conf['url_enlace'].'administrador/controlpantalla/modificarContenido/'.$data['id_totem'].'">Modificar contenido</a>';
							}
										
							$_html .='</div></div>';
						}
					}else{
						$_html='<h3>No hay resultados</h3>';
					}

				}else{
					$_html='<h3>No hay resultados</h3>';
				}
				//echo "<pre>";print_r($_datos);echo "</pre>";exit;
				echo $_html;
				exit;

			}else{
				$this->redireccionar('error/access/404');
			}
    	}
    }
	
    public function verContenido($_id)
    {
		$this->_acl->acceso('ver_access');
		
		$this->_view->setCss(array('style_contenidos'));
		$this->_view->totem = $this->_trabajosGestion->traerTotem($_id);	
		$this->_view->asignaciones = $this->_trabajosGestion->traerAsigsPorTotem($_id);
		
		// echo "<pre>";print_r($this->_view->asignaciones);echo "</pre>";exit;
		
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('ver_contenido', 'controlpantalla');
    }
    public function modificarContenido($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('style_contenidos'));
		$this->_view->totem = $this->_trabajosGestion->traerTotem($_id);	
		$this->_view->asignaciones = $this->_trabajosGestion->traerAsigsPorTotem($_id);
		$this->_view->id = $_id; 

		unset($_SESSION['_bitacora']);
		$_SESSION['_bitacora']['_id_totem'] = $_id;
		
		//echo "<pre>";print_r($this->_view->asignaciones);echo "</pre>";exit;
		
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('modificar_contenido', 'controlpantalla');
    }

    public function bitacora($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('style_contenidos'));
		$this->_view->id = $_id; 
		$this->_view->totem = $this->_trabajosGestion->traerTotem($_id);
		$this->_view->bitacora = $this->_trabajosGestion->traerBitacoraPorTotem($_id);	
		
		//echo "<pre>";print_r($this->_view->bitacora);echo "</pre>";exit;
		
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('bitacora', 'controlpantalla');
    }
	
	public function ultimoCambio($_totem, $_id)
    {
		$this->_acl->acceso('encargado_access');
		$_update = $this->_trabajosGestion->desHacerUltimoCambio($_id);
		/*$_update = contenidos_asignaciones_updat::find(array('conditions' => array('id_asignacion = ?', $_id),'order' => 'fecha_upade desc'))->to_array();
		echo "<pre>";print_r($_update);echo "</pre>";exit;*/
		if($_update==true){
			$this->redireccionar('administrador/controlpantalla/modificarContenido/'.$_totem);
		}
		
    }
	public function consolidarTotems()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				$_SESSION['editar_asignacion']['_totems']  = $_POST['totems'];
				//echo "<pre>";print_r($_SESSION['seccion']);exit;
				echo '1';
	            exit();

			}else{
				$this->redireccionar('error/access/404');
			}	
        }
		
    }
    
    public function editarContenido($_id)
    {
    	$this->_acl->acceso('encargado_access');
		
		$_asig = $this->_trabajosGestion->traerAsignacion($_id);
		unset($_SESSION['editar_asignacion']);
		//unset($_SESSION['asignacion_old']);
		unset($_SESSION['_bitacora_old']);

		//$_SESSION['_bitacora']['_id_totem'] = $_id;
		$_SESSION['_bitacora']['_catalogos'] = '';
		$_SESSION['_bitacora']['_videos'] = '';
		$_SESSION['_bitacora']['_footer'] = '';
		$_SESSION['_bitacora']['_fondos'] = '';
		$_SESSION['_bitacora']['_inactivas'] = '';
		$_SESSION['_bitacora_old']['_catalogos'] = $_asig->ids_catalogos;
		$_SESSION['_bitacora_old']['_videos'] = $_asig->ids_videos;
		$_SESSION['_bitacora_old']['_footer'] = $_asig->ids_footers;
		$_SESSION['_bitacora_old']['_fondos'] = $_asig->ids_fondos;
		$_SESSION['_bitacora_old']['_inactivas'] = $_asig->ids_inactivas;

		$_SESSION['editar_asignacion']['_id'] = $_id;
		$_SESSION['editar_asignacion']['_totem'] = $_asig->id_totem;
		$_SESSION['editar_asignacion']['_catalogos'] = $_asig->ids_catalogos;
		$_SESSION['editar_asignacion']['_videos'] = $_asig->ids_videos;
		$_SESSION['editar_asignacion']['_footer'] = $_asig->ids_footers;
		$_SESSION['editar_asignacion']['_fondos'] = $_asig->ids_fondos;
		$_SESSION['editar_asignacion']['_inactivas'] = $_asig->ids_inactivas;
		$_SESSION['editar_asignacion']['_fecha_inicial'] = $_asig->vigencia_desde->format('d-m-Y H:i');
		$_SESSION['editar_asignacion']['_fecha_final'] = $_asig->vigencia_hasta->format('d-m-Y H:i');
		$_SESSION['editar_asignacion']['_dias_semana'] = $_asig->dias_semana;
		/*$_SESSION['asignacion_old']['_id'] = $_id;
		$_SESSION['asignacion_old']['_catalogos'] = $_asig->ids_catalogos;
		$_SESSION['asignacion_old']['_videos'] = $_asig->ids_videos;
		$_SESSION['asignacion_old']['_footer'] = $_asig->ids_footers;
		$_SESSION['asignacion_old']['_fondos'] = $_asig->ids_fondos;
		$_SESSION['asignacion_old']['_fecha_inicial'] = $_asig->vigencia_desde->format('d-m-Y H:i');
		$_SESSION['asignacion_old']['_fecha_final'] = $_asig->vigencia_hasta->format('d-m-Y H:i');*/
		//echo "<pre>";print_r($_asig);echo "</pre>";
		//echo "<pre>";print_r($_SESSION['editar_asignacion']);echo "</pre>";//exit;
		$this->redireccionar('administrador/controlpantalla/cargarHeader');
    	
    }
    public function cargarHeader()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->videos = $this->_trabajosGestion->traerVideos();
		// $data = contenidos_asignacione::find(2)->to_array();
		// echo "<pre>";print_r($data);echo "</pre>";exit;
		
		// echo "<pre>";print_r($_SESSION['_bitacora_old']['_videos'] );echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_header', 'controlpantalla');
    }
    
    public function consolidarHeader()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){
			//echo "<pre>";print_r($_POST);exit;
        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

	        	$_SESSION['editar_asignacion']['_videos']  = $_POST['videos'];
				
				if($_POST['videos']!=''){
					$_vid =explode(',',$_POST['videos']);
					$_vid_old= explode(',',$_SESSION['_bitacora_old']['_videos']);
					for ($i=0; $i < count($_vid); $i++) { 
						if(!in_array($_vid[$i], $_vid_old)){
							$_kk[]  = $_vid[$i];
						}
					}

					$_SESSION['_bitacora']['_videos'] = (isset($_kk)) ? implode(',', $_kk) : ''; 
				}else{
					$_SESSION['_bitacora']['_videos']  = $_POST['videos'];
				}
				
	         	
				//$_SESSION['editar_asignacion']['_videos']  = $_POST['videos'];
				//
				//echo "<pre>";print_r($_SESSION['seccion']);exit;
				echo '1';
	            exit();

        	}else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    }
	public function cargarCatalogos()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->catalogos = $this->_trabajosGestion->traerCatalogos();
		//echo "<pre>";print_r($_SESSION['editar_asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_catalogos', 'controlpantalla');
    }
    public function consolidarCatalogos()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
				
				//echo "<pre>";print_r($_POST);exit;         	
				$_SESSION['editar_asignacion']['_catalogos']  = $_POST['catalogos'];

				if($_POST['catalogos']!=''){
					$_cat =explode(',',$_POST['catalogos']);
					$_cat_old= explode(',',$_SESSION['_bitacora_old']['_catalogos']);
					for ($i=0; $i < count($_cat); $i++) { 
						if(!in_array($_cat[$i], $_cat_old)){
							$_kk[]  = $_cat[$i];
						}
					}			
					$_SESSION['_bitacora']['_catalogos'] = (isset($_kk)) ? implode(',', $_kk) : '';
				}else{
					$_SESSION['_bitacora']['_catalogos']  = $_POST['catalogos'];
				}
				//
				//echo "<pre>";print_r($_SESSION['seccion']);exit;
				echo '1';
	            exit();

        	}else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    }
    
    public function cargarFooter()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->footers = $this->_trabajosGestion->traerPromociones();
		//echo "<pre>";print_r($_SESSION['editar_asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_footers', 'controlpantalla');
    }

    public function consolidarFooter()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				//echo "<pre>";print_r($_POST);exit;         	
				$_SESSION['editar_asignacion']['_footer']  = $_POST['footer'];

				if($_POST['footer']!=''){
					$_foo =explode(',',$_POST['footer']);
					$_foo_old= explode(',',$_SESSION['_bitacora_old']['_footer']);
					for ($i=0; $i < count($_foo); $i++) { 
						if(!in_array($_foo[$i], $_foo_old)){
							$_kk[]  = $_foo[$i];
						}
					}
					$_SESSION['_bitacora']['_footer'] = (isset($_kk)) ? implode(',', $_kk) : '';
				}else{
					$_SESSION['_bitacora']['_footer']  = $_POST['footer'];
				}
				
				
				//
				//echo "<pre>";print_r($_SESSION['seccion']);exit;
				echo '1';
	            exit();

            }else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    }
    public function cargarFondos()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->fondos = $this->_trabajosGestion->traerFondos();
		//echo "<pre>";print_r($_SESSION['editar_asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_fondos', 'controlpantalla');
    }
    public function consolidarFondos()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				//echo "<pre>";print_r($_POST);exit;         	
				$_SESSION['editar_asignacion']['_fondos']  = $_POST['fondos'];

				if($_POST['fondos']!=''){
					$_fon =explode(',',$_POST['fondos']);
					$_fon_old= explode(',',$_SESSION['_bitacora_old']['_fondos']);
					for ($i=0; $i < count($_fon); $i++) { 
						if(!in_array($_fon[$i], $_fon_old)){
							$_kk[]  = $_fon[$i];
						}
					}
					
					$_SESSION['_bitacora']['_fondos'] = (isset($_kk)) ? implode(',', $_kk) : '';
				}else{
					$_SESSION['_bitacora']['_fondos']  = $_POST['fondos'];
				}
				//
				//echo "<pre>";print_r($_SESSION['editar_asignacion']);exit;
				echo '1';
	            exit();

            }else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    }

    public function cargarInactivas()
    {
    	$this->_acl->acceso('encargado_access');
		
		//$this->_view->setCss(array('styles.cargar'));
		//$this->_view->setJs(array('jquery.sticky'));
		$this->_view->setCss(array('style_contenidos'));
		
		$this->_view->inactivas = $this->_trabajosGestion->traerInactivas();
		//echo "<pre>";print_r($_SESSION['asignacion']);echo "</pre>";//exit;
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar_inactivas', 'controlpantalla');
    }

    public function consolidarInactivas()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				//echo "<pre>";print_r($_POST);exit;         	
				$_SESSION['editar_asignacion']['_inactivas']  = $_POST['inactivas'];

				if($_POST['inactivas']!=''){
					$_in =explode(',',$_POST['inactivas']);
					$_in_old= explode(',',$_SESSION['_bitacora_old']['_inactivas']);
					for ($i=0; $i < count($_in); $i++) { 
						if(!in_array($_in[$i], $_in_old)){
							$_kk[]  = $_in[$i];
						}
					}
					
					$_SESSION['_bitacora']['_inactivas'] = (isset($_kk)) ? implode(',', $_kk) : '';
				}else{
					$_SESSION['_bitacora']['_inactivas']  = $_POST['inactivas'];
				}
				//
				//echo "<pre>";print_r($_SESSION['editar_asignacion']);exit;
				echo '1';
	            exit();

            }else{
				$this->redireccionar('error/access/404');
			}
				
        }
		
    }
    
 	public function selecVigencia()
    {
    	$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('jquery-ui-timepicker-addon','sweetalert','fastselect.min','style_contenidos'));
		$this->_view->setJs(array('jquery-ui-timepicker-addon','sweetalert.min','fastsearch.min','fastselect.min'));

		//$this->_view->setCss(array('style_contenidos'));
		
		//$this->_view->fondos = $this->_trabajosGestion->traerFondos();
		// echo "<pre>";print_r($_SESSION['editar_asignacion']);echo "</pre>";//exit;

		if(isset($_SESSION['editar_asignacion']['_fecha_inicial'])){
			$time_desde = strtotime($_SESSION['editar_asignacion']['_fecha_inicial']);
			$this->_view->fecha_inicial = date('d-m-Y H:i',$time_desde);
		}
		if(isset($_SESSION['editar_asignacion']['_fecha_final'])){
			$time_hasta = strtotime($_SESSION['editar_asignacion']['_fecha_final']);
			$this->_view->fecha_final = date('d-m-Y H:i',$time_hasta);
		}

		$this->_view->dias = array('Lunes','Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado', 'Domingo');
		$this->_view->dias_semana = explode(',', $_SESSION['editar_asignacion']['_dias_semana']);

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){


				//echo "<pre>";print_r($_POST);exit; 
				$time_desde = strtotime($_POST['fecha_inicial']);
				$time_hasta = strtotime($_POST['fecha_final']);
				$_vigencia_desde = date('Y-m-d H:i:s',$time_desde);
				$_vigencia_hasta = date('Y-m-d H:i:s',$time_hasta);
						        	
				$_SESSION['editar_asignacion']['_fecha_inicial']  = $_vigencia_desde;
				$_SESSION['editar_asignacion']['_fecha_final']  = $_vigencia_hasta;
				$_SESSION['editar_asignacion']['_dias_semana']  = implode(',', $_POST['dias']);
				
				if($_SESSION['editar_asignacion']['_videos']=='' && $_SESSION['editar_asignacion']['_catalogos']=='' && $_SESSION['editar_asignacion']['_footer']=='' && $_SESSION['editar_asignacion']['_fondos']=='' && $_SESSION['editar_asignacion']['_inactivas']==''){
					echo '<h4 class="text-danger">Debe seleccionar al menos un contenido</h4>';
					exit;
				}

				echo '1';
	           	exit;	

           	}else{
				$this->redireccionar('error/access/404');
			}	
				
        }
		
    	$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('selec_vigencia', 'controlpantalla');
    }
	
	public function consolidarVigencia()
    {
    	$this->_acl->acceso('encargado_access');
				
		
        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				//echo "<pre>";print_r($_POST);exit; 
				/*$time_desde = strtotime($_POST['fecha_inicial']);
				$time_hasta = strtotime($_POST['fecha_final']);
				$_vigencia_desde = date('Y-m-d H:i:s',$time_desde);
				$_vigencia_hasta = date('Y-m-d H:i:s',$time_hasta);
						        	
				$_SESSION['editar_asignacion']['_fecha_inicial']  = $_vigencia_desde;
				$_SESSION['editar_asignacion']['_fecha_final']  = $_vigencia_hasta;
				
				if($_SESSION['editar_asignacion']['_videos']=='' && $_SESSION['editar_asignacion']['_catalogos']=='' && $_SESSION['editar_asignacion']['_footer']=='' && $_SESSION['editar_asignacion']['_fondos']=='' && $_SESSION['editar_asignacion']['_inactivas']==''){
					exit('<h4 class="text-danger">Debe seleccionar al menos un contenido</h4>');
				}*/

				$_bitacora = $this->_trabajosGestion->editarBitacora($_SESSION['_bitacora'], $this->_sess->get('usuario'),'modificado');
				if($_bitacora == false){
					echo 'error carga bitacora';
	            	exit();
				}

				$_editar = $this->_trabajosGestion->editarAsignacion($_SESSION['editar_asignacion'], $this->_sess->get('usuario'));
				if($_editar == true){

					//mandar mail a superadmin 

					/*$_body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
							<html xmlns="http://www.w3.org/1999/xhtml">
							<head>
							<meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
							<title>Cencosud</title>
							</head>					
							<body>
							<p>Se realizo un cambio en el totem: <strong>'.admin::traerTotemPorID($_SESSION['editar_asignacion']['_totem'])->nombre.'</strong></p>													
							</body>
							</html>';
					
					$this->envioMail->IsSMTP();
					$this->envioMail->SMTPAuth = true;
					$this->envioMail->Host = "smtphub.cencosud.cl";
					$this->envioMail->Username = "_MailCarteleria"; 
					$this->envioMail->Password = "a23uj8rs"; 
					$this->envioMail->Port = 25; 					
					$this->envioMail->From ='info@jumbo.com.ar';
					// $this->envioMail->From ='info@cencosud.com';
					$this->envioMail->FromName ='Cencosud';
					$this->envioMail->Subject = 'Modificacion de contenidos';				
					$this->envioMail->Body = $_body;
								
					$this->envioMail->AddAddress($this->_sess->get('user_mail'));
					$this->envioMail->addCC('flaviosantiago.fiorentini@jumbo.com.ar');
					$this->envioMail->IsHTML(true);	
					
					$exito = $this->envioMail->Send();
					
					$intentos=1;
					
					while ((!$exito) && ($intentos < 3)) {
						sleep(5);			
						$exito = $this->envioMail->Send();				
						$intentos=$intentos+1;			
					}
					
					if(!$exito) {			
						echo "Problemas enviando correo electrónico a ".$this->envioMail->ErrorInfo;
						exit;				
					}*/
					

					unset($_SESSION['editar_asignacion']);
					unset($_SESSION['_bitacora']);
					unset($_SESSION['_bitacora_old']);
					echo '1';
	            	exit;
				}
				
				//echo "<pre>";print_r($_SESSION['editar_asignacion']);exit;

			}else{
				$this->redireccionar('error/access/404');
			}
			
				
        }
		
    }

    public function exportaDatos($_id)
    {

    	$this->_acl->acceso('encargado_access');

		$_totem = $this->_trabajosGestion->traerTotem($_id);
		$_datos = $this->_trabajosGestion->traerBitacoraPorTotem($_id);
		$this->_view->filename = 'bitacora_'.admin::crearTitulo($_totem->nombre).'_'.date('dmY').'.xls';	

		$this->_view->html ='<table border="1" cellpadding="10" cellspacing="0" bordercolor="#666666" style="border-collapse:collapse;">
		<tbody>
		<tr>
		<th colspan="8">
		<h2>'.$_totem->nombre.'<br><small>'.$_totem->localidad.'</small></h2>
		</th>
		</tr>		
		<tr style="font-weight: bold;">
		<td>Fecha</td>
		<td>header</td>	
		<td>Catálogo</td>	
		<td>Footer</td>	
		<td>Fondo de Pantalla</td>	
		<td>Pantalla Inactiva</td>
		<td>Acción</td>
		<td>Usuario</td>	
		</tr>';

		foreach ($_datos as $data) {
			$this->_view->html .='<tr>';
			$this->_view->html .='<td valign="top">'.admin::convertirFecha($data->fecha->format('j-m-Y')).'</td>';
			
			if($data->ids_videos!=''){
				$this->_view->html .='<td valign="top">';
				$_dat = explode(',', $data->ids_videos);
				for($i=0;$i<count($_dat);$i++){
					if($i==0){
						$this->_view->html .= admin::traerVideoPorID($_dat[$i])->titulo;
					}else{
						$this->_view->html .='<br>'.admin::traerVideoPorID($_dat[$i])->titulo;
					}

				}
				$this->_view->html .='</td>';	      
	    	}else{
	    		$this->_view->html .='<td valign="top">-</td>';
	    	}

	    	
	    	if($data->ids_catalogos!=''){
				$this->_view->html .='<td valign="top">';
				$_dat = explode(',', $data->ids_catalogos);
				for($i=0;$i<count($_dat);$i++){
					if($i==0){
						$this->_view->html .= admin::traerCatalogoPorID($_dat[$i])->titulo;
					}else{
						$this->_view->html .='<br>'.admin::traerCatalogoPorID($_dat[$i])->titulo;
					}

				}
				$this->_view->html .='</td>';	      
	    	}else{
	    		$this->_view->html .='<td valign="top">-</td>';
	    	}

	    	if($data->ids_footers!=''){
				$this->_view->html .='<td valign="top">';
				$_dat = explode(',', $data->ids_footers);
				for($i=0;$i<count($_dat);$i++){
					if($i==0){
						$this->_view->html .=admin::traerPromocionPorID($_dat[$i])->titulo;
					}else{
						$this->_view->html .='<br>'.admin::traerPromocionPorID($_dat[$i])->titulo;
					}

				}
				$this->_view->html .='</td>';	      
	    	}else{
	    		$this->_view->html .='<td valign="top">-</td>';
	    	}

	    	if($data->ids_fondos!=''){
				$this->_view->html .='<td valign="top">';
				$_dat = explode(',', $data->ids_fondos);
				for($i=0;$i<count($_dat);$i++){
					if($i==0){
						$this->_view->html .=admin::traerFondoPorID($_dat[$i])->titulo;
					}else{
						$this->_view->html .='<br>'.admin::traerFondoPorID($_dat[$i])->titulo;
					}

				}
				$this->_view->html .='</td>';	      
	    	}else{
	    		$this->_view->html .='<td valign="top">-</td>';
	    	}


	    	if($data->ids_inactivas!=''){
				$this->_view->html .='<td valign="top">';
				$_dat = explode(',', $data->ids_inactivas);
				for($i=0;$i<count($_dat);$i++){
					if($i==0){
						$this->_view->html .=admin::traerInactivaPorID($_dat[$i])->titulo;
					}else{
						$this->_view->html .='<br>'.admin::traerInactivaPorID($_dat[$i])->titulo;
					}

				}
				$this->_view->html .='</td>';	      
	    	}else{
	    		$this->_view->html .='<td valign="top">-</td>';
	    	}
	    	
	    	$this->_view->html .='<td valign="top">'.ucfirst($data->accion).'</td>';
	    	// $this->_view->html .='<td valign="top">'.usuarios::traerUsuarioPorId($data->id_user)->nombre.'</td>';
	    	$this->_view->html .='<td valign="top">'.$data->id_user.'</td>';
	    	$this->_view->html .='</tr>';
		}
	
		
		$this->_view->html .='</tbody>
		</table>';


		$this->_view->titulo = 'Administrador - Seguimiento';
		$this->_view->renderizar('exportar_bitacora', 'controlpantalla','vacio');



    }



    public function borrarContBaja()
    {   	

    	//setear asig de alta y bajas segun fecha de hoy
    	$this->setAltaBaja();
    	
    	//traer asig de baja
    	$_bajas = $this->_trabajosGestion->traerAsignacionesBaja();
    	
    	//traer las asig de baja con mas de 60 dias
    	$fecha_hoy = date('Y-m-d');
    	foreach ($_bajas as $val) {
    		$_hasta = $val->vigencia_hasta->format('Y-m-d');
    		$nuevafecha = strtotime('+60 day' , strtotime($_hasta ));
			$nuevafecha = date('Y-m-d' , $nuevafecha);
			// echo $nuevafecha."<br>";

			if($nuevafecha < $fecha_hoy){
				$_data_id[]= $val->id;
				$_data[]= $val;
			}
    		
    	}
    	/*$ids = implode(',', $_data_id);
    	$_asig =  contenidos_asignacione::find_by_sql('SELECT id FROM contenidos_asignaciones WHERE FIND_IN_SET ("4",ids_catalogos) AND  id NOT IN ('.$ids.')');	
    	$_asig = $this->_trabajosGestion->armarArray($_asig);
    	for ($i=0; $i < count($_asig); $i++) { 
    		$_asig_id[] = $_asig[$i]['id'];
    	}
    	$_res = array_intersect($_asig_id, $_data_id);*/
    	
    	// echo $ids;
    	// echo "<pre>";print_r($_asig_id);//exit;
    	 // echo "<pre>";print_r($_data_id);exit;
    	// echo "<pre>";print_r($_res);exit;

  		//borrar contenidos no utilizados en otras asignaciones
  		if(isset($_data)){

	  		foreach ($_data as $val) {
	  			//borrar cada contenido
	  			if ($val->ids_catalogos!='') {
	  				$_borrar_cat = $this->_trabajosGestion->borrarVariosCatalogo($val->ids_catalogos, $this->_conf['ruta_img_cargadas'], 'catalogos', $_data_id);
	  			}
	  		 	if ($val->ids_videos!='') {
	  		 		$_borrar_vid = $this->_trabajosGestion->borrarVariosVideos($val->ids_videos, $this->_conf['ruta_videos'], $_data_id);
	  		 	}
	  		 	if ($val->ids_footers!='') {
	  		 		$_borrar_foot = $this->_trabajosGestion->borrarVariosFooters($val->ids_footers, $this->_conf['ruta_img_cargadas'], 'promociones', $_data_id);
	  		 	}
	  		 	if ($val->ids_fondos!='') {
	  		 		$_borrar_fon = $this->_trabajosGestion->borrarVariosFondos($val->ids_fondos, $this->_conf['ruta_img_cargadas'], 'fondos', $_data_id);
	  		 	}
	  		 	if ($val->ids_inactivas!='') {
	  		 		$_borrar_inac = $this->_trabajosGestion->borrarVariosInactivas($val->ids_inactivas, $this->_conf['ruta_videos'], $_data_id);
	  		 	}
	  		 	
	  		 	


	  		 	//borrar asignacion
	  		 	$borrar = contenidos_asignacione::find($val->id);
	  		 	$borrar->delete();	
	  		 } 
  		}


		// return true;

		echo "<pre>";print_r($_data_id);exit;

    	

    }

    private function setAltaBaja($_id=false)
	{			
	
		$asignaciones = $this->_trabajosGestion->traerTodasAsignaciones();
		$_fechahoy = date('Y-m-d H:i:s');
		foreach($asignaciones as $asig){
			if($_fechahoy >= $asig->vigencia_desde->format('Y-m-d H:i:s') && $_fechahoy <= $asig->vigencia_hasta->format('Y-m-d H:i:s')){
				//echo "si";
				if($asig->estado!='alta'){
					$this->_trabajosGestion->updateEstadoAsig($asig->id,'alta');
				}
				
			}else{
				//echo "fuera de vigencia";
				if($asig->estado!='baja'){
					$this->_trabajosGestion->updateEstadoAsig($asig->id,'baja');
				}
				
			}
		}
		
    }
	
	
}