<?php

use controllers\administradorController\administradorController;

class totemsController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		$this->getLibrary('class.admin');
		$this->_trabajosGestion = new admin();

		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';
		
		unset($_SESSION['editar_asignacion']);
		unset($_SESSION['asignacion']);
		unset($_SESSION['_bitacora']);
		unset($_SESSION['_bitacora_old']);

		/*if(!$this->_sess->get('_supermercado') || $this->_sess->get('_supermercado')==''){
			$this->redireccionar('administrador');	
		}*/

		// echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
			
    }
    
    public function index()
    {			
    	$this->_acl->acceso('encargado_access');
		$this->redireccionar('administrador/totems/listar');	
    }
	
		
	
	public function listar($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		/*$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();*/
		
		$this->_view->cod = $this->_trabajosGestion->traerTotems($this->_sess->get('_supermercado'));
		
		/*$this->_view->beneficios = $paginador->paginar($beneficios, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/beneficios/listarAlta');*/
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listar', 'totems');	
    }
	
	
	
	
	/*	
	
	public function orden($_id, $_orden, $_estado)
	{
		
		$this->_acl->acceso('admin_access');
		
		$_id = (int) $_id;
		$_orden = (int) $_orden;
		
		$this->_trabajosGestion->cambiarOrdenBeneficios($_id, $_orden);
		
		if($_estado==2){
			$this->redireccionar('administrador/slider/listarAlta');
		}else{
			$this->redireccionar('administrador/slider/listarBaja');
		}
	}
	
	
	public function ordenarEquipo()
	{
		$this->_acl->acceso('admin_access');
		
		if($_POST){
			$_puntos = explode(',',$_POST['puntos']);
			foreach($_puntos as $i => $pt){
				$_idPunto = explode('-', $pt);
				$this->_trabajosGestion->cambiarOrdenEquipo($_idPunto[1], $i+1);
			}
			
			echo 1;
	
		}
	}
	*/
	
	public function editar($_id)
	{
		$this->_acl->acceso('admin_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerTotem($_id);		
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo->id);
		
		//$this->_view->super = $this->_trabajosGestion->traerSupermercados();
		$this->_view->provincias = $this->_trabajosGestion->traerProvincias();
		$this->_view->tipo_locales = $this->_trabajosGestion->traerTipoLocales();
		
		
		/*$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('jqColor','jQueryColorPicker.min','dropzone.min'));*/
		
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
				
					$this->_view->data = $_POST;				
																
					//echo "<pre>";print_r($_POST);echo "</pre>";exit;			
							
					if(!validador::getInt('provincia')){
						$this->_view->_error ='Debe completar el campo provincia';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 
					if(!validador::getInt('tipo_local')){
						$this->_view->_error ='Debe completar el campo tipo local';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 

					if(!validador::getTexto('nombre')){
						$this->_view->_error ='Debe completar el campo nombre';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 

					if(!validador::getTexto('direccion')){
						$this->_view->_error ='Debe completar el campo direccion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 

					if(!validador::getTexto('localidad')){
						$this->_view->_error ='Debe completar el campo localidad';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 

					$_excluidos = (isset($_POST['excluidos'])) ? 'si' : 'no';		
					
					$_editar = contenidos_totem::find($this->_view->trabajo->id);
					$_editar->id_supermercado = $this->_xss->xss_clean($this->_sess->get('_supermercado'));
					$_editar->id_provincia = $this->_xss->xss_clean(validador::getInt('provincia'));
					$_editar->id_tipo_local = $this->_xss->xss_clean(validador::getInt('tipo_local'));
					$_editar->excluidos = $_excluidos;
					$_editar->nombre = $this->_xss->xss_clean(validador::getTexto('nombre'));
					$_editar->direccion = $this->_xss->xss_clean(validador::getTexto('direccion'));
					$_editar->localidad = $this->_xss->xss_clean(validador::getTexto('localidad'));
					$_editar->codigo = $this->_view->data['codigo'];
					$_editar->id_user = $this->_xss->xss_clean($this->_sess->get('usuario'));
					$_editar->save();
					
					$this->_sess->destroy('edicion_actual');
					//$this->_sess->destroy('img_id');
					$this->redireccionar('administrador/totems/listar');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editar', 'totems');	
    }
	
	
	
	
	
	public function cargar($_categoria = null)
    {	
		$this->_acl->acceso('admin_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)9999999999));
		}
		
		/*$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('jqColor','jQueryColorPicker.min','dropzone.min'));*/
		
		//$this->_view->super = $this->_trabajosGestion->traerSupermercados();
		$this->_view->provincias = $this->_trabajosGestion->traerProvincias();
		$this->_view->tipo_locales = $this->_trabajosGestion->traerTipoLocales();
		
		//echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		
		
		
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					// echo "<pre>";print_r($_FILES);echo "</pre>";exit;
					
					if(!validador::getInt('provincia')){
						$this->_view->_error ='Debe completar el campo provincia';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 
					if(!validador::getInt('tipo_local')){
						$this->_view->_error ='Debe completar el campo tipo local';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 

					if(!validador::getTexto('nombre')){
						$this->_view->_error ='Debe completar el campo nombre';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 

					if(!validador::getTexto('direccion')){
						$this->_view->_error ='Debe completar el campo direccion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 

					if(!validador::getTexto('localidad')){
						$this->_view->_error ='Debe completar el campo localidad';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'totems');
						exit;
					} 


					$_excluidos = (isset($_POST['excluidos'])) ? 'si' : 'no';
					$_fechaBd = date('Y-m-d');				
					
					$reel = contenidos_totem::create(array(					
						'id_supermercado'	=> $this->_xss->xss_clean($this->_sess->get('_supermercado')),
						'id_provincia'		=> $this->_xss->xss_clean(validador::getInt('provincia')),
						'id_tipo_local'		=> $this->_xss->xss_clean(validador::getInt('tipo_local')),
						'excluidos'			=> $_excluidos,					
						'nombre'			=> $this->_xss->xss_clean(validador::getTexto('nombre')),
						'direccion'			=> $this->_xss->xss_clean(validador::getTexto('direccion')),
						'localidad'			=> $this->_xss->xss_clean(validador::getTexto('localidad')),
						'codigo'			=> $this->_view->data['codigo'],
						'id_user'			=> $this->_xss->xss_clean($this->_sess->get('usuario')),
						'fecha'				=> "$_fechaBd",
					));
								
					
					$this->_sess->destroy('carga_actual');
					//$this->_sess->destroy('img_id');
					$this->redireccionar('administrador/totems/listar');
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'totems');	
    }
	
	public function generarID()
	{
		$this->_acl->acceso('admin_access');
		
		$_cod = admin::cadenaAleatoriaSegura(15);
		
		echo $_cod;
	}
	
	
	public function borrar()
	{
		$this->_acl->acceso('admin_access');
		//$_id = (int) $_id;

		if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
			$_id = (int) $_POST['_id'];
	
			
			/*$this->_trabajosGestion->borrarCatalogo($_id, $this->_conf['ruta_img_cargadas'], 'catalogos');
			$this->redireccionar('administrador/contenidos/listarCatalogo');*/

			$_borrar = $this->_trabajosGestion->borrarTotem($_id);
			if ($_borrar==false) {
				echo "enuso";
			}else{
				echo "ok";
			}
			

		}else{
			$this->redireccionar('error/access/404');
		}
		
		//$this->_view->trabajo = $this->_trabajosGestion->traerTrabajo($_id);		
		
		//$this->redireccionar('administrador/totems/listar');
	}
	
	


	public function verTotem($_totem)
	{
		$this->_acl->acceso('encargado_access');

		$_totem = (int) $_totem;

		$this->_view->totem = $_totem;
		$this->_view->super = ($this->_sess->get('_supermercado')==1) ? 'jumbo' : 'disco';

		$_cron = $this->ejecutarCron($_totem);
			
		// echo "<pre>";print_r($_cron);exit;
		
		//array data asignaciones alta
		$_asig = $this->_trabajosGestion->traerAsigAltaVerTotemPdo($_totem, 'alta');

		 // echo "<pre>";print_r($_asig);exit;

		if(!empty($_asig)){
			//catalogos
			foreach ($_asig as $dat) {
				$_cata[]=$dat['ids_catalogos'];						
			}
			$_cata = array_map('intval', $_cata);
			$_cata = implode(',', $_cata);
			$_cata = explode(',',$_cata);
			$_cata = array_unique($_cata);
			$_cata = array_filter($_cata);
			$_cata = implode(',', $_cata);
			$_ids['catalogos']= $_cata;
			if($_cata){
				$_catalogos = $this->_trabajosGestion->traerCatalogosVerTotemPdo($_cata);
				if($_catalogos){
					$_data['catalogos'] = $_catalogos;
				}
			}

			
			//videos
			foreach ($_asig as $dat) {
				$_vid[]=$dat['ids_videos'];						
			}
			$_vid = array_map('intval', $_vid);
			$_vid = implode(',', $_vid);
			$_vid = explode(',',$_vid);
			$_vid = array_unique($_vid);
			$_vid = array_filter($_vid);
			$_vid = implode(',', $_vid);
			$_ids['videos']= $_vid;
			if($_vid){
				$_videos = $this->_trabajosGestion->traerVideosVerTotemPdo($_vid);
				if($_videos){
					$_data['videos'] = $_videos;
				}
			}
			
			//footers
			foreach ($_asig as $dat) {
				$_fot[]=$dat['ids_footers'];						
			}
			$_fot = array_map('intval', $_fot);
			$_fot = implode(',', $_fot);
			$_fot = explode(',',$_fot);
			$_fot = array_unique($_fot);
			$_fot = array_filter($_fot);
			$_fot = implode(',', $_fot);
			$_ids['footers']= $_fot;
			if($_fot){
				$_footers = $this->_trabajosGestion->traerFootersVerTotemPdo($_fot);
				if($_footers){
					$_data['footers'] = $_footers;
				}
			}
			
			//fondos
			foreach ($_asig as $dat) {
				$_fon[]=$dat['ids_fondos'];						
			}
			$_fon = array_map('intval', $_fon);
			$_fon = implode(',', $_fon);
			$_fon = explode(',',$_fon);
			$_fon = array_unique($_fon);
			$_fon = array_filter($_fon);
			$_fon = implode(',', $_fon);
			$_ids['fondos']= $_fon;
			if($_fon){
				$_fondos = $this->_trabajosGestion->traerFondosVerTotemPdo($_fon);
				if($_fondos){
					$_data['fondos'] = $_fondos;
				}
			}

			//pantalla inactiva
			foreach ($_asig as $dat) {
				$_inac[]=$dat['ids_inactivas'];						
			}
			$_inac = array_map('intval', $_inac);
			$_inac = implode(',', $_inac);
			$_inac = explode(',',$_inac);
			$_inac = array_unique($_inac);
			$_inac = array_filter($_inac);
			$_inac = implode(',', $_inac);
			$_ids['inactivas']= $_inac;
			if($_inac){
				$_inactivas = $this->_trabajosGestion->traerInactivasVerTotemPdo($_inac);
				if($_inactivas){
					$_data['inactivas'] = $_inactivas;
				}
			}

			
			$_data_json_final['ids'] = $_ids;
			$_data_json_final['contenidos'] = $_data;

			 // echo "<pre>";print_r($_data_json_final);echo "</pre>";exit;
						
			$data = $_data_json_final['contenidos'];
			$this->_view->data_cont = $_data_json_final['contenidos'];
			// $this->_view->data_cont[] = $_data_json_final['contenidos']['videos'];
			/*$this->_view->data_cont = serialize($_data_json_final['contenidos']);
			$this->_view->data_cont = base64_encode($this->_view->data_cont)*/;
			$_ruta_cont = $this->_conf['base_url']."public/img/subidas/";
			$_ruta_cont_vid = $this->_conf['base_url']."public/videos/";
			
			 // echo "<pre>";print_r($this->_view->data_cont);echo "</pre>";exit;
			
			if(isset($data['catalogos']) && !empty($data['catalogos'])){
				foreach($data['catalogos'] as $dat){		
					$dir = $this->_conf['ruta_img_cargadas']. "catalogos/cat_".$dat['identificador'].'/thumb';
					$photo = scandir($dir, 1);
					$this->_view->thumb[] = $_ruta_cont."catalogos/cat_".$dat['identificador']."/thumb/".$photo[0];
					$this->_view->tipo_thumb[] = "cat_".$dat['identificador'];
					
				}
			}

			 

			if(isset($data['videos']) && !empty($data['videos'])){
				foreach($data['videos'] as $dat){
					$dir = $this->_conf['ruta_videos']."vid_".$dat['identificador'].'/thumb';
					$photo = scandir($dir, 1);
					$this->_view->thumb[] = $_ruta_cont_vid."vid_".$dat['identificador']."/thumb/".$photo[0];
					$this->_view->tipo_thumb[] = "vid_".$dat['identificador'];
					
				}
			}

			if(!isset($data['catalogos']) && empty($data['catalogos']) && !isset($data['videos']) && empty($data['videos'])){
				
				$this->_view->thumb[] = $this->_conf['base_url'].'views/layout/totem/img/contenidos_default/'.$this->_view->super.'/catalogos/cat_2827483108/thumb/01.jpg';
				$this->_view->tipo_thumb[] = "cat_2827483108";
			}

			 // echo "<pre>";print_r($this->_view->tipo_thumb);echo "</pre>";//exit;
			 //  echo "<pre>";print_r($this->_view->thumb);echo "</pre>";//exit;
			
			
			if(isset($data['footers']) && !empty($data['footers'])){
				foreach($data['footers'] as $dat){
					$this->_view->_footer[] = $_ruta_cont.'promociones/'.$dat['path'];
				}				
				
			}

			 // echo "<pre>";print_r($this->_view->_footer);echo "</pre>";//exit;

			
			if(isset($data['fondos']) && !empty($data['fondos'])){
				$k = array_rand($data['fondos'],1);
				$this->_view->_fondo = $_ruta_cont.'fondos/'.$data['fondos'][$k]['path'];
			}

			 // echo "<pre>";print_r($this->_view->_fondo);echo "</pre>";//exit;

			if(isset($data['inactivas']) && !empty($data['inactivas'])){
				$v = array_rand($data['inactivas'],1);
				$this->_view->_inact = $_ruta_cont_vid.'inac_'.$data['inactivas'][$v]['identificador'].'/'.$data['inactivas'][$v]['path']; 
			}
			
			 // echo "<pre>";print_r($this->_view->_inact);echo "</pre>";//exit;
			
			if(isset($data['catalogos'])){
				$directory = $this->_conf['ruta_img_cargadas']."catalogos/cat_".$data['catalogos'][0]['identificador'];
				$dirint = dir($directory);
				while (($archivo = $dirint->read()) !== false){
					if (preg_match( '/\.(?:jpg|png|gif)$/i', $archivo)) {
						$this->_view->_data_img[]= $_ruta_cont."catalogos/cat_".$data['catalogos'][0]['identificador'].'/'.$archivo;
						$this->_view->_size = getimagesize($directory.'/'.$archivo);
					}
				}
				$dirint->close();
				natcasesort($this->_view->_data_img);

			}elseif(isset($data['videos'])){

				foreach ($data['videos'] as $dat) {
					$this->_view->_data_vid[]= $_ruta_cont_vid."vid_".$dat['identificador']."/".$dat['path'];
				}


				
			}

			//echo "<pre>";print_r($this->_view->_size);echo "</pre>";//exit;
			// echo "<pre>";print_r($this->_view->_data_img);echo "</pre>";//exit;
			
		}else{
			 $this->_view->nada = "No hay contenidos vigentes";
			if(!isset($data['catalogos']) && empty($data['catalogos']) && !isset($data['videos']) && empty($data['videos'])){
				
				$this->_view->thumb[] = $this->_conf['base_url'].'views/layout/totem/img/contenidos_default/'.$this->_view->super.'/catalogos/cat_2827483108/thumb/01.jpg';
				$this->_view->tipo_thumb[] = "cat_2827483108";
			}
		}

		$_nombre_totem = admin::traerTotemPorIdPod($_totem);
		$this->_view->titulo = 'TOTEM: '. $_nombre_totem['nombre'];		
		//$this->_view->titulo = 'TOTEM: '. admin::traerTotemPorID($_totem)->nombre;
        $this->_view->renderizar('ver', 'totems','totem');	


	}

	public function ejecutarCron($_id)
	{
		$this->_acl->acceso('encargado_access');			
	
		$asignaciones = $this->_trabajosGestion->traerAsignacionesPdo($_id);
		$_fechahoy = date('Y-m-d H:i:s');
		$_dia = jddayofweek (cal_to_jd(CAL_GREGORIAN, date("m"),date("d"), date("Y")) , 0);
		$_array_dias = array('Domingo', 'Lunes','Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
		$_dia_semana = $_array_dias[$_dia];

		if($asignaciones){
			foreach($asignaciones as $asig){
				// if($_fechahoy >= $asig->vigencia_desde->format('Y-m-d H:i:s') && $_fechahoy <= $asig->vigencia_hasta->format('Y-m-d H:i:s')){
				if($_fechahoy >= date('Y-m-d H:i:s', strtotime($asig['vigencia_desde'])) && $_fechahoy <= date('Y-m-d H:i:s', strtotime($asig['vigencia_hasta']))){
					
					if($asig['dias_semana']!=''){
						$_dias = explode(',', $asig['dias_semana']);
						if(in_array($_dia_semana, $_dias)){
							$this->_trabajosGestion->updateEstadoAsigPdo($asig['id'],'alta');
						}else{
							$this->_trabajosGestion->updateEstadoAsigPdo($asig['id'],'baja');
						}
					}else{
						if($asig['estado']!='alta'){
							$this->_trabajosGestion->updateEstadoAsigPdo($asig['id'],'alta');
						}
					}


					
					
				}else{
					if($asig['estado']!='baja'){
						$this->_trabajosGestion->updateEstadoAsigPdo($asig['id'],'baja');
					}
					
				}
			}			
		}
		
		
    }


    public function traerContenido()
    {
    	$this->_acl->acceso('encargado_access');

    	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

    		if(!validador::numerico($_POST['totem'])){
				$jsondata['error']= "El totem debe ser un valor numerico";
				echo json_encode($jsondata);
				exit;
			} 

    		if(!validador::getTexto('id')){
				$jsondata['error']= "El identificador debe ser un texto";
				echo json_encode($jsondata);
				exit;
			} 

			if(!validador::getTexto('valor')){
				$jsondata['error']= "El valor debe ser un texto";
				echo json_encode($jsondata);
				exit;
			} 	

	    	if($_POST['valor']=='cargar_catalogo'){

	    		// echo "<pre>";print_r($_POST);echo "</pre>";exit;

	    		// $_cont = $_POST['data'];
				$_id = $_POST['id'];
				$_totem = (int) $_POST['totem'];
				$_dato = explode('_', $_id);
				$_ruta_cont = $this->_conf['base_url']."public/img/subidas/";
				$_ruta_cont_vid = $this->_conf['base_url']."public/videos/";

				$_cron = $this->ejecutarCron($_totem);			
							
				//array data asignaciones alta
				$_asig = $this->_trabajosGestion->traerAsigAltaVerTotemPdo($_totem, 'alta');

				 // echo "<pre>";print_r($_asig);exit;

				if(!empty($_asig)){
					//catalogos
					foreach ($_asig as $dat) {
						$_cata[]=$dat['ids_catalogos'];						
					}
					$_cata = array_map('intval', $_cata);
					$_cata = implode(',', $_cata);
					$_cata = explode(',',$_cata);
					$_cata = array_unique($_cata);
					$_cata = array_filter($_cata);
					$_cata = implode(',', $_cata);
					// $_ids['catalogos']= $_cata;
					if($_cata){
						$_catalogos = $this->_trabajosGestion->traerCatalogosVerTotemPdo($_cata);
						if($_catalogos){
							$_data['catalogos'] = $_catalogos;
						}
					}

					
					//videos
					foreach ($_asig as $dat) {
						$_vid[]=$dat['ids_videos'];						
					}
					$_vid = array_map('intval', $_vid);
					$_vid = implode(',', $_vid);
					$_vid = explode(',',$_vid);
					$_vid = array_unique($_vid);
					$_vid = array_filter($_vid);
					$_vid = implode(',', $_vid);
					// $_ids['videos']= $_vid;
					if($_vid){
						$_videos = $this->_trabajosGestion->traerVideosVerTotemPdo($_vid);
						if($_videos){
							$_data['videos'] = $_videos;
						}
					}
						

					
					// $_data_json_final['ids'] = $_ids;
					$_cont = $_data;
				}

				// echo "<pre>";print_r($_cont);exit;

				if($_dato[0]=='cat'){

					foreach ($_cont['catalogos'] as $val) {
						if($val['identificador'] == $_dato[1]){
							$jsondata['orientacion'] = $val['imagenes_orientacion'];
							$jsondata['tipo'] = $_dato[0];

							$directory = $this->_conf['ruta_img_cargadas']."catalogos/cat_".$val['identificador'];
							$dirint = dir($directory);
							while (($archivo = $dirint->read()) !== false){
								if (preg_match( '/\.(?:jpg|png|gif)$/i', $archivo)) {
									$_data_img[]= $_ruta_cont."catalogos/cat_".$val['identificador'].'/'.$archivo;
									
								}
							}
							$dirint->close();
							natcasesort($_data_img);

							$jsondata['catalogo'] = '<div class="flipbook">';
							foreach($_data_img as $img){
								// $_rand_nunber = mt_rand(1000000000, mt_getrandmax());
								$jsondata['catalogo'] .= '<div style="background-image:url('.$img.')"></div>';
							}   
							$jsondata['catalogo'] .= '</div>';

							break;
						}
					}

				}

				if($_dato[0]=='vid'){

					foreach ($_cont['videos'] as $val) {
						if($val['identificador'] == $_dato[1]){						
							$jsondata['tipo'] = $_dato[0];						
							$jsondata['catalogo'] = '<video width="800" loop="">
								<source src="'.$_ruta_cont_vid."vid_".$val['identificador']."/".$val['path'].'" type="video/mp4">	
							</video>';											

							break;
						}
					}
					
				}

				$jsondata['error']='';
				
				echo json_encode($jsondata);
				exit;

			}

		}else{
			$this->redireccionar('error/access/404');
		}




    }
	
	
	
}