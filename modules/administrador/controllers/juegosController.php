<?php

use controllers\administradorController\administradorController;

class juegosController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		
		$this->getLibrary('class.validador');
		
		$this->getLibrary('class.admin');		
		$this->_trabajosGestion = new admin();
		
		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		
		
    }
    
    
	
	public function index($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		/*$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();*/

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		// $this->_view->datos = $this->_trabajosGestion->traerJuegos();
		
		/*$this->_view->beneficios = $paginador->paginar($beneficios, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/beneficios/listarAlta');*/
		
		// echo "<pre>";print_r($this->_view->datos);echo "</pre>";exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'juegos');	
    }
	
	public function listarChoice($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		/*$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();*/

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		 $this->_view->datos = $this->_trabajosGestion->traerChoices();
		
		/*$this->_view->beneficios = $paginador->paginar($beneficios, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/beneficios/listarAlta');*/
		
		// echo "<pre>";print_r($this->_view->datos);echo "</pre>";exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listarchoice', 'juegos');	
    }
	
	
	/*
	
	public function vista_previa_catalogos($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('basic'));
		$this->_view->setJs(array('modernizr.2.5.3.min', 'turn'));
		
		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);
		$this->_view->imagenes = $this->_trabajosGestion->traerGaleriaPorIdentificador($this->_view->trabajo->identificador, $this->_view->trabajo->imagenes_orientacion);
		$this->_view->size = getimagesize($this->_conf['base_url']. 'public/img/subidas/catalogos/cat_'.$this->_view->trabajo->identificador.'/'.$this->_view->imagenes[0]->path);
		
		//echo $this->_view->size;
		//echo "<pre>";print_r($this->_view->size);exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('vista_previa_catalogos', 'tendencias');	
    }
	*/
	
	
	
	public function editarChoice($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
			
		
		// $this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		// $this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited'));

		
		$this->_view->trabajo = $this->_trabajosGestion->traerChoice($_id);
		$preg = base64_decode($this->_view->trabajo['preguntas']);
	    $this->_view->trabajo['_preg'] = unserialize($preg);
	    $resp = base64_decode($this->_view->trabajo['respuestas']);
	    $this->_view->trabajo['_resp']  = unserialize($resp);
	    $_resp_correctas = base64_decode($this->_view->trabajo['respuesta_correcta']);
	    $this->_view->trabajo['_resp_correctas'] = unserialize($_resp_correctas);
	    $puntos = base64_decode($this->_view->trabajo['puntos']);
	    $this->_view->trabajo['_puntos'] = unserialize($puntos);
		

		
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo['identificador']);
		
		

		// echo "<pre>";print_r($this->_view->trabajo);exit;		
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					 // echo "<pre>";print_r($_POST);exit;

					/*if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'capacitaciones');
						exit;
					} 

					if(!validador::getInt('codigo')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					}*/
					
					/*
					if($this->_view->data['tags']!=''){
						$_tags = explode(',', $this->_view->data['tags']);
						foreach ($_tags as $val) {
							$_hay = contenidos_tag::find(array('conditions' => array('nombre = ?', $val)));
							if(!$_hay){
								$tag = new contenidos_tag();
								$tag->nombre = $val;						
								$tag->save();
							}
							
						}
						
					}*/
					
					$_enc_preg = serialize($_POST['_preg']);
                    $_enc_preg = base64_encode($_enc_preg);

                    $_enc_resp = serialize($_POST['_resp']);
                    $_enc_resp = base64_encode($_enc_resp);

                    $_resp_correctas = serialize($_POST['_resp_correcta']);
                    $_resp_correctas = base64_encode($_resp_correctas);

                    $_puntos = serialize($_POST['_puntos']);
                    $_puntos = base64_encode($_puntos);


					
					$cat = contenidos_multiple_choice::find($this->_view->trabajo['id']);
					$cat->id_tipo_juego = $this->_view->data['_tipo_juego'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->preguntas = $_enc_preg;
					$cat->respuestas = $_enc_resp;
					$cat->respuesta_correcta = $_resp_correctas;
					$cat->puntos = $_puntos;
					// $cat->identificador = $this->_sess->get('carga_actual');
					// $cat->estado = 'alta';
					// $cat->fecha = date('Y-m-d');
					$cat->save();	
								
					
					/*$cat = contenidos_capacitacione::find($this->_view->trabajo['id']);
					$cat->categorias = implode(',', $this->_view->data['categorias']);
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->id_juego = $this->_view->data['juego'];
					$cat->bajada = $this->_xss->xss_clean(validador::getTexto('bajada'));
					$cat->descripcion = $this->_xss->xss_clean(validador::getTexto('descripcion'));
					$cat->video = $this->_view->data['video'];
					$cat->tags = $this->_view->data['tags'];
					$cat->save();*/
								
					

					$this->_sess->destroy('edicion_actual');
					$this->redireccionar('administrador/juegos/listarChoice');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editarchoice', 'juegos');	
    }
	
	
	
	
	
	public function cargarChoice($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		// $this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		// $this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited'));

		// $this->_view->categorias = $this->_trabajosGestion->traerCategorias();
		// $this->_view->tags = $this->_trabajosGestion->traerTags();

		 // echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		// echo "<pre>";print_r($this->_view->tags);echo "</pre>";exit;

		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					// echo "<pre>";print_r($_POST);exit;

					/*if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'capacitaciones');
						exit;
					} 

					if(!validador::getInt('bajada')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'tendencias');
						exit;
					} */
					
						
					/*
					$_fechaBd = date('Y-m-d');

					if($this->_view->data['tags']!=''){
						$_tags = explode(',', $this->_view->data['tags']);
						foreach ($_tags as $val) {
							$_hay = contenidos_tag::find(array('conditions' => array('nombre = ?', $val)));
							if(!$_hay){
								$tag = new contenidos_tag();
								$tag->nombre = $val;						
								$tag->save();
							}
							
						}
						
					}*/


					$_enc_preg = serialize($_POST['_preg']);
                    $_enc_preg = base64_encode($_enc_preg);

                    $_enc_resp = serialize($_POST['_resp']);
                    $_enc_resp = base64_encode($_enc_resp);

                    $_resp_correctas = serialize($_POST['_resp_correcta']);
                    $_resp_correctas = base64_encode($_resp_correctas);

                    $_puntos = serialize($_POST['_puntos']);
                    $_puntos = base64_encode($_puntos);


					
					$cat = new contenidos_multiple_choice();
					$cat->id_tipo_juego = $this->_view->data['_tipo_juego'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->preguntas = $_enc_preg;
					$cat->respuestas = $_enc_resp;
					$cat->respuesta_correcta = $_resp_correctas;
					$cat->puntos = $_puntos;
					$cat->identificador = $this->_sess->get('carga_actual');
					$cat->estado = 'alta';
					$cat->fecha = date('Y-m-d');
					$cat->save();

					$val = new contenidos_juego();
					$val->id_juego = $cat->id;
					$val->id_tipo_juego = $this->_view->data['_tipo_juego'];
					$val->tabla_juegos = 'contenidos_multiple_choices';					
					$val->fecha = date('Y-m-d');
					$val->save();	

								
					
					$this->_sess->destroy('carga_actual');
					$this->redireccionar('administrador/juegos/listarChoice');
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargarchoice', 'juegos');	
    }
	
	
	public function borrarChoice()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		

				$_borrar = $this->_trabajosGestion->borrarChoice($_id);
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}
	
	
	
	
}