<?php

use controllers\administradorController\administradorController;

class usuariosController extends administradorController
{
	public $usuariosGestion;
	public $_filtro;
	public $_error;
	
    public function __construct() 
    {
        parent::__construct();

        // $this->redireccionar('administrador');

		$this->getLibrary('class.validador');
		
		$this->getLibrary('class.usuarios');
		$this->usuariosGestion = new usuarios();
		$this->getLibrary('class.admin');
		$this->_trabajosGestion = new admin();
		
		$this->_filtro = '';
		$this->_error = 'has-error';
    }
    
    public function index()
    {
		$this->_acl->acceso('admin_access');
		$this->redireccionar('administrador/usuarios/listar');	
       /* $this->_view->titulo = 'Administrador - Usuarios';
        $this->_view->renderizar('index', 'usuarios');*/
    }
	
	public function listar($pagina = false)
	{
		$this->_acl->acceso('admin_access');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();
		
		// Traemos los roles
		$this->_view->roles = $this->usuariosGestion->traerRoles();
		
		if($_POST){
			if($_POST['_filtrar'] == 1){
				$this->_filtro = validador::getPostParam('_roles');
			}
		}
		
		// Y los carhamos en el paginador
		$this->_view->usuarios =$this->usuariosGestion->TraerUsuarios($this->_filtro);
		//$this->_view->usuarios = $paginador->paginar($this->usuariosGestion->TraerUsuarios($this->_filtro), $pagina, 20);
		
		// Creamos la paginacion
		//$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/usuarios/listar');
        $this->_view->titulo = 'Administrador - Listar Usuarios';
        $this->_view->renderizar('listar', 'usuarios');
	}
	
	public function cargar()
	{
		$this->_acl->acceso('admin_access');

		// $this->_view->setCss(array('style_contenidos','fastselect'));
		// $this->_view->setJs(array('fastsearch.min','fastselect.min'));
		
		// Traemos los roles
		$this->_view->roles = $this->usuariosGestion->traerRoles();
		//$this->_view->supermercados = $this->_trabajosGestion->traerSupermercados();
		$this->_view->provincias = $this->_trabajosGestion->traerProvincias();
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				if($_POST['envio01'] == 1){
					$this->_view->data = $_POST;


					
					if(!validador::getTexto('usuario_nombre')){
						$this->_view->errorNombre = $this->_error;
						$this->_view->_error = 'Debe colocar un Nombre';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
					
					if(!validador::getTexto('usuario_usuario')){
						$this->_view->errorUsuario = $this->_error;
						$this->_view->_error = 'Debe colocar un Usuario';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
					
					if(!validador::getTexto('usuario_email')){
						$this->_view->errorEmail = $this->_error;
						$this->_view->_error = 'Debe colocar un Email';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
					
					if(!validador::validarEmail(validador::getPostParam('usuario_email'))){
						$this->_view->errorEmail = $this->_error;
						$this->_view->_error = 'Debe colocar un Email V&aacute;lido';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
					
					if($this->usuariosGestion->verificarEmail(validador::getPostParam('usuario_email'))){
						$this->_view->_error = 'Este email ya esta registrado';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
					
					if(!validador::getTexto('usuario_pass')){
						$this->_view->errorPass = $this->_error;
						$this->_view->_error = 'Debe colocar un Password';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
					
					if(!validador::getTexto('usuario_repetir_pass')){
						$this->_view->errorPassRepetir = $this->_error;
						$this->_view->_error = 'Debe repetir el Password';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
					
					if(validador::getTexto('usuario_pass') != validador::getTexto('usuario_repetir_pass')){
						$this->_view->_error = 'Los Password no coinciden';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
					if(validador::getPostParam('usuario_rol')!=1){
						/*if(!$this->_view->data['super_gestion']){
							$this->_view->errorSuper = $this->_error;
							$this->_view->_error = 'Debe seleccionar un supermecardos';
							$this->_view->titulo = 'Administrador - Cargar Usuarios';
							$this->_view->renderizar('cargar', 'usuarios');
							exit;
						}*/
						/*if(count($this->_view->data['super_gestion'])>4){
							$this->_view->errorSuper = $this->_error;
							$this->_view->_error = 'No debe seleccionar mas de 4 supermecardos';
							$this->_view->titulo = 'Administrador - Cargar Usuarios';
							$this->_view->renderizar('cargar', 'usuarios');
							exit;
						}*/
						/*if(!$this->_view->data['prov_gestion']){
							$this->_view->errorProv = $this->_error;
							$this->_view->_error = 'Debe seleccionar una provincia';
							$this->_view->titulo = 'Administrador - Cargar Usuarios';
							$this->_view->renderizar('cargar', 'usuarios');
							exit;
						}
						if(count($this->_view->data['prov_gestion'])>4){
							$this->_view->errorProv = $this->_error;
							$this->_view->_error = 'No debe seleccionar mas de 4 provincias';
							$this->_view->titulo = 'Administrador - Cargar Usuarios';
							$this->_view->renderizar('cargar', 'usuarios');
							exit;
						}
*/					}

					//echo "<pre>";print_r($_POST);exit;
					
					$_user = $this->usuariosGestion->registrarUsuario(
												validador::getTexto('usuario_nombre'), 
												validador::getTexto('usuario_usuario'), 
												validador::getPostParam('usuario_email'), 
												validador::getPostParam('usuario_rol'), 
												validador::getTexto('usuario_pass'), 
												1, 
												$this->_conf['hash_key']
											);
					//$_super = (validador::getPostParam('usuario_rol')!=1) ? implode(',', $this->_view->data['super_gestion']) : '';
					/*$_prov = (validador::getPostParam('usuario_rol')!=1) ? implode(',', $this->_view->data['prov_gestion']) : '';
					
					$this->usuariosGestion->registrarUsuarioGestion(
												$_user, 
												validador::getPostParam('usuario_rol'),
												//$_super, 
												$_prov
											);*/
					$this->redireccionar('administrador/usuarios/listar');	
				}	

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		
        $this->_view->titulo = 'Administrador - Cargar Usuarios';
        $this->_view->renderizar('cargar', 'usuarios');
	}
	
	public function editar($_id)
    {
		$this->_acl->acceso('admin_access');

		// $this->_view->setCss(array('style_contenidos','fastselect'));
		// $this->_view->setJs(array('fastsearch.min','fastselect.min'));

		validador::validarParametroInt($_id,$this->_conf['base_url']);
		
		$this->_view->usuario = $this->usuariosGestion->traerUsuario($_id);
		$this->_view->roles = $this->usuariosGestion->traerRolesEditar($this->_view->usuario->role);
		// $this->_view->provincias = $this->_trabajosGestion->traerProvincias();
		
		/*if($this->_view->usuario->role!=1){		
			$this->_view->usuarios_gestion = $this->usuariosGestion->traerUsuarioGestion($_id);
			//$this->_view->supermercados = $this->_trabajosGestion->traerSupermercados();			
			//$this->_view->super_select = explode(',',$this->_view->usuarios_gestion->supermercados);
			if($this->_view->usuarios_gestion->provincias==''){
				$this->_view->prov_select = array();
			}else{
				$this->_view->prov_select = explode(',',$this->_view->usuarios_gestion->provincias);
			}		
		}else{
			$this->_view->prov_select = array();
		}*/
		
		//echo "<pre>";print_r($this->_view->provincias);exit;
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		

				if($_POST['envio01'] == 1){
					//echo "<pre>";print_r($_POST);exit;
					$this->_view->data = $_POST;
					if(!validador::getTexto('usuario_nombre')){
						$this->_view->errorNombre = $this->_error;
						$this->_view->_error = 'Debe colocar un Nombre';
						$this->_view->titulo = 'Administrador - Editar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}
					
					if(!validador::getTexto('usuario_usuario')){
						$this->_view->errorUsuario = $this->_error;
						$this->_view->_error = 'Debe colocar un Usuario';
						$this->_view->titulo = 'Administrador - Editar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}
					
					if(!validador::getTexto('usuario_email')){
						$this->_view->errorEmail = $this->_error;
						$this->_view->_error = 'Debe colocar un Email';
						$this->_view->titulo = 'Administrador - Editar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}
					
					if(!validador::validarEmail(validador::getPostParam('usuario_email'))){
						$this->_view->errorEmail = $this->_error;
						$this->_view->_error = 'Debe colocar un Email V&aacute;lido';
						$this->_view->titulo = 'Administrador - Editar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}
					
					if(validador::getPostParam('usuario_email') != $this->_view->usuario->email){
						if($this->usuariosGestion->verificarEmail(validador::getPostParam('usuario_email'))){
							$this->_view->_error = 'Este email ya esta registrado';
							$this->_view->titulo = 'Administrador - Editar Usuarios';
							$this->_view->renderizar('editar', 'usuarios');
							exit;
						}
					}
					
					if(validador::getTexto('usuario_pass')){
						
						if(!validador::getTexto('usuario_repetir_pass')){
							$this->_view->errorPassRepetir = $this->_error;
							$this->_view->_error = 'Debe repetir el Password';
							$this->_view->titulo = 'Administrador - Editar Usuarios';
							$this->_view->renderizar('editar', 'usuarios');
							exit;
						}
						
						if(validador::getTexto('usuario_pass') != validador::getTexto('usuario_repetir_pass')){
							$this->_view->_error = 'Los Password no coinciden';
							$this->_view->titulo = 'Administrador - Cargar Usuarios';
							$this->_view->renderizar('editar', 'usuarios');
							exit;
						}
						$_nuevo_pass = validador::getTexto('usuario_pass');
					}else{
						$_nuevo_pass = '';	
					}
					if(validador::getPostParam('usuario_rol')!=1){
						/*if(!$this->_view->data['super_gestion']){
							$this->_view->errorSuper = $this->_error;
							$this->_view->_error = 'Debe seleccionar un supermecardos';
							$this->_view->titulo = 'Administrador - Cargar Usuarios';
							$this->_view->renderizar('editar', 'usuarios');
							exit;
						}*/
						/*if(count($this->_view->data['super_gestion'])>4){
							$this->_view->errorSuper = $this->_error;
							$this->_view->_error = 'No debe seleccionar mas de 4 supermecardos';
							$this->_view->titulo = 'Administrador - Cargar Usuarios';
							$this->_view->renderizar('editar', 'usuarios');
							exit;
						}*/
						/*if(!$this->_view->data['prov_gestion']){
							$this->_view->errorProv = $this->_error;
							$this->_view->_error = 'Debe seleccionar una provincia';
							$this->_view->titulo = 'Administrador - Cargar Usuarios';
							$this->_view->renderizar('editar', 'usuarios');
							exit;
						}
						if(count($this->_view->data['prov_gestion'])>4){
							$this->_view->errorProv = $this->_error;
							$this->_view->_error = 'No debe seleccionar mas de 4 provincias';
							$this->_view->titulo = 'Administrador - Cargar Usuarios';
							$this->_view->renderizar('editar', 'usuarios');
							exit;
						}*/
					}
					
					
					$_edit = $this->usuariosGestion->editarUsuario(
												$this->_view->usuario->id, 
												validador::getTexto('usuario_nombre'), 
												validador::getTexto('usuario_usuario'), 
												validador::getPostParam('usuario_email'), 
												validador::getPostParam('usuario_rol'), 
												$_nuevo_pass, 
												$this->_conf['hash_key']
											);
					//$_super = (validador::getPostParam('usuario_rol')!=1) ? implode(',', $this->_view->data['super_gestion']) : '';
					/*$_prov = (validador::getPostParam('usuario_rol')!=1) ? implode(',', $this->_view->data['prov_gestion']) : '';
					
					$_edit_gestion = $this->usuariosGestion->registrarUsuarioGestion(
												$this->_view->usuario->id, 
												validador::getPostParam('usuario_rol'),
												//$_super, 
												$_prov
											);*/
											
					$this->redireccionar('administrador/usuarios/listar');
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
        $this->_view->titulo = 'Administrador - Usuarios - Editar';
        $this->_view->renderizar('editar', 'usuarios');
    }


    ////////////////////////user home//////////////////////////////////////////////////////////////


    public function listarUserHome($pagina = false)
	{
		$this->_acl->acceso('admin_access');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();
		
		// Traemos los roles
		$this->_view->roles = $this->usuariosGestion->traerRoles();
		
		if($_POST){
			if($_POST['_filtrar'] == 1){
				$this->_filtro = validador::getPostParam('_roles');
			}
		}
		
		// Y los carhamos en el paginador
		$this->_view->usuarios =$this->usuariosGestion->TraerUsuarios($this->_filtro);
		//$this->_view->usuarios = $paginador->paginar($this->usuariosGestion->TraerUsuarios($this->_filtro), $pagina, 20);
		
		// Creamos la paginacion
		//$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/usuarios/listar');
        $this->_view->titulo = 'Administrador - Listar Usuarios';
        $this->_view->renderizar('listar_user_home', 'user_home');
	}
	
	public function cargarUserHome()
	{
		$this->_acl->acceso('admin_access');

		$this->_view->setCss(array('style_contenidos','fastselect'));
		$this->_view->setJs(array('fastsearch.min','fastselect.min'));
		
		// Traemos los roles
		$this->_view->roles = $this->usuariosGestion->traerRoles();
		//$this->_view->supermercados = $this->_trabajosGestion->traerSupermercados();
		$this->_view->provincias = $this->_trabajosGestion->traerProvincias();
		
		if($_POST){
			if($_POST['envio01'] == 1){
				$this->_view->data = $_POST;


				
				if(!validador::getTexto('usuario_nombre')){
					$this->_view->errorNombre = $this->_error;
					$this->_view->_error = 'Debe colocar un Nombre';
					$this->_view->titulo = 'Administrador - Cargar Usuarios';
					$this->_view->renderizar('cargar', 'usuarios');
					exit;
				}
				
				if(!validador::getTexto('usuario_usuario')){
					$this->_view->errorUsuario = $this->_error;
					$this->_view->_error = 'Debe colocar un Usuario';
					$this->_view->titulo = 'Administrador - Cargar Usuarios';
					$this->_view->renderizar('cargar', 'usuarios');
					exit;
				}
				
				if(!validador::getTexto('usuario_email')){
					$this->_view->errorEmail = $this->_error;
					$this->_view->_error = 'Debe colocar un Email';
					$this->_view->titulo = 'Administrador - Cargar Usuarios';
					$this->_view->renderizar('cargar', 'usuarios');
					exit;
				}
				
				if(!validador::validarEmail(validador::getPostParam('usuario_email'))){
					$this->_view->errorEmail = $this->_error;
					$this->_view->_error = 'Debe colocar un Email V&aacute;lido';
					$this->_view->titulo = 'Administrador - Cargar Usuarios';
					$this->_view->renderizar('cargar', 'usuarios');
					exit;
				}
				
				if($this->usuariosGestion->verificarEmail(validador::getPostParam('usuario_email'))){
					$this->_view->_error = 'Este email ya esta registrado';
					$this->_view->titulo = 'Administrador - Cargar Usuarios';
					$this->_view->renderizar('cargar', 'usuarios');
					exit;
				}
				
				if(!validador::getTexto('usuario_pass')){
					$this->_view->errorPass = $this->_error;
					$this->_view->_error = 'Debe colocar un Password';
					$this->_view->titulo = 'Administrador - Cargar Usuarios';
					$this->_view->renderizar('cargar', 'usuarios');
					exit;
				}
				
				if(!validador::getTexto('usuario_repetir_pass')){
					$this->_view->errorPassRepetir = $this->_error;
					$this->_view->_error = 'Debe repetir el Password';
					$this->_view->titulo = 'Administrador - Cargar Usuarios';
					$this->_view->renderizar('cargar', 'usuarios');
					exit;
				}
				
				if(validador::getTexto('usuario_pass') != validador::getTexto('usuario_repetir_pass')){
					$this->_view->_error = 'Los Password no coinciden';
					$this->_view->titulo = 'Administrador - Cargar Usuarios';
					$this->_view->renderizar('cargar', 'usuarios');
					exit;
				}
				if(validador::getPostParam('usuario_rol')!=1){
					/*if(!$this->_view->data['super_gestion']){
						$this->_view->errorSuper = $this->_error;
						$this->_view->_error = 'Debe seleccionar un supermecardos';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}*/
					/*if(count($this->_view->data['super_gestion'])>4){
						$this->_view->errorSuper = $this->_error;
						$this->_view->_error = 'No debe seleccionar mas de 4 supermecardos';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}*/
					if(!$this->_view->data['prov_gestion']){
						$this->_view->errorProv = $this->_error;
						$this->_view->_error = 'Debe seleccionar una provincia';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
					if(count($this->_view->data['prov_gestion'])>4){
						$this->_view->errorProv = $this->_error;
						$this->_view->_error = 'No debe seleccionar mas de 4 provincias';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('cargar', 'usuarios');
						exit;
					}
				}

				//echo "<pre>";print_r($_POST);exit;
				
				$_user = $this->usuariosGestion->registrarUsuario(
											validador::getTexto('usuario_nombre'), 
											validador::getTexto('usuario_usuario'), 
											validador::getPostParam('usuario_email'), 
											validador::getPostParam('usuario_rol'), 
											validador::getTexto('usuario_pass'), 
											1, 
											$this->_conf['hash_key']
										);
				//$_super = (validador::getPostParam('usuario_rol')!=1) ? implode(',', $this->_view->data['super_gestion']) : '';
				$_prov = (validador::getPostParam('usuario_rol')!=1) ? implode(',', $this->_view->data['prov_gestion']) : '';
				
				$this->usuariosGestion->registrarUsuarioGestion(
											$_user, 
											validador::getPostParam('usuario_rol'),
											//$_super, 
											$_prov
										);
				$this->redireccionar('administrador/usuarios/listarUserHome');	
			}	
		}
		
        $this->_view->titulo = 'Administrador - Cargar Usuarios';
        $this->_view->renderizar('cargar_user_home', 'user_home');
	}
	
	public function editarUserHome($_id)
    {
		$this->_acl->acceso('admin_access');

		$this->_view->setCss(array('style_contenidos','fastselect'));
		$this->_view->setJs(array('fastsearch.min','fastselect.min'));

		validador::validarParametroInt($_id,$this->_conf['base_url']);
		
		$this->_view->usuario = $this->usuariosGestion->traerUsuario($_id);
		$this->_view->roles = $this->usuariosGestion->traerRolesEditar($this->_view->usuario->role);
		$this->_view->provincias = $this->_trabajosGestion->traerProvincias();
		
		if($this->_view->usuario->role!=1){		
			$this->_view->usuarios_gestion = $this->usuariosGestion->traerUsuarioGestion($_id);
			//$this->_view->supermercados = $this->_trabajosGestion->traerSupermercados();			
			//$this->_view->super_select = explode(',',$this->_view->usuarios_gestion->supermercados);
			if($this->_view->usuarios_gestion->provincias==''){
				$this->_view->prov_select = array();
			}else{
				$this->_view->prov_select = explode(',',$this->_view->usuarios_gestion->provincias);
			}		
		}else{
			$this->_view->prov_select = array();
		}
		
		//echo "<pre>";print_r($this->_view->provincias);exit;
		
		if($_POST){
			if($_POST['envio01'] == 1){
				//echo "<pre>";print_r($_POST);exit;
				$this->_view->data = $_POST;
				if(!validador::getTexto('usuario_nombre')){
					$this->_view->errorNombre = $this->_error;
					$this->_view->_error = 'Debe colocar un Nombre';
					$this->_view->titulo = 'Administrador - Editar Usuarios';
					$this->_view->renderizar('editar', 'usuarios');
					exit;
				}
				
				if(!validador::getTexto('usuario_usuario')){
					$this->_view->errorUsuario = $this->_error;
					$this->_view->_error = 'Debe colocar un Usuario';
					$this->_view->titulo = 'Administrador - Editar Usuarios';
					$this->_view->renderizar('editar', 'usuarios');
					exit;
				}
				
				if(!validador::getTexto('usuario_email')){
					$this->_view->errorEmail = $this->_error;
					$this->_view->_error = 'Debe colocar un Email';
					$this->_view->titulo = 'Administrador - Editar Usuarios';
					$this->_view->renderizar('editar', 'usuarios');
					exit;
				}
				
				if(!validador::validarEmail(validador::getPostParam('usuario_email'))){
					$this->_view->errorEmail = $this->_error;
					$this->_view->_error = 'Debe colocar un Email V&aacute;lido';
					$this->_view->titulo = 'Administrador - Editar Usuarios';
					$this->_view->renderizar('editar', 'usuarios');
					exit;
				}
				
				if(validador::getPostParam('usuario_email') != $this->_view->usuario->email){
					if($this->usuariosGestion->verificarEmail(validador::getPostParam('usuario_email'))){
						$this->_view->_error = 'Este email ya esta registrado';
						$this->_view->titulo = 'Administrador - Editar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}
				}
				
				if(validador::getTexto('usuario_pass')){
					
					if(!validador::getTexto('usuario_repetir_pass')){
						$this->_view->errorPassRepetir = $this->_error;
						$this->_view->_error = 'Debe repetir el Password';
						$this->_view->titulo = 'Administrador - Editar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}
					
					if(validador::getTexto('usuario_pass') != validador::getTexto('usuario_repetir_pass')){
						$this->_view->_error = 'Los Password no coinciden';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}
					$_nuevo_pass = validador::getTexto('usuario_pass');
				}else{
					$_nuevo_pass = '';	
				}
				if(validador::getPostParam('usuario_rol')!=1){
					/*if(!$this->_view->data['super_gestion']){
						$this->_view->errorSuper = $this->_error;
						$this->_view->_error = 'Debe seleccionar un supermecardos';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}*/
					/*if(count($this->_view->data['super_gestion'])>4){
						$this->_view->errorSuper = $this->_error;
						$this->_view->_error = 'No debe seleccionar mas de 4 supermecardos';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}*/
					if(!$this->_view->data['prov_gestion']){
						$this->_view->errorProv = $this->_error;
						$this->_view->_error = 'Debe seleccionar una provincia';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}
					if(count($this->_view->data['prov_gestion'])>4){
						$this->_view->errorProv = $this->_error;
						$this->_view->_error = 'No debe seleccionar mas de 4 provincias';
						$this->_view->titulo = 'Administrador - Cargar Usuarios';
						$this->_view->renderizar('editar', 'usuarios');
						exit;
					}
				}
				
				
				$_edit = $this->usuariosGestion->editarUsuario(
											$this->_view->usuario->id, 
											validador::getTexto('usuario_nombre'), 
											validador::getTexto('usuario_usuario'), 
											validador::getPostParam('usuario_email'), 
											validador::getPostParam('usuario_rol'), 
											$_nuevo_pass, 
											$this->_conf['hash_key']
										);
				//$_super = (validador::getPostParam('usuario_rol')!=1) ? implode(',', $this->_view->data['super_gestion']) : '';
				$_prov = (validador::getPostParam('usuario_rol')!=1) ? implode(',', $this->_view->data['prov_gestion']) : '';
				
				$_edit_gestion = $this->usuariosGestion->registrarUsuarioGestion(
											$this->_view->usuario->id, 
											validador::getPostParam('usuario_rol'),
											//$_super, 
											$_prov
										);
				$this->redireccionar('administrador/usuarios/listarUserHome');
			}
		}
        $this->_view->titulo = 'Administrador - Usuarios - Editar';
        $this->_view->renderizar('editar_user_home', 'user_home');
    }



	
	public function borrar($_id)
	{
		$this->_acl->acceso('admin_access');
		validador::validarParametroInt($_id,$this->_conf['base_url']);

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
				
				$_borrar = $this->usuariosGestion->borrarUsuario($_id);;
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		
		/*$this->usuariosGestion->borrarUsuario($_id);		
		$this->redireccionar('administrador/usuarios/listar');*/

		
	}
	
	
	
	
}
