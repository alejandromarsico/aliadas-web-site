<?php

use controllers\administradorController\administradorController;

class reporteController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		
		$this->getLibrary('class.validador');
		
		$this->getLibrary('class.admin');		
		$this->_trabajosGestion = new admin();
		
		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		
		
    }
    
    
	
	public function index($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		/*$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();*/

		// $this->_sess->destroy('carga_actual');
		// $this->_sess->destroy('edicion_actual');

		// $this->_view->setCss(array('sweetalert'));
  //       $this->_view->setJs(array('sweetalert.min'));
		
		// $this->_view->datos = $this->_trabajosGestion->traerJuegos();
		
		/*$this->_view->beneficios = $paginador->paginar($beneficios, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/beneficios/listarAlta');*/
		
		// echo "<pre>";print_r($this->_view->datos);echo "</pre>";exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'reporte');	
    }
	
	public function listarChoice($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		/*$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();*/

		// $this->_sess->destroy('carga_actual');
		// $this->_sess->destroy('edicion_actual');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		 $this->_view->datos = $this->_trabajosGestion->traerChoices();
		
		/*$this->_view->beneficios = $paginador->paginar($beneficios, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/beneficios/listarAlta');*/
		
		// echo "<pre>";print_r($this->_view->datos);echo "</pre>";exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listarchoice', 'reporte');	
    }
	
	

	
	
	
	public function resultadosChoice($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
			
		
		// $this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		// $this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited'));

		$this->_view->id = $_id;
		$this->_view->trabajo = $this->_trabajosGestion->traerChoice($_id);
		$preg = base64_decode($this->_view->trabajo['preguntas']);
	    $this->_view->trabajo['_preg'] = unserialize($preg);
	    // $resp = base64_decode($this->_view->trabajo['respuestas']);
	    // $this->_view->trabajo['_resp']  = unserialize($resp);
	    // $_resp_correctas = base64_decode($this->_view->trabajo['respuesta_correcta']);
	    // $this->_view->trabajo['_resp_correctas'] = unserialize($_resp_correctas);
	    $puntos = base64_decode($this->_view->trabajo['puntos']);
	    $this->_view->trabajo['_puntos'] = unserialize($puntos);
		$this->_view->trabajo['_puntaje_total'] = array_sum($this->_view->trabajo['_puntos']);
		$this->_view->trabajo['_cantidad_preg'] =count($this->_view->trabajo['_preg']);
		$this->_view->trabajo['_puntaje_por_preg'] = $this->_view->trabajo['_puntaje_total'] / $this->_view->trabajo['_cantidad_preg'];
		
		// $this->_sess->set('edicion_actual', $this->_view->trabajo['identificador']);
		
		$this->_view->users = $this->_trabajosGestion->traerUsersPorJuego($this->_view->trabajo['id']);
		if($this->_view->users){


			for ($i=0; $i < count($this->_view->users); $i++) { 
				$_user = array();
				$_user = $this->_trabajosGestion->traerUser($this->_view->users[$i]['id_user']);
				$this->_view->users[$i]['_user'] = $_user['nombre']; 

				$_farmacia = array();
				$_farmacia = $this->_trabajosGestion->traerCliente($_user['id_cliente']);
				$this->_view->users[$i]['_farmacia'] = $_farmacia['razon_social']; 

				$this->_view->users[$i]['_preg_bien_contestadas'] = $this->_view->users[$i]['puntos'] / $this->_view->trabajo['_puntaje_por_preg'];
				$this->_view->users[$i]['_nota_final'] = round($this->_view->users[$i]['_preg_bien_contestadas'] / $this->_view->trabajo['_cantidad_preg'],2) * 10;
			}
		
			$this->_view->users = admin::array_msort($this->_view->users, array('_nota_final'=>SORT_DESC));
		}

		// echo "<pre>";print_r($this->_view->trabajo);echo "</pre>";	
		// echo "<pre>";print_r($this->_view->users);echo "</pre>";exit;	
			
	
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('resultadosChoice', 'reporte');	
    }

    public function exportaDatos($_id)
    {

    	$this->_acl->acceso('encargado_access');

    	$this->_view->trabajo = $this->_trabajosGestion->traerChoice($_id);
		$preg = base64_decode($this->_view->trabajo['preguntas']);
	    $this->_view->trabajo['_preg'] = unserialize($preg);
	    $puntos = base64_decode($this->_view->trabajo['puntos']);
	    $this->_view->trabajo['_puntos'] = unserialize($puntos);
		$this->_view->trabajo['_puntaje_total'] = array_sum($this->_view->trabajo['_puntos']);
		$this->_view->trabajo['_cantidad_preg'] =count($this->_view->trabajo['_preg']);
		$this->_view->trabajo['_puntaje_por_preg'] = $this->_view->trabajo['_puntaje_total'] / $this->_view->trabajo['_cantidad_preg'];
		
		$this->_view->users = $this->_trabajosGestion->traerUsersPorJuego($this->_view->trabajo['id']);
		if($this->_view->users){
			
			for ($i=0; $i < count($this->_view->users); $i++) { 
				$_user = array();
				$_user = $this->_trabajosGestion->traerUser($this->_view->users[$i]['id_user']);
				$this->_view->users[$i]['_user'] = $_user['nombre']; 

				$_farmacia = array();
				$_farmacia = $this->_trabajosGestion->traerCliente($_user['id_cliente']);
				$this->_view->users[$i]['_farmacia'] = $_farmacia['razon_social']; 

				$this->_view->users[$i]['_preg_bien_contestadas'] = $this->_view->users[$i]['puntos'] / $this->_view->trabajo['_puntaje_por_preg'];
				$this->_view->users[$i]['_nota_final'] = round($this->_view->users[$i]['_preg_bien_contestadas'] / $this->_view->trabajo['_cantidad_preg'],2) * 10;
			}

			$this->_view->users = admin::array_msort($this->_view->users, array('_nota_final'=>SORT_DESC));
			// sort ($this->_view->users);
			
			// echo "<pre>";print_r($this->_view->users);echo "</pre>";exit;
				
			
			$this->_view->filename = 'reporte_'.admin::crearTitulo($this->_view->trabajo['titulo']).'_'.date('dmY').'.xls';	

			$this->_view->html ='<table border="1" cellpadding="10" cellspacing="0" bordercolor="#666666" style="border-collapse:collapse;">
			<tbody>
			<tr>
			<th colspan="5">
			<h2>
				'.admin::convertirCaracteres($this->_view->trabajo['titulo']).'<br><small>Preguntas totales:'.$this->_view->trabajo['_cantidad_preg'].'</small><br><small>Puntaje por pregunta:'.$this->_view->trabajo['_puntaje_por_preg'].'</small>
			</h2>
			</th>
			</tr>		
			<tr style="font-weight: bold;">
			<td>Usuario</td>
			<td>Email</td>
			<td>Farmacia</td>	
			<td>Preguntas bien contestadas</td>	
			<td>Nota final</td>				
			</tr>';


			/*for ($x=0; $x < count($this->_view->users); $x++) { 

				$this->_view->html .='<tr>';
				$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($this->_view->users[$x]['_user']).'</td>';

				$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($this->_view->users[$x]['_farmacia']).'</td>';

				$this->_view->html .='<td valign="top">'.$this->_view->users[$x]['_preg_bien_contestadas'].'</td>';
				$this->_view->html .='<td valign="top">'.$this->_view->users[$x]['_nota_final'].'</td>';

				$this->_view->html .='</tr>';
			}*/

			foreach ($this->_view->users as $val) {
				$this->_view->html .='<tr>';
				$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($val['_user']).'</td>';
				$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($val['email']).'</td>';
				$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($val['_farmacia']).'</td>';

				$this->_view->html .='<td valign="top">'.$val['_preg_bien_contestadas'].'</td>';
				$this->_view->html .='<td valign="top">'.$val['_nota_final'].'</td>';

				$this->_view->html .='</tr>';
			}

			// echo "<pre>";print_r($this->_view->users);echo "</pre>";
			// echo $this->_view->html;exit;


			/*for ($i=0; $i < count($this->_view->users); $i++) { 

				$this->_view->html .='<tr>';

				$_user = array();
				$_user = $this->_trabajosGestion->traerUser($this->_view->users[$i]['id_user']);
				// $this->_view->users[$i]['_user'] = $_user['nombre']; 			
				$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($_user['nombre']).'</td>';

				$_farmacia = array();
				$_farmacia = $this->_trabajosGestion->traerCliente($_user['id_cliente']);
				// $this->_view->users[$i]['_farmacia'] = $_farmacia['razon_social']; 
				$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($_farmacia['razon_social']).'</td>';

				$this->_view->users[$i]['_preg_bien_contestadas'] = $this->_view->users[$i]['puntos'] / $this->_view->trabajo['_puntaje_por_preg'];
				$this->_view->html .='<td valign="top">'.$this->_view->users[$i]['_preg_bien_contestadas'].'</td>';
				$this->_view->users[$i]['_nota_final'] = round($this->_view->users[$i]['_preg_bien_contestadas'] / $this->_view->trabajo['_cantidad_preg'],2);
				$this->_view->html .='<td valign="top">'.$this->_view->users[$i]['_nota_final'].'</td>';

				$this->_view->html .='</tr>';
			}*/

			
		
			
			$this->_view->html .='</tbody>
			</table>';
		}

		$this->_view->titulo = 'Administrador - Seguimiento';
		$this->_view->renderizar('exportar_datos', 'reporte','vacio');



    }






	
	
	
	
	
	public function cargarChoice($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		// $this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		// $this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited'));

		// $this->_view->categorias = $this->_trabajosGestion->traerCategorias();
		// $this->_view->tags = $this->_trabajosGestion->traerTags();

		 // echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		// echo "<pre>";print_r($this->_view->tags);echo "</pre>";exit;

		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					// echo "<pre>";print_r($_POST);exit;

					/*if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'capacitaciones');
						exit;
					} 

					if(!validador::getInt('bajada')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'tendencias');
						exit;
					} */
					
						
					/*
					$_fechaBd = date('Y-m-d');

					if($this->_view->data['tags']!=''){
						$_tags = explode(',', $this->_view->data['tags']);
						foreach ($_tags as $val) {
							$_hay = contenidos_tag::find(array('conditions' => array('nombre = ?', $val)));
							if(!$_hay){
								$tag = new contenidos_tag();
								$tag->nombre = $val;						
								$tag->save();
							}
							
						}
						
					}*/


					$_enc_preg = serialize($_POST['_preg']);
                    $_enc_preg = base64_encode($_enc_preg);

                    $_enc_resp = serialize($_POST['_resp']);
                    $_enc_resp = base64_encode($_enc_resp);

                    $_resp_correctas = serialize($_POST['_resp_correcta']);
                    $_resp_correctas = base64_encode($_resp_correctas);

                    $_puntos = serialize($_POST['_puntos']);
                    $_puntos = base64_encode($_puntos);


					
					$cat = new contenidos_multiple_choice();
					$cat->id_tipo_juego = $this->_view->data['_tipo_juego'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->preguntas = $_enc_preg;
					$cat->respuestas = $_enc_resp;
					$cat->respuesta_correcta = $_resp_correctas;
					$cat->puntos = $_puntos;
					$cat->identificador = $this->_sess->get('carga_actual');
					$cat->estado = 'alta';
					$cat->fecha = date('Y-m-d');
					$cat->save();

					$val = new contenidos_juego();
					$val->id_juego = $cat->id;
					$val->id_tipo_juego = $this->_view->data['_tipo_juego'];
					$val->tabla_juegos = 'contenidos_multiple_choices';					
					$val->fecha = date('Y-m-d');
					$val->save();	

								
					
					$this->_sess->destroy('carga_actual');
					$this->redireccionar('administrador/juegos/listarChoice');
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargarchoice', 'juegos');	
    }
	
	
	public function borrarChoice()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		

				$_borrar = $this->_trabajosGestion->borrarChoice($_id);
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}
	
	
	
	
}