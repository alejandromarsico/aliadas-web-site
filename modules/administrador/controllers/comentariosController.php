<?php

use controllers\administradorController\administradorController;

class comentariosController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		
		$this->getLibrary('class.validador');
		
		$this->getLibrary('class.admin');				
		$this->_trabajosGestion = new admin();
		
		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		
		
    }
    
    public function index()
    {	
		
		$this->redireccionar('administrador/comentarios/listar');	
    }
	
	public function listar($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		$this->_view->setCss(array('sweetalert','styles'));
        $this->_view->setJs(array('sweetalert.min'));
		
		 $datos = $this->_trabajosGestion->traerComentarios();

		 // echo "<pre>";print_r($datos);echo "</pre>";exit;
		
		$this->_view->datos = $paginador->paginar($datos, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/comentarios/listar');
		
		 
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'comentarios');	
    }
	
	
	
	
	public function borrar()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];		

				$_borrar = $this->_trabajosGestion->borrarComentario($_id);
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}
	
	
	public function buscadorNotas()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['key'];
				$_prod ='';
				$_prod  = $this->_trabajosGestion->traerBuscadorNotas($_val);		

				 // echo "<pre>";print_r($_prod);exit;		
			
				
				$_html = '';
				foreach($_prod as $prod){ 
					
	        		$_html .= '<div><a class="suggest-element" data="'.admin::convertirCaracteres($prod['titulo']).'" id="'.$prod['id'].'">'.admin::convertirCaracteres($prod['titulo']).'</a></div>';
	        
				}			
				
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}

	public function buscadorNotasFinal()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerBuscadorComentariosNotas($_val);				
			
				
				$_html = '<ul id="lista" class="list-group listar">';
				foreach($this->_view->prod as $prod){ 
					
	        		$_html .='<li id="elemento-'. $prod['id'].'" class="list-group-item">
		        				<p>'.admin::convertirCaracteres($prod['comentario']);
		        	$_html .='<small>Usuario: <b>'.$prod['usuario'].'</b></small>';
		        				$_padre = ($prod['id_comentario_padre']!=0) ? admin::traerComentario($prod['id_comentario_padre']) : '';
				                if($_padre!=''){
								$_html .='<small>Comentario padre: <b>'.$_padre['comentario'].'</b></small>';
				                }				                 
		        	$_html .='<small>Seccion: <b>'.$prod['seccion'].'</b></small>';
		        				$_nota = admin::traerNota($prod['id_nota'], $prod['seccion']);
				                if($_nota!=''){                  
				                $_html .='<small>Nota: <b>'.$_nota['titulo'].'</b></small>';  
				                }  
		        	$_html .='</p>
								<span>	
									<a href="javascript:void(0);" class="_borrar_'.$prod['id'].'" title="Borrar">
					                    <img src="'.$this->_conf['base_url'].'public/img/ico-cruz.png" alt="eliminar"/>
					                </a>									
								</span>	
								<script>
				                  $(document).ready(function () {
				                        $("._borrar_'.$prod['id'].'").click(function () {
				                            swal({
				                                title: "Estas seguro de borrar este contenido?",
				                                text: "Los datos se perderán permanentemente!",
				                                type: "warning",
				                                showCancelButton: true,
				                                confirmButtonColor: "#DD6B55",
				                                confirmButtonText: "Si, que se borre!",
				                                cancelButtonText: "No, mejor no!",
				                                closeOnConfirm: false,
				                                closeOnCancel: false },
				                            function (isConfirm) {
				                                if (isConfirm) {
				                                    var url= _root_ + "administrador/comentarios/borrar";
				                                    var dataString = "_id='.$prod['id'].'>&_csrf='.$this->_sess->get('_csrf').'";
				                                    $.ajax({
				                                            type: "POST",
				                                            url: url,
				                                            data: dataString,
				                                            success: function(data){
				                                              if(data=="ok"){
				                                                swal("Borrado!", "El contenido se borró con exito.", "success");
				                                                setTimeout(function() {
				                                                    location.reload();
				                                                }, 200);
				                                              }else{
				                                                swal("Cancelado", "No se puede borrar", "error");
				                                              }
				                                                
				                                                
				                                            }
				                                    });
				                                } else {
				                                    swal("Cancelado", "El contenido esta guardado", "error");
				                                }
				                            });
				                        });
				                    });
				                </script>						
							</li>';
	        
				}			
				$_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}

	public function buscador()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerBuscadorComentarios(ucwords(strtolower($_val)));				
			
				
				$_html = '';
				foreach($this->_view->prod as $prod){


					$_html .='<div class="forum-item">
	        					<div class="row">
	        						<div class="col-md-10">
										<a href="" class="forum-item-title">
											Comentario: <b>'.admin::convertirCaracteres($prod['comentario']).'</b>
										</a>';
							$_html .='<small>Usuario: <b>'.$prod['usuario'].'</b></small>';
				        				$_padre = ($prod['id_comentario_padre']!=0) ? admin::traerComentario($prod['id_comentario_padre']) : '';
						                if($_padre!=''){
										$_html .='<small>Comentario padre: <b>'.$_padre['comentario'].'</b></small>';
						                }				                 
				        	$_html .='<small>Seccion: <b>'.$prod['seccion'].'</b></small>';
				        				$_nota = admin::traerNota($prod['id_nota'], $prod['seccion']);
						                if($_nota!=''){                  
						                $_html .='<small>Nota: <b>'.$_nota['titulo'].'</b></small>';  
						                }  
				        	$_html .='</div>
									<div class="col-md-2 forum-info">
						                <div class="tooltip-demo pull-right">   
											<a href="javascript:void(0);" class="btn btn-danger btn-round _borrar_'.$prod['id'].'" title="Borrar">
					                            Eliminar
					                        </a>&nbsp;&nbsp;
										</div>
					                </div>
					            </div>
					        </div>
					        <script>
			                  $(document).ready(function () {
			                        $("._borrar_'.$prod['id'].'").click(function () {
			                            swal({
			                                title: "Estas seguro de borrar este contenido?",
			                                text: "Los datos se perderán permanentemente!",
			                                type: "warning",
			                                showCancelButton: true,
			                                confirmButtonColor: "#DD6B55",
			                                confirmButtonText: "Si, que se borre!",
			                                cancelButtonText: "No, mejor no!",
			                                closeOnConfirm: false,
			                                closeOnCancel: false },
			                            function (isConfirm) {
			                                if (isConfirm) {
			                                    var url= _root_ + "administrador/comentarios/borrar";
			                                    var dataString = "_id='.$prod['id'].'>&_csrf='.$this->_sess->get('_csrf').'";
			                                    $.ajax({
			                                            type: "POST",
			                                            url: url,
			                                            data: dataString,
			                                            success: function(data){
			                                              if(data=="ok"){
			                                                swal("Borrado!", "El contenido se borró con exito.", "success");
			                                                setTimeout(function() {
			                                                    location.reload();
			                                                }, 200);
			                                              }else{
			                                                swal("Cancelado", "No se puede borrar", "error");
			                                              }
			                                                
			                                                
			                                            }
			                                    });
			                                } else {
			                                    swal("Cancelado", "El contenido esta guardado", "error");
			                                }
			                            });
			                        });
			                    });
			                </script>';

					
	        		/*$_html .='<li id="elemento-'. $prod['id'].'" class="list-group-item">
		        				<p>'.admin::convertirCaracteres($prod['comentario']);
		        	$_html .='<small>Usuario: <b>'.$prod['usuario'].'</b></small>';
		        				$_padre = ($prod['id_comentario_padre']!=0) ? admin::traerComentario($prod['id_comentario_padre']) : '';
				                if($_padre!=''){
								$_html .='<small>Comentario padre: <b>'.$_padre['comentario'].'</b></small>';
				                }				                 
		        	$_html .='<small>Seccion: <b>'.$prod['seccion'].'</b></small>';
		        				$_nota = admin::traerNota($prod['id_nota'], $prod['seccion']);
				                if($_nota!=''){                  
				                $_html .='<small>Nota: <b>'.$_nota['titulo'].'</b></small>';  
				                }  
		        	$_html .='</p>
								<span>	
									<a href="javascript:void(0);" class="_borrar_'.$prod['id'].'" title="Borrar">
					                    <img src="'.$this->_conf['base_url'].'public/img/ico-cruz.png" alt="eliminar"/>
					                </a>									
								</span>	
								<script>
				                  $(document).ready(function () {
				                        $("._borrar_'.$prod['id'].'").click(function () {
				                            swal({
				                                title: "Estas seguro de borrar este contenido?",
				                                text: "Los datos se perderán permanentemente!",
				                                type: "warning",
				                                showCancelButton: true,
				                                confirmButtonColor: "#DD6B55",
				                                confirmButtonText: "Si, que se borre!",
				                                cancelButtonText: "No, mejor no!",
				                                closeOnConfirm: false,
				                                closeOnCancel: false },
				                            function (isConfirm) {
				                                if (isConfirm) {
				                                    var url= _root_ + "administrador/comentarios/borrar";
				                                    var dataString = "_id='.$prod['id'].'>&_csrf='.$this->_sess->get('_csrf').'";
				                                    $.ajax({
				                                            type: "POST",
				                                            url: url,
				                                            data: dataString,
				                                            success: function(data){
				                                              if(data=="ok"){
				                                                swal("Borrado!", "El contenido se borró con exito.", "success");
				                                                setTimeout(function() {
				                                                    location.reload();
				                                                }, 200);
				                                              }else{
				                                                swal("Cancelado", "No se puede borrar", "error");
				                                              }
				                                                
				                                                
				                                            }
				                                    });
				                                } else {
				                                    swal("Cancelado", "El contenido esta guardado", "error");
				                                }
				                            });
				                        });
				                    });
				                </script>						
							</li>';*/
	        
				}			
				// $_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}


	
	public function editar($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
			
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));

		$this->_view->trabajo = $this->_trabajosGestion->traerBanner($_id);
		$this->_view->ids_secciones = explode(',', $this->_view->trabajo['ids_seccion']);
		$this->_view->ids_posicion = explode(',', $this->_view->trabajo['ids_posicion']);
		$this->_view->id_formato = $this->_view->trabajo['id_formato'];
		$this->_view->formato_img = $this->_trabajosGestion->traerFormatoPorId($this->_view->trabajo['id_formato']);
		
		$this->_view->secciones = $this->_trabajosGestion->traerSecciones();
		$this->_view->posiciones = $this->_trabajosGestion->traerPosiciones($this->_view->ids_secciones);	
		$this->_view->formatos = $this->_trabajosGestion->traerFormatos($this->_view->ids_secciones, $this->_view->ids_posicion);	
		
		
		
				
		$this->_sess->set('edicion_actual', $this->_view->trabajo['identificador']);
		
		 // echo "<pre>";print_r($this->_view->cat_ids);exit;		
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					// echo "<pre>";print_r($this->_view->data);exit;

					/*if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'capacitaciones');
						exit;
					} 

					if(!validador::getInt('codigo')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					}*/
					
					
				
					
					$_sec = implode(',', $_POST['secciones']);
					$_pos = implode(',', $_POST['posiciones']);


					$cat = contenidos_banner::find($this->_view->trabajo['id']);
					$cat->ids_seccion = $_sec;
					$cat->ids_posicion = $_pos;
					$cat->id_formato = $this->_view->data['formatos'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					//$cat->identificador = $this->_sess->get('carga_actual');
					//$cat->fecha = date('Y-m-d');
					$cat->save();			
					
								
					

					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('img_id');					
					$this->redireccionar('administrador/banners');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editar', 'banners');	
    }
	
	
	public function cargar($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));

		$this->_view->secciones = $this->_trabajosGestion->traerSecciones();
		// $this->_view->posiciones = $this->_trabajosGestion->traerPosicionesTodas();

		 // echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		// echo "<pre>";print_r($this->_view->tags);echo "</pre>";exit;

		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					// echo "<pre>";print_r($_POST);exit;

					$_sec = implode(',', $_POST['secciones']);
					$_pos = implode(',', $_POST['posiciones']);


					$cat = new contenidos_banner();
					$cat->ids_seccion = $_sec;
					$cat->ids_posicion = $_pos;
					$cat->id_formato = $this->_view->data['formatos'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->identificador = $this->_sess->get('carga_actual');
					$cat->fecha = date('Y-m-d');
					$cat->save();			
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('img_id');
					//$this->_sess->destroy('img_lote');
					$this->redireccionar('administrador/banners');
					
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'banners');	
    }
	
	


   
	
	
	
}