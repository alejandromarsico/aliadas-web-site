<?php

use controllers\administradorController\administradorController;

class clientesController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		$this->getLibrary('class.admin');		
		$this->_trabajosGestion = new admin();
		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		
		
    }
    
    public function index()
    {	
		
		$this->redireccionar('administrador/clientes/listar');	
    }
	
	public function listar($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		// $this->_view->setCss(array('sweetalert'));
       //  $this->_view->setJs(array('sweetalert.min'));
		
		 $datos = $this->_trabajosGestion->traerClientes();

		 // echo "<pre>";print_r($datos);echo "</pre>";exit;
		
		$this->_view->datos = $paginador->paginar($datos, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/clientes/listar');
		
		 
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'clientes');	
    }
	
	
	
	
	/*
	
	public function vista_previa_catalogos($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('basic'));
		$this->_view->setJs(array('modernizr.2.5.3.min', 'turn'));
		
		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);
		$this->_view->imagenes = $this->_trabajosGestion->traerGaleriaPorIdentificador($this->_view->trabajo->identificador, $this->_view->trabajo->imagenes_orientacion);
		$this->_view->size = getimagesize($this->_conf['base_url']. 'public/img/subidas/catalogos/cat_'.$this->_view->trabajo->identificador.'/'.$this->_view->imagenes[0]->path);
		
		//echo $this->_view->size;
		//echo "<pre>";print_r($this->_view->size);exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('vista_previa_catalogos', 'tendencias');	
    }
	*/
	
	
	
	public function editar($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
			
		
		// $this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('jquery.maskedinput'));

		$this->_view->trabajo = $this->_trabajosGestion->traerCliente($_id);
				
		// $this->_sess->set('edicion_actual', $this->_view->trabajo['identificador']);
		
		 // echo "<pre>";print_r($this->_view->cat_ids);exit;		
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					// echo "<pre>";print_r($this->_view->data);exit;

					/*if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'capacitaciones');
						exit;
					} 

					if(!validador::getInt('codigo')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					}*/
					
					
				
					
						
								
					
					$cat = contenidos_cliente::find($this->_view->trabajo['id']);
					$cat->numero_cliente = $this->_view->data['numero_cliente'];
					$cat->razon_social = $this->_xss->xss_clean(validador::getTexto('razon_social'));
					$cat->ciudad = $this->_xss->xss_clean(validador::getTexto('ciudad'));
					$cat->provincia = $this->_xss->xss_clean(validador::getTexto('provincia'));
					// $cat->email = $this->_view->data['email'];
					// $cat->password = $this->_view->data['password'];
					// $cat->nombre = $this->_xss->xss_clean(validador::getTexto('nombre'));
					// $cat->fecha_nacimiento = $this->_view->data['fecha_nacimiento'];					
					$cat->save();
								
					

					// $this->_sess->destroy('edicion_actual');					
					$this->redireccionar('administrador/clientes');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editar', 'clientes');	
    }
	
	
	
	
	
	public function cargar($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		$this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited'));

		// $this->_view->categorias = $this->_trabajosGestion->traerCategorias();
		// $this->_view->tags = $this->_trabajosGestion->traerTags();

		 // echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		// echo "<pre>";print_r($this->_view->tags);echo "</pre>";exit;

		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					// echo "<pre>";print_r($_POST);exit;



					//Leer excel
	                $_excel = contenidos_archivos_exce::find(array('conditions' => array('id = ?', 1)));
	                if($_excel){
	                	require_once dirname(__FILE__) . '/../../../libs/PHPExcel/IOFactory.php';				

						$objReader = PHPExcel_IOFactory::createReader('Excel2007');
						//$objReader->setReadDataOnly(true);
						$objPHPExcel = $objReader->load(dirname(__FILE__) ."/../../../public/files/".$_excel->path);
						$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);

	                    $_cont= array();
	                    for ($i=0; $i < count($sheetData); $i++) { 
	                        if(count(array_filter($sheetData[$i])) != 0){
	                            $_cont[] = $sheetData[$i];
	                        }
	                    }

	                    /*$_contenido = serialize($_cont);
	                    $_contenido = base64_encode($_contenido);*/

						

	                    // echo "<pre>";print_r($_cont);echo "</pre>";exit;

	                    

	                    for ($i=1; $i < count($_cont); $i++) { 

	                    	$_dat = contenidos_cliente::find(array('conditions' => array('numero_cliente = ?', $_cont[$i][0])));
	                    	if(!$_dat){
	                    		$cat = new contenidos_cliente();
								$cat->numero_cliente = $_cont[$i][0];
								$cat->razon_social = $_cont[$i][1];
								$cat->ciudad = $_cont[$i][2];
								$cat->provincia = $_cont[$i][3];				
								$cat->fecha = date('Y-m-d');
								$cat->save();
	                    	}

	                    	
	                    }


	                    $this->_sess->destroy('carga_actual');
						$this->redireccionar('administrador/clientes');

						
	                }else{
						$this->redireccionar('error/access/404');				
					}

					
							
								
					
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'clientes');	
    }


   	public function cargar_reporte()
    {

    	


    	require_once dirname(__FILE__) . '/../../../libs/PHPExcel/IOFactory.php';				

		$objReader = PHPExcel_IOFactory::createReader('Excel2007');
		$objPHPExcel = $objReader->load(dirname(__FILE__) ."/../../../public/files/carga_users.xlsx");
		$sheetData = $objPHPExcel->getActiveSheet()->toArray(null, true, true, false);

        $_cont= array();
        for ($i=0; $i < count($sheetData); $i++) { 
            if(count(array_filter($sheetData[$i])) != 0){
                $_cont[] = $sheetData[$i];
            }
        }

       // echo "<pre>";print_r($_cont);echo "</pre>";exit;

        /*for ($i=0; $i < count($_cont); $i++) { 
        	if($i!=0){
	        	// $_alg = parse_url($_cont[$i][6], PHP_URL_HOST);
	         	//if(!$this->_sess->get('carga_actual')){
				// 	$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
				// }
        		$_sec=array();	
        		$_cat = array();
				
        		$_dat = explode('|', $_cont[$i][5]);
        		foreach ($_dat as $val) {
        			$_data = explode('>', $val);
        			$_sec[] = $_data[0];
        			$_cat1 = array();
        			$_cat1 = $_data[1];
        			$_cate = explode(' ', $_cat1);
        			$_categ = array();
        			$_categ[] = $_cate[1];
        			foreach ($_categ as $dat) {
        				if($dat =='Maquillaje'){
        					$dat='Make up';
        				}
        				if($dat =='Electro'){
        					$dat='Electrobeauty';
        				}
        				$_post = admin::traerCategoriaPorNombre($dat);
        				$_cat[] = $_post->id;
        			}
        			// $_cat[] = $_cate[1];
        		}

				// $_sec = explode('>', $_cont[$i][5]);
				// $_cat = explode(' ', $_sec[1]);
				
        		$_sec = array_unique($_sec);
				$_cont[$i][] = $_sec;
				$_cont[$i][] = $_cat;

				// $_cont[$i][] = implode(',', $_cat);

				// $_cont[$i][] = $_sec[0];
				// $_cont[$i][] = $_cat;
				$_cont[$i][] = rand((int)1135687452,(int)999999999);
	        	// $_cont[$i][] = explode('|', $_cont[$i][3]);
	        	$_cont[$i][] = explode('|', $_cont[$i][4]);
	        	// $_cont[$i][] = explode('|', $_cont[$i][7]);
	        	$_cont[$i][] = explode('|', $_cont[$i][8]);
	        	$_cont[$i][] = date('Y-m-d', strtotime($_cont[$i][6]));

	        	sort($_cont[$i][10]);
	        	$_cont[$i][10] = implode(',', $_cont[$i][10]);
	        }
        }*/

        /*for ($i=0; $i < count($_cont); $i++) { 
        	if($i!=0){
	        	// $_alg = parse_url($_cont[$i][6], PHP_URL_HOST);
	         	//if(!$this->_sess->get('carga_actual')){
				// 	$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
				// }
        		// $_sec=array();	
        		$_cat = array();
				
        		$_dat = explode('|', $_cont[$i][5]);
        		foreach ($_dat as $val) {
        			// $_data = explode('>', $val);
        			// $_sec[] = $_data[0];
        			// $_cat1 = array();
        			// $_cat1 = $_data[1];
        			// $_cate = explode(' ', $_cat1);
        			// $_categ = array();
        			// $_categ[] = $_cate[1];
        			// foreach ($_categ as $dat) {
        				if($val =='Maquillajes'){
        					$val='Make up';
        				}
        				if($val =='Electro'){
        					$val='Electrobeauty';
        				}
        				$_post = admin::traerCategoriaPorNombre($val);
        				$_cat[] = $_post->id;
        			// }
        			// $_cat[] = $_cate[1];
        		}

				// $_sec = explode('>', $_cont[$i][5]);
				// $_cat = explode(' ', $_sec[1]);
				
         		//$_sec = array_unique($_sec);
				// $_cont[$i][] = $_sec;
				$_cont[$i][] = $_cat;

				// $_cont[$i][] = implode(',', $_cat);

				// $_cont[$i][] = $_sec[0];
				// $_cont[$i][] = $_cat;
				$_cont[$i][] = rand((int)1135687452,(int)999999999);
				$_cont[$i][] = pathinfo($_cont[$i][4]);
	        	// $_cont[$i][] = explode('|', $_cont[$i][3]);
	        	// $_cont[$i][] = explode('|', $_cont[$i][4]);
	        	// $_cont[$i][] = explode('|', $_cont[$i][7]);
	        	// $_cont[$i][] = explode('|', $_cont[$i][8]);
	        	// $_cont[$i][] = date('Y-m-d', strtotime($_cont[$i][6]));
				$_cont[$i][] = date('Y-m-d');
	        	sort($_cont[$i][7]);
	        	$_cont[$i][7] = implode(',', $_cont[$i][7]);
	        }
        }*/

      	

       	 // echo "<pre>";print_r($_cont);echo "</pre>";exit;
        // exit;
        


       /* for ($i=1; $i < count($_cont); $i++) { 

        	if($_cont[$i][9][0]=='Tendencias'){

        		$cat = new contenidos_tendencia();
				$cat->tipo = 'tendencias';
				$cat->categorias = $_cont[$i][10];
				$cat->titulo = $this->_xss->xss_clean(ucwords(strtolower($_cont[$i][1])));
				// $cat->bajada = $this->_xss->xss_clean(validador::getTexto('bajada'));
				$cat->descripcion = $this->_xss->xss_clean($_cont[$i][2]);
				// $cat->video = $this->_view->data['video'];
				// $cat->tags = $this->_view->data['tags'];
				$cat->notas_viejas = 'si';
				$cat->identificador = $_cont[$i][11];
				$cat->estado = 'alta';					
				$cat->fecha = $_cont[$i][14];
				$cat->save();

				// cargar img
				if($_cont[$i][4]!=''){

					foreach ($_cont[$i][12] as $val) {
						$img = new contenidos_imagene();
						$img->nombre = $val;
						$img->identificador = $_cont[$i][11];
						$img->path = $val;
						$img->posicion = 'tendencias';
						$img->orden = 0;
						$img->fecha_alt = $_cont[$i][14];
						$img->save();
						
					}
				}

				// cargar archivos
				if($_cont[$i][8]!=''){

					foreach ($_cont[$i][13] as $val) {
						$algo = pathinfo($val);   

						$arc = new contenidos_archivo();
						$arc->nombre = $algo['filename'];
						$arc->identificador = $_cont[$i][11];
						$arc->path = $val;
						$arc->formato = $algo['extension'];
						$arc->orden = 0;
						$arc->fecha_alt = $_cont[$i][14];
						$arc->save();
						
					}
				}


        	}else if($_cont[$i][9][0]=='Capacitaciones'){

        		$cat = new contenidos_capacitacione();
				$cat->destacado = 'no';
				$cat->categorias = $_cont[$i][10];
				$cat->titulo = $this->_xss->xss_clean(ucwords(strtolower($_cont[$i][1])));
				// $cat->bajada = $this->_xss->xss_clean(validador::getTexto('bajada'));
				$cat->descripcion = $this->_xss->xss_clean($_cont[$i][2]);
				// $cat->video = $this->_view->data['video'];
				// $cat->tags = $this->_view->data['tags'];
				$cat->notas_viejas = 'si';
				$cat->identificador = $_cont[$i][11];
				$cat->estado = 'alta';					
				$cat->fecha = $_cont[$i][14];
				$cat->save();

				// cargar img
				if($_cont[$i][4]!=''){

					foreach ($_cont[$i][12] as $val) {
						$img = new contenidos_imagene();
						$img->nombre = $val;
						$img->identificador = $_cont[$i][11];
						$img->path = $val;
						$img->posicion = 'capacitaciones';
						$img->orden = 0;
						$img->fecha_alt = $_cont[$i][14];
						$img->save();
						
					}
				}

				// cargar archivos
				if($_cont[$i][8]!=''){

					foreach ($_cont[$i][13] as $val) {
						$arch = pathinfo($val);   

						$arc = new contenidos_archivo();
						$arc->nombre = $arch['filename'];
						$arc->identificador = $_cont[$i][11];
						$arc->path = $val;
						$arc->formato = $arch['extension'];
						$arc->orden = 0;
						$arc->fecha_alt = $_cont[$i][14];
						$arc->save();
						
					}
				}



        	}

        	

        	
        }*/
        $_cont_2 = array();
        for ($i=1; $i < count($_cont); $i++) { 

         	if($_cont[$i][5]!=''){
         		if(validador::numerico($_cont[$i][5])==true){

         			$_dat = contenidos_cliente::find(array('conditions' => array('numero_cliente = ?', $_cont[$i][5])));
		        	if($_dat){

		        		$_fecha = date('Y-m-d', strtotime($_cont[$i][6]));
		        		$_fecha_nac = date('d/m/Y', strtotime($_cont[$i][2]));

		        		 // $_cont_2[$i][] = $_cont[$i][2];
		        		 // // $_cont_2[] = $_cont[$i][5];
		        		 // //  $_cont_2[$i][] = $_cont[$i][6];
		        		 // $_cont_2[$i][] = $_fecha_nac;

		        		$user = new contenidos_user();
						$user->id_cliente = $_dat->id;
						$user->nombre = $this->_xss->xss_clean($_cont[$i][0]);						
						$user->email = $this->_xss->xss_clean($_cont[$i][1]);							
						$user->password = $this->_xss->xss_clean($_cont[$i][4]);							
						$user->fecha_nacimiento = $this->_xss->xss_clean($_fecha_nac);
						$user->fecha = $_fecha;
						$user->save();

		        	}
					
				}

         		
         	}
        	

    		/*$cat = new contenidos_lanzamiento();
			// $cat->tipo = 'tendencias';			
			$cat->titulo = $this->_xss->xss_clean(ucwords(strtolower($_cont[$i][1])));
			$cat->categorias = $_cont[$i][7];
			$cat->bajada = $this->_xss->xss_clean($_cont[$i][2]);
			$cat->descripcion = $this->_xss->xss_clean($_cont[$i][3]);
			// $cat->video = $this->_view->data['video'];
			// $cat->tags = $this->_view->data['tags'];
			$cat->notas_viejas = 'si';
			$cat->identificador = $_cont[$i][8];
			$cat->estado = 'alta';					
			$cat->fecha = $_cont[$i][10];
			$cat->save();

			// cargar img
			if($_cont[$i][4]!=''){

				// foreach ($_cont[$i][12] as $val) {
					$img = new contenidos_imagene();
					$img->nombre = $_cont[$i][9]['filename'];
					$img->identificador = $_cont[$i][8];
					$img->path = $_cont[$i][9]['basename'];
					$img->posicion = 'lanzamientos';
					$img->orden = 0;
					$img->fecha_alt = $_cont[$i][10];
					$img->save();
					
				// }
			}*/
        	
        }

         // echo "<pre>";print_r($_cont_2);echo "</pre>";exit;

        exit('ok');

		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'clientes');	

		
    }
	
	
	/*public function borrar()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		

				$_borrar = $this->_trabajosGestion->borrarCapacitacion($_id, $this->_conf['ruta_img_cargadas'], $this->_conf['ruta_archivos_descargas'], 'capacitaciones');
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}*/
	
	public function exportaDatos()
    {

    	$this->_acl->acceso('encargado_access');

		$_datos = $this->_trabajosGestion->traerClientes();
		$this->_view->filename = 'clientes_'.date('d-m-Y').'.xls';	

		$this->_view->html ='<table border="1" cellpadding="10" cellspacing="0" bordercolor="#666666" style="border-collapse:collapse;">
		<tbody>
		<tr style="font-weight: bold;">
		<td>Nro de Cliente</td>
		<td>Razón Social</td>	
		<td>Localidad</td>	
		<td>Provincia</td>		
		</tr>';

		foreach ($_datos as $data) {
			$this->_view->html .='<tr>';
			$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($data['numero_cliente']).'</td>';
			$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($data['razon_social']).'</td>';
			$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($data['ciudad']).'</td>';
			$this->_view->html .='<td valign="top">'.admin::convertirCaracteres($data['provincia']).'</td>';
			// $this->_view->html .='<td valign="top">'.$data['fecha_nacimiento'].'</td>';
			// $this->_view->html .='<td valign="top">'.$data['email'].'</td>';
			// $this->_view->html .='<td valign="top">'.$data['password'].'</td>';
	    	$this->_view->html .='</tr>';
		}
	
		
		$this->_view->html .='</tbody>
		</table>';


		$this->_view->titulo = 'Administrador - Seguimiento';
		$this->_view->renderizar('exportar', 'clientes','vacio');



    }

    public function buscador()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerBuscadorClientes(ucwords(strtolower($_val)));
				
				/*echo "<pre>";print_r($this->_view->users);echo"</pre>";*/
				
				$_html = '';
				foreach($this->_view->prod as $prod){ 

					
	        		$_html .='<div class="forum-item">
	        					<div class="row">
	        						<div class="col-md-10">
										<a href="" class="forum-item-title">'.$prod['razon_social'].'</a>
										<small>Nº Cliente: <b>'.$prod['numero_cliente'].'</b></small>
									</div>
									
									<div class="col-md-2 forum-info">
						                <div class="tooltip-demo pull-right">   							
											<a class="btn btn-warning btn-round" href="'.$this->_conf['url_enlace'].'administrador/clientes/editar/'. $prod['id'].'">
												Editar
											</a>&nbsp;&nbsp;
										</div>
					                </div>
					            </div>
					        </div>';
					
	        
				}			
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	
	
}