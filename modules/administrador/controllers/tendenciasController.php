<?php

use controllers\administradorController\administradorController;

class tendenciasController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		$this->getLibrary('class.admin');		
		$this->_trabajosGestion = new admin();
		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		
		
    }
    
    public function index()
    {	
		
		$this->redireccionar('administrador/tendencias/listar');	
    }
	
	public function listar($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		$this->_view->datos = $this->_trabajosGestion->traerTendencias();
		
		// $this->_view->datos = $paginador->paginar($this->_view->datos, $pagina, 20);
		// $this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/tendencias/listar');
		
		// echo "<pre>";print_r($this->_view->datos);echo "</pre>";exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'tendencias');	
    }
	
	
	
	
	/*
	
	public function vista_previa_catalogos($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('basic'));
		$this->_view->setJs(array('modernizr.2.5.3.min', 'turn'));
		
		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);
		$this->_view->imagenes = $this->_trabajosGestion->traerGaleriaPorIdentificador($this->_view->trabajo->identificador, $this->_view->trabajo->imagenes_orientacion);
		$this->_view->size = getimagesize($this->_conf['base_url']. 'public/img/subidas/catalogos/cat_'.$this->_view->trabajo->identificador.'/'.$this->_view->imagenes[0]->path);
		
		//echo $this->_view->size;
		//echo "<pre>";print_r($this->_view->size);exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('vista_previa_catalogos', 'tendencias');	
    }
	*/
	
	
	
	public function editar($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
			
		
		$this->_view->setCss(array('sweetalert'));
		$this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		$this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited','sweetalert.min'));

		$this->_view->categorias = $this->_trabajosGestion->traerCategorias();
		$this->_view->tags = $this->_trabajosGestion->traerTags();
		$this->_view->trabajo = $this->_trabajosGestion->traerTendencia($_id);
		$this->_view->cat_ids = explode(',', $this->_view->trabajo['categorias']);
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo['identificador']);

		$_bloque_titulo = base64_decode($this->_view->trabajo['bloque_titulo']);
        $_bloque_titulo = unserialize($_bloque_titulo);
        $this->_view->trabajo['_bloque_titulo'] = $_bloque_titulo;

        $_bloque_subtitulo = base64_decode($this->_view->trabajo['bloque_subtitulo']);
        $_bloque_subtitulo = unserialize($_bloque_subtitulo);
        $this->_view->trabajo['_bloque_subtitulo'] = $_bloque_subtitulo;

        $_bloque_contenido = base64_decode($this->_view->trabajo['bloque_contenido']);
        $_bloque_contenido = unserialize($_bloque_contenido);
        $this->_view->trabajo['_bloque_contenido'] = $_bloque_contenido;

        $_bloque_img_desktop = base64_decode($this->_view->trabajo['bloque_img_desktop']);
        $_bloque_img_desktop = unserialize($_bloque_img_desktop);
        $this->_view->trabajo['_bloque_img_desktop'] = $_bloque_img_desktop;

        $_bloque_img_mobile = base64_decode($this->_view->trabajo['bloque_img_mobile']);
        $_bloque_img_mobile = unserialize($_bloque_img_mobile);
        $this->_view->trabajo['_bloque_img_mobile'] = $_bloque_img_mobile;

        $_cont =array();
        $_cont[] = ($_bloque_titulo) ? count($_bloque_titulo) : 0;
        $_cont[] = ($_bloque_subtitulo) ? count($_bloque_subtitulo) : 0;        
        $_cont[] = ($_bloque_contenido) ? count($_bloque_contenido) : 0;
        $_cont[] = ($_bloque_img_desktop) ? count($_bloque_img_desktop) : 0;
        $_cont[] = ($_bloque_img_mobile) ? count($_bloque_img_mobile) : 0;
        arsort($_cont);
		$this->_view->cont = $_cont[0];
		
		 // echo "<pre>";print_r($this->_view->cat_ids);exit;		
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					// echo "<pre>";print_r($this->_view->data);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					} 

					/*if(!validador::getInt('codigo')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					}*/


					$_img_desk = ($_POST['_multimedia_desk']!='') ? explode('|', $_POST['_multimedia_desk']) : '';
					$_img_mob = ($_POST['_multimedia_mob']!='') ? explode('|', $_POST['_multimedia_mob']) : '';

					$_bloque_titulo = serialize($_POST['_titulo']);
	                $_bloque_titulo = base64_encode($_bloque_titulo);
	                $_bloque_subtitulo = serialize($_POST['_subtitulo']);
	                $_bloque_subtitulo = base64_encode($_bloque_subtitulo);
	                $_bloque_contenido = serialize($_POST['_contenido']);
	                $_bloque_contenido = base64_encode($_bloque_contenido);
					$_bloque_img_desktop = serialize($_img_desk);
	                $_bloque_img_desktop = base64_encode($_bloque_img_desktop);
	                $_bloque_img_mobile = serialize($_img_mob);
	                $_bloque_img_mobile = base64_encode($_bloque_img_mobile);
					
					
					if($this->_view->data['tags']!=''){
						$_tags = explode(',', $this->_view->data['tags']);
						foreach ($_tags as $val) {
							$_hay = contenidos_tag::find(array('conditions' => array('nombre = ?', $val)));
							if(!$_hay){
								$tag = new contenidos_tag();
								$tag->nombre = $val;						
								$tag->save();
							}
							
						}
						
					}
					
						
								
					
					$cat = contenidos_tendencia::find($this->_view->trabajo['id']);
					$cat->tipo = $this->_view->data['tipo'];
					// $cat->novedades = $this->_view->data['novedades'];
					$cat->categorias = implode(',', $this->_view->data['categorias']);
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->bajada = $this->_xss->xss_clean(validador::getTexto('bajada'));
					$cat->descripcion = $this->_xss->xss_clean(validador::getTexto('descripcion'));
					$cat->video = $this->_view->data['video'];
					$cat->bloque_titulo = $_bloque_titulo;
					$cat->bloque_subtitulo = $_bloque_subtitulo;
					$cat->bloque_contenido = $_bloque_contenido;
					$cat->bloque_img_desktop = $_bloque_img_desktop;
					$cat->bloque_img_mobile = $_bloque_img_mobile;
					$cat->tags = $this->_view->data['tags'];
					$cat->save();
								
					

					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('img_id');
					$this->_sess->destroy('img_lote');
					$this->redireccionar('administrador/tendencias');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editar', 'tendencias');	
    }
	
	
	
	
	
	public function cargar($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		$this->_view->setCss(array('sweetalert'));
		$this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		$this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited','sweetalert.min'));

		$this->_view->categorias = $this->_trabajosGestion->traerCategorias();
		$this->_view->tags = $this->_trabajosGestion->traerTags();

		 // echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		// echo "<pre>";print_r($this->_view->tags);echo "</pre>";exit;

		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					  // echo "<pre>";print_r($_POST);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'tendencias');
						exit;
					} 

					/*if(!validador::getInt('bajada')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'tendencias');
						exit;
					} */

					$_img_desk = ($_POST['_multimedia_desk']!='') ? explode('|', $_POST['_multimedia_desk']) : '';
					$_img_mob = ($_POST['_multimedia_mob']!='') ? explode('|', $_POST['_multimedia_mob']) : '';

					$_bloque_titulo = serialize($_POST['_titulo']);
	                $_bloque_titulo = base64_encode($_bloque_titulo);
	                $_bloque_subtitulo = serialize($_POST['_subtitulo']);
	                $_bloque_subtitulo = base64_encode($_bloque_subtitulo);
	                $_bloque_contenido = serialize($_POST['_contenido']);
	                $_bloque_contenido = base64_encode($_bloque_contenido);
					$_bloque_img_desktop = serialize($_img_desk);
	                $_bloque_img_desktop = base64_encode($_bloque_img_desktop);
	                $_bloque_img_mobile = serialize($_img_mob);
	                $_bloque_img_mobile = base64_encode($_bloque_img_mobile);
						
					
					$_fechaBd = date('Y-m-d');

					if($this->_view->data['tags']!=''){
						$_tags = explode(',', $this->_view->data['tags']);
						foreach ($_tags as $val) {
							$_hay = contenidos_tag::find(array('conditions' => array('nombre = ?', $val)));
							if(!$_hay){
								$tag = new contenidos_tag();
								$tag->nombre = $val;						
								$tag->save();
							}
							
						}
						
					}
					
					$cat = new contenidos_tendencia();
					$cat->tipo = $this->_view->data['tipo'];
					// $cat->novedades = $this->_view->data['novedades'];
					$cat->categorias = implode(',', $this->_view->data['categorias']);
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->bajada = $this->_xss->xss_clean(validador::getTexto('bajada'));
					$cat->descripcion = $this->_xss->xss_clean(validador::getTexto('descripcion'));
					$cat->video = $this->_view->data['video'];
					$cat->bloque_titulo = $_bloque_titulo;
					$cat->bloque_subtitulo = $_bloque_subtitulo;
					$cat->bloque_contenido = $_bloque_contenido;
					$cat->bloque_img_desktop = $_bloque_img_desktop;
					$cat->bloque_img_mobile = $_bloque_img_mobile;
					$cat->tags = $this->_view->data['tags'];
					$cat->notas_viejas = 'no';
					$cat->identificador = $this->_sess->get('carga_actual');
					$cat->estado = 'alta';					
					$cat->fecha = "$_fechaBd";
					$cat->save();			
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('img_id');
					$this->_sess->destroy('img_lote');
					$this->redireccionar('administrador/tendencias');
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'tendencias');	
    }


    public function modificarItemBloque()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = $_POST['id'];

				
				$this->_sess->set('_item_bloque', $_id);			
				
				echo "ok";
				
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}

	public function eliminarImagenBloque()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['id'];

				
				$_borrar = $this->_trabajosGestion->borrarImgBloque($_id, $this->_conf['ruta_img_cargadas'], 'tendencias');		
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}


	
	
	public function borrar()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		

				$_borrar = $this->_trabajosGestion->borrarTendencia($_id, $this->_conf['ruta_img_cargadas'], $this->_conf['ruta_archivos_descargas'], 'tendencias');
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}

	public function buscador()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				
				$_datos  = $this->_trabajosGestion->traerBuscadorTendencias(ucwords(strtolower($_val)));
				
				// echo "<pre>";print_r($_datos);echo"</pre>";exit;
				
				if($_datos ){

					$_html = '';
					foreach($_datos as $datos){ 

						$_cat=array();
			            $_cat = explode(',', $datos['categorias']);       
			            $_arr_cat=array();
			            $_arr_label=array();
			            foreach ($_cat as $val) {
			              $_arr = admin::traerCategoria($val);
			              $_arr_cat[] = $_arr['nombre']; 
			              $_arr_label[] = $_arr['clase']; 
			            }
			            $_cate = implode(', ', $_arr_cat); 
			            $_clases = implode(' ', $_arr_label);

							        							
						$_html .= '<div class="forum-item grid-item '.$_clases.'">
							            <div class="row">
							                <div class="col-md-10">
							                    
							                    <a href="" class="forum-item-title">
							                       '.admin::convertirCaracteres($datos['titulo']).'
							                    </a>
							          			<small>Categorias: <b>'.$_cate.'</b></small>
							                </div>

							                <div class="col-md-2 forum-info">
							                    <div class="tooltip-demo pull-right">						                    

							                        <a class="btn btn-warning btn-round" href="'. $this->_conf['url_enlace'].'administrador/tendencias/editar/'.$datos['id'].'">
							                           Editar
							                        </a>&nbsp;&nbsp;

							                        <a href="javascript:void(0);" class="btn btn-danger btn-round _borrar_'. $datos['id'].'" title="Borrar">
							                            Eliminar
							                        </a>&nbsp;&nbsp;

							                    </div>
							                </div>
							            </div>
							        </div>';

					        $_html .= '<script>					        			
								        $(document).ready(function () {
								                $("._borrar_'.$datos['id'].'").click(function () {
								                    swal({
								                        title: "Estas seguro de borrar este contenido?",
								                        text: "Los datos se perderán permanentemente!",
								                        type: "warning",
								                        showCancelButton: true,
								                        confirmButtonColor: "#DD6B55",
								                        confirmButtonText: "Si, que se borre!",
								                        cancelButtonText: "No, mejor no!",
								                        closeOnConfirm: false,
								                        closeOnCancel: false },
								                    function (isConfirm) {
								                        if (isConfirm) {
								                            var url= _root_ + "administrador/tendencias/borrar";
								                            var dataString = "_id='.$datos['id'].'&_csrf='.$this->_sess->get('_csrf').'";
								                            $.ajax({
								                                    type: "POST",
								                                    url: url,
								                                    data: dataString,
								                                    success: function(data){
								                                      if(data=="ok"){
								                                        swal("Borrado!", "El contenido se borró con exito.", "success");
								                                        setTimeout(function() {
								                                            location.reload();
								                                        }, 200);
								                                      }else{
								                                        swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
								                                      }
								                                        
								                                        
								                                    }
								                            });
								                        } else {
								                            swal("Cancelado", "El contenido esta guardado", "error");
								                        }
								                    });
								                });
								            });
								        </script>';
		        
					}
					
				
				}else{
					$_html = 'No hay resultados';
				}

							
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	
	
	
}