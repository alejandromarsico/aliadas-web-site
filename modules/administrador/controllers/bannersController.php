<?php

use controllers\administradorController\administradorController;

class bannersController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		
		$this->getLibrary('class.validador');
		
		$this->getLibrary('class.admin');				
		$this->_trabajosGestion = new admin();
		
		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		
		
    }
    
    public function index()
    {	
		
		$this->redireccionar('administrador/banners/listardos');	
    }
	
	public function listar($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		 $datos = $this->_trabajosGestion->traerBanners();

		 // echo "<pre>";print_r($datos);echo "</pre>";exit;
		
		$this->_view->datos = $paginador->paginar($datos, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/banners/listar');
		
		 
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'banners');	
    }
	
	
	public function editar($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
			
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));

		$this->_view->trabajo = $this->_trabajosGestion->traerBanner($_id);
		$this->_view->ids_secciones = explode(',', $this->_view->trabajo['ids_seccion']);
		$this->_view->ids_posicion = explode(',', $this->_view->trabajo['ids_posicion']);
		$this->_view->id_formato = $this->_view->trabajo['id_formato'];
		$this->_view->formato_img = $this->_trabajosGestion->traerFormatoPorId($this->_view->trabajo['id_formato']);
		
		$this->_view->secciones = $this->_trabajosGestion->traerSecciones();
		$this->_view->posiciones = $this->_trabajosGestion->traerPosiciones($this->_view->ids_secciones);	
		$this->_view->formatos = $this->_trabajosGestion->traerFormatos($this->_view->ids_secciones, $this->_view->ids_posicion);	
		
		
		
				
		$this->_sess->set('edicion_actual', $this->_view->trabajo['identificador']);
		
		 // echo "<pre>";print_r($this->_view->cat_ids);exit;		
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					// echo "<pre>";print_r($this->_view->data);exit;

					/*if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'capacitaciones');
						exit;
					} 

					if(!validador::getInt('codigo')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					}*/
					
					
				
					
					$_sec = implode(',', $_POST['secciones']);
					$_pos = implode(',', $_POST['posiciones']);


					$cat = contenidos_banner::find($this->_view->trabajo['id']);
					$cat->ids_seccion = $_sec;
					$cat->ids_posicion = $_pos;
					$cat->id_formato = $this->_view->data['formatos'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->link = $this->_xss->xss_clean(validador::getTexto('link'));
					//$cat->identificador = $this->_sess->get('carga_actual');
					//$cat->fecha = date('Y-m-d');
					$cat->save();			
					
								
					

					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('img_id');					
					$this->redireccionar('administrador/banners');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editar', 'banners');	
    }
	
	
	public function cargar($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));

		$this->_view->secciones = $this->_trabajosGestion->traerSecciones();
		// $this->_view->posiciones = $this->_trabajosGestion->traerPosicionesTodas();

		 // echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		// echo "<pre>";print_r($this->_view->tags);echo "</pre>";exit;

		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					// echo "<pre>";print_r($_POST);exit;

					$_sec = implode(',', $_POST['secciones']);
					$_pos = implode(',', $_POST['posiciones']);


					$cat = new contenidos_banner();
					$cat->ids_seccion = $_sec;
					$cat->ids_posicion = $_pos;
					$cat->id_formato = $this->_view->data['formatos'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->link = $this->_xss->xss_clean(validador::getTexto('link'));
					$cat->identificador = $this->_sess->get('carga_actual');
					$cat->fecha = date('Y-m-d');
					$cat->save();			
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('img_id');
					//$this->_sess->destroy('img_lote');
					$this->redireccionar('administrador/banners');
					
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'banners');	
    }
	

	
	
	
	public function borrar()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		

				$_borrar = $this->_trabajosGestion->borrarBanner($_id, $this->_conf['ruta_img_cargadas'], 'banners');
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}

	public function buscador()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerBuscadorBanner(ucwords(strtolower($_val)));				
			
				
				$_html = '';
				foreach($this->_view->prod as $prod){ 


					$_html .='<div class="forum-item">
	        					<div class="row">
	        						<div class="col-md-10">
										<a href="" class="forum-item-title">'.$prod['titulo'].'</a>
									</div>
									<div class="col-md-2 forum-info">
						                <div class="tooltip-demo pull-right">   							
											<a class="btn btn-warning btn-round" href="'.$this->_conf['url_enlace'].'administrador/users/editar/'. $prod['id'].'">
												Editar
											</a>&nbsp;&nbsp;
											<a href="javascript:void(0);" class="btn btn-danger btn-round _borrar_'.$prod['id'].'" title="Borrar">
					                            Eliminar
					                        </a>&nbsp;&nbsp;
										</div>
					                </div>
					            </div>
					        </div>
					        <script>
			                  $(document).ready(function () {
			                        $("._borrar_'.$prod['id'].'").click(function () {
			                            swal({
			                                title: "Estas seguro de borrar este contenido?",
			                                text: "Los datos se perderán permanentemente!",
			                                type: "warning",
			                                showCancelButton: true,
			                                confirmButtonColor: "#DD6B55",
			                                confirmButtonText: "Si, que se borre!",
			                                cancelButtonText: "No, mejor no!",
			                                closeOnConfirm: false,
			                                closeOnCancel: false },
			                            function (isConfirm) {
			                                if (isConfirm) {
			                                    var url= _root_ + "administrador/banners/borrar";
			                                    var dataString = "_id='.$prod['id'].'>&_csrf='.$this->_sess->get('_csrf').'";
			                                    $.ajax({
			                                            type: "POST",
			                                            url: url,
			                                            data: dataString,
			                                            success: function(data){
			                                              if(data=="ok"){
			                                                swal("Borrado!", "El contenido se borró con exito.", "success");
			                                                setTimeout(function() {
			                                                    location.reload();
			                                                }, 200);
			                                              }else{
			                                                swal("Cancelado", "No se puede borrar", "error");
			                                              }
			                                                
			                                                
			                                            }
			                                    });
			                                } else {
			                                    swal("Cancelado", "El contenido esta guardado", "error");
			                                }
			                            });
			                        });
			                    });
			                </script>';
					
	        		/*$_html .='<li id="elemento-'. $prod['id'].'" class="list-group-item">
		        				<p>'.$prod['titulo'].'</p>
								<span>							
									<a href="'.$this->_conf['url_enlace'].'administrador/banners/editar/'.$prod['id'].'">
										<img src="'.$this->_conf['base_url'].'public/img/ico-pencil.png" alt="editar" style="width: 27px"/>
									</a>
									<a href="javascript:void(0);" class="_borrar_'.$prod['id'].'" title="Borrar">
					                    <img src="'.$this->_conf['base_url'].'public/img/ico-cruz.png" alt="eliminar"/>
					                </a>									
								</span>	
								<script>
				                  $(document).ready(function () {
				                        $("._borrar_'.$prod['id'].'").click(function () {
				                            swal({
				                                title: "Estas seguro de borrar este contenido?",
				                                text: "Los datos se perderán permanentemente!",
				                                type: "warning",
				                                showCancelButton: true,
				                                confirmButtonColor: "#DD6B55",
				                                confirmButtonText: "Si, que se borre!",
				                                cancelButtonText: "No, mejor no!",
				                                closeOnConfirm: false,
				                                closeOnCancel: false },
				                            function (isConfirm) {
				                                if (isConfirm) {
				                                    var url= _root_ + "administrador/banners/borrar";
				                                    var dataString = "_id='.$prod['id'].'>&_csrf='.$this->_sess->get('_csrf').'";
				                                    $.ajax({
				                                            type: "POST",
				                                            url: url,
				                                            data: dataString,
				                                            success: function(data){
				                                              if(data=="ok"){
				                                                swal("Borrado!", "El contenido se borró con exito.", "success");
				                                                setTimeout(function() {
				                                                    location.reload();
				                                                }, 200);
				                                              }else{
				                                                swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
				                                              }
				                                                
				                                                
				                                            }
				                                    });
				                                } else {
				                                    swal("Cancelado", "El contenido esta guardado", "error");
				                                }
				                            });
				                        });
				                    });
				                </script>						
							</li>';*/
	        
				}			
				// $_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	

	public function traerPosiciones()
	{
		$this->_acl->acceso('encargado_access');

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

				// echo "<pre>";print_r($_POST);exit;

				// $_sec = implode(',', $_POST['secciones']);
				$_sec = (isset($_POST['secciones'])) ? $_POST['secciones'] : '';

				if($_sec != ''){

					$_dat = $this->_trabajosGestion->traerPosiciones($_sec);
				
					// echo "<pre>";print_r($_dat);exit;

					$_html ="";
					if($_dat){
						foreach ($_dat as $val) {
							$_html .= '<div class="col-lg-2 checkbox">
				                        <label>
				                          <input class="_posiciones" type="checkbox" name="posiciones[]" value="'.$val['id'].'"> '.$val['posicion'].'                       
				                      </label>
				                    </div>';
						}
					}else{
						$_html ="";
					}

				}else{
					$_html ="";
				}

				

				echo $_html;
				exit;

			}

		}

	}

	public function traerFormatos()
	{
		$this->_acl->acceso('encargado_access');

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

				 // echo "<pre>";print_r($_POST);exit;

				$_sec = (isset($_POST['secciones'])) ? $_POST['secciones'] : '';
				$_pos = (isset($_POST['posiciones'])) ? $_POST['posiciones'] : '';

				if($_sec!='' && $_pos!=''){
					$_dat = $this->_trabajosGestion->traerFormatos($_sec, $_pos);
				
					// echo "<pre>";print_r($_dat);exit;
					if($_dat){
						$_html ='<select class="form-control" id="formatos" name="formatos">
								<option value="">Seleccione un formato</option>';
						if($_dat){
							foreach ($_dat as $val) {
								$_html .= '<option value="'.$val['id'].'">'.$val['formato'].'</option>';
							}
							$_html .='</select>';
						}
					}else{
						$_html='';
					}
				}else{
					$_html='';
				}
				

				

				echo $_html;
				exit;

			}

		}

	}

	public function traerFormatoImg()
	{
		$this->_acl->acceso('encargado_access');

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

				// echo "<pre>";print_r($_POST);exit;

				$_format = $_POST['valor'];
				//$_format =array();	

				/*foreach ($_sec as $val) {
					$_dat = $this->_trabajosGestion->traerPosiciones($val);
					if(!in_array($_dat, $_pos)){
						$_pos[] = $_dat;
					}
				}*/

				$_dat = $this->_trabajosGestion->traerFormatoPorId($_format);
				
				// echo "<pre>";print_r($_dat);exit;

				$jsondata['formato'] = $_dat['formato'];
				$jsondata['ancho'] = $this->_conf['formatos_img']['banner_'.$_dat['formato']]['ancho'];
				$jsondata['alto'] = $this->_conf['formatos_img']['banner_'.$_dat['formato']]['alto'];

				
				echo json_encode($jsondata);
    			exit();

			}

		}

	}

	/////////////////////////////////////////////////////////////////////////

	public function listardos($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		 $datos = $this->_trabajosGestion->traerBannersDos();

		 // echo "<pre>";print_r($datos);echo "</pre>";exit;
		
		$this->_view->datos = $paginador->paginar($datos, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/banners/listardos');
		
		 
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('indexdos', 'banners');	
    }
	
	
	public function cargardos($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;

		// $this->_sess->destroy('carga_actual');
		// $this->_sess->destroy('img_id');
		// $this->_sess->destroy('img_lote');
		// $this->_sess->destroy('lote_img_gal');
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));

		$this->_view->secciones = $this->_trabajosGestion->traerSecciones();
		// $this->_view->posiciones = $this->_trabajosGestion->traerPosicionesTodas();

		 // echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		// echo "<pre>";print_r($this->_view->tags);echo "</pre>";exit;

		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					// echo "<pre>";print_r($_POST);exit;

					$_sec = implode(',', $_POST['secciones']);
					$_pos = implode(',', $_POST['posiciones']);

					$_links = base64_encode(serialize($_POST['img_link']));


					$cat = new contenidos_banners_do();
					$cat->ids_seccion = $_sec;
					$cat->ids_posicion = $_pos;
					$cat->id_formato = $this->_view->data['formatos'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->link = $_links;
					$cat->identificador = $this->_sess->get('carga_actual');
					$cat->fecha = date('Y-m-d');
					$cat->save();			
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('img_id');
					$this->_sess->destroy('img_lote');
					$this->_sess->destroy('lote_img_gal');
					$this->redireccionar('administrador/banners/listardos');
					
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargardos', 'banners');	
    }


    public function editardos($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		

		// $this->_sess->destroy('edicion_actual');
		// $this->_sess->destroy('img_id');
		// $this->_sess->destroy('img_lote');
		// $this->_sess->destroy('lote_img_gal');
			
		
		$this->_view->setCssPlugin(array('dropzone.min'));
		$this->_view->setJs(array('dropzone'));

		$this->_view->trabajo = $this->_trabajosGestion->traerBannerDos($_id);
		$this->_view->ids_secciones = explode(',', $this->_view->trabajo['ids_seccion']);
		$this->_view->ids_posicion = explode(',', $this->_view->trabajo['ids_posicion']);
		$this->_view->id_formato = $this->_view->trabajo['id_formato'];
		$this->_view->formato_img = $this->_trabajosGestion->traerFormatoDosPorId($this->_view->trabajo['id_formato']);
		
		$this->_view->secciones = $this->_trabajosGestion->traerSecciones();
		$this->_view->posiciones = $this->_trabajosGestion->traerPosicionesDos($this->_view->ids_secciones);	
		$this->_view->formatos = $this->_trabajosGestion->traerFormatosDos($this->_view->ids_secciones, $this->_view->ids_posicion);			
		$this->_view->trabajo['link'] = unserialize(base64_decode($this->_view->trabajo['link']));
		
				
		$this->_sess->set('edicion_actual', $this->_view->trabajo['identificador']);
		
		 // echo "<pre>";print_r($this->_view->trabajo);exit;		
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					// echo "<pre>";print_r($this->_view->data);exit;

					/*if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'capacitaciones');
						exit;
					} 

					if(!validador::getInt('codigo')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					} 

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'tendencias');
						exit;
					}*/
					
					if(isset($this->_view->data['eliminar_gal']) && $this->_view->data['eliminar_gal'][0]!=''){
						foreach($this->_view->data['eliminar_gal'] as $_gal){
							$this->_trabajosGestion->eliminarImgGal($_gal, $this->_conf['ruta_img_cargadas'], 'banners');
							unset($_POST['img_link'][$_gal]);		
						}
					}
				
					
					$_sec = implode(',', $_POST['secciones']);
					$_pos = implode(',', $_POST['posiciones']);
					$_links = base64_encode(serialize($_POST['img_link']));
					
					$cat = contenidos_banners_do::find($this->_view->trabajo['id']);
					$cat->ids_seccion = $_sec;
					$cat->ids_posicion = $_pos;
					$cat->id_formato = $this->_view->data['formatos'];
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->link = $_links;
					//$cat->identificador = $this->_sess->get('carga_actual');
					//$cat->fecha = date('Y-m-d');
					$cat->save();			
					
								
					

					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('img_id');
					$this->_sess->destroy('img_lote');
					$this->_sess->destroy('lote_img_gal');					
					$this->redireccionar('administrador/banners/listardos');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editardos', 'banners');	
    }


	public function traerPosicionesDos()
	{
		$this->_acl->acceso('encargado_access');

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

				// echo "<pre>";print_r($_POST);exit;

				// $_sec = implode(',', $_POST['secciones']);
				$_sec = (isset($_POST['secciones'])) ? $_POST['secciones'] : '';

				if($_sec != ''){

					$_dat = $this->_trabajosGestion->traerPosicionesDos($_sec);
				
					// echo "<pre>";print_r($_dat);exit;

					$_html ="";
					if($_dat){
						foreach ($_dat as $val) {
							$_html .= '<div class="col-lg-2 checkbox">
				                        <label>
				                          <input class="_posiciones" type="checkbox" name="posiciones[]" value="'.$val['id'].'"> '.$val['posicion'].'                       
				                      </label>
				                    </div>';
						}
					}else{
						$_html ="";
					}

				}else{
					$_html ="";
				}

				

				echo $_html;
				exit;

			}

		}

	}

	public function traerFormatosDos()
	{
		$this->_acl->acceso('encargado_access');

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

				 // echo "<pre>";print_r($_POST);exit;

				$_sec = (isset($_POST['secciones'])) ? $_POST['secciones'] : '';
				$_pos = (isset($_POST['posiciones'])) ? $_POST['posiciones'] : '';

				if($_sec!='' && $_pos!=''){
					$_dat = $this->_trabajosGestion->traerFormatosDos($_sec, $_pos);
				
					// echo "<pre>";print_r($_dat);exit;
					if($_dat){
						$_html ='<select class="form-control" id="formatos" name="formatos">
								<option value="">Seleccione un formato</option>';
						if($_dat){
							foreach ($_dat as $val) {
								$_html .= '<option value="'.$val['id'].'">'.$val['item'].'</option>';
							}
							$_html .='</select>';
						}
					}else{
						$_html='';
					}
				}else{
					$_html='';
				}
				

				

				echo $_html;
				exit;

			}

		}

	}


	public function traerFormatoImgDos()
	{
		$this->_acl->acceso('encargado_access');

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	

				// echo "<pre>";print_r($_POST);exit;

				$_format = $_POST['valor'];
				//$_format =array();	

				/*foreach ($_sec as $val) {
					$_dat = $this->_trabajosGestion->traerPosiciones($val);
					if(!in_array($_dat, $_pos)){
						$_pos[] = $_dat;
					}
				}*/

				$_dat = $this->_trabajosGestion->traerFormatoDosPorId($_format);
				
				// echo "<pre>";print_r($_dat);exit;

				$jsondata['formato'] = $_dat['formato'];
				$jsondata['ancho'] = $this->_conf['formatos_img']['banner_'.$_dat['formato']]['ancho'];
				// $jsondata['alto'] = $this->_conf['formatos_img']['banner_'.$_dat['formato']]['alto'];

				
				echo json_encode($jsondata);
    			exit();

			}

		}

	}

    public function buscadorDos()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerBuscadorBannerDos(ucwords(strtolower($_val)));				
			
				
				$_html = '';
				foreach($this->_view->prod as $prod){ 


					$_html .='<div class="forum-item">
	        					<div class="row">
	        						<div class="col-md-10">
										<a href="" class="forum-item-title">'.$prod['titulo'].'</a>
									</div>
									<div class="col-md-2 forum-info">
						                <div class="tooltip-demo pull-right">   							
											<a class="btn btn-warning btn-round" href="'.$this->_conf['url_enlace'].'administrador/banners/editardos/'. $prod['id'].'">
												Editar
											</a>&nbsp;&nbsp;
											<a href="javascript:void(0);" class="btn btn-danger btn-round _borrar_'.$prod['id'].'" title="Borrar">
					                            Eliminar
					                        </a>&nbsp;&nbsp;
										</div>
					                </div>
					            </div>
					        </div>
					        <script>
			                  $(document).ready(function () {
			                        $("._borrar_'.$prod['id'].'").click(function () {
			                            swal({
			                                title: "Estas seguro de borrar este contenido?",
			                                text: "Los datos se perderán permanentemente!",
			                                type: "warning",
			                                showCancelButton: true,
			                                confirmButtonColor: "#DD6B55",
			                                confirmButtonText: "Si, que se borre!",
			                                cancelButtonText: "No, mejor no!",
			                                closeOnConfirm: false,
			                                closeOnCancel: false },
			                            function (isConfirm) {
			                                if (isConfirm) {
			                                    var url= _root_ + "administrador/banners/borrarDos";
			                                    var dataString = "_id='.$prod['id'].'>&_csrf='.$this->_sess->get('_csrf').'";
			                                    $.ajax({
			                                            type: "POST",
			                                            url: url,
			                                            data: dataString,
			                                            success: function(data){
			                                              if(data=="ok"){
			                                                swal("Borrado!", "El contenido se borró con exito.", "success");
			                                                setTimeout(function() {
			                                                    location.reload();
			                                                }, 200);
			                                              }else{
			                                                swal("Cancelado", "No se puede borrar", "error");
			                                              }
			                                                
			                                                
			                                            }
			                                    });
			                                } else {
			                                    swal("Cancelado", "El contenido esta guardado", "error");
			                                }
			                            });
			                        });
			                    });
			                </script>';
					
	        	
	        
				}			
				// $_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	public function borrarDos()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		

				$_borrar = $this->_trabajosGestion->borrarBannerDos($_id, $this->_conf['ruta_img_cargadas'], 'banners');
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}
	
}