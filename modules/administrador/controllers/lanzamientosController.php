<?php

use controllers\administradorController\administradorController;

class lanzamientosController extends administradorController
{
	public $_trabajosGestion;
	public $_xss;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		$this->getLibrary('class.admin');		
		$this->_trabajosGestion = new admin();
		$this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';

		
		
    }
    
    public function index()
    {	
		
		$this->redireccionar('administrador/lanzamientos/listar');	
    }
	
	public function listar($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		//$this->_view->setJs(array('jquery.btechco.excelexport','jquery.base64','exportar_promo'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();

		$this->_sess->destroy('carga_actual');
		$this->_sess->destroy('edicion_actual');

		$this->_view->setCss(array('sweetalert'));
        $this->_view->setJs(array('sweetalert.min'));
		
		$this->_view->datos = $this->_trabajosGestion->traerLanzamientos();
		
		// $this->_view->datos = $paginador->paginar($this->_view->datos, $pagina, 20);
		// $this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/lanzamientos/listar');
		
		// echo "<pre>";print_r($this->_view->datos);echo "</pre>";exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('index', 'lanzamientos');	
    }
	
	
	
	
	/*public function cambiarEstado($_id, $_val)
	{
		$this->_acl->acceso('encargado_access');
		$_id = (int) $_id;
		
		$this->_trabajosGestion->cambiarEstadoCatalogo($_id, $_val);
		($_val=='alta') ? $this->redireccionar('administrador/catalogos/baja') : $this->redireccionar('administrador/catalogos/alta');
		
	
	}
	
	public function activarCatalogo($_id)
	{
		$this->_acl->acceso('encargado_access');
		$_id = (int) $_id;
		
		$this->_trabajosGestion->cambiarEstadoCatalogo($_id, 'alta');
		$this->redireccionar('administrador/contenidos/listarCatalogo');
		
	
	}
	public function ordenarGal()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){
			$_puntos = explode(',',$_POST['puntos']);
			foreach($_puntos as $i => $pt){
				$_idPunto = explode('-', $pt);
				$this->_trabajosGestion->cambiarOrdenGaleria($_idPunto[1], $i+1);
			}
			
			echo 1;
	
		}
	}
	
	
	public function buscadorCatalogo()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				$_est = $_POST['estado'];
				
				$this->_view->prod  = $this->_trabajosGestion->traerCatalogoBuscador(ucwords(strtolower($_val)),$_est);
				
				
				
				$_html = '<ul id="lista" class="list-group listar">';
				foreach($this->_view->prod as $prod){ 

					$_img = admin::traerImgOrientacion($prod->identificador,'thumb');       
	        		if(isset($_img) && $_img !=''){
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-catalogo" style="background-image: url('.$this->_conf['base_url'] . "public/img/subidas/catalogos/cat_".$prod->identificador."/thumb/". $_img[0]->path.')">';
	        		}else{
	        			$_html .='<li id="elemento-'. $prod->id.'" class="list-group-item item-catalogo">';
	        		}
										
					$_html .='<div class="contInfo">
							<p>'.$prod->titulo.'</p>
							<span class="flotar_derecha" style="margin-right:15px">			
															
								<a href="'.$this->_conf['url_enlace'].'administrador/contenidos/editarCatalogo/'. $prod->id.'">
									<img src="'.$this->_conf['base_url'].'public/img/ico-pencil.png" alt="editar" style="width: 27px"/>
								</a>
								
								<a href="javascript:void(0);" class="_borrar_'. $prod->id.'" title="Borrar">
		                            <img src="'.$this->_conf['url_enlace'].'public/img/ico-cruz.png" alt="eliminar"/>
		                        </a>
							</span>
							</div>
						</li>
						<script>
		                    $(document).ready(function () {
		                          $("._borrar_'.$prod->id.'").click(function () {
		                              swal({
		                                  title: "Estas seguro de borrar este contenido?",
		                                  text: "Los datos se perderán permanentemente!",
		                                  type: "warning",
		                                  showCancelButton: true,
		                                  confirmButtonColor: "#DD6B55",
		                                  confirmButtonText: "Si, que se borre!",
		                                  cancelButtonText: "No, mejor no!",
		                                  closeOnConfirm: false,
		                                  closeOnCancel: false },
		                              function (isConfirm) {
		                                  if (isConfirm) {
		                                      var url= _root_ + "administrador/contenidos/borrarCatalogo";
		                                      var dataString = "_id='.$prod->id.'&_csrf='.$this->_sess->get('_csrf').'";
		                                      $.ajax({
		                                              type: "POST",
		                                              url: url,
		                                              data: dataString,
		                                              success: function(data){
		                                                if(data=="ok"){
		                                                  swal("Borrado!", "El contenido se borró con exito.", "success");
		                                                  setTimeout(function() {
		                                                      window.location.reload()
		                                                  }, 200);
		                                                }else{
		                                                  swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
		                                                }
		                                                  
		                                                  
		                                              }
		                                      });
		                                  } else {
		                                      swal("Cancelado", "El contenido esta guardado", "error");
		                                  }
		                              });
		                          });
		                      });
		                   </script>';
	        
				}			
				$_html .='</ul>';
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	public function vista_previa_catalogos($_id)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setCss(array('basic'));
		$this->_view->setJs(array('modernizr.2.5.3.min', 'turn'));
		
		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerCatalogo($_id);
		$this->_view->imagenes = $this->_trabajosGestion->traerGaleriaPorIdentificador($this->_view->trabajo->identificador, $this->_view->trabajo->imagenes_orientacion);
		$this->_view->size = getimagesize($this->_conf['base_url']. 'public/img/subidas/catalogos/cat_'.$this->_view->trabajo->identificador.'/'.$this->_view->imagenes[0]->path);
		
		//echo $this->_view->size;
		//echo "<pre>";print_r($this->_view->size);exit;
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('vista_previa_catalogos', 'contenidos');	
    }
	*/
	
	
	
	public function editar($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
			
		
		$this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		$this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited'));

		$this->_view->categorias = $this->_trabajosGestion->traerCategorias();
		$this->_view->tags = $this->_trabajosGestion->traerTags();
		$this->_view->trabajo = $this->_trabajosGestion->traerLanzamiento($_id);
		$this->_view->cat_ids = explode(',', $this->_view->trabajo['categorias']);
		
		$this->_sess->set('edicion_actual', $this->_view->trabajo['identificador']);
		
		 // echo "<pre>";print_r($this->_view->cat_ids);exit;		
		
				
			
		if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					// echo "<pre>";print_r($this->_view->data);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'lanzamientos');
						exit;
					} 

					if(!validador::getTexto('codigo')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'lanzamientos');
						exit;
					} 

					if(!validador::getTexto('bajada')){
						$this->_view->_error ='Debe completar el campo bajada';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'lanzamientos');
						exit;
					}

					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('editar', 'lanzamientos');
						exit;
					}
					
					
					if($this->_view->data['tags']!=''){
						$_tags = explode(',', $this->_view->data['tags']);
						foreach ($_tags as $val) {
							$_hay = contenidos_tag::find(array('conditions' => array('nombre = ?', $val)));
							if(!$_hay){
								$tag = new contenidos_tag();
								$tag->nombre = $val;						
								$tag->save();
							}
							
						}
						
					}
					
						
								
					
					$_editar = contenidos_lanzamiento::find($this->_view->trabajo['id']);
					// $_editar->destacado = $this->_view->data['destacado'];
					$_editar->categorias = implode(',', $this->_view->data['categorias']);
					$_editar->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$_editar->codigo = validador::getInt('codigo');
					$_editar->bajada = $this->_xss->xss_clean(validador::getTexto('bajada'));
					$_editar->descripcion = $this->_xss->xss_clean(validador::getTexto('descripcion'));
					$_editar->video = $this->_view->data['video'];
					$_editar->link = $this->_view->data['link'];
					$_editar->tags = $this->_view->data['tags'];
					$_editar->save();
								
					

					$this->_sess->destroy('edicion_actual');
					$this->_sess->destroy('img_id');
					$this->_sess->destroy('img_lote');
					$this->redireccionar('administrador/lanzamientos');
					
												
					
				}

			}else{
				$this->redireccionar('error/access/404');
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editar', 'lanzamientos');	
    }
	
	
	
	
	
	public function cargar($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand((int)1135687452,(int)999999999));
		}
		
		$this->_view->setCssPlugin(array('dropzone.min','jquery.tagsinput-revisited'));
		$this->_view->setJs(array('dropzone', 'jquery.tagsinput-revisited'));

		$this->_view->categorias = $this->_trabajosGestion->traerCategorias();
		$this->_view->tags = $this->_trabajosGestion->traerTags();

		 // echo "<pre>";print_r($_SESSION);echo "</pre>";//exit;
		//unset($_SESSION['lote_img_gal']);

		// echo "<pre>";print_r($this->_view->tags);echo "</pre>";exit;

		//$this->_view->cod = $this->_trabajosGestion->traerIDComputadoras();
		
		//$this->_sess->destroy('img_id');
		
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;					
				
					// echo "<pre>";print_r($_POST);exit;

					if(!validador::getTexto('titulo')){
						$this->_view->_error ='Debe completar el campo titulo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'lanzamientos');
						exit;
					} 

					if(!validador::getTexto('codigo')){
						$this->_view->_error ='Debe completar el campo codigo';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'lanzamientos');
						exit;
					} 

					if(!validador::getTexto('bajada')){
						$this->_view->_error ='Debe completar el campo bajada';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'lanzamientos');
						exit;
					} 
					
					if(!validador::getTexto('descripcion')){
						$this->_view->_error ='Debe completar el campo descripcion';
						$this->_view->titulo = 'Administrador - Seguimiento';
						$this->_view->renderizar('cargar', 'lanzamientos');
						exit;
					} 
						
					
					$_fechaBd = date('Y-m-d');

					if($this->_view->data['tags']!=''){
						$_tags = explode(',', $this->_view->data['tags']);
						foreach ($_tags as $val) {
							$_hay = contenidos_tag::find(array('conditions' => array('nombre = ?', $val)));
							if(!$_hay){
								$tag = new contenidos_tag();
								$tag->nombre = $val;						
								$tag->save();
							}
							
						}
						
					}
					
					$cat = new contenidos_lanzamiento();
					// $cat->destacado = $this->_view->data['destacado'];
					$cat->categorias = implode(',', $this->_view->data['categorias']);
					$cat->titulo = $this->_xss->xss_clean(validador::getTexto('titulo'));
					$cat->codigo = validador::getInt('codigo');
					$cat->bajada = $this->_xss->xss_clean(validador::getTexto('bajada'));
					$cat->descripcion = $this->_xss->xss_clean(validador::getTexto('descripcion'));
					$cat->video = $this->_view->data['video'];
					$cat->link = $this->_view->data['link'];
					$cat->tags = $this->_view->data['tags'];
					$cat->identificador = $this->_sess->get('carga_actual');
					$cat->estado = 'alta';					
					$cat->fecha = "$_fechaBd";
					$cat->save();			
					
								
					
					$this->_sess->destroy('carga_actual');
					$this->_sess->destroy('img_id');
					$this->_sess->destroy('img_lote');
					$this->redireccionar('administrador/lanzamientos');
				}

			}else{
				$this->redireccionar('error/access/404');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'lanzamientos');	
    }
	
	
	public function borrar()
	{
		$this->_acl->acceso('encargado_access');
		//$_id = (int) $_id;
		

		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){		
			
				$_id = (int) $_POST['_id'];
		
				
				/*$this->_trabajosGestion->borrarCatalogo($_id, $this->_conf['ruta_img_cargadas'], 'catalogos');
				$this->redireccionar('administrador/contenidos/listarCatalogo');*/

				$_borrar = $this->_trabajosGestion->borrarLanzamiento($_id, $this->_conf['ruta_img_cargadas'], 'lanzamientos');
				if ($_borrar==false) {
					echo "enuso";
				}else{
					echo "ok";
				}
				

			}else{
				$this->redireccionar('error/access/404');
			}
		}
		

	}

	public function buscador()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){	
			
				$_val = $_POST['valor'];
				
				$_datos  = $this->_trabajosGestion->traerBuscadorLanzamientos(ucwords(strtolower($_val)));
				
				// echo "<pre>";print_r($_datos);echo"</pre>";exit;
				
				if($_datos ){

					$_html = '';
					foreach($_datos as $datos){ 

						$_cat=array();
			            $_cat = explode(',', $datos['categorias']);       
			            $_arr_cat=array();
			            $_arr_label=array();
			            foreach ($_cat as $val) {
			              $_arr = admin::traerCategoria($val);
			              $_arr_cat[] = $_arr['nombre']; 
			              $_arr_label[] = $_arr['clase']; 
			            }
			            $_cate = implode(', ', $_arr_cat); 
			            $_clases = implode(' ', $_arr_label);

							        							
						$_html .= '<div class="forum-item grid-item '.$_clases.'">
							            <div class="row">
							                <div class="col-md-10">
							                    
							                    <a href="" class="forum-item-title">
							                       '.admin::convertirCaracteres($datos['titulo']).'
							                    </a>
							          			<small>Categorias: <b>'.$_cate.'</b></small>
							                </div>

							                <div class="col-md-2 forum-info">
							                    <div class="tooltip-demo pull-right">						                    

							                        <a class="btn btn-warning btn-round" href="'. $this->_conf['url_enlace'].'administrador/lanzamientos/editar/'.$datos['id'].'">
							                           Editar
							                        </a>&nbsp;&nbsp;

							                        <a href="javascript:void(0);" class="btn btn-danger btn-round _borrar_'. $datos['id'].'" title="Borrar">
							                            Eliminar
							                        </a>&nbsp;&nbsp;

							                    </div>
							                </div>
							            </div>
							        </div>';

					        $_html .= '<script>					        			
								        $(document).ready(function () {
								                $("._borrar_'.$datos['id'].'").click(function () {
								                    swal({
								                        title: "Estas seguro de borrar este contenido?",
								                        text: "Los datos se perderán permanentemente!",
								                        type: "warning",
								                        showCancelButton: true,
								                        confirmButtonColor: "#DD6B55",
								                        confirmButtonText: "Si, que se borre!",
								                        cancelButtonText: "No, mejor no!",
								                        closeOnConfirm: false,
								                        closeOnCancel: false },
								                    function (isConfirm) {
								                        if (isConfirm) {
								                            var url= _root_ + "administrador/lanzamientos/borrar";
								                            var dataString = "_id='.$datos['id'].'&_csrf='.$this->_sess->get('_csrf').'";
								                            $.ajax({
								                                    type: "POST",
								                                    url: url,
								                                    data: dataString,
								                                    success: function(data){
								                                      if(data=="ok"){
								                                        swal("Borrado!", "El contenido se borró con exito.", "success");
								                                        setTimeout(function() {
								                                            location.reload();
								                                        }, 200);
								                                      }else{
								                                        swal("Cancelado", "No se puede borrar porque el contenido esta en uso", "error");
								                                      }
								                                        
								                                        
								                                    }
								                            });
								                        } else {
								                            swal("Cancelado", "El contenido esta guardado", "error");
								                        }
								                    });
								                });
								            });
								        </script>';
		        
					}
					
				
				}else{
					$_html = 'No hay resultados';
				}

							
				
				echo $_html;

			}else{
				$this->redireccionar('error/access/404');
			}
			
		}
	}
	
	
	
	
}