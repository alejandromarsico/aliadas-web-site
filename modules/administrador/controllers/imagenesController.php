<?php

use controllers\administradorController\administradorController;

class imagenesController extends administradorController
{
	public $_trabajosGestion;
	
    public function __construct() 
    {
		parent::__construct();
		$this->getLibrary('class.validador');
		
		$this->getLibrary('class.admin');
		$this->_trabajosGestion = new admin();
		
		$this->getLibrary('class.upload');
		
		$this->_error = 'has-error';
		$this->_filtro = '';
    }
    
   	
	
	public function cargImg($_seccion, $_directorio='')
	{
		$this->_acl->acceso('encargado_access');

		if (!empty($_FILES)) {

			$filename = strtolower($_FILES['file']['name']);
			$whitelist = array('jpg', 'png', 'gif', 'jpeg');
			// $whitelist = array('mp4','avi','mpeg');  
			$backlist = array('php', 'php3', 'php4', 'phtml','exe','bat','js');
			$tmp = explode('.', $filename);
			$file_extension = end($tmp); 
			if(!in_array($file_extension, $whitelist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}
			if(in_array($file_extension, $backlist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}

			if($this->_sess->get('img_id')) $this->_sess->destroy('img_id');
			$_info = pathinfo($_FILES['file']['name']);
			$_nombre_archivo =  basename($_FILES['file']['name'], '.' . $_info['extension']);
			$_nombre_interno = admin::cadenaAleatoriaSegura(15);
			//$_formatos_img = Conf\formatos_img::data();
			$size = getimagesize($_FILES['file']['tmp_name']);
			
			
			$foo = new upload($_FILES['file']);
			$foo->file_new_name_body 		= $_nombre_interno;
			//$foo->image_convert 			= 'jpg';
			$foo->image_resize          	= true;
			$foo->jpeg_quality          	= 100;
			$foo->png_compression       	= 10;
			/*$foo->image_ratio_fill      	= true;
			$foo->image_ratio_y         	= true;*/
			$foo->image_ratio_crop			= true;	
			if($_seccion=='banners'){
				$foo->image_x = $this->_conf['formatos_img'][$_directorio]['ancho'];
				$foo->image_y = $this->_conf['formatos_img'][$_directorio]['alto'];
			}else{
				$foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
				$foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];
			}
						
			//$foo->image_background_color 	= '#00000';			
			//$foo->process($this->_imagenesGestion->gestionDirectoriosOriginales($this->_conf['ruta_img_cargadas'] . 'slider' . DS . 'originales' . DS, date("Y-m-d")));
			
			if($_seccion=='lanzamientos' || $_seccion=='tendencias' || $_seccion=='clubaliadas' || $_seccion=='capacitaciones' || $_seccion=='tutoriales'|| $_seccion=='linearios'|| $_seccion=='eventos' || $_seccion=='banners'){
				$foo->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS);
			}/*else if($_seccion=='fondos'){
				$foo->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS);
			}else if($_seccion=='videos'){
				$foo->process($this->_conf['ruta_videos'] . $_directorio . DS. 'thumb'. DS);	
			}else{
				$foo->process($this->_conf['ruta_img_cargadas'] . 'catalogos'. DS. $_directorio. DS . 'thumb'. DS);
			}*/
			
			if($foo->processed){
			
							
				if($_seccion=='tendencias' || $_seccion=='capacitaciones' || $_seccion=='tutoriales' || $_seccion=='linearios'|| $_seccion=='eventos'){
					
					$thumb = new upload($foo->file_dst_pathname);
					$thumb->image_resize = true;
					$thumb->image_ratio_crop = true;
					$thumb->image_x = $this->_conf['formatos_img'][$_seccion.'_thumb']['ancho'];
					$thumb->image_y = $this->_conf['formatos_img'][$_seccion.'_thumb']['alto'];
					$thumb->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS . 'thumb'. DS);
				
				}
				
								
				$_nombre = (isset($_POST['imagen_principal_nombre']) && $_POST['imagen_principal_nombre'] != '') ? $_POST['imagen_principal_nombre'] : $_FILES['file']['name'];

				// Base Datos
				//$_imagen = $this->_trabajosGestion->cargarImgDB($this->_sess->get('carga_actual'), $foo->file_dst_name, $_nombre);
				/*if($_seccion=='promociones'){
					$_imagen = $this->_trabajosGestion->cargarImgDB($this->_sess->get('carga_actual'), $foo->file_dst_name, $_nombre, 'promociones', $this->_conf['ruta_img_cargadas']);
				}else if($_seccion=='fondos'){
					$_imagen = $this->_trabajosGestion->cargarImgDB($this->_sess->get('carga_actual'), $foo->file_dst_name, $_nombre, 'fondos', $this->_conf['ruta_img_cargadas']);
				}else if($_seccion=='videos'){
					$_imagen = $this->_trabajosGestion->cargarImgDB($this->_sess->get('carga_actual'), $foo->file_dst_name, $_nombre, $_directorio, $this->_conf['ruta_videos']);
				}else{*/
					$_imagen = $this->_trabajosGestion->cargarImgDB($this->_sess->get('carga_actual'), $foo->file_dst_name, $_nombre, $_seccion, $this->_conf['ruta_img_cargadas']);
				// }
				
				if($_imagen) $this->_sess->set('img_id', $_imagen);
				exit;
			}
		}
	}

	public function cargImgGal($_seccion, $_directorio)
	{
		$this->_acl->acceso('encargado_access');

		if(!isset($_SESSION['lote_img_gal'])) $_SESSION['lote_img_gal'] = array();
		
		if(!$this->_sess->get('img_lote')){
			$this->_sess->set('img_lote', 'si');
		}
		if (!empty($_FILES)) {

			$filename = strtolower($_FILES['file']['name']);
			$whitelist = array('jpg', 'png', 'gif', 'jpeg');
			// $whitelist = array('mp4','avi','mpeg');  
			$backlist = array('php', 'php3', 'php4', 'phtml','exe','bat','js');
			$tmp = explode('.', $filename);
			$file_extension = end($tmp); 
			if(!in_array($file_extension, $whitelist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}
			if(in_array($file_extension, $backlist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}


			$_info = pathinfo($_FILES['file']['name']);
			$_nombre_archivo =  basename($_FILES['file']['name'], '.' . $_info['extension']);
			$_nombre_interno = admin::cadenaAleatoriaSegura(15);
			//$_formatos_img = Conf\formatos_img::data();
			$size = getimagesize($_FILES['file']['tmp_name']);
			
			
			$foo = new upload($_FILES['file']);
			$foo->file_new_name_body 		= $_nombre_interno;
			//$foo->image_convert 			= 'jpg';
			$foo->image_resize          	= true;
			$foo->jpeg_quality          	= 100;
			$foo->png_compression       	= 10;
			// $foo->image_ratio_fill      	= true;
			$foo->image_ratio_y         	= true;
			// $foo->image_ratio_crop			= true;	
			if($_seccion=='banners'){
				$foo->image_x = $this->_conf['formatos_img'][$_directorio]['ancho'];
				// $tamañoOrig = admin::CapturarAnchoAlto($foo->file_dst_pathname);
				// $foo->image_y =  round(($foo->image_x/$tamañoOrig[0])*$tamañoOrig[1]);
				// $foo->image_y = $this->_conf['formatos_img'][$_directorio]['alto'];
			}else{
				$foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
				$foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];
			}
			//$foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
			//$foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];			
			//$foo->image_background_color 	= '#00000';			
			//$foo->process($this->_imagenesGestion->gestionDirectoriosOriginales($this->_conf['ruta_img_cargadas'] . 'slider' . DS . 'originales' . DS, date("Y-m-d")));
			$foo->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS);

			if($foo->processed){
				
				
				/*$chica = new upload($foo->file_dst_pathname);
				$chica->image_resize = true;
				$chica->image_ratio_crop = 'T';
				$chica->image_x = $this->_conf['formatos_img']['thumb']['ancho'];
				$chica->image_y = $this->_conf['formatos_img']['thumb']['alto'];
				$chica->process($this->_conf['ruta_img_cargadas'] . 'catalogos'. DS. $_directorio . DS . 'thumb'. DS);			
					
				$thumb = new upload($foo->file_dst_pathname);
				$thumb->image_resize = true;
				$thumb->image_ratio_crop = true;
				$thumb->image_x = $this->_conf['formatos_img'][$_seccion.'_thumb']['ancho'];
				$thumb->image_y = $this->_conf['formatos_img'][$_seccion.'_thumb']['alto'];
				$thumb->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS . 'miniatura'. DS);*/
				
				
								
				$_nombre = (isset($_POST['imagen_principal_nombre']) && $_POST['imagen_principal_nombre'] != '') ? $_POST['imagen_principal_nombre'] : $_FILES['file']['name'];
				// Base Datos
				// $_orientacion = explode("_",$_seccion);
				// $_imagen = $this->_trabajosGestion->cargarImgDBLote($this->_sess->get('carga_actual'), $foo->file_dst_name, $_nombre, $_orientacion[1]);
				$_imagen = $this->_trabajosGestion->cargarImgDBGal($this->_sess->get('carga_actual'), $foo->file_dst_name, $_nombre, $_seccion, $this->_conf['ruta_img_cargadas']);
				//if($_imagen) $this->_sess->set('img_id', $_imagen);
				if($_imagen) $_SESSION['lote_img_gal'][$_imagen] = 'ok';
				
				// echo "<pre>";print_r($_SESSION['lote_img_gal']);echo "</pre>";
				
			}
			
		}
	}

	public function cargImgGalEditar($_seccion, $_directorio)
	{
		$this->_acl->acceso('encargado_access');

		if(!isset($_SESSION['lote_img_gal'])) $_SESSION['lote_img_gal'] = array();
		
		if(!$this->_sess->get('img_lote')){
			$this->_sess->set('img_lote', 'si');
		}
		if (!empty($_FILES)) {

			$filename = strtolower($_FILES['file']['name']);
			$whitelist = array('jpg', 'png', 'gif', 'jpeg');
			// $whitelist = array('mp4','avi','mpeg');  
			$backlist = array('php', 'php3', 'php4', 'phtml','exe','bat','js');
			$tmp = explode('.', $filename);
			$file_extension = end($tmp); 
			if(!in_array($file_extension, $whitelist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}
			if(in_array($file_extension, $backlist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}


			$_info = pathinfo($_FILES['file']['name']);
			$_nombre_archivo =  basename($_FILES['file']['name'], '.' . $_info['extension']);
			$_nombre_interno = admin::cadenaAleatoriaSegura(15);
			//$_formatos_img = Conf\formatos_img::data();
			$size = getimagesize($_FILES['file']['tmp_name']);
			
			
			$foo = new upload($_FILES['file']);
			$foo->file_new_name_body 		= $_nombre_interno;
			//$foo->image_convert 			= 'jpg';
			$foo->image_resize          	= true;
			$foo->jpeg_quality          	= 100;
			$foo->png_compression       	= 10;
			// $foo->image_ratio_fill      	= true;
			$foo->image_ratio_y         	= true;
			if($_seccion=='banners'){
				$foo->image_x = $this->_conf['formatos_img'][$_directorio]['ancho'];
				// $foo->image_y = $this->_conf['formatos_img'][$_directorio]['alto'];
				// $tamañoOrig = admin::CapturarAnchoAlto($foo->file_dst_pathname);
				// $foo->image_y =  round(($foo->image_x/$tamañoOrig[0])*$tamañoOrig[1]);
			}else{
				$foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
				$foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];
			}
			// $foo->image_ratio_crop			= true;	
			//$foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
			//$foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];			
			//$foo->image_background_color 	= '#00000';			
			//$foo->process($this->_imagenesGestion->gestionDirectoriosOriginales($this->_conf['ruta_img_cargadas'] . 'slider' . DS . 'originales' . DS, date("Y-m-d")));
			$foo->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS);

			if($foo->processed){
				
				
				/*$chica = new upload($foo->file_dst_pathname);
				$chica->image_resize = true;
				$chica->image_ratio_crop = 'T';
				$chica->image_x = $this->_conf['formatos_img']['thumb']['ancho'];
				$chica->image_y = $this->_conf['formatos_img']['thumb']['alto'];
				$chica->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS . 'thumb'. DS);			
					
				$thumb = new upload($foo->file_dst_pathname);
				$thumb->image_resize = true;
				$thumb->image_ratio_crop = true;
				$thumb->image_x = $this->_conf['formatos_img'][$_seccion.'_thumb']['ancho'];
				$thumb->image_y = $this->_conf['formatos_img'][$_seccion.'_thumb']['alto'];
				$thumb->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS . 'miniatura'. DS);*/
				
				
								
				$_nombre = (isset($_POST['imagen_principal_nombre']) && $_POST['imagen_principal_nombre'] != '') ? $_POST['imagen_principal_nombre'] : $_FILES['file']['name'];
				// Base Datos
				// $_orientacion = explode("_",$_seccion);
				// $_imagen = $this->_trabajosGestion->cargarImgDBLote($this->_sess->get('edicion_actual'), $foo->file_dst_name, $_nombre,$_orientacion[1]);
				$_imagen = $this->_trabajosGestion->cargarImgDBGal($this->_sess->get('edicion_actual'), $foo->file_dst_name, $_nombre, $_seccion, $this->_conf['ruta_img_cargadas']);

				if($_imagen) $_SESSION['lote_img_gal'][$_imagen] = 'ok';
				
				/*echo "<pre>";
				print_r($_SESSION['lote_img_gal']);*/
				
			}
			
		}
	}
	
	
	public function cargImgNuevo($_seccion, $_directorio='')
	{
		$this->_acl->acceso('encargado_access');

		if (!empty($_FILES)) {

			$filename = strtolower($_FILES['file']['name']);
			$whitelist = array('jpg', 'png', 'gif', 'jpeg');
			// $whitelist = array('mp4','avi','mpeg');  
			$backlist = array('php', 'php3', 'php4', 'phtml','exe','bat','js');
			$tmp = explode('.', $filename);
			$file_extension = end($tmp); 
			if(!in_array($file_extension, $whitelist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}
			if(in_array($file_extension, $backlist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}

			$this->_trabajosGestion->limpiarDirectorio($this->_conf['ruta_img_cargadas'] . 'temporales/');
			$this->_trabajosGestion->limpiarDirectorio($this->_conf['ruta_img_cargadas'] . 'originales/');

			$this->_sess->destroy(array('img_id','img_path','img_nombre','img_tipo'));

			if($this->_sess->get('img_id')) $this->_sess->destroy('img_id');
			$_info = pathinfo($_FILES['file']['name']);
			$_nombre_archivo =  basename($_FILES['file']['name'], '.' . $_info['extension']);
			$_nombre_interno = admin::cadenaAleatoriaSegura(15);
			//$_formatos_img = Conf\formatos_img::data();
			$size = getimagesize($_FILES['file']['tmp_name']);
	
	
			$foo = new upload($_FILES['file']);
			$foo->file_new_name_body 		= $_nombre_interno;
			//$foo->image_convert 			= 'jpg';
			$foo->image_resize          	= true;
			//$foo->jpeg_quality          	= 100;
			$foo->png_compression       	= 10;
			//$foo->image_ratio_fill      	= true;
			//$foo->image_ratio_y         	= true;
			$foo->image_ratio_crop			= true;	
			// $foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
			// $foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];	
			$foo->image_ratio_y         = true;	
			if($_seccion=='banners'){
				$foo->image_x = $this->_conf['formatos_img'][$_directorio]['ancho'];
				// $foo->image_y = $this->_conf['formatos_img'][$_directorio]['alto'];
			}else{
				$foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
				// $foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];
			}	
			//$foo->image_background_color 	= '#00000';			
			//$foo->process($this->_imagenesGestion->gestionDirectoriosOriginales($this->_conf['ruta_img_cargadas'] . 'slider' . DS . 'originales' . DS, date("Y-m-d")));
			//$foo->process($this->_conf['ruta_img_cargadas'] . 'catalogos'. DS. $_directorio. DS . 'thumb'. DS);
			/*if($_seccion=='lanzamientos' || $_seccion=='tendencias' || $_seccion=='clubaliadas' || $_seccion=='capacitaciones' || $_seccion=='banners'|| $_seccion=='tutoriales' || $_seccion=='linearios'|| $_seccion=='eventos'){
				$foo->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS);
			}*/

			$foo->process($this->_conf['ruta_img_cargadas'] . 'originales'  . DS);
			
			
			if($foo->processed){			

			
				/*if($_seccion=='tendencias' || $_seccion=='capacitaciones'|| $_seccion=='tutoriales' || $_seccion=='linearios'|| $_seccion=='eventos'){
					
					$thumb = new upload($foo->file_dst_pathname);
					$thumb->image_resize = true;
					$thumb->image_ratio_crop = true;
					$thumb->image_x = $this->_conf['formatos_img'][$_seccion.'_thumb']['ancho'];
					$thumb->image_y = $this->_conf['formatos_img'][$_seccion.'_thumb']['alto'];
					$thumb->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS . 'thumb'. DS);
				
				}*/


								
				$_nombre = (isset($_POST['imagen_principal_nombre']) && $_POST['imagen_principal_nombre'] != '') ? $_POST['imagen_principal_nombre'] : $_FILES['file']['name'];
				
				// Base Datos
				// $_imagen = $this->_trabajosGestion->cargarImgDB($this->_sess->get('edicion_actual'), $foo->file_dst_name, $_nombre, $_seccion, $this->_conf['ruta_img_cargadas']);

				
				$this->_sess->set('img_nombre', $_nombre);
				$this->_sess->set('img_path', $foo->file_dst_name);
				// if($_imagen) $this->_sess->set('img_id', $_imagen);
				exit;
			}
		}
	}

	public function cargImgNuevoV2($_seccion, $_tipo='', $_directorio='')
	{
		$this->_acl->acceso('encargado_access');

		if (!empty($_FILES)) {

			$filename = strtolower($_FILES['file']['name']);
			$whitelist = array('jpg', 'png', 'gif', 'jpeg');
			// $whitelist = array('mp4','avi','mpeg');  
			$backlist = array('php', 'php3', 'php4', 'phtml','exe','bat','js');
			$tmp = explode('.', $filename);
			$file_extension = end($tmp); 
			if(!in_array($file_extension, $whitelist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}
			if(in_array($file_extension, $backlist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}

			$this->_trabajosGestion->limpiarDirectorio($this->_conf['ruta_img_cargadas'] . 'temporales/');
			$this->_trabajosGestion->limpiarDirectorio($this->_conf['ruta_img_cargadas'] . 'originales/');

			$this->_sess->destroy(array('img_id','img_path','img_nombre','img_tipo'));

			if($this->_sess->get('img_id')) $this->_sess->destroy('img_id');
			$_info = pathinfo($_FILES['file']['name']);
			$_nombre_archivo =  basename($_FILES['file']['name'], '.' . $_info['extension']);
			$_nombre_interno = admin::cadenaAleatoriaSegura(15);
			//$_formatos_img = Conf\formatos_img::data();
			$size = getimagesize($_FILES['file']['tmp_name']);
	
	
			$foo = new upload($_FILES['file']);
			$foo->file_new_name_body 		= $_nombre_interno;
			//$foo->image_convert 			= 'jpg';
			$foo->image_resize          	= true;
			//$foo->jpeg_quality          	= 100;
			$foo->png_compression       	= 10;
			//$foo->image_ratio_fill      	= true;
			//$foo->image_ratio_y         	= true;
			$foo->image_ratio_crop			= true;	
			// $foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
			// $foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];	
			$foo->image_ratio_y         = true;	
			if($_seccion=='banners'){
				$foo->image_x = $this->_conf['formatos_img'][$_directorio]['ancho'];
				// $foo->image_y = $this->_conf['formatos_img'][$_directorio]['alto'];
			}else{
				$foo->image_x = $this->_conf['formatos_img'][$_seccion.'_'.$_tipo]['ancho'];
				// $foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];
			}	
			//$foo->image_background_color 	= '#00000';			
			//$foo->process($this->_imagenesGestion->gestionDirectoriosOriginales($this->_conf['ruta_img_cargadas'] . 'slider' . DS . 'originales' . DS, date("Y-m-d")));
			//$foo->process($this->_conf['ruta_img_cargadas'] . 'catalogos'. DS. $_directorio. DS . 'thumb'. DS);
			/*if($_seccion=='lanzamientos' || $_seccion=='tendencias' || $_seccion=='clubaliadas' || $_seccion=='capacitaciones' || $_seccion=='banners'|| $_seccion=='tutoriales' || $_seccion=='linearios'|| $_seccion=='eventos'){
				$foo->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS);
			}*/

			$foo->process($this->_conf['ruta_img_cargadas'] . 'originales'  . DS);
			
			
			if($foo->processed){			

			
				/*if($_seccion=='tendencias' || $_seccion=='capacitaciones'|| $_seccion=='tutoriales' || $_seccion=='linearios'|| $_seccion=='eventos'){
					
					$thumb = new upload($foo->file_dst_pathname);
					$thumb->image_resize = true;
					$thumb->image_ratio_crop = true;
					$thumb->image_x = $this->_conf['formatos_img'][$_seccion.'_thumb']['ancho'];
					$thumb->image_y = $this->_conf['formatos_img'][$_seccion.'_thumb']['alto'];
					$thumb->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS . 'thumb'. DS);
				
				}*/


								
				$_nombre = (isset($_POST['imagen_principal_nombre']) && $_POST['imagen_principal_nombre'] != '') ? $_POST['imagen_principal_nombre'] : $_FILES['file']['name'];
				
				// Base Datos
				// $_imagen = $this->_trabajosGestion->cargarImgDB($this->_sess->get('edicion_actual'), $foo->file_dst_name, $_nombre, $_seccion, $this->_conf['ruta_img_cargadas']);

				
				$this->_sess->set('img_nombre', $_nombre);
				$this->_sess->set('img_path', $foo->file_dst_name);
				// if($_imagen) $this->_sess->set('img_id', $_imagen);
				exit;
			}
		}
	}



	/*public function cargImgNuevo($_tipo = false)
	{
		if(!$_tipo) return false;
		if(!in_array($_tipo, $this->_tipo_imagen)) return false;
		if($_tipo == 'slider') if(!isset($_GET['tipo']) || $_GET['tipo'] == '') return false;

		if (!empty($_FILES)) {

			$_formatos = Conf\formatos_img::data();
			$_formato = ($_tipo == 'slider' ? $_formatos[$_tipo][$_GET['tipo']] : $_formatos[$_tipo]);

			$_info = pathinfo($_FILES['file']['name']);
			$_nombre_archivo =  basename($_FILES['file']['name'], '.' . $_info['extension']);
			$_nombre_interno = ImagenesAdministrador::cadenaAleatoriaSegura(15);

		    $foo = new upload($_FILES['file']);
		    $foo->file_new_name_body 	= $_nombre_interno;
		    $foo->image_resize          = true;
		    //$foo->image_convert 		= 'jpg';
		    //$foo->jpeg_quality 		= 100;
			$foo->image_ratio_y         = true;
			$foo->image_x               = ($_tipo == 'slider') ? $_formato['ancho'] : $_formato['principal']['ancho'];
			//$foo->file_name_body_pre 	= 'pre_';
			$foo->process(Conf\ruta_img_cargadas::data() . 'noticias' . DS . 'originales');

		    if($foo->processed){
		    	if($_tipo == 'iconosmarcas' || $_tipo == 'poster'){
					
					// Creamos una sesion con el path de la imagen
					//$this->_sess->set('img_path', $_nombre_interno . '.png');
					$this->_sess->set('img_path', $foo->file_dst_name);
					//echo $this->_sess->get('img_path');
				
				}else{
					// NUEVO sistema, entrelazado JPEG
					$_im = imagecreatefromjpeg(Conf\ruta_img_cargadas::data() . 'noticias' . DS . 'originales' . DS . 'pre_' . $_nombre_interno . '.jpg');
					imageinterlace($_im, 1);
					imagejpeg($_im, Conf\ruta_img_cargadas::data() . 'noticias' . DS . 'originales' . DS . $_nombre_interno . '.jpg', 100);
					imagedestroy($_im);
					unlink(Conf\ruta_img_cargadas::data() . 'noticias' . DS . 'originales' . DS . 'pre_' . $_nombre_interno . '.jpg');
	
					// Creamos una sesion con el path de la imagen
					$this->_sess->set('img_path', $_nombre_interno . '.jpg');
				}				
				

				// sesion con la categoria
				$this->_sess->set('img_categoria', $_POST['imagen_categoria']);
				// Y agregamos (06/01/2015) una sesion con el nombre de la imagen
				if(isset($_POST['imagen_principal_nombre'])){
					$this->_sess->set('img_nombre', ($_POST['imagen_principal_nombre'] != '') ? $_POST['imagen_principal_nombre'] : $_nombre_archivo);
				}else{
					$this->_sess->set('img_nombre', $_nombre_archivo);
				}
		    }
		}
	}*/

	public function recortarimagennuevo($_accion, $_retorno, $_tipo = false)
	{
		$this->_view->accion = $_accion;
		$this->_view->_retorno = (int) $_retorno;
		$_identificador = ($_accion == 'editar') ? $this->_sess->get('edicion_actual') : $this->_sess->get('carga_actual');

		// if(!$_tipo) return false;
		// if(!in_array($_tipo, $this->_tipo_imagen)) return false;
		// if($_tipo == 'slider') if(!isset($_GET['tipo']) || $_GET['tipo'] == '') exit('Error inesperado 1: No lee un parametro get.');

		$this->_view->tip = $_tipo;
		$this->_view->tipo = $_tipo;
		// $this->_view->formatos = Conf\formatos_img::data();
		// $this->_view->formato = ($_tipo == 'slider' ? $this->_view->formatos[$_tipo][$_GET['tipo']] : $this->_view->formatos[$_tipo]);

		// 30/12/2014 --> Nuevo sistema: se crea un temporal con la imagen
		$_directorio_temporal = $this->_trabajosGestion->crearDirTempImagenPrinc($_identificador, $_tipo);

		if($_POST){
			if(isset($_POST['envio02']) && $_POST['envio02'] == 1){

				$targ_w = $this->_conf['formatos_img'][$_tipo]['ancho'];
				$targ_h = $this->_conf['formatos_img'][$_tipo]['alto'];
				$jpeg_quality = 100;
			
				/*ATENCION!! Si la imagen sube en negro, hay que agregar la ruta completa ($this->_conf['base_url'])*/
				 $src = $this->_conf['ruta_img_cargadas'] . 'originales/'. $this->_sess->get('img_path'); 
				// $src = $this->_conf['base_url'] . "public/img/subidas/originales/". $this->_sess->get('img_path'); 
				
				/*$_ext = pathinfo($src);
				if ($_ext["extension"] == "png"){

					$img_r = imagecreatefrompng($src);
					$dst_r = imagecreatetruecolor($targ_w, $targ_h);	
					imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);
					$_nom_archivo = uniqid() . '.png';
					$_img  = $this->_conf['ruta_img_cargadas'] . 'originales/' . $_nom_archivo;
					// $_img = $this->_conf['base_url'] . "public/img/subidas/originales/". $_nom_archivo; 
					imagepng($dst_r, $_img);

				}else{

					$img_r = imagecreatefromjpeg($src);
					$dst_r = imagecreatetruecolor($targ_w, $targ_h);			
					imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);
					$_nom_archivo = uniqid() . '.jpg';
					$_img  = $this->_conf['ruta_img_cargadas'] . 'originales/' . $_nom_archivo;
					imagejpeg($dst_r, $_img, $jpeg_quality);

				}*/


				$img_r = imagecreatefromjpeg($src);
				$dst_r = imagecreatetruecolor($targ_w, $targ_h);			
				imagecopyresampled($dst_r,$img_r,0,0,$_POST['x'],$_POST['y'],$targ_w,$targ_h,$_POST['w'],$_POST['h']);
				$_nom_archivo = uniqid() . '.jpg';
				$_img  = $this->_conf['ruta_img_cargadas'] . 'originales/' . $_nom_archivo;
				imagejpeg($dst_r, $_img, $jpeg_quality);
				
				

				if($_tipo == 'tendencias' || $_tipo == 'tutoriales' || $_tipo == 'linearios' || $_tipo == 'eventos' || $_tipo == 'capacitaciones'){

					$principal = new upload($_img);
					$principal->image_resize = true;
					$principal->image_ratio_crop = true;
					//$principal->jpeg_quality = 100;
					$principal->image_x = $this->_conf['formatos_img'][$_tipo]['ancho'];
					$principal->image_y = $this->_conf['formatos_img'][$_tipo]['alto'];
					//$principal->file_name_body_pre = 'principal_';
					
					$principal->process($_directorio_temporal);

					if($principal->processed){

						$file_parts = pathinfo($_img);

						if ($file_parts["extension"] == "png"){
						// if (exif_imagetype($_directorio_temporal . DS . $_nom_archivo) == IMAGETYPE_PNG) {
							$_im = imagecreatefrompng($_directorio_temporal . DS . 'principal_' . $_nom_archivo);
							imageinterlace($_im, 1);
							imagealphablending($_im, false);
							imagesavealpha($_im, true);
							imagepng($_im, $_directorio_temporal . DS .  $_nom_archivo);
							imagedestroy($_im);
							unlink($_directorio_temporal . DS . 'principal_' . $_nom_archivo);
						}else{					
							$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'principal_' . $_nom_archivo);
							imageinterlace($_im, 1);
							imagejpeg($_im, $_directorio_temporal . DS . $_nom_archivo, 100);
							imagedestroy($_im);
							unlink($_directorio_temporal . DS . 'principal_' . $_nom_archivo);				
						}
					}else{
						echo  $principal->log. '<br>';
						exit();
					}

										

					// thumb general
					$thumb = new upload($_img);
					$thumb->image_resize = true;
					$thumb->image_ratio_crop = true;
					//$thumb->jpeg_quality = 100;
					/*$thumb->image_x = 400;
					$thumb->image_y = 200;
					$thumb->process($_directorio_temporal . DS . 'thumb');
					
					if (exif_imagetype($_directorio_temporal . DS . 'thumb' . DS . $_nom_archivo) == IMAGETYPE_PNG) {
						$_im = imagecreatefrompng($_directorio_temporal . DS . 'thumb' . DS . $_nom_archivo);
						imageinterlace($_im, 1);
						imagealphablending($_im, false);
						imagesavealpha($_im, true);
						imagepng($_im, $_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_nom_archivo);
						imagedestroy($_im);
						unlink($_directorio_temporal . DS . 'thumb' . DS . $_nom_archivo);
					}else{					
						$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'thumb' . DS . $_nom_archivo);
						imageinterlace($_im, 1);
						imagejpeg($_im, $_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_nom_archivo, 100);
						imagedestroy($_im);
						unlink($_directorio_temporal . DS . 'thumb' . DS . $_nom_archivo);				
					}*/

					$thumb->image_x = $this->_conf['formatos_img'][$_tipo.'_thumb']['ancho'];
					$thumb->image_y = $this->_conf['formatos_img'][$_tipo.'_thumb']['alto'];
					//$thumb->image_background_color = $_formatos['color_fondo_img'];
					$thumb->process($_directorio_temporal . DS . 'thumb');

					if($thumb->processed){

						$file_parts = pathinfo($_img);

						if ($file_parts["extension"] == "png"){					
						// if (exif_imagetype($_directorio_temporal . DS . 'thumb' . DS . $_nom_archivo) == IMAGETYPE_PNG) {
							$_im = imagecreatefrompng($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_nom_archivo);
							imageinterlace($_im, 1);
							imagealphablending($_im, false);
							imagesavealpha($_im, true);
							imagepng($_im, $_directorio_temporal . DS . 'thumb' . DS . $_nom_archivo);
							imagedestroy($_im);
							unlink($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_nom_archivo);
						}else{					
							$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_nom_archivo);
							imageinterlace($_im, 1);
							imagejpeg($_im, $_directorio_temporal . DS . 'thumb' . DS . $_nom_archivo, 100);
							imagedestroy($_im);
							unlink($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_nom_archivo);				
						}
					}else{
						echo  $thumb->log. '<br>';
						exit();
					}
					

				}else{

					$principal = new upload($_img);
					$principal->image_resize = true;
					$principal->image_ratio_crop = true;
					//$principal->jpeg_quality = 100;
					$principal->image_x = $this->_conf['formatos_img'][$_tipo]['ancho'];
					$principal->image_y = $this->_conf['formatos_img'][$_tipo]['alto'];
					//$principal->file_name_body_pre = 'principal_';
					
					$principal->process($_directorio_temporal);

					if($principal->processed){

						$file_parts = pathinfo($_img);

						if ($file_parts["extension"] == "png"){
						// if (exif_imagetype($_directorio_temporal . DS . $_nom_archivo) == IMAGETYPE_PNG) {
							$_im = imagecreatefrompng($_directorio_temporal . DS . 'principal_' . $_nom_archivo);
							imageinterlace($_im, 1);
							imagealphablending($_im, false);
							imagesavealpha($_im, true);
							imagepng($_im, $_directorio_temporal . DS .  $_nom_archivo);
							imagedestroy($_im);
							unlink($_directorio_temporal . DS . 'principal_' . $_nom_archivo);
						}else{					
							$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'principal_' . $_nom_archivo);
							imageinterlace($_im, 1);
							imagejpeg($_im, $_directorio_temporal . DS . $_nom_archivo, 100);
							imagedestroy($_im);
							unlink($_directorio_temporal . DS . 'principal_' . $_nom_archivo);				
						}
					}else{
						echo  $principal->log. '<br>';
						exit();
					}



				}

				//Eliminamos la imagen original
				// $this->_trabajosGestion->eliminarImagenOriginal($this->_conf['ruta_img_cargadas'] . 'originales/', $this->_sess->get('img_path'));
				// $this->_trabajosGestion->eliminarImagenOriginal($this->_conf['ruta_img_cargadas'] . 'originales/', $_nom_archivo);

				// $this->_sess->set('tipo_recorte', 'manual');
				$this->_sess->set('img_path', $_nom_archivo);
				$this->_sess->set('img_tipo', $_tipo);
				echo 2;
			}
		}
		$this->_view->titulo = 'Administrador - Recortar Imagen';
		$this->_view->renderizar('recortarimagennuevo', 'imagenes', 'recorte');
	}


	public function recortar_imagen_auto($_accion, $_tipo = false)
	{
		$this->_acl->acceso('encargado_access');

		$ruta_imagen  = $this->_conf['ruta_img_cargadas'] . 'originales/' . $this->_sess->get('img_path');
		$this->_view->accion = $_accion;
		$_new_name = uniqid();
		$_identificador = ($_accion == 'cargar') ? $this->_sess->get('carga_actual') : $this->_sess->get('edicion_actual');

		// if(!$_tipo) return false;
		// if(!in_array($_tipo, $this->_tipo_imagen)) return false;
		// if($_tipo == 'slider') if(!isset($_GET['tipo']) || $_GET['tipo'] == '') exit('Error inesperado 2: No lee un parametro get.');

		$this->_view->tipo =  $_tipo;
		// $_formatos = Conf\formatos_img::data();
		// $_formato = ($_tipo == 'slider' ? $_formatos[$_tipo][$_GET['tipo']] : $_formatos[$_tipo]);

		// 30/12/2014 --> Nuevo sistema: se crea un temporal con la imagen
		$_directorio_temporal = $this->_trabajosGestion->crearDirTempImagenPrinc($_identificador, $_tipo);
		
		// chequeamos el formato de la imagen
		// $size = getimagesize($ruta_imagen);

		if($_tipo == 'tendencias' || $_tipo == 'tutoriales' || $_tipo == 'linearios' || $_tipo == 'eventos' || $_tipo == 'capacitaciones'){

			
			$principal = new upload($ruta_imagen);
			$principal->file_new_name_body = $_new_name;
			$principal->image_resize = true;
			/*$principal->image_ratio_fill = true;
			$principal->jpeg_quality = 100;*/
			// $principal->image_y = $_formato['principal']['alto'];
			// $principal->image_x = $_formato['principal']['ancho'];

			$principal->image_x = $this->_conf['formatos_img'][$_tipo]['ancho'];
			$principal->image_y = $this->_conf['formatos_img'][$_tipo]['alto'];
			//$principal->image_background_color = $_formatos['color_fondo_img'];
			//$principal->file_name_body_pre 	= 'principal_';
			$principal->process($_directorio_temporal);

			if($principal->processed){

				$file_parts = pathinfo($ruta_imagen);

				if ($file_parts["extension"] == "png"){
				// if (exif_imagetype($_directorio_temporal . DS . $_new_name . '.png') == IMAGETYPE_PNG) {
					$_im = imagecreatefrompng($_directorio_temporal . DS . 'principal_' . $_new_name . '.png');
					imageinterlace($_im, 1);
					imagealphablending($_im, false);
					imagesavealpha($_im, true);
					imagepng($_im, $_directorio_temporal . DS .  $_new_name . '.png');
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'principal_' . $_new_name . '.png');
				}else{					
					$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'principal_' . $_new_name . '.jpg');
					imageinterlace($_im, 1);
					imagejpeg($_im, $_directorio_temporal . DS . $_new_name . '.jpg', 100);
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'principal_' . $_new_name . '.jpg');				
				}
			}else{
				echo  $principal->log. '<br>';
				exit();
			}

			
			// thumb
			$thumb = new upload($ruta_imagen);
			$thumb->file_new_name_body = $_new_name;
			$thumb->image_resize = true;
			// $thumb->image_ratio_fill = true;
			// $thumb->jpeg_quality = 100;
			// $thumb->image_x = 400;
			// $thumb->image_y = 200;
			$thumb->image_x = $this->_conf['formatos_img'][$_tipo.'_thumb']['ancho'];
			$thumb->image_y = $this->_conf['formatos_img'][$_tipo.'_thumb']['alto'];
			//$thumb->image_background_color = $_formatos['color_fondo_img'];
			$thumb->process($_directorio_temporal . DS . 'thumb');

			if($thumb->processed){

				$file_parts = pathinfo($ruta_imagen);

				if ($file_parts["extension"] == "png"){			
				// if (exif_imagetype($_directorio_temporal . DS . 'thumb' . DS . $_new_name . '.png') == IMAGETYPE_PNG) {
					$_im = imagecreatefrompng($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_new_name . '.png');
					imageinterlace($_im, 1);
					imagealphablending($_im, false);
					imagesavealpha($_im, true);
					imagepng($_im, $_directorio_temporal . DS . 'thumb' . DS . $_new_name . '.png');
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_new_name . '.png');
				}else{					
					$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_new_name . '.jpg');
					imageinterlace($_im, 1);
					imagejpeg($_im, $_directorio_temporal . DS . 'thumb' . DS . $_new_name . '.jpg', 100);
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_new_name . '.jpg');				
				}	
			}else{
				echo  $thumb->log. '<br>';
				exit();
			}

			

		}else{

			$principal = new upload($ruta_imagen);
			$principal->file_new_name_body = $_new_name;
			$principal->image_resize = true;
			/*$principal->image_ratio_fill = true;
			$principal->jpeg_quality = 100;*/
			// $principal->image_y = $_formato['principal']['alto'];
			// $principal->image_x = $_formato['principal']['ancho'];

			$principal->image_x = $this->_conf['formatos_img'][$_tipo]['ancho'];
			$principal->image_y = $this->_conf['formatos_img'][$_tipo]['alto'];
			//$principal->image_background_color = $_formatos['color_fondo_img'];
			//$principal->file_name_body_pre 	= 'principal_';
			$principal->process($_directorio_temporal);

			if($principal->processed){

				$file_parts = pathinfo($ruta_imagen);

				if ($file_parts["extension"] == "png"){
				// if (exif_imagetype($_directorio_temporal . DS . $_new_name . '.png') == IMAGETYPE_PNG) {
					$_im = imagecreatefrompng($_directorio_temporal . DS . 'principal_' . $_new_name . '.png');
					imageinterlace($_im, 1);
					imagealphablending($_im, false);
					imagesavealpha($_im, true);
					imagepng($_im, $_directorio_temporal . DS .  $_new_name . '.png');
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'principal_' . $_new_name . '.png');
				}else{					
					$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'principal_' . $_new_name . '.jpg');
					imageinterlace($_im, 1);
					imagejpeg($_im, $_directorio_temporal . DS . $_new_name . '.jpg', 100);
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'principal_' . $_new_name . '.jpg');				
				}
			}else{
				echo  $principal->log. '<br>';
				exit();
			}

			

		}


		//Eliminamos la imagen original
		$this->_trabajosGestion->eliminarImagenOriginal($this->_conf['ruta_img_cargadas'] . 'originales/', $this->_sess->get('img_path'));
		// reescribimos la sesion con el nombre de la imagen
		// $this->_sess->set('tipo_recorte', 'auto');
		$this->_sess->set('img_path', $principal->file_dst_name);
		$this->_sess->set('img_tipo', $_tipo);
	}

	public function recortar_imagen_auto_V2($_accion, $_tipo = false, $_pos= false)
	{
		$this->_acl->acceso('encargado_access');

		$ruta_imagen  = $this->_conf['ruta_img_cargadas'] . 'originales/' . $this->_sess->get('img_path');
		$this->_view->accion = $_accion;
		$_new_name = uniqid();
		$_identificador = ($_accion == 'cargar') ? $this->_sess->get('carga_actual') : $this->_sess->get('edicion_actual');

		// if(!$_tipo) return false;
		// if(!in_array($_tipo, $this->_tipo_imagen)) return false;
		// if($_tipo == 'slider') if(!isset($_GET['tipo']) || $_GET['tipo'] == '') exit('Error inesperado 2: No lee un parametro get.');

		$this->_view->tipo =  $_tipo;
		// $_formatos = Conf\formatos_img::data();
		// $_formato = ($_tipo == 'slider' ? $_formatos[$_tipo][$_GET['tipo']] : $_formatos[$_tipo]);

		// 30/12/2014 --> Nuevo sistema: se crea un temporal con la imagen
		$_directorio_temporal = $this->_trabajosGestion->crearDirTempImagenPrinc($_identificador, $_tipo);
		
		// chequeamos el formato de la imagen
		// $size = getimagesize($ruta_imagen);

		if($_tipo == 'tendencias' || $_tipo == 'tutoriales' || $_tipo == 'linearios' || $_tipo == 'eventos' || $_tipo == 'capacitaciones'){

			
			$principal = new upload($ruta_imagen);
			$principal->file_new_name_body = $_new_name;
			$principal->image_resize = true;
			/*$principal->image_ratio_fill = true;
			$principal->jpeg_quality = 100;*/
			// $principal->image_y = $_formato['principal']['alto'];
			// $principal->image_x = $_formato['principal']['ancho'];

			$principal->image_x = $this->_conf['formatos_img'][$_tipo.'_'.$_pos]['ancho'];
			$principal->image_y = $this->_conf['formatos_img'][$_tipo.'_'.$_pos]['alto'];
			//$principal->image_background_color = $_formatos['color_fondo_img'];
			//$principal->file_name_body_pre 	= 'principal_';
			$principal->process($_directorio_temporal);

			if($principal->processed){

				$file_parts = pathinfo($ruta_imagen);

				if ($file_parts["extension"] == "png"){
				// if (exif_imagetype($_directorio_temporal . DS . $_new_name . '.png') == IMAGETYPE_PNG) {
					$_im = imagecreatefrompng($_directorio_temporal . DS . 'principal_' . $_new_name . '.png');
					imageinterlace($_im, 1);
					imagealphablending($_im, false);
					imagesavealpha($_im, true);
					imagepng($_im, $_directorio_temporal . DS .  $_new_name . '.png');
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'principal_' . $_new_name . '.png');
				}else{					
					$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'principal_' . $_new_name . '.jpg');
					imageinterlace($_im, 1);
					imagejpeg($_im, $_directorio_temporal . DS . $_new_name . '.jpg', 100);
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'principal_' . $_new_name . '.jpg');				
				}
			}else{
				echo  $principal->log. '<br>';
				exit();
			}

			
			// thumb
			$thumb = new upload($ruta_imagen);
			$thumb->file_new_name_body = $_new_name;
			$thumb->image_resize = true;
			// $thumb->image_ratio_fill = true;
			// $thumb->jpeg_quality = 100;
			// $thumb->image_x = 400;
			// $thumb->image_y = 200;
			$thumb->image_x = $this->_conf['formatos_img'][$_tipo.'_thumb']['ancho'];
			$thumb->image_y = $this->_conf['formatos_img'][$_tipo.'_thumb']['alto'];
			//$thumb->image_background_color = $_formatos['color_fondo_img'];
			$thumb->process($_directorio_temporal . DS . 'thumb');

			if($thumb->processed){

				$file_parts = pathinfo($ruta_imagen);

				if ($file_parts["extension"] == "png"){			
				// if (exif_imagetype($_directorio_temporal . DS . 'thumb' . DS . $_new_name . '.png') == IMAGETYPE_PNG) {
					$_im = imagecreatefrompng($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_new_name . '.png');
					imageinterlace($_im, 1);
					imagealphablending($_im, false);
					imagesavealpha($_im, true);
					imagepng($_im, $_directorio_temporal . DS . 'thumb' . DS . $_new_name . '.png');
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_new_name . '.png');
				}else{					
					$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_new_name . '.jpg');
					imageinterlace($_im, 1);
					imagejpeg($_im, $_directorio_temporal . DS . 'thumb' . DS . $_new_name . '.jpg', 100);
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'thumb' . DS . 'thumb_' . $_new_name . '.jpg');				
				}	
			}else{
				echo  $thumb->log. '<br>';
				exit();
			}

			

		}else{

			$principal = new upload($ruta_imagen);
			$principal->file_new_name_body = $_new_name;
			$principal->image_resize = true;
			/*$principal->image_ratio_fill = true;
			$principal->jpeg_quality = 100;*/
			// $principal->image_y = $_formato['principal']['alto'];
			// $principal->image_x = $_formato['principal']['ancho'];

			$principal->image_x = $this->_conf['formatos_img'][$_tipo.'_'.$_pos]['ancho'];
			$principal->image_y = $this->_conf['formatos_img'][$_tipo.'_'.$_pos]['alto'];
			//$principal->image_background_color = $_formatos['color_fondo_img'];
			//$principal->file_name_body_pre 	= 'principal_';
			$principal->process($_directorio_temporal);

			if($principal->processed){

				$file_parts = pathinfo($ruta_imagen);

				if ($file_parts["extension"] == "png"){
				// if (exif_imagetype($_directorio_temporal . DS . $_new_name . '.png') == IMAGETYPE_PNG) {
					$_im = imagecreatefrompng($_directorio_temporal . DS . 'principal_' . $_new_name . '.png');
					imageinterlace($_im, 1);
					imagealphablending($_im, false);
					imagesavealpha($_im, true);
					imagepng($_im, $_directorio_temporal . DS .  $_new_name . '.png');
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'principal_' . $_new_name . '.png');
				}else{					
					$_im = imagecreatefromjpeg($_directorio_temporal . DS . 'principal_' . $_new_name . '.jpg');
					imageinterlace($_im, 1);
					imagejpeg($_im, $_directorio_temporal . DS . $_new_name . '.jpg', 100);
					imagedestroy($_im);
					unlink($_directorio_temporal . DS . 'principal_' . $_new_name . '.jpg');				
				}
			}else{
				echo  $principal->log. '<br>';
				exit();
			}

			

		}


		//Eliminamos la imagen original
		$this->_trabajosGestion->eliminarImagenOriginal($this->_conf['ruta_img_cargadas'] . 'originales/', $this->_sess->get('img_path'));
		// reescribimos la sesion con el nombre de la imagen
		// $this->_sess->set('tipo_recorte', 'auto');
		$this->_sess->set('img_path', $principal->file_dst_name);
		$this->_sess->set('img_tipo', $_tipo);
		$this->_sess->set('img_pos', $_pos);
	}
	
	
	public function cargImgLote($_seccion, $_directorio)
	{
		$this->_acl->acceso('encargado_access');

		if(!isset($_SESSION['lote_img_gal'])) $_SESSION['lote_img_gal'] = array();
		
		if(!$this->_sess->get('img_lote')){
			$this->_sess->set('img_lote', 'si');
		}
		if (!empty($_FILES)) {

			$filename = strtolower($_FILES['file']['name']);
			$whitelist = array('jpg', 'png', 'gif', 'jpeg');
			// $whitelist = array('mp4','avi','mpeg');  
			$backlist = array('php', 'php3', 'php4', 'phtml','exe','bat','js');
			$tmp = explode('.', $filename);
			$file_extension = end($tmp); 
			if(!in_array($file_extension, $whitelist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}
			if(in_array($file_extension, $backlist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}


			$_info = pathinfo($_FILES['file']['name']);
			$_nombre_archivo =  basename($_FILES['file']['name'], '.' . $_info['extension']);
			$_nombre_interno = admin::cadenaAleatoriaSegura(15);
			//$_formatos_img = Conf\formatos_img::data();
			$size = getimagesize($_FILES['file']['tmp_name']);
			
			
			$foo = new upload($_FILES['file']);
			$foo->file_new_name_body 		= $_nombre_interno;
			//$foo->image_convert 			= 'jpg';
			$foo->image_resize          	= false;
			$foo->jpeg_quality          	= 100;
			$foo->png_compression       	= 10;
			/*$foo->image_ratio_fill      	= true;
			$foo->image_ratio_y         	= true;*/
			$foo->image_ratio_crop			= true;	
			//$foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
			//$foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];			
			//$foo->image_background_color 	= '#00000';			
			//$foo->process($this->_imagenesGestion->gestionDirectoriosOriginales($this->_conf['ruta_img_cargadas'] . 'slider' . DS . 'originales' . DS, date("Y-m-d")));
			$foo->process($this->_conf['ruta_img_cargadas'] . 'catalogos'. DS. $_directorio. DS);
			if($foo->processed){
				
				
				/*$chica = new upload($foo->file_dst_pathname);
				$chica->image_resize = true;
				$chica->image_ratio_crop = 'T';
				$chica->image_x = $this->_conf['formatos_img']['thumb']['ancho'];
				$chica->image_y = $this->_conf['formatos_img']['thumb']['alto'];
				$chica->process($this->_conf['ruta_img_cargadas'] . 'catalogos'. DS. $_directorio . DS . 'thumb'. DS);			
					
				$thumb = new upload($foo->file_dst_pathname);
				$thumb->image_resize = true;
				$thumb->image_ratio_crop = true;
				$thumb->image_x = $this->_conf['formatos_img'][$_seccion.'_thumb']['ancho'];
				$thumb->image_y = $this->_conf['formatos_img'][$_seccion.'_thumb']['alto'];
				$thumb->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS . 'miniatura'. DS);*/
				
				
								
				$_nombre = (isset($_POST['imagen_principal_nombre']) && $_POST['imagen_principal_nombre'] != '') ? $_POST['imagen_principal_nombre'] : $_FILES['file']['name'];
				// Base Datos
				$_orientacion = explode("_",$_seccion);
				$_imagen = $this->_trabajosGestion->cargarImgDBLote($this->_sess->get('carga_actual'), $foo->file_dst_name, $_nombre, $_orientacion[1]);
				//if($_imagen) $this->_sess->set('img_id', $_imagen);
				if($_imagen) $_SESSION['lote_img_gal'][$_imagen] = 'ok';
				
				// echo "<pre>";print_r($_SESSION['lote_img_gal']);echo "</pre>";
				
			}
			
		}
	}
	
	
	public function cargImgLoteEditar($_seccion, $_directorio)
	{
		$this->_acl->acceso('encargado_access');

		if(!isset($_SESSION['lote_img_gal'])) $_SESSION['lote_img_gal'] = array();
		
		if(!$this->_sess->get('img_lote')){
			$this->_sess->set('img_lote', 'si');
		}
		if (!empty($_FILES)) {

			$filename = strtolower($_FILES['file']['name']);
			$whitelist = array('jpg', 'png', 'gif', 'jpeg');
			// $whitelist = array('mp4','avi','mpeg');  
			$backlist = array('php', 'php3', 'php4', 'phtml','exe','bat','js');
			$tmp = explode('.', $filename);
			$file_extension = end($tmp); 
			if(!in_array($file_extension, $whitelist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}
			if(in_array($file_extension, $backlist)){
				$this->_sess->set('img_error', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}


			$_info = pathinfo($_FILES['file']['name']);
			$_nombre_archivo =  basename($_FILES['file']['name'], '.' . $_info['extension']);
			$_nombre_interno = admin::cadenaAleatoriaSegura(15);
			//$_formatos_img = Conf\formatos_img::data();
			$size = getimagesize($_FILES['file']['tmp_name']);
			
			
			$foo = new upload($_FILES['file']);
			$foo->file_new_name_body 		= $_nombre_interno;
			//$foo->image_convert 			= 'jpg';
			$foo->image_resize          	= false;
			$foo->jpeg_quality          	= 100;
			$foo->png_compression       	= 10;
			/*$foo->image_ratio_fill      	= true;
			$foo->image_ratio_y         	= true;*/
			$foo->image_ratio_crop			= true;	
			//$foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
			//$foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];			
			//$foo->image_background_color 	= '#00000';			
			//$foo->process($this->_imagenesGestion->gestionDirectoriosOriginales($this->_conf['ruta_img_cargadas'] . 'slider' . DS . 'originales' . DS, date("Y-m-d")));
			$foo->process($this->_conf['ruta_img_cargadas'] . 'catalogos'. DS. $_directorio. DS);
			if($foo->processed){
				
				
				/*$chica = new upload($foo->file_dst_pathname);
				$chica->image_resize = true;
				$chica->image_ratio_crop = 'T';
				$chica->image_x = $this->_conf['formatos_img']['thumb']['ancho'];
				$chica->image_y = $this->_conf['formatos_img']['thumb']['alto'];
				$chica->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS . 'thumb'. DS);			
					
				$thumb = new upload($foo->file_dst_pathname);
				$thumb->image_resize = true;
				$thumb->image_ratio_crop = true;
				$thumb->image_x = $this->_conf['formatos_img'][$_seccion.'_thumb']['ancho'];
				$thumb->image_y = $this->_conf['formatos_img'][$_seccion.'_thumb']['alto'];
				$thumb->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS . 'miniatura'. DS);*/
				
				
								
				$_nombre = (isset($_POST['imagen_principal_nombre']) && $_POST['imagen_principal_nombre'] != '') ? $_POST['imagen_principal_nombre'] : $_FILES['file']['name'];
				// Base Datos
				$_orientacion = explode("_",$_seccion);
				$_imagen = $this->_trabajosGestion->cargarImgDBLote($this->_sess->get('edicion_actual'), $foo->file_dst_name, $_nombre,$_orientacion[1]);
				//if($_imagen) $this->_sess->set('img_id', $_imagen);
				if($_imagen) $_SESSION['lote_img_gal'][$_imagen] = 'ok';
				
				/*echo "<pre>";
				print_r($_SESSION['lote_img_gal']);*/
				
			}
			
		}
	}
	
	
	
	public function traerimagenfinal($_inst)
	{	
		$this->_acl->acceso('encargado_access');

		$_inst = (int) $_inst;
		
		$_identificador = $this->_sess->get('carga_actual') ? $this->_sess->get('carga_actual') : $this->_sess->get('edicion_actual');

		// Imagen principal
		/*if($this->_sess->get('tipo_recorte') =='auto'){
			$_img = 'public/img/subidas/temporales' . DS . $_identificador . DS . $this->_sess->get('img_path');
		}else{
			$_img = 'public/img/subidas/temporales' . DS . $_identificador . DS . $this->_sess->get('img_path_2');
		}*/
		$_img = 'public/img/subidas/temporales' . DS . $_identificador . DS . $this->_sess->get('img_path');
		

		echo '<div class="col-lg-12">
			  	<img src="' . $this->_conf['base_url'] . $_img . '" class="img-responsive">	
			  	<div style="margin-top:20px;">		  	
		     		<a href="javascript:void(0);" onclick="consolidar_imagen(1);" class="btn btn-info">Consolidar imagen</a>&nbsp;&nbsp;
					<a href="javascript:void(0);" onclick="consolidar_imagen(2);" class="btn btn-danger">Descartar Imagen</a>
				</div>			  	
			</div>';
		exit();
	}

	public function traerimagenfinal_V2($_inst, $_pos)
	{	
		$this->_acl->acceso('encargado_access');

		$_inst = (int) $_inst;
		
		$_identificador = $this->_sess->get('carga_actual') ? $this->_sess->get('carga_actual') : $this->_sess->get('edicion_actual');

		// Imagen principal
		/*if($this->_sess->get('tipo_recorte') =='auto'){
			$_img = 'public/img/subidas/temporales' . DS . $_identificador . DS . $this->_sess->get('img_path');
		}else{
			$_img = 'public/img/subidas/temporales' . DS . $_identificador . DS . $this->_sess->get('img_path_2');
		}*/
		$_img = 'public/img/subidas/temporales' . DS . $_identificador . DS . $this->_sess->get('img_path');
		

		echo '<div class="col-lg-12">
			  	<img src="' . $this->_conf['base_url'] . $_img . '" class="img-responsive">	
			  	<div style="margin-top:20px;">		  	
		     		<a href="javascript:void(0);" onclick="consolidar_imagen_'.$_pos.'(\''.$this->_sess->get('_item_bloque').'\');" class="btn btn-info">Consolidar imagen</a>&nbsp;&nbsp;
					<a href="javascript:void(0);" onclick="cancelarimagen_'.$_pos.'();" class="btn btn-danger">Descartar Imagen</a>
				</div>			  	
			</div>';
		exit();
	}

	public function cancelarimagen()
	{
		$this->_acl->acceso('encargado_access');

		$this->_trabajosGestion->limpiarDirectorio($this->_conf['ruta_img_cargadas'] . 'temporales/');
		$this->_trabajosGestion->limpiarDirectorio($this->_conf['ruta_img_cargadas'] . 'originales/');

		$this->_sess->destroy(array('img_id','img_path','img_nombre','img_tipo', 'img_pos'));

		echo 'no';
		exit;
	}

	public function consolidarimagen()
	{
		$this->_acl->acceso('encargado_access');

		if($this->_sess->get('img_error')){
			echo 'archivo invalido';
			$this->_sess->destroy('img_error');
			exit();
		}
		
		$_identificador = $this->_sess->get('carga_actual') ? $this->_sess->get('carga_actual') : $this->_sess->get('edicion_actual');

		/*switch ($this->_sess->get('img_tipo')) {
			
			case 'tendencias': 
				$_data = array('tipo' => 'poster', 'formato' => 'principal');
				break;
			case 'noticias': 
				$_data = array('tipo' => 'noticias', 'formato' => 'principal'); 
				break;
			default: return false; break;
		}*/

		// chequeamos si exste img principal nueva cargada
		// $_img_nueva = $this->_imagenesGestion->chequearImgPrincProc($this->_sess->get('carga_actual'));
		$_img_nueva = (file_exists($this->_conf['ruta_img_cargadas'] . 'temporales/' . $_identificador)) ? true : false;		

		if($_img_nueva){

			// Copiamos las copias de la imagen principal a los directorios finales-->
			// principal
			$principal = new upload($this->_conf['ruta_img_cargadas'] . 'temporales/' . $_identificador. DS . $this->_sess->get('img_path'));
			// $principal->process($this->_imagenesGestion->gestionDirectorios(Conf\ruta_img_cargadas::data() . 'noticias' . DS . 'principal' . DS, date("Y-m-d")));
			$principal->process($this->_conf['ruta_img_cargadas'] . $this->_sess->get('img_tipo') . DS);

			// thumb
			if($this->_sess->get('img_tipo') != 'lanzamientos' && $this->_sess->get('img_tipo') != 'banners' && $this->_sess->get('img_tipo') != 'clubalidas'){
				$thumb = new upload($this->_conf['ruta_img_cargadas'] . 'temporales/' . $_identificador. DS . 'thumb' . DS . $this->_sess->get('img_path'));
				// $thumb->process($this->_imagenesGestion->gestionDirectorios(Conf\ruta_img_cargadas::data() . 'noticias' . DS . 'thumb' . DS, date("Y-m-d")));
				$thumb->process($this->_conf['ruta_img_cargadas'] . $this->_sess->get('img_tipo') . DS . 'thumb' . DS);
			}

			// Cargamos a la base de datos
			/*$imagen = $this->_imagenesGestion->cargarImgDB(
												$this->_sess->get('img_categoria'), 
												$this->_sess->get('carga_actual'), identificador
												$this->_sess->get('img_path'), 
												$this->_sess->get('img_nombre'),
												$_data['tipo'],
												$_data['formato']);*/

			$_imagen = $this->_trabajosGestion->cargarImgDB(
												$_identificador,
												$this->_sess->get('img_path'),
												$this->_sess->get('img_nombre'), 
												$this->_sess->get('img_tipo'),
												$this->_conf['ruta_img_cargadas']);

			// y borramos el directorio temporal
			$this->_trabajosGestion->eliminarDirTempImagenPrinc($this->_conf['ruta_img_cargadas'] . 'temporales/' . $_identificador);

			// $this->_sess->destroy(array('img_categoria','img_path','img_nombre','carga_actual'));
			
			/*if($this->_sess->get('img_tipo') != 'lanzamientos' && $this->_sess->get('img_tipo') != 'banners' && $this->_sess->get('img_tipo') != 'clubalidas'){

				$_html = "<div>
			        <img src=\"" . $this->_conf['base_url'] . "public/img/subidas/". $this->_sess->get('img_tipo') . "/thumb/". $this->_sess->get('img_path') . "\" class=\"img-thumbnail\">
			    </div>";
			else{
				$_html = "<div>
			        <img src=\"" . $this->_conf['base_url'] . "public/img/subidas/". $this->_sess->get('img_tipo') . $this->_sess->get('img_path') . "\" class=\"img-thumbnail\">
			    </div>";
			}*/

			$_data_img = $this->_trabajosGestion->traerDataBasicaImagen($_imagen);

			if(file_exists($this->_conf['ruta_img_cargadas'] . $this->_sess->get('img_tipo') . "/" . $_data_img->path)){
	            
				if(file_exists($this->_conf['ruta_img_cargadas'] . $this->_sess->get('img_tipo') . "/thumb/" . $_data_img->path)){
					$_url_img = $this->_conf['base_url'] . "public/img/subidas/". $this->_sess->get('img_tipo') . "/thumb/" . $_data_img->path;
				}else{
					$_url_img = $this->_conf['base_url'] . "public/img/subidas/". $this->_sess->get('img_tipo') . "/" . $_data_img->path;
				}

	        }else if(file_exists($this->_conf['ruta_img_cargadas'] . "anteriores/images/" . $_data_img->path)){
	            $_url_img = $this->_conf['base_url'] . "public/img/subidas/anteriores/images/". $_data_img->path; 
	        }else{
	        	exit("Ha ocurrido un error. La imágen no existe o se ha corrompido.");
	        }
			
		    $_html = "<div id=\"" . $_imagen . "\">
				        <img src=\"" . $_url_img . "\" class=\"img-thumbnail\">
				    </div>";
		
			$this->_sess->destroy(array('img_id','img_path','img_nombre','img_tipo', 'img_error'));
		   	echo $_html;
		   	exit;
			// $this->redireccionar('administrador/imagenes');
			
		}
	}
	
	public function consolidarimagen_V2()
	{
		$this->_acl->acceso('encargado_access');


		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

				if($this->_sess->get('img_error')){
					$jsondata['html'] = 'archivo invalido';
					$this->_sess->destroy('img_error');
				   	echo json_encode($jsondata);
					exit();
				}

				$_valor = $_POST['val'];
				
				$_identificador = $this->_sess->get('carga_actual') ? $this->_sess->get('carga_actual') : $this->_sess->get('edicion_actual');

				
				// chequeamos si exste img principal nueva cargada
				// $_img_nueva = $this->_imagenesGestion->chequearImgPrincProc($this->_sess->get('carga_actual'));
				$_img_nueva = (file_exists($this->_conf['ruta_img_cargadas'] . 'temporales/' . $_identificador)) ? true : false;		

				if($_img_nueva){

					// Copiamos las copias de la imagen principal a los directorios finales-->
					// principal
					$principal = new upload($this->_conf['ruta_img_cargadas'] . 'temporales/' . $_identificador. DS . $this->_sess->get('img_path'));
					// $principal->process($this->_imagenesGestion->gestionDirectorios(Conf\ruta_img_cargadas::data() . 'noticias' . DS . 'principal' . DS, date("Y-m-d")));
					$principal->process($this->_conf['ruta_img_cargadas'] . $this->_sess->get('img_tipo') . DS);

					// thumb
					if($this->_sess->get('img_tipo') != 'lanzamientos' && $this->_sess->get('img_tipo') != 'banners' && $this->_sess->get('img_tipo') != 'clubalidas'){
						$thumb = new upload($this->_conf['ruta_img_cargadas'] . 'temporales/' . $_identificador. DS . 'thumb' . DS . $this->_sess->get('img_path'));
						// $thumb->process($this->_imagenesGestion->gestionDirectorios(Conf\ruta_img_cargadas::data() . 'noticias' . DS . 'thumb' . DS, date("Y-m-d")));
						$thumb->process($this->_conf['ruta_img_cargadas'] . $this->_sess->get('img_tipo') . DS . 'thumb' . DS);
					}

					// Cargamos a la base de datos				

					$_imagen = $this->_trabajosGestion->cargarImgDBV2(
														$_identificador,
														$this->_sess->get('img_path'),
														$this->_sess->get('img_nombre'), 
														$this->_sess->get('img_tipo'),
														$this->_conf['ruta_img_cargadas'],
														$this->_sess->get('img_pos'));

					// y borramos el directorio temporal
					$this->_trabajosGestion->eliminarDirTempImagenPrinc($this->_conf['ruta_img_cargadas'] . 'temporales/' . $_identificador);

					
					$_data_img = $this->_trabajosGestion->traerDataBasicaImagen($_imagen);

					if(file_exists($this->_conf['ruta_img_cargadas'] . $this->_sess->get('img_tipo') . "/" . $_data_img->path)){

						if($this->_sess->get('img_pos') == 'mobile'){
							$_url_img = $this->_conf['base_url'] . "public/img/subidas/". $this->_sess->get('img_tipo') . "/" . $_data_img->path;
						}else{
			            
							if(file_exists($this->_conf['ruta_img_cargadas'] . $this->_sess->get('img_tipo') . "/thumb/" . $_data_img->path)){
								$_url_img = $this->_conf['base_url'] . "public/img/subidas/". $this->_sess->get('img_tipo') . "/thumb/" . $_data_img->path;
							}else{
								$_url_img = $this->_conf['base_url'] . "public/img/subidas/". $this->_sess->get('img_tipo') . "/" . $_data_img->path;
							}
						}

			        }else if(file_exists($this->_conf['ruta_img_cargadas'] . "anteriores/images/" . $_data_img->path)){
			            $_url_img = $this->_conf['base_url'] . "public/img/subidas/anteriores/images/". $_data_img->path; 
			        }else{
			        	exit("Ha ocurrido un error. La imágen no existe o se ha corrompido.");
			        }
					
				    $jsondata['html'] = "<div id=\"" . $_imagen . "\">
						        <img src=\"" . $_url_img . "\" class=\"img-thumbnail\" style=\"width: 90%;\">
						        <a href=\"javascript:void(0);\" class=\"btn btn-danger\" onclick=\"eliminarImg(".$_imagen.");\" style=\"position: absolute;top: 0;margin-left: 2%;\">X</a>
						    </div>";
				
					$this->_sess->destroy(array('img_id','img_path','img_nombre','img_tipo', 'img_pos', 'img_error'));

					$jsondata['valor'] = $_valor;
				   	// echo $_html;
				   	echo json_encode($jsondata);
				   	exit;
					
				}
			}		

		}
	}




	/*public function traerImagenfinal($_seccion, $_directorio)
	{	
		$this->_acl->acceso('encargado_access');

		if($this->_sess->get('img_error')){
			echo 'archivo invalido';
			$this->_sess->destroy('img_error');
			exit();
		}

		//if($_formato == false) exit("NO se pudo procesar la imágen");
		//$_formatos_img = Conf\formatos_img::data();
		//if(!in_array($_formato, $_formatos_img['formatos'])) exit("NO se pudo procesar la imágen");
		$_data_img = $this->_trabajosGestion->traerDataBasicaImagen($this->_sess->get('img_id'));
		// Imagen principal
		$_img = $this->_conf['ruta_img_cargadas'] . $_seccion . "/".$_directorio. "/thumb/" . $_data_img->path;
		if(!file_exists($_img)) exit("Ha ocurrido un error. La imágen no existe o se ha corrompido.");
		
		
	    echo "<div id=\"" . $this->_sess->get('img_id') . "\">
	        <img src=\"" . $this->_conf['base_url'] . "public/img/subidas/".$_seccion . "/".$_directorio. "/thumb/". $_data_img->path . "\" class=\"img-thumbnail\">
	    </div>";		
		
		$this->_sess->destroy('img_id');
		
		exit();
	}*/


	
	
	public function traerImagenfinalLanzamientos($_seccion)
	{	
		$this->_acl->acceso('encargado_access');

		if($this->_sess->get('img_error')){
			echo 'archivo invalido';
			$this->_sess->destroy('img_error');
			exit();
		}

		//if($_formato == false) exit("NO se pudo procesar la imágen");
		//$_formatos_img = Conf\formatos_img::data();
		//if(!in_array($_formato, $_formatos_img['formatos'])) exit("NO se pudo procesar la imágen");
		$_data_img = $this->_trabajosGestion->traerDataBasicaImagen($this->_sess->get('img_id'));
		// Imagen principal
		$_img = $this->_conf['ruta_img_cargadas'] . $_seccion . "/" . $_data_img->path;
		if(!file_exists($_img)) exit("Ha ocurrido un error. La imágen no existe o se ha corrompido.");
		
		
	    echo "<div id=\"" . $this->_sess->get('img_id') . "\">
	        <img src=\"" . $this->_conf['base_url'] . "public/img/subidas/".$_seccion . "/". $_data_img->path . "\" class=\"img-thumbnail\">
	    </div>";		
		
		$this->_sess->destroy('img_id');
		
		exit();
	}

	public function traerImagenfinalTendencias($_seccion)
	{	
		$this->_acl->acceso('encargado_access');

		if($this->_sess->get('img_error')){
			echo 'archivo invalido';
			$this->_sess->destroy('img_error');
			exit();
		}

		//if($_formato == false) exit("NO se pudo procesar la imágen");
		//$_formatos_img = Conf\formatos_img::data();
		//if(!in_array($_formato, $_formatos_img['formatos'])) exit("NO se pudo procesar la imágen");
		$_data_img = $this->_trabajosGestion->traerDataBasicaImagen($this->_sess->get('img_id'));
		// Imagen principal
		// $_img = $this->_conf['ruta_img_cargadas'] . $_seccion . "/" . $_data_img->path;
		// if(!file_exists($_img)) exit("Ha ocurrido un error. La imágen no existe o se ha corrompido.");

		if(file_exists($this->_conf['ruta_img_cargadas'] . $_seccion . "/" . $_data_img->path)){
            $_url_img = $this->_conf['base_url'] . "public/img/subidas/".$_seccion . "/thumb/" . $_data_img->path;
        }else if(file_exists($this->_conf['ruta_img_cargadas'] . "anteriores/images/" . $_data_img->path)){
            $_url_img = $this->_conf['base_url'] . "public/img/subidas/anteriores/images/". $_data_img->path; 
        }else{
        	exit("Ha ocurrido un error. La imágen no existe o se ha corrompido.");
        }
		
		/*echo "<div id=\"" . $this->_sess->get('img_id') . "\">
	        <img src=\"" . $this->_conf['base_url'] . "public/img/subidas/".$_seccion . "/thumb/". $_data_img->path . "\" class=\"img-thumbnail\">
	    </div>";*/
	    echo "<div id=\"" . $this->_sess->get('img_id') . "\">
	        <img src=\"" . $_url_img . "\" class=\"img-thumbnail\">
	    </div>";		
		
		$this->_sess->destroy('img_id');
		
		exit();
	}
	
	
	public function traerImagenfinalVideo($_directorio)
	{	
		$this->_acl->acceso('encargado_access');

		if($this->_sess->get('img_error')){
			echo 'archivo invalido';
			$this->_sess->destroy('img_error');
			exit();
		}

		//if($_formato == false) exit("NO se pudo procesar la imágen");
		//$_formatos_img = Conf\formatos_img::data();
		//if(!in_array($_formato, $_formatos_img['formatos'])) exit("NO se pudo procesar la imágen");
		$_data_img = $this->_trabajosGestion->traerDataBasicaImagen($this->_sess->get('img_id'));
		// Imagen principal
		$_img = $this->_conf['ruta_videos'] .$_directorio. "/thumb/" . $_data_img->path;
		if(!file_exists($_img)) exit("Ha ocurrido un error. La imágen no existe o se ha corrompido.");
		
		
	    echo "<div id=\"" . $this->_sess->get('img_id') . "\">
	        <img src=\"" . $this->_conf['base_url'] . "public/videos/".$_directorio. "/thumb/". $_data_img->path . "\" class=\"img-thumbnail\">
	    </div>";		
		
		$this->_sess->destroy('img_id');
		
		exit();
	}

	
	
	
	
	
	public function traerImagenesGalerias($_seccion)
	{
		$this->_acl->acceso('encargado_access');

		if($this->_sess->get('img_error')){
			echo 'archivo invalido';
			$this->_sess->destroy('img_error');
			exit();
		}


		if(isset($_SESSION['lote_img_gal']) && !empty($_SESSION['lote_img_gal'])){
			$_ids = array();
			foreach ($_SESSION['lote_img_gal'] as $key => $value){
				$_ids[] = $key;
			}
			$_ids = implode(',', $_ids);
			$_data = $this->_trabajosGestion->traerImagenesPorLote($_ids);
			
			//echo $_ids;
			// echo "<pre>";print_r($_data);echo "</pre>";
			
			unset($_SESSION['lote_img_gal']);
			if(is_array($_data) && !empty($_data)){
				$_imp = '';
				//for($im=0;$im<count($_data);$im++){
				foreach($_data as $data){
					
					$_img = $this->_conf['ruta_img_cargadas'] . $_seccion . "/" . $data->path;
					//if(file_exists("public/img/subidas/".$_seccion."/" .  $data->path)){
					if(file_exists($_img)){
						$_imp .= "<div id=\"img-" . $data->id . "\" style=\"margin-bottom: 20px\">
						<img src=\"".$this->_conf['base_url']."public/img/subidas/".$_seccion."/". $data->path . "\" class=\"img-thumbnail _banner\" width=\"80%\">						
						<input class=\"form-control\" name=\"img_link[".$data->id."]\" type=\"text\" placeholder=\"Link\" />
						</div>";
					}
				}
				unset($_SESSION['img_lote']);
				echo $_imp;
				exit;
			}
			exit("<p>No se encontraron datos de las imagenes cargadas 1.</p>");
		}
		exit("<p>No se encontraron datos de las imagenes cargadas 2.</p>");
	}
	
	
	public function traerImagenesGaleriasEditar($_seccion)
	{
		$this->_acl->acceso('encargado_access');

		if($this->_sess->get('img_error')){
			echo 'archivo invalido';
			$this->_sess->destroy('img_error');
			exit();
		}


		if(isset($_SESSION['lote_img_gal']) && !empty($_SESSION['lote_img_gal'])){
			$_ids = array();
			foreach ($_SESSION['lote_img_gal'] as $key => $value){
				$_ids[] = $key;
			}
			$_ids = implode(',', $_ids);
			$_data = $this->_trabajosGestion->traerImagenesPorLote($_ids);
			
			//echo $_ids;
			/*echo "<pre>";
			print_r($_data);*/
			
			unset($_SESSION['lote_img_gal']);
			if(is_array($_data) && !empty($_data)){
				$_imp = '';
				//for($im=0;$im<count($_data);$im++){
				foreach($_data as $data){
					
					$_img = $this->_conf['ruta_img_cargadas'] . $_seccion . "/" . $data->path;
					//if(file_exists("public/img/subidas/".$_seccion."/" .  $data->path)){
					if(file_exists($_img)){
						$_imp .= "<div id=\"img-" . $data->id . "\" style=\"margin-bottom: 20px\">
						<img src=\"" . $this->_conf['base_url'] . "public/img/subidas/".$_seccion. "/" . $data->path . "\" class=\"img-thumbnail _banner\" width=\"80%\">
						<input name=\"eliminar_gal[]\" type=\"checkbox\" value=\"".$data->id."\" /> Eliminar Imagen
						<input class=\"form-control\" name=\"img_link[".$data->id."]\" type=\"text\" placeholder=\"Link\" />
					
						</div>";
					}
				}
				unset($_SESSION['img_lote']);
				echo $_imp;
				exit;
			}
			exit("<p>No se encontraron datos de las imagenes cargadas 1.</p>");
		}
		exit("<p>No se encontraron datos de las imagenes cargadas 2.</p>");
	}
	
	
	public function traerImagenfinalMod4($_seccion)
	{	
		//if($_formato == false) exit("NO se pudo procesar la imágen");
		//$_formatos_img = Conf\formatos_img::data();
		//if(!in_array($_formato, $_formatos_img['formatos'])) exit("NO se pudo procesar la imágen");
		$this->_view->data_img = $this->_trabajosGestion->traerDataBasicaImagen($this->_sess->get('img_id'));
		// Imagen principal
		$_img = $this->_conf['ruta_img_cargadas'] . $_seccion . "/" . $this->_view->data_img->path;
		if(!file_exists($_img)) exit("Ha ocurrido un error. La imágen no existe o se ha corrompido.");
		
		
	    echo "<div id=\"" . $this->_sess->get('img_id') . "\">
	        <img src=\"" . $this->_conf['base_url'] . "public/img/subidas/".$_seccion."/" . $this->_view->data_img->path . "\" class=\"img-thumbnail\">
	    </div>";		
		
		$this->_sess->destroy('img_id');
		
		exit();
	}
	
		
		
		
		
		
		
	
	public function listar($pagina = false)
    {
		$this->_acl->acceso('encargado_access');
		
		$this->_view->setJs(array('funciones'));
		
		$pagina = (!validador::filtrarInt($pagina)) ? false : (int) $pagina;
		$paginador = new Paginador();
		
		$img = $this->_trabajosGestion->traerDatosImagenes();
		
		$this->_view->imagenes = $paginador->paginar($img, $pagina, 20);
		$this->_view->paginacion = $paginador->getView('paginador-bootstrap', 'administrador/imagenes/listar');
			
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('listar', 'imagenes');	
    }
	
	
	/*public function orden($_id, $_orden)
	{
		
		$this->_acl->acceso('encargado_access');
		
		$_id = (int) $_id;
		$_orden = (int) $_orden;
		
		$this->_trabajosGestion->cambiarOrdenDestacados($_id, $_orden);
		$this->redireccionar('administrador/destacados/listar');
		
	}*/
	
	
	public function editar($_id)
	{
		//$this->_acl->acceso('admin_access');
		$this->_acl->acceso('encargado_access');
		
		validador::validarParametroInt($_id,$this->_conf['base_url']);		
		
		$this->_view->trabajo = $this->_trabajosGestion->traerDatosImagen($_id);		
	
		//$this->_view->estados = $this->_trabajosGestion->traerEstados();		
		
		/*echo "<pre>";
		print_r($this->_view->imgs);
		echo "</pre>";*/
		
		$this->_view->setJs(array('funciones'));
		
									
		
				
			
		if($_POST){
			
			
			if($_POST['envio01'] == 1){
			
				$this->_view->data = $_POST;	
				
				/*echo "<pre>";
				print_r($_FILES);
				echo "</pre>";
				exit;*/
				
								
				//if($_FILES['imgGaleria']['name'][0]!=''){
				if(is_array($_FILES['imagen']) && $_FILES['imagen']['name'][0]!=''){
					
						/*$imagen = getimagesize($_FILES['imagen']['tmp_name']);   
  						$ancho = $imagen[0];          
  						$alto = $imagen[1];	
						
						$val = ($alto>$ancho) ? '1' : 'T';	*/
					
						$upload = new upload($_FILES['imagen'], 'es_Es');
						$upload->allowed = array('image/*');
						$upload->file_new_name_body = uniqid();
						$upload->process($this->_conf['ruta_img_cargadas'] . 'originales' . DS);
						
						if($upload->processed){
							
							$thumb = new Upload($upload->file_dst_pathname);
							$thumb->image_resize = true;
							$thumb->image_ratio_crop = 'T';
							$thumb->image_x = $this->_conf['formatos_img']['thumb']['ancho'];
							$thumb->image_y = $this->_conf['formatos_img']['thumb']['alto'];
							/*$tamañoOrig = admin::CapturarAnchoAlto($upload->file_dst_pathname);
							$thumb->image_y =  round(($thumb->image_x/$tamañoOrig[0])*$tamañoOrig[1]);*/
							$thumb->file_name_body_pre = 'thumb_';
							$thumb->process($this->_conf['ruta_img_cargadas']);
			
							$mediana = new Upload($upload->file_dst_pathname);
							$mediana->image_resize = true;
							$mediana->image_ratio_crop = 'T';
							$mediana->image_x = $this->_conf['formatos_img']['mediana']['ancho'];
							$mediana->image_y = $this->_conf['formatos_img']['mediana']['alto'];
							// $tamañoOrig = admin::CapturarAnchoAlto($upload->file_dst_pathname);
							// $mediana->image_y =  round(($mediana->image_x/$tamañoOrig[0])*$tamañoOrig[1]);
							$mediana->file_name_body_pre = 'mediana_';
							$mediana->process($this->_conf['ruta_img_cargadas']);
							
							$grande = new Upload($upload->file_dst_pathname);
							$grande->image_resize = true;
							$grande->image_ratio_crop = true;
							$grande->image_x = $this->_conf['formatos_img']['grande']['ancho'];
							//$grande->image_y = $this->_conf['formatos_img']['grande']['alto'];
							$tamañoOrig = admin::CapturarAnchoAlto($upload->file_dst_pathname);
							$grande->image_y =  round(($grande->image_x/$tamañoOrig[0])*$tamañoOrig[1]);
							$grande->file_name_body_pre = 'grande_';
							$grande->process($this->_conf['ruta_img_cargadas']);
			
							$imagen = contenidos_imagene::find(array('conditions' => array('identificador = ?', $this->_view->trabajo->identificador)));
							$_fechaBd = date('Y-m-d H:i:s', strtotime('now'));
							if($imagen){
								$imagen->path = $upload->file_dst_name;
								$imagen->save();
							
							}else{
								$imagen = new contenidos_imagene();
								$imagen->identificador = $this->_view->trabajo->identificador;
								$imagen->path = $upload->file_dst_name;
								$imagen->orden = 0;
								$imagen->fecha_alt = "$_fechaBd";
								$imagen->save();
							}
							
							$zip = new ZipArchive;
							$zip->open($this->_conf['ruta_archivos_descargas'].$upload->file_dst_name.".zip",ZipArchive::CREATE);
							$zip->addFile($this->_conf['ruta_img_cargadas'].$upload->file_dst_name);
							$zip->close();
														
							
						}else{
							throw new Exception('Error al cargar la imagen');
						}
				}
				
				
				
				
							
				
				$_editar = contenidos_datos_imagene::find($this->_view->trabajo->id);
				$_editar->dia = $this->_view->data['dia'];
				$_editar->titulo = validador::getTexto('titulo');
				$_editar->descripcion = validador::getTexto('desc');
				$_editar->identificador = $this->_view->trabajo->identificador;
				$_editar->save();
				
				
				$this->redireccionar('administrador/imagenes/listar');
											
				
			}
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('editar', 'imagenes');	
    }
	
	
	
	
	
	public function cargar($_categoria = null)
    {	
		$this->_acl->acceso('encargado_access');
		//$this->_view->_categoria = (int) $_categoria;
	
		/*if(!$this->_sess->get('carga_actual')){
			$this->_sess->set('carga_actual', rand(1135687452,9999999999));
		}*/
		
		//$this->_view->setCssPlugin(array('tinymce.min'));
		$this->_view->setJs(array('funciones'));
		
			
		
		
		if($_POST){
			
			
			if($_POST['envio01'] == 1){
				
				$this->_view->data = $_POST;
				
			
				/*echo "<pre>";
				print_r($_POST);
				print_r($_FILES);
				echo "</pre>";
				exit;*/
				
				
				if($_FILES['imagen']){					
					
					$files = array();
					foreach ($_FILES['imagen'] as $k => $l) {
						foreach ($l as $i => $v) {
							if (!array_key_exists($i, $files))
								$files[$i] = array();
							$files[$i][$k] = $v;
						}
					}
					
					/*echo'<pre>';
					print_r($files);
					echo $this->_view->data['ruta_img'];
					exit;
						*/	
					$cont=0;			
					foreach ($files as $file) {
						
						$this->_sess->set('carga_actual', rand((int)1135687452,(int)9999999999));
						
						if($file['error']!=4){
							$upload = new Upload($file);
							//$upload->allowed = array('image');
							$upload->file_new_name_body = uniqid();
							if ($upload->uploaded) {
								$upload->Process($this->_conf['ruta_img_cargadas'] . 'originales' . DS);
							
								if($upload->processed){				
									
									$thumb = new Upload($upload->file_dst_pathname);
									$thumb->image_resize = true;
									$thumb->image_ratio_crop = 'T';
									$thumb->image_x = $this->_conf['formatos_img']['thumb']['ancho'];
									$thumb->image_y = $this->_conf['formatos_img']['thumb']['alto'];
									/*$tamañoOrig = admin::CapturarAnchoAlto($upload->file_dst_pathname);
									$thumb->image_y =  round(($thumb->image_x/$tamañoOrig[0])*$tamañoOrig[1]);*/
									$thumb->file_name_body_pre = 'thumb_';
									$thumb->process($this->_conf['ruta_img_cargadas']);
					
									$mediana = new Upload($upload->file_dst_pathname);
									$mediana->image_resize = true;
									$mediana->image_ratio_crop = 'T';
									$mediana->image_x = $this->_conf['formatos_img']['mediana']['ancho'];
									$mediana->image_y = $this->_conf['formatos_img']['mediana']['alto'];
									/*$tamañoOrig = admin::CapturarAnchoAlto($upload->file_dst_pathname);
									$mediana->image_y =  round(($mediana->image_x/$tamañoOrig[0])*$tamañoOrig[1]);*/
									$mediana->file_name_body_pre = 'mediana_';
									$mediana->process($this->_conf['ruta_img_cargadas']);
									
									$grande = new Upload($upload->file_dst_pathname);
									$grande->image_resize = true;
									$grande->image_ratio_crop = true;
									$grande->image_x = $this->_conf['formatos_img']['grande']['ancho'];
									//$grande->image_y = $this->_conf['formatos_img']['grande']['alto'];
									$tamañoOrig = admin::CapturarAnchoAlto($upload->file_dst_pathname);
									$grande->image_y =  round(($grande->image_x/$tamañoOrig[0])*$tamañoOrig[1]);
									$grande->file_name_body_pre = 'grande_';
									$grande->process($this->_conf['ruta_img_cargadas']);
					
									//
									/*$imagen = contenidos_imagene::find(array('conditions' => array('identificador = ?', $this->_sess->get('carga_actual'))));
									if($imagen){
										$imagen->path = $upload->file_dst_name;
										$imagen->bajada = $_POST['bajada'][$i];
										$imagen->save();
									
									}else{*/
									$_fechaBd = date('Y-m-d H:i:s', strtotime('now'));
									$imagen = new contenidos_imagene();
									$imagen->identificador = $this->_sess->get('carga_actual');
									$imagen->path = $upload->file_dst_name;
									$imagen->orden = 0;
									$imagen->fecha_alt = "$_fechaBd";
									$imagen->save();
									
									$_fechaBd2 = date('Y-m-d');

									$reel = contenidos_datos_imagene::create(array(					
										'dia'				=> $this->_view->data['dia'][$cont],
										'titulo'			=> $this->_view->data['titulo'][$cont],
										'descripcion'		=> $this->_view->data['desc'][$cont],	
										'identificador'		=> $this->_sess->get('carga_actual'),					
										'nombre_orig_img'	=> '',
										'fecha'				=> "$_fechaBd2",
									));
									
									$this->_sess->destroy('carga_actual');
									
									
								}else{
									throw new Exception('Error al cargar la imagen');
								}
							}else{
								throw new Exception('Error al cargar la imagen');
							}
						}
					$cont++;
					}
					
					
				}
				
			//exit;
				
				
			/*	if(is_array($_FILES['imagen']) && $_FILES['imagen']['name'][0]!=''){
					
					for($i=0;$i< count($_FILES['imagen']['name']);$i++){
					
						$upload = new upload($_FILES['imagen'], 'es_Es');
						$upload->allowed = array('image/*');
						$upload->file_new_name_body = uniqid();
						$upload->process($this->_conf['ruta_img_cargadas'] . 'originales' . DS);
						
						if($upload->processed){
							
							$thumb = new upload($upload->file_dst_pathname);
							$thumb->image_resize = true;
							$thumb->image_ratio_crop = true;
							$thumb->image_x = $this->_conf['formatos_img']['thumb']['ancho'];
							//$thumb->image_y = $this->_conf['formatos_img']['thumb']['alto'];
							$tamañoOrig = admin::CapturarAnchoAlto($upload->file_dst_pathname);
							$thumb->image_y =  round(($thumb->image_x/$tamañoOrig[0])*$tamañoOrig[1]);
							$thumb->file_name_body_pre = 'thumb_';
							$thumb->process($this->_conf['ruta_img_cargadas']);
			
							$mediana = new upload($upload->file_dst_pathname);
							$mediana->image_resize = true;
							$mediana->image_ratio_crop = true;
							$mediana->image_x = $this->_conf['formatos_img']['mediana']['ancho'];
							//$grande->image_y = $this->_conf['formatos_img']['mediana']['alto'];
							$tamañoOrig = admin::CapturarAnchoAlto($upload->file_dst_pathname);
							$mediana->image_y =  round(($mediana->image_x/$tamañoOrig[0])*$tamañoOrig[1]);
							$mediana->file_name_body_pre = 'mediana_';
							$mediana->process($this->_conf['ruta_img_cargadas']);
							
							$grande = new upload($upload->file_dst_pathname);
							$grande->image_resize = true;
							$grande->image_ratio_crop = true;
							$grande->image_x = $this->_conf['formatos_img']['grande']['ancho'];
							//$grande->image_y = $this->_conf['formatos_img']['grande']['alto'];
							$tamañoOrig = admin::CapturarAnchoAlto($upload->file_dst_pathname);
							$grande->image_y =  round(($grande->image_x/$tamañoOrig[0])*$tamañoOrig[1]);
							$grande->file_name_body_pre = 'grande_';
							$grande->process($this->_conf['ruta_img_cargadas']);
			
							$imagen = new contenidos_imagene();
							$imagen->identificador = $this->_sess->get('carga_actual');
							$imagen->path = $upload->file_dst_name;
							$imagen->orden = 0;
							$imagen->fecha_alt = date("Y-m-d H:i:s", strtotime('now'));
							$imagen->save();
							
						}else{
							throw new Exception('Error al cargar la imagen');
						}
					}
				}*/
				
				
			/*for($i=0;$i<count($this->_view->data['dia']);$i++){
				$reel = contenidos_datos_imagene::create(array(					
					'dia'				=> $this->_view->data['dia'][$i],
					'titulo'			=> $this->_view->data['titulo'][$i],
					'descripcion'		=> $this->_view->data['desc'][$i],	
					'identificador'		=> $this->_sess->get('carga_actual'),					
					'nombre_orig_img'	=> '',
					'fecha'				=> date('Y-m-d'),
				));
			}*/
				
				
							
				
				
				$this->redireccionar('administrador/imagenes/listar');
			}	
		}
	
		$this->_view->titulo = 'Administrador - Seguimiento';
        $this->_view->renderizar('cargar', 'imagenes');	
    }
	
	public function borrar($_id)
	{
		$this->_acl->acceso('encargado_access');
		$_id = (int) $_id;
		
		//$this->_view->trabajo = $this->_trabajosGestion->traerTrabajo($_id);
		
		$this->_trabajosGestion->borrarImg($_id, $this->_conf['ruta_img_cargadas']);
		$this->redireccionar('administrador/imagenes/listar');
	}
	
	public function buscador()
	{
		$this->_acl->acceso('encargado_access');
		
		if($_POST){
			
			$_val = $_POST['valor'];
			
			$this->_view->imgs  = $this->_trabajosGestion->traerImgBuscador(ucwords(strtolower($_val)));
			
			/*echo "<pre>";
			print_r($this->_view->users);
			echo"</pre>";*/
			
			$_html = '<ul class="list-group">';
			foreach($this->_view->imgs as $imgs){           
        
				$_html .='<li class="list-group-item" onmouseover="this.classList.add(\'item_sobre\');" onmouseout="this.classList.remove(\'item_sobre\');">					
						<img style="width:200px;" class="img-thumbnail" src="'.$this->_conf['base_url'] . 'public/img/subidas/thumb_' . admin::traerImgChica($imgs->identificador)->path.'"/>';
						if($imgs->titulo!=''){
				$_html .='<span class="btn btn-info btn-sm">'.admin::convertirCaracteres($imgs->titulo).'</span>';
						}
				$_html .='&nbsp;
						<span class="btn btn-primary btn-sm">Día '.$imgs->dia.'</span>					
						<span class="flotar_derecha" style="margin-right:15px">				
							<a href="'.$this->_conf['url_enlace'].'administrador/imagenes/editar/'. $imgs->id.'" class="btn btn-success btn-sm">
								editar
							</a>
							<a href="'.$this->_conf['url_enlace'].'administrador/imagenes/borrar/'. $imgs->id.'" class="btn btn-danger btn-sm">
								Borrar
							</a>
						</span>
					</li>';
        
			}			
			$_html .='</ul>';
			
			echo $_html;
			
		}
	}
	
	
	
	
	
	
}