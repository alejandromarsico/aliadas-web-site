<?php
use Nucleo\Pd\Pd;
use Nucleo\Registro\Registro;
use \Exception as EX;

class home
{
	
	private function armarArray($_val)
	{
		$arrayResult = array_map(function($res){
		  return $res->attributes();
		}, $_val);
		return $arrayResult;
	}
	
	private static function armarArrayStatic($_val)
	{
		$arrayResult = array_map(function($res){
		  return $res->attributes();
		}, $_val);
		return $arrayResult;
	}
	
	public function traerListaComentarios($_seccion, $_nota)
	{
		//$_res = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_comentarios` as cc ORDER BY cc.id ASC");
		$_res = Pd::instancia()->prepare("SELECT  cc.*,ci.path 
											FROM contenidos_comentarios as cc
											INNER JOIN  contenidos_users as cu
											ON cc.id_user = cu.id
											INNER JOIN  contenidos_imagenes as ci
											ON cu.identificador = ci.identificador
											WHERE cc.seccion = :seccion AND cc.id_nota = :nota 
											ORDER BY cc.id DESC");

		/*$results = contenidos_video::find_by_sql('SELECT  cc.*,ci.path 
													FROM contenidos_comentarios as cc
													INNER JOIN  contenidos_users as cu
													ON cc.id_user = cu.id
													INNER JOIN  contenidos_imagenes as ci
													ON cu.identificador = ci.identificador
													WHERE ch.id IN ('.$_ids.')');*/


		$_res->execute(array(':seccion' => $_seccion, ':nota' => $_nota));
		$_res = $_res->fetchAll(PDO::FETCH_ASSOC);

		return ($_res) ? $_res : false;
	}


	public function addComentarios($commentId, $comment, $user, $id_user, $id_nota, $seccion, $date)
	{		
		$result = Pd::instancia()->prepare("INSERT INTO `contenidos_comentarios` (id_comentario_padre,comentario,usuario,id_user,id_nota,seccion,fecha) VALUES (:comment_id, :comment, :usuario, :id_user, :id_nota, :seccion, :fecha)");
		$result->execute(array(":comment_id" => $commentId, ":comment" => $comment, ":usuario" => $user, ":id_user" => $id_user, ":id_nota" => $id_nota, ":seccion" => $seccion, ":fecha" => $date));
		
		return ($result) ? true : false;
	}

	/*public function addComentarios2($autor_id, $post_id, $comentario, $date)
	{		
		$result = Pd::instancia()->prepare("INSERT INTO `contenidos_comentarios` VALUES (null, :autor_id, :post_id, :comentario, :fecha)");
		$result->execute(array(":autor_id" => $autor_id, ":post_id" => $post_id, ":comentario" => $comentario, ":fecha" => $date));
		
		return ($result) ? true : false;
	}*/
	public static function traerClientePorUsers($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_clientes` as cl WHERE cl.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerDataUsers($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cu.* FROM `contenidos_users` as cu WHERE cu.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerUsers()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cu.* FROM `contenidos_users` as cu");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}
	
	public function traerUser($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cu.* FROM `contenidos_users` as cu WHERE cu.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function checkUserPlay($_id, $_id_juego)
	{
		$result = Pd::instancia()->prepare("SELECT cuj.id FROM `contenidos_users_juegos` as cuj WHERE cuj.id_user = :id AND cuj.id_juego = :juego");
		$result->execute(array(":id" => $_id, ":juego" => $_id_juego));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerDataBasicaImagen($_id)
	{
		return  contenidos_imagene::find(array('conditions' => array('id = ?', $_id)));
	}

	public function cargarImgDB($_identificador, $_path, $_nombre, $_posicion, $_ruta)
	{
		$_nombre = strtolower(trim($_nombre));
		$_nombre = htmlentities($_nombre, ENT_QUOTES | ENT_IGNORE, "UTF-8");
		
		$_editar = contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ?', $_identificador, $_posicion)));

		$_fechaBd = date('Y-m-d');
		
		if($_editar){
			
			$_borrarImg = $this->eliminarImagenes($_editar->path, $_ruta, $_posicion);			
			if($_borrarImg==true){			
				$_editar->nombre = $_nombre;
				$_editar->identificador = $_identificador;
				$_editar->path = $_path;
				$_editar->posicion = $_posicion;		
				$_editar->orden = 0;							
				$_editar->fecha_alt = "$_fechaBd";
				$_editar->save();
				return $_editar->id;
			}
		}else{
			$imagen = new contenidos_imagene();
			$imagen->nombre = $_nombre;
			$imagen->identificador = $_identificador;
			$imagen->path = $_path;	
			$imagen->posicion = $_posicion;		
			$imagen->orden = 0;		
			$imagen->fecha_alt = "$_fechaBd";
			$imagen->save();	
			return $imagen->id;
		}
	
				
	}

	public function eliminarImagenes($_path, $_ruta, $_seccion)
	{
			
		if(is_readable($_ruta . $_seccion . '/'.$_path)){
			@unlink($_ruta . $_seccion . '/'. $_path);
		}			
		if(is_readable($_ruta  . $_seccion . '/thumb/'. $_path)){
			@unlink($_ruta . $_seccion . '/thumb/'. $_path);
		}
		
		return true;
	}

	public function traerUserPuntos($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cuj.* FROM `contenidos_users_juegos` as cuj WHERE cuj.id_user = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerUserPuntosCanjeados($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cupc.* FROM `contenidos_users_puntos_canjeados` as cupc WHERE cupc.id_user = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerUserPuntosTotales($_id)
	{
		$result1 = Pd::instancia()->prepare("SELECT SUM(cuj.puntos) as puntos_totales FROM `contenidos_users_juegos` as cuj WHERE cuj.id_user = :id");
		$result1->execute(array(":id" => $_id));
		$result1 = $result1->fetch(PDO::FETCH_ASSOC);

		$result2 = Pd::instancia()->prepare("SELECT SUM(cupc.puntos) as puntos_canjeados FROM `contenidos_users_puntos_canjeados` as cupc WHERE cupc.id_user = :id");
		$result2->execute(array(":id" => $_id));
		$result2 = $result2->fetch(PDO::FETCH_ASSOC);

		$result = array_merge($result1,$result2);

		return ($result) ? $result : false;
	}

	public static function traerUserPuntosTotalesStatic($_id)
	{
		$result1 = Pd::instancia()->prepare("SELECT SUM(cuj.puntos) as puntos_totales FROM `contenidos_users_juegos` as cuj WHERE cuj.id_user = :id");
		$result1->execute(array(":id" => $_id));
		$result1 = $result1->fetch(PDO::FETCH_ASSOC);

		$result2 = Pd::instancia()->prepare("SELECT SUM(cupc.puntos) as puntos_canjeados FROM `contenidos_users_puntos_canjeados` as cupc WHERE cupc.id_user = :id");
		$result2->execute(array(":id" => $_id));
		$result2 = $result2->fetch(PDO::FETCH_ASSOC);

		// $result = array_merge($result1,$result2);

		$result = $result1['puntos_totales'] - $result2['puntos_canjeados'];  

		return ($result) ? $result : false;
	}



	public static function traerPorTablaJuego($_id, $_tabla)
	{
		$result = Pd::instancia()->prepare("SELECT * FROM `$_tabla` WHERE id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerTipoJuego($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ctj.* FROM `contenidos_tipo_juegos` as ctj WHERE ctj.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	/*public function traerCapacitaciones($_limit)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc ORDER BY cc.fecha DESC LIMIT $_limit");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function contarRegistrosCapacitaciones()
	{
		$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_capacitaciones` as cc");
		$_reg->execute();
		$_registros = $_reg->fetch(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros['cant'] : false;
	}

	public function paginadorCapacitaciones($_pag, $_limit)
	{
		$_offset = ($_pag-1) * $_limit;
		$_reg = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc ORDER BY cc.fecha DESC LIMIT $_limit OFFSET $_offset");
		$_reg->execute();
		$_registros = $_reg->fetchAll(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros : false;
	}
*/


	public function traerCapacitaciones()
	{
		
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc ORDER BY cc.fecha DESC");			
		
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	/*public function traerCapacitaciones($_limit, $_cat='')
	{

		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cc.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cc.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc WHERE ".$_html." ORDER BY cc.fecha DESC LIMIT $_limit");
			
		}else{
			$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc ORDER BY cc.fecha DESC LIMIT $_limit");			
		}
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}*/

	public static function contarRegistrosCapacitaciones($_cat='')
	{
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cc.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cc.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_capacitaciones` as cc WHERE ".$_html."");
		}else{
			$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_capacitaciones` as cc");
			
		}
		$_reg->execute();
		$_registros = $_reg->fetch(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros['cant'] : false;
	}

	public function paginadorCapacitaciones($_pag, $_limit, $_cat='')
	{
		$_offset = ($_pag-1) * $_limit;		
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cc.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cc.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$_reg = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc WHERE ".$_html." ORDER BY cc.fecha DESC LIMIT $_limit OFFSET $_offset");			
		}else{
			$_reg = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc ORDER BY ct.fecha DESC LIMIT $_limit OFFSET $_offset");
		}
		$_reg->execute();
		$_registros = $_reg->fetchAll(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros : false;
	}

	

	public function traerCapacitacionesPorCat($_limit, $_cat='')
	{	
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cc.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cc.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc WHERE ".$_html." ORDER BY cc.fecha DESC LIMIT $_limit");
			
		}else{
			$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc ORDER BY cc.fecha DESC LIMIT $_limit");
		}
		
		$result->execute();	
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		
		return ($result) ? $result : false;
		
	}

	public function traerCapacitacionesRelacionadas($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc WHERE cc.id != :id ORDER BY RAND() LIMIT 4");
		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	public function traerCapacitacion($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc WHERE cc.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerCapacitacionDestacada()
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc WHERE cc.destacado = 'si' ORDER BY cc.id DESC");
		$result->execute();
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	public function traerJuegoPorTipoId($_id, $_tipo)
	{
		$result = Pd::instancia()->prepare("SELECT ctj.* FROM `contenidos_juegos` as ctj WHERE ctj.id_juego = :id AND ctj.id_tipo_juego = :tipo");
		$result->execute(array(":id" => $_id, ":tipo" => $_tipo));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerJuegoPorTabla($_id, $_tabla)
	{
		$result = Pd::instancia()->prepare("SELECT * FROM `$_tabla` WHERE id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerLanzamientos()
	{
		
		// $result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl ORDER BY cl.fecha DESC");	
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl WHERE cl.fecha >= date_sub(curdate(), interval 6 month) ORDER BY cl.fecha DESC");			
		
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerLanzamientosHome($_limit, $_cat='')
	{
		// $_lanz = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl ORDER BY cl.fecha DESC LIMIT $_limit");

		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cl.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cl.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl WHERE ".$_html." ORDER BY cl.fecha DESC LIMIT $_limit");
			
		}else{
			$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl ORDER BY cl.fecha DESC LIMIT $_limit");			
		}
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerLanzamientosHome2()
	{
		$result = Pd::instancia()->prepare("SELECT cdl.* FROM `contenidos_destacados_lanzamientos` as cdl ORDER BY cdl.posicion ASC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function contarRegistrosLanz($_cat='')
	{
		// $_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_lanzamientos` as cl");
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cl.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cl.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_lanzamientos` as cl WHERE ".$_html."");
			// $_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_lanzamientos` as cl FIND_IN_SET ('".$_cat."', cl.categorias)");
		}else{
			$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_lanzamientos` as cl");
			
		}
		$_reg->execute();
		$_registros = $_reg->fetch(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros['cant'] : false;
	}

	public function paginadorLanz($_pag, $_limit, $_cat='')
	{
		$_offset = ($_pag-1) * $_limit;
		// $_reg = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl ORDER BY cl.fecha DESC LIMIT $_limit OFFSET $_offset");
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cl.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cl.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$_reg = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl WHERE ".$_html." ORDER BY cl.fecha DESC LIMIT $_limit OFFSET $_offset");
			// $_reg = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl WHERE FIND_IN_SET ('".$_cat."', cl.categorias) ORDER BY cl.fecha DESC LIMIT $_limit OFFSET $_offset");
		}else{
			$_reg = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl ORDER BY cl.fecha DESC LIMIT $_limit OFFSET $_offset");
		}
		$_reg->execute();
		$_registros = $_reg->fetchAll(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros : false;
	}

	

	public function traerLanzamientosPorCat($_limit, $_cat='')
	{	
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cl.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cl.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl WHERE ".$_html." ORDER BY cl.fecha DESC LIMIT $_limit");
			
		}else{
			$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl ORDER BY cl.fecha DESC LIMIT $_limit");
		}
		
		// $result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl WHERE FIND_IN_SET ('".$_cat."', cl.categorias) ORDER BY cl.fecha DESC LIMIT $_limit");	
		$result->execute();	
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		
		return ($result) ? $result : false;
		
	}
	
	public function traerLanzamiento($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl WHERE cl.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


/*
	public function traerVideos($_limit, $_cat='')
	{
		if($_cat!=''){
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct WHERE FIND_IN_SET ('".$_cat."', ct.categorias) ORDER BY ct.fecha DESC LIMIT $_limit");
		}else{
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct ORDER BY ct.fecha DESC LIMIT $_limit");			
		}		
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function contarRegistrosVideos($_cat='')
	{
		if($_cat!=''){
			$result = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_tutoriales` as ct FIND_IN_SET ('".$_cat."', ct.categorias)");
		}else{
			$result = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_tutoriales` as ct");
			
		}
		$result->execute();
		$_registros = $result->fetch(PDO::FETCH_ASSOC);
		$result = null;
		return ($_registros) ? $_registros['cant'] : false;
	}

	public function paginadorVideos($_pag, $_limit, $_cat='')
	{
		$_offset = ($_pag-1) * $_limit;

		if($_cat!=''){
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct WHERE FIND_IN_SET ('".$_cat."', ct.categorias) ORDER BY ct.fecha DESC LIMIT $_limit OFFSET $_offset");
		}else{
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct ORDER BY ct.fecha DESC LIMIT $_limit OFFSET $_offset");
		}

		$result->execute();
		$_registros = $result->fetchAll(PDO::FETCH_ASSOC);
		$result = null;
		return ($_registros) ? $_registros : false;
	}*/

	public function traerEventos()
	{
		$result = Pd::instancia()->prepare("SELECT ce.* FROM `contenidos_eventos` as ce ORDER BY ce.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerEvento($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ce.* FROM `contenidos_eventos` as ce WHERE ce.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerEventosRelacionadas($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ce.* FROM `contenidos_eventos` as ce WHERE ce.id != :id ORDER BY RAND() LIMIT 4");
		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}








	public static function traerCategoria($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_categorias` as cc WHERE cc.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerArchivosIdentificador($_identificador)
	{
		return  contenidos_archivo::find('all',array('conditions' => array('identificador = ?', $_identificador)));
	}


	public function traerTendencia($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct WHERE ct.id = :id");

		/*$result = Pd::instancia()->prepare("SELECT  ct.*,ca.path ,ca.formato
											FROM contenidos_tendencias as ct											
											LEFT JOIN  contenidos_archivos as ca
											ON ct.identificador = ca.identificador
											WHERE ct.id = :id");*/

		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	
	public function traerTendencias()
	{		
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct ORDER BY ct.fecha DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	/*public function traerTendencias($_limit, $_cat='')
	{

		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='ct.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='ct.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct WHERE ".$_html." ORDER BY ct.fecha DESC LIMIT $_limit");
			
		}else{
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct ORDER BY ct.fecha DESC LIMIT $_limit");			
		}
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}*/

	public static function contarRegistrosTendencias($_cat='')
	{
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='ct.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='ct.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_tendencias` as ct WHERE ".$_html."");
		}else{
			$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_tendencias` as ct");
			
		}
		$_reg->execute();
		$_registros = $_reg->fetch(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros['cant'] : false;
	}

	public function paginadorTendencias($_pag, $_limit, $_cat='')
	{
		$_offset = ($_pag-1) * $_limit;		
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='ct.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='ct.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$_reg = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct WHERE ".$_html." ORDER BY ct.fecha DESC LIMIT $_limit OFFSET $_offset");			
		}else{
			$_reg = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct ORDER BY ct.fecha DESC LIMIT $_limit OFFSET $_offset");
		}
		$_reg->execute();
		$_registros = $_reg->fetchAll(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros : false;
	}

	

	public function traerTendenciasPorCat($_limit, $_cat='')
	{	
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='ct.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='ct.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct WHERE ".$_html." ORDER BY ct.fecha DESC LIMIT $_limit");
			
		}else{
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct ORDER BY ct.fecha DESC LIMIT $_limit");
		}
		
		$result->execute();	
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		
		return ($result) ? $result : false;
		
	}




	public function traerTendenciasRelacionadas($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct WHERE ct.id != :id ORDER BY RAND() LIMIT 4");
		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerClubaliadas()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_clubaliadas` as cc ORDER BY cc.puntos DESC");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}
	
	public function traerClubaliadasOrden($_val)
	{
		$_lanz = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_clubaliadas` as cc ORDER BY cc.puntos $_val");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}
	/*public function traerClubaliadas($_limit)
	{
		$_lanz = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_clubaliadas` as cc ORDER BY cc.fecha DESC LIMIT $_limit");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}*/

	public static function contarRegistrosClubaliadas()
	{
		$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_clubaliadas` as cc");
		$_reg->execute();
		$_registros = $_reg->fetch(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros['cant'] : false;
	}

	public function paginadorClubaliadas($_pag, $_limit)
	{
		$_offset = ($_pag-1) * $_limit;
		$_reg = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_clubaliadas` as cc ORDER BY cc.fecha DESC LIMIT $_limit OFFSET $_offset");
		$_reg->execute();
		$_registros = $_reg->fetchAll(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros : false;
	}

	public function traerClubaliada($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_clubaliadas` as cc WHERE cc.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerClubaliadaStatic($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_clubaliadas` as cc WHERE cc.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	/*public function traerBuscadorTags($_valor)
	{
		$result = Pd::instancia()->prepare("(SELECT cl.id,cl.titulo,'lanzamientos' as tabla FROM `contenidos_lanzamientos` as cl WHERE FIND_IN_SET ('".$_valor."', cl.tags))
											UNION ALL 
											(SELECT ccp.id,ccp.titulo,'capacitaciones' as tabla FROM `contenidos_capacitaciones` as ccp WHERE FIND_IN_SET ('".$_valor."', ccp.tags))
											UNION ALL 
											(SELECT ct.id,ct.titulo,'tendencias' as tabla FROM `contenidos_tendencias` as ct WHERE FIND_IN_SET ('".$_valor."', ct.tags))
											UNION ALL 
											(SELECT ctu.id,ctu.titulo,'tutoriales' as tabla FROM `contenidos_tutoriales` as ctu WHERE FIND_IN_SET ('".$_valor."', ctu.tags))
											UNION ALL 
											(SELECT ce.id,ce.titulo,'eventos' as tabla FROM `contenidos_eventos` as ce WHERE FIND_IN_SET ('".$_valor."', ce.tags))
											UNION ALL 
											(SELECT cli.id,cli.titulo,'linearios' as tabla FROM `contenidos_linearios` as cli WHERE FIND_IN_SET ('".$_valor."', cli.tags)) ORDER BY id DESC");



		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}*/


	public function traerBuscadorTags($_valor)
	{
		$result = Pd::instancia()->prepare("(SELECT cl.id,cl.titulo,'lanzamientos' as tabla FROM `contenidos_lanzamientos` as cl WHERE cl.tags LIKE '%".$_valor."%')
											UNION ALL 
											(SELECT ccp.id,ccp.titulo,'capacitaciones' as tabla FROM `contenidos_capacitaciones` as ccp WHERE ccp.tags LIKE '%".$_valor."%')
											UNION ALL 
											(SELECT ct.id,ct.titulo,'tendencias' as tabla FROM `contenidos_tendencias` as ct WHERE ct.tags LIKE '%".$_valor."%')
											UNION ALL 
											(SELECT ctu.id,ctu.titulo,'tutoriales' as tabla FROM `contenidos_tutoriales` as ctu WHERE ctu.tags LIKE '%".$_valor."%')
											UNION ALL 
											(SELECT ce.id,ce.titulo,'eventos' as tabla FROM `contenidos_eventos` as ce WHERE ce.tags LIKE '%".$_valor."%')
											UNION ALL 
											(SELECT cli.id,cli.titulo,'linearios' as tabla FROM `contenidos_linearios` as cli WHERE cli.tags LIKE '%".$_valor."%') ORDER BY id DESC");



		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}


	public function traerTags($_valor)
	{
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tags` as ct WHERE ct.nombre LIKE '%".$_valor."%'");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function traerBannerPorSeccion($_seccion, $_posicion, $_formato='')
	{
		if($_formato!=''){
			$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners` as cb WHERE FIND_IN_SET ('".$_seccion."', cb.ids_seccion) AND FIND_IN_SET ('".$_posicion."', cb.ids_posicion) AND cb.id_formato = :formato");
			$result->execute(array(":formato" => $_formato));
		}else{
			$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners` as cb WHERE FIND_IN_SET ('".$_seccion."', cb.ids_seccion) AND FIND_IN_SET ('".$_posicion."', cb.ids_posicion)");
			$result->execute();
		}
				
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerBannerPorSeccionDos($_seccion, $_posicion, $_formato='')
	{
		if($_formato!=''){
			$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners_dos` as cb WHERE FIND_IN_SET ('".$_seccion."', cb.ids_seccion) AND FIND_IN_SET ('".$_posicion."', cb.ids_posicion) AND cb.id_formato = :formato");
			$result->execute(array(":formato" => $_formato));
		}else{
			$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners_dos` as cb WHERE FIND_IN_SET ('".$_seccion."', cb.ids_seccion) AND FIND_IN_SET ('".$_posicion."', cb.ids_posicion)");
			$result->execute();
		}
				
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerBannerPorSeccionRandom($_seccion, $_posicion, $_formato='')
	{
		if($_formato!=''){
			$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners` as cb WHERE FIND_IN_SET ('".$_seccion."', cb.ids_seccion) AND FIND_IN_SET ('".$_posicion."', cb.ids_posicion) AND cb.id_formato = :formato ORDER BY RAND() LIMIT 1");
			$result->execute(array(":formato" => $_formato));
		}else{
			$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners` as cb WHERE FIND_IN_SET ('".$_seccion."', cb.ids_seccion) AND FIND_IN_SET ('".$_posicion."', cb.ids_posicion) ORDER BY RAND() LIMIT 1");
			$result->execute();
		}
				
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}



	public static function traerDataImagenPorIdentificador($_identificador, $_pos)
	{	
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ?', $_identificador, $_pos)));		
	}

	public static function traerDataImagenPorIdentificador2($_identificador, $_pos, $_tipo='')
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ? AND tipo = ?', $_identificador, $_pos, $_tipo)));
	}
	
	/*public static function traerDataImagenPorIdentificadorV2($_identificador, $_pos, $_tipo='')
	{
		return  contenidos_imagene::find(array('all','conditions' => array('identificador = ? AND posicion = ? AND tipo = ?', $_identificador, $_pos, $_tipo),'order' => 'orden asc'));
	}*/

	public static function traerDataImagenPorIdentificadorV2($_identificador,$_pos)
	{
		// return  contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND posicion = ?', $_identificador, $_pos),'order' => 'orden asc'));

		$result = Pd::instancia()->prepare("SELECT ci.* FROM `contenidos_imagenes` as ci WHERE ci.identificador = :identificador AND ci.posicion = :pos ORDER BY ci.orden ASC");
		$result->execute(array(":identificador" => $_identificador, ":pos" => $_pos));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerNovedades($_limit)
	{
		$_cap = self::traerDestPorTabla('capacitaciones');
		$_cap = ($_cap!='') ? implode(',', $_cap) : 0;
		$_ten = self::traerDestPorTabla('tendencias');
		$_ten = ($_ten!='') ? implode(',', $_ten) : 0;
		$_lin = self::traerDestPorTabla('linearios');
		$_lin = ($_lin!='') ? implode(',', $_lin) : 0; 
		$_eve = self::traerDestPorTabla('eventos');
		$_eve = ($_eve!='') ? implode(',', $_eve) : 0;
		$_tut = self::traerDestPorTabla('tutoriales');
		$_tut = ($_tut!='') ? implode(',', $_tut) : 0;

		$result = Pd::instancia()->prepare("(SELECT ccp.id,ccp.titulo,ccp.categorias,ccp.bajada,ccp.identificador,ccp.fecha,'capacitaciones' as tabla FROM `contenidos_capacitaciones` as ccp WHERE ccp.id NOT IN ($_cap)) 
			UNION ALL 
			(SELECT ct.id,ct.titulo,ct.categorias,ct.bajada,ct.identificador,ct.fecha,'tendencias' as tabla FROM `contenidos_tendencias` as ct WHERE ct.id NOT IN ($_ten))			
			UNION ALL 
			(SELECT cl.id,cl.titulo,cl.categorias,cl.bajada,cl.identificador,cl.fecha,'linearios' as tabla FROM `contenidos_linearios` as cl WHERE cl.id NOT IN ($_lin))
			UNION ALL 
			(SELECT ce.id,ce.titulo,ce.categorias,ce.bajada,ce.identificador,ce.fecha,'eventos' as tabla FROM `contenidos_eventos` as ce WHERE ce.id NOT IN ($_eve))
			UNION ALL 
			(SELECT ctt.id,ctt.titulo,ctt.categorias,ctt.bajada,ctt.identificador,ctt.fecha,'tutoriales' as tabla FROM `contenidos_tutoriales` as ctt WHERE ctt.id NOT IN ($_tut)) ORDER BY id DESC LIMIT $_limit");

		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public static function contarRegistrosNovedades()
	{
		$_cap = self::traerDestPorTabla('capacitaciones');
		$_cap = ($_cap!='') ? implode(',', $_cap) : 0;
		$_ten = self::traerDestPorTabla('tendencias');
		$_ten = ($_ten!='') ? implode(',', $_ten) : 0;
		$_lin = self::traerDestPorTabla('linearios');
		$_lin = ($_lin!='') ? implode(',', $_lin) : 0; 
		$_eve = self::traerDestPorTabla('eventos');
		$_eve = ($_eve!='') ? implode(',', $_eve) : 0;
		$_tut = self::traerDestPorTabla('tutoriales');
		$_tut = ($_tut!='') ? implode(',', $_tut) : 0;

		$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM 
										(SELECT ccp.id FROM `contenidos_capacitaciones` as ccp WHERE ccp.id NOT IN ($_cap) 
										UNION ALL 
										SELECT ct.id FROM `contenidos_tendencias` as ct WHERE ct.id NOT IN ($_ten)
										UNION ALL 
										SELECT cl.id FROM `contenidos_linearios` as cl WHERE cl.id NOT IN ($_lin)
										UNION ALL 
										SELECT ce.id FROM `contenidos_eventos` as ce WHERE ce.id NOT IN ($_eve)
										UNION ALL 
										SELECT ctt.id FROM `contenidos_tutoriales` as ctt WHERE ctt.id NOT IN ($_tut)) as cant");
		$_reg->execute();
		$_registros = $_reg->fetch(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros['cant'] : false;
	}

	public function paginadorNovedades($_pag, $_limit)
	{
		$_cap = self::traerDestPorTabla('capacitaciones');
		$_cap = ($_cap!='') ? implode(',', $_cap) : 0;
		$_ten = self::traerDestPorTabla('tendencias');
		$_ten = ($_ten!='') ? implode(',', $_ten) : 0;
		$_lin = self::traerDestPorTabla('linearios');
		$_lin = ($_lin!='') ? implode(',', $_lin) : 0; 
		$_eve = self::traerDestPorTabla('eventos');
		$_eve = ($_eve!='') ? implode(',', $_eve) : 0;
		$_tut = self::traerDestPorTabla('tutoriales');
		$_tut = ($_tut!='') ? implode(',', $_tut) : 0;

		$_offset = ($_pag-1) * $_limit;
		
		$_reg = Pd::instancia()->prepare("(SELECT ccp.id,ccp.titulo,ccp.categorias,ccp.bajada,ccp.identificador,ccp.fecha,'capacitaciones' as tabla FROM `contenidos_capacitaciones` as ccp WHERE ccp.id NOT IN ($_cap)) 
			UNION ALL 
			(SELECT ct.id,ct.titulo,ct.categorias,ct.bajada,ct.identificador,ct.fecha,'tendencias' as tabla FROM `contenidos_tendencias` as ct WHERE ct.id NOT IN ($_ten))			
			UNION ALL 
			(SELECT cl.id,cl.titulo,cl.categorias,cl.bajada,cl.identificador,cl.fecha,'linearios' as tabla FROM `contenidos_linearios` as cl WHERE cl.id NOT IN ($_lin))
			UNION ALL 
			(SELECT ce.id,ce.titulo,ce.categorias,ce.bajada,ce.identificador,ce.fecha,'eventos' as tabla FROM `contenidos_eventos` as ce WHERE ce.id NOT IN ($_eve))
			UNION ALL 
			(SELECT ctt.id,ctt.titulo,ctt.categorias,ctt.bajada,ctt.identificador,ctt.fecha,'tutoriales' as tabla FROM `contenidos_tutoriales` as ctt WHERE ctt.id NOT IN ($_tut)) ORDER BY id DESC LIMIT $_limit OFFSET $_offset");

		/*$_reg = Pd::instancia()->prepare("(SELECT ccp.id,ccp.titulo,ccp.categorias,ccp.identificador,'capacitaciones' as tabla FROM `contenidos_capacitaciones` as ccp) 
			UNION ALL 
			(SELECT ct.id,ct.titulo,ct.categorias,ct.identificador,'tendencias' as tabla FROM `contenidos_tendencias` as ct)
			UNION ALL 
			(SELECT cl.id,cl.titulo,cl.categorias,cl.identificador,'lanzamientos' as tabla FROM `contenidos_lanzamientos` as cl) ORDER BY id DESC LIMIT $_limit OFFSET $_offset");*/
		$_reg->execute();
		$_registros = $_reg->fetchAll(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros : false;
	}

	/*public function traerDestacados()
	{
		$result = Pd::instancia()->prepare("SELECT ccp.id,ccp.titulo,ccp.categorias,ccp.identificador,'capacitaciones' as tabla FROM `contenidos_capacitaciones` as ccp WHERE ccp.destacado = 'si' 
			UNION ALL 
			SELECT ct.id,ct.titulo,ct.categorias,ct.identificador,'tendencias' as tabla FROM `contenidos_tendencias` as ct WHERE ct.destacado = 'si'
			UNION ALL 
			SELECT cl.id,cl.titulo,cl.categorias,cl.identificador,'lanzamientos' as tabla FROM `contenidos_lanzamientos` as cl WHERE cl.destacado = 'si'");

		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}*/

	public function traerDestacados()
	{
		$result = Pd::instancia()->prepare("SELECT cd.* FROM `contenidos_destacados` as cd ORDER BY cd.posicion ASC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerDestPorTabla($_seccion)
	{
		$_tabla = 'contenidos_'.$_seccion;
		$result = Pd::instancia()->prepare("SELECT id_nota FROM `contenidos_destacados` WHERE tabla = :tabla");
		$result->execute(array(":tabla" => $_tabla));
		$result = $result->fetchAll(PDO::FETCH_COLUMN);

		return ($result) ? $result : false;
	}

	public static function traerNota($_id, $_seccion)
	{
		$_tabla = 'contenidos_'.$_seccion;
		$result = Pd::instancia()->prepare("SELECT * FROM $_tabla WHERE id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerBuscadorCategorias($_valor)
	{
		$result1 = Pd::instancia()->prepare("SELECT cl.id,cl.titulo,'lanzamientos' as tabla FROM `contenidos_lanzamientos` as cl WHERE FIND_IN_SET ('".$_valor."', cl.categorias) ORDER BY cl.id DESC LIMIT 10");
		$result1->execute();
		$result1 = $result1->fetchAll(PDO::FETCH_ASSOC);


		$result2 = Pd::instancia()->prepare("SELECT ccp.id,ccp.titulo,'capacitaciones' as tabla FROM `contenidos_capacitaciones` as ccp WHERE FIND_IN_SET ('".$_valor."', ccp.categorias) ORDER BY ccp.id DESC LIMIT 2");
		$result2->execute();
		$result2 = $result2->fetchAll(PDO::FETCH_ASSOC);

		$result3 = Pd::instancia()->prepare("SELECT ct.id,ct.titulo,'tendencias' as tabla FROM `contenidos_tendencias` as ct WHERE FIND_IN_SET ('".$_valor."', ct.categorias) ORDER BY ct.id DESC LIMIT 2");
		$result3->execute();
		$result3 = $result3->fetchAll(PDO::FETCH_ASSOC);

		$result4 = Pd::instancia()->prepare("SELECT cli.id,cli.titulo,'linearios' as tabla FROM `contenidos_linearios` as cli WHERE FIND_IN_SET ('".$_valor."', cli.categorias) ORDER BY cli.id DESC LIMIT 2");
		$result4->execute();
		$result4 = $result4->fetchAll(PDO::FETCH_ASSOC);

		$result5 = Pd::instancia()->prepare("SELECT ce.id,ce.titulo,'eventos' as tabla FROM `contenidos_eventos` as ce WHERE FIND_IN_SET ('".$_valor."', ce.categorias) ORDER BY ce.id DESC LIMIT 2");
		$result5->execute();
		$result5 = $result5->fetchAll(PDO::FETCH_ASSOC);

		$result6 = Pd::instancia()->prepare("SELECT ctu.id,ctu.titulo,'tutoriales' as tabla FROM `contenidos_tutoriales` as ctu WHERE FIND_IN_SET ('".$_valor."', ctu.categorias) ORDER BY ctu.id DESC LIMIT 2");
		$result6->execute();
		$result6 = $result6->fetchAll(PDO::FETCH_ASSOC);

		

		$result10 = array_merge($result2, $result3, $result4, $result5, $result6);
		
		if($result10){
			$result['ultimos'] =$result10;
		}
		if($result1){
			$result['lanzamientos'] =$result1;
		}
		/*if($result2){
			$result['capacitaciones'] =$result2;
		}
		if($result3){
			$result['tendencias'] =$result3;
		}
		*/
		
		

		return ($result) ? $result : false;
		
	}


	public static function traerCategorias()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_categorias` as cc");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}

	public function traerVideo($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct WHERE ct.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerVideos()
	{
		
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct ORDER BY ct.fecha DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	/*public function traerVideos($_limit, $_cat='')
	{
		if($_cat!=''){
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct WHERE FIND_IN_SET ('".$_cat."', ct.categorias) ORDER BY ct.fecha DESC LIMIT $_limit");
		}else{
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct ORDER BY ct.fecha DESC LIMIT $_limit");			
		}		
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}*/

	public static function contarRegistrosVideos($_cat='')
	{
		if($_cat!=''){
			$result = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_tutoriales` as ct FIND_IN_SET ('".$_cat."', ct.categorias)");
		}else{
			$result = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_tutoriales` as ct");
			
		}
		$result->execute();
		$_registros = $result->fetch(PDO::FETCH_ASSOC);
		$result = null;
		return ($_registros) ? $_registros['cant'] : false;
	}

	public function paginadorVideos($_pag, $_limit, $_cat='')
	{
		$_offset = ($_pag-1) * $_limit;

		if($_cat!=''){
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct WHERE FIND_IN_SET ('".$_cat."', ct.categorias) ORDER BY ct.fecha DESC LIMIT $_limit OFFSET $_offset");
		}else{
			$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct ORDER BY ct.fecha DESC LIMIT $_limit OFFSET $_offset");
		}

		$result->execute();
		$_registros = $result->fetchAll(PDO::FETCH_ASSOC);
		$result = null;
		return ($_registros) ? $_registros : false;
	}

	public function traerVideosRelacionadas($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct WHERE ct.id != :id ORDER BY RAND() LIMIT 4");
		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerVideosPorCat($_limit, $_cat)
	{	
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct WHERE FIND_IN_SET ('".$_valor."', ct.categorias) ORDER BY ct.fecha DESC LIMIT $_limit");	
		$result->execute();	
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		
		return ($result) ? $result : false;
		
	}

	public function traerLineario($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl WHERE cl.id = :id");

		/*$result = Pd::instancia()->prepare("SELECT  ct.*,ca.path ,ca.formato
											FROM contenidos_tendencias as ct											
											LEFT JOIN  contenidos_archivos as ca
											ON ct.identificador = ca.identificador
											WHERE ct.id = :id");*/

		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	
	public function traerLinearios()
	{

		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl ORDER BY cl.fecha DESC");			
		
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	/*public function traerLinearios($_limit, $_cat='')
	{

		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cl.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cl.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl WHERE ".$_html." ORDER BY cl.fecha DESC LIMIT $_limit");
			
		}else{
			$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl ORDER BY cl.fecha DESC LIMIT $_limit");			
		}
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}*/

	public static function contarRegistrosLinearios($_cat='')
	{
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cl.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cl.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_linearios` as cl WHERE ".$_html."");
		}else{
			$_reg = Pd::instancia()->prepare("SELECT count(*) as cant FROM `contenidos_linearios` as cl");
			
		}
		$_reg->execute();
		$_registros = $_reg->fetch(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros['cant'] : false;
	}

	public function paginadorLinearios($_pag, $_limit, $_cat='')
	{
		$_offset = ($_pag-1) * $_limit;		
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cl.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cl.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$_reg = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl WHERE ".$_html." ORDER BY cl.fecha DESC LIMIT $_limit OFFSET $_offset");			
		}else{
			$_reg = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl ORDER BY cl.fecha DESC LIMIT $_limit OFFSET $_offset");
		}
		$_reg->execute();
		$_registros = $_reg->fetchAll(PDO::FETCH_ASSOC);
		$_reg = null;
		return ($_registros) ? $_registros : false;
	}

	

	public function traerLineariosPorCat($_limit, $_cat='')
	{	
		if($_cat!=''){
			$_html='';
			foreach ($_cat as $val) {
				if($val === end($_cat)){
					$_html .='cl.categorias LIKE "%'.$val.'%"';
				}else{
					$_html .='cl.categorias LIKE "%'.$val.'%" OR ';
				}				
			}
			$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl WHERE ".$_html." ORDER BY cl.fecha DESC LIMIT $_limit");
			
		}else{
			$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl ORDER BY cl.fecha DESC LIMIT $_limit");
		}
		
		$result->execute();	
		$result = $result->fetchAll(PDO::FETCH_ASSOC);
		
		return ($result) ? $result : false;
		
	}




	public function traerLineariosRelacionadas($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl WHERE cl.id != :id ORDER BY RAND() LIMIT 4");
		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}























	///////////////////////////////////////////////////////////////////////////////



	public function traerTotemPorToken($_token)
	{
		return contenidos_totem::find(array('conditions' => array('codigo = ?', $_token)));
	}
	public function traerAsigAlta($_id)
	{	
		$results = contenidos_asignacione::find('all',array('select' => 'id,id_totem,ids_catalogos,ids_videos,ids_footers,ids_fondos,ids_inactivas','conditions' => array('id_totem = ? AND estado = ?', $_id, 'alta')));
		return $this->armarArray($results);
	}
	public function traerAsignaciones($_id)
	{	
		if($_id!=false){
			return contenidos_asignacione::find('all',array('conditions' => array('id_totem = ?', $_id)));
		}else{
			return contenidos_asignacione::all();
		}
		
	}

	public function traerAsigModif($_id)
	{
		/*$results =  contenidos_asignacione::find_by_sql("SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ('".$_id."', $_campo)");
		return $this->armarArray($results);*/
		$results = contenidos_asignacione::find('all',array('select' => 'id,id_totem,ids_catalogos,ids_videos,ids_footers,ids_fondos,ids_inactivas,update_contenido','conditions' => array('id_totem = ? AND estado = ? AND update_contenido != ?', $_id, 'alta', '')));
		return $this->armarArray($results);
	}
	
	public function updateEstadoAsig($_id, $_estado)
	{	
		$_asig = contenidos_asignacione::find($_id);		
		$_asig->estado = $_estado;
		
		return ($_asig->save()) ? true : false;
		/*$con = Conexion::con();		
		$result = $con->prepare("UPDATE $_tabla SET estado = :estado WHERE id = :id");
		$result->execute(array(":id" => $_id, ":estado" => $_estado));
		return ($result) ? true : false;*/
	}
	
	public function traerCatalogos($_ids)
	{
		$results = contenidos_catalogo::find_by_sql('SELECT  id,titulo,imagenes_orientacion,identificador FROM contenidos_catalogos WHERE id IN ('.$_ids.')');
		return $this->armarArray($results);
	}
	/*public function traerVideos($_ids)
	{
		$results = contenidos_video::find_by_sql('SELECT  ch.id,ch.identificador,cv.path 
													FROM contenidos_header as ch
													INNER JOIN  contenidos_videos as cv
													ON ch.identificador = cv.identificador
													WHERE ch.id IN ('.$_ids.')');
		return $this->armarArray($results);
	}*/
	public function traerFooters($_ids)
	{
		$results = contenidos_promocione::find_by_sql('SELECT  cp.id,cp.identificador,ci.path 
													FROM contenidos_promociones as cp
													INNER JOIN  contenidos_imagenes as ci
													ON cp.identificador = ci.identificador
													WHERE cp.id IN ('.$_ids.')');
		return $this->armarArray($results);
	}
	public function traerFondos($_ids)
	{
		$results = contenidos_fondo::find_by_sql('SELECT  cf.id,cf.identificador,ci.path 
													FROM contenidos_fondos as cf
													INNER JOIN  contenidos_imagenes as ci
													ON cf.identificador = ci.identificador
													WHERE cf.id IN ('.$_ids.')');
		return $this->armarArray($results);
	}
	
	public function traerInactivas($_ids)
	{
		$results = contenidos_pantalla_inactiv::find_by_sql('SELECT  cpi.id,cpi.identificador,cv.path 
																FROM contenidos_pantalla_inactiva as cpi
																INNER JOIN  contenidos_videos as cv
																ON cpi.identificador = cv.identificador
																WHERE cpi.id IN ('.$_ids.')');
		return $this->armarArray($results);
	}


	public static function super_unique($array)
	{
		$result = array_map("unserialize", array_unique(array_map("serialize", $array)));
		foreach ($result as $key => $value){
			if ( is_array($value) ){
			  $result[$key] = self::super_unique($value);
			}
		}
		return $result;
	}
	
	public function traerIDComputadora($_id)
	{
		/*$results =  contenidos_codigo::find('all',array('select' => 'id, logo_super', 'conditions' => array('codigo = ?', $_id)));
		return $this->armarArray($results);*/
		return contenidos_codigo::find(array('select' => 'id, logo_super', 'conditions' => array('codigo = ?', $_id)));
	}
	
	public function traerPromociones($_id,$_estado='alta')
	{
		$results =  contenidos_promocione::find('all',array('conditions' => array('id_computadora = ? AND estado = ?', $_id, $_estado)));
		return $this->armarArray($results);
	}
	
	public function traerImgPorIdentificador($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	public function traerVideoPorIdentificador($_identificador)
	{
		return  contenidos_video::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	public function copiarDirectorios($fuente, $destino)
	{
		if(is_dir($fuente)){
			$dir=opendir($fuente);
			while($archivo=readdir($dir)){
				if($archivo!="." && $archivo!=".."){
					if(is_dir($fuente."/".$archivo)){
						if(!is_dir($destino."/".$archivo)){
							mkdir($destino."/".$archivo);
						}
						$this->copiarDirectorios($fuente."/".$archivo, $destino."/".$archivo);
					}
					else{
						copy($fuente."/".$archivo, $destino."/".$archivo);
					}
				}
			}
			closedir($dir);
		}else{
			copy($fuente, $destino);
		}
	}
	
	public function copiarDirectorios2($src,$dst,$rwrite=false)
	{ 
		$dir = opendir($src); 
		if (!file_exists($dst)) mkdir($dst); 
		while(false !== ( $file = readdir($dir)) ) { 
			if (( $file != '.' ) && ( $file != '..' )) { 
				if ( is_dir($src . '/' . $file) ) { 
					$this->copiarDirectorios2($src . '/' . $file,$dst . '/' . $file,$rwrite); 
				} 
				else { 
					if ($rwrite) copy($src . '/' . $file,$dst . '/' . $file);
				} 
			} 
		} 
		closedir($dir); 
	}
	
	public function borrarDir($carpeta)
	{
      foreach(glob($carpeta . "/*") as $archivos_carpeta){             
        if (is_dir($archivos_carpeta)){
          $this->borrarDir($archivos_carpeta);
        } else {
        unlink($archivos_carpeta);
        }
      }
      rmdir($carpeta);
    }


	public static function diferenciaArrayMultidimensional($array1, $array2)
	{
		foreach($array1 as $key => $value){
			if(is_array($value)){
				if(!isset($array2[$key])){
					$difference[$key] = $value;
				}elseif(!is_array($array2[$key])){
					$difference[$key] = $value;
				}else{
					$new_diff = self::diferenciaArrayMultidimensional($value, $array2[$key]);
					if($new_diff != FALSE){
						$difference[$key] = $new_diff;
					}
				}
			}elseif(!isset($array2[$key]) || $array2[$key] != $value){
				$difference[$key] = $value;
			}
		}
		return !isset($difference) ? 0 : $difference;
	}


	
	public function selectSincroAsig($_totem, $_key, $_val)
	{
		$_campo = 'ids_'.$_key;
		//return contenidos_asignacione::find_by_sql("UPDATE contenidos_asignaciones SET fecha_sincronizacion = now() WHERE id_totem = ".$_totem." AND estado = 'alta' AND FIND_IN_SET ('".$_val."',".$_campo.")");
		$_data = contenidos_asignacione::find_by_sql("SELECT * FROM contenidos_asignaciones WHERE id_totem = ".$_totem." AND estado = 'alta' AND FIND_IN_SET ('".$_val."',".$_campo.")");
		if($_data){
			foreach($_data as $dat){
				$this->updateSincroAsig($dat->id);
			}
		}
		return true;
	}
	private function updateSincroAsig($_id)
	{
		$_data = contenidos_asignacione::find($_id);
		if($_data){
			$_fechaBd = date('Y-m-d H:i:s');
			$_data->fecha_sincronizacion = "$_fechaBd";
			$_data->save();
		}
	}
	
	public static function cambiarNombreImg($_identificador, $_path)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND path = ?', $_identificador, $_path)));
	}
	
	
	
	
	
	
	
	
	
	
	
	public function traerTrabajos($_estado, $_area, $_prov)
	{
		//return  contenidos_trabajo::find('all',array('conditions' => array('estado = ?', $_estado),'order' => 'fecha_publicacion asc'));
		//return contenidos_trabajo::find_by_sql("SELECT id FROM contenidos_trabajos WHERE estado = ".$_estado." OR id_area IN (".$_area.") OR provincia IN (".$_prov.") ORDER BY fecha_publicacion ASC");
		//return contenidos_trabajo::find_by_sql("SELECT id FROM contenidos_trabajos WHERE estado = 1 OR id_area IN (0) OR provincia IN (9) ORDER BY fecha_publicacion ASC");
		
		/*$_area = 0;
		$_prov = 0;*/
		
		$arrayCont = array();
		$_html	='SELECT * FROM contenidos_trabajos WHERE ';
		
		if($_area!=0){
			$arrayCont[] = 'id_area IN ('.$_area.')';
		}
		
		if($_prov!=0){
			$arrayCont[] = 'provincia IN ('.$_prov.')';
		}
		
		foreach ($arrayCont as $arr){
			$_html .= $arr. " AND ";																									
		}
		
		$_html .= "estado = ".$_estado." ORDER BY fecha_publicacion ASC";
		
		$_data = contenidos_trabajo::find_by_sql($_html);
		
		return $_data;
		
			
	}
	
	
	public function traerPostulacionEspontanea()
	{
		return contenidos_trabajo::find(array('conditions' => array('id_area = ? AND estado = ?', 0, 3)));
			
	}
	
	
	public function traerTrabajo($_id)
	{
		return contenidos_trabajo::find(array('conditions' => array('identificador = ?', $_id)));
			
	}
	
	public static function traerProvincia($_id)
	{
		return contenidos_provincia::find($_id);
	}
	
	public static function traerLocalidad($_id)
	{
		return contenidos_localidade::find($_id);
	}
	
	public static function traerLocalidadPorId($_id)
	{
		//return contenidos_localidade::find($_id);
		return contenidos_localidade::find(array('conditions' => array('id_localidad = ?', $_id)));
	}
	
	public function traerSexo()
	{
		return contenidos_sex::all(array('order' => 'id asc'));
	}
	
	public function traerEstadoCivil()
	{
		return contenidos_estado_civi::all(array('order' => 'id asc'));
	}
	
	public function traerNacionalidades()
	{
		return contenidos_nacionalidade::all(array('order' => 'nombre asc'));
	}
	
	public function traerProvincias()
	{
		return contenidos_provincia::all(array('order' => 'nombre asc'));
	}
	
	public function traerLocalidadesPorId($_id)
	{
		return contenidos_localidade::find('all',array('conditions' => array('id_prov = ?', $_id),'order' => 'nombre asc'));
			
	}
	public function traerUniversidades()
	{
		return contenidos_universidade::all(array('order' => 'nombre asc'));
	}
	
	public function traerCarreras()
	{
		return contenidos_carrera::all(array('order' => 'nombre asc'));
	}
	
	public function traerTipoDoc()
	{
		return contenidos_tipo_document::all(array('order' => 'id asc'));
	}
	
		public function traerAreas()
	{
		return contenidos_area::all(array('order' => 'nombre asc'));
	}
	
	public function traerArea($_id)
	{
		return contenidos_area::find($_id);
	}
	
	public function traerSectores()
	{
		return  contenidos_sectore::find('all',array('order' => 'id asc'));
			
	}
	
	public static function traerAreaPorID($_id)
	{
		return contenidos_area::find($_id);
	}
	
	
	
	
	
	
	
	
	
	/*public function traerUser($_id)
	{
		return contenidos_user::find($_id);
	}
	
	public static function traerUserPorId($_id)
	{
		return contenidos_user::find($_id);
	}*/
	
	public static function traerDataImagen($_id)
	{
	
		return  contenidos_imagene::find(array('conditions' => array('id = ?', $_id)));
		
	}
	
	
	
	public static function traerImgMod($_identificador, $_modulo, $_pos)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND modulo = ? AND posicion = ?', $_identificador, $_modulo, $_pos)));
	}
	
	public static function traerImgMedio($_identificador, $_id)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND medio = ?', $_identificador, $_id)));
	}
	
	
	public function traerSliders()
	{
		$results = contenidos_slider::all(array('order' => 'orden asc'));
		$_datos = $this->armarArray($results);
		return $_datos;
		/*$arrayResult = array_map(function($res){
		  return $res->attributes();
		}, $results);
		return $arrayResult;*/
	}
	
	public function traerEquipoCompleto()
	{
		$results = contenidos_equip::all(array('order' => 'orden asc'));
		$_datos = $this->armarArray($results);
		return $_datos;
	}
	
	public function traerNoticias()
	{
		$results = contenidos_blo::all(array('order' => 'orden asc'));
		$_datos = $this->armarArray($results);
		return $_datos;
	}
	
	public function traerCatNoticias()
	{
		$results = contenidos_blo::all(array('select' => 'DISTINCT categoria','order' => 'categoria asc'));
		$_datos = $this->armarArray($results);
		return $_datos;
	}
	
	public function traerNoticia($_id)
	{
		return contenidos_blo::find($_id);
	}
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	
	public function traerDatosImagenes($_dia, $_limit)
	{
		//return  contenidos_datos_imagene::find('all',array('conditions' => array('dia = ?', $_dia)));
		return contenidos_datos_imagene::find('all',array('conditions' => array('dia = ?', $_dia),'order' => 'id asc','limit' => $_limit));
	}
	
	public static function traerImgChica($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	
	public static function contarRegistros($_dia)
	{
		return contenidos_datos_imagene::find(array('conditions' => array('dia = ?', $_dia),'select' => 'count(*) as cant'));
	}
	
	public function paginador($_dia,$_pag,$_limit)
	{
		//contenidos_producto::find('all', array('limit' => 10, 'offset' => 5));
		//"select * from persona order by nombre limit %d offset %d ", 5, ($pag-1)*5);
		return contenidos_datos_imagene::find('all',array('conditions' => array('dia = ?', $_dia),'order' => 'id asc','limit' => $_limit,'offset' => ($_pag-1)*$_limit));
	}
	
	
	
	
	
	
	
	
	public function traerDestacadoModuloActivo($_modulo)
	{
		return contenidos_destacados_modulo::find(array('conditions' => array('modulo = ?', $_modulo)));
	}
	
	/*public function traerDestacados()
	{
		return  contenidos_destacado::all(array('order' => 'orden asc'));
			
	}*/
	
	public static function traerImg($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	public function traerBeneficios($_estado)
	{
		return  contenidos_beneficio::find('all',array('conditions' => array('estado = ?', $_estado),'order' => 'orden asc'));
			
	}
	
	public function traerManuales()
	{
		return  contenidos_manuale::all(array('order' => 'orden asc'));
			
	}
	
	
	
	public static function traerArchivo($_identificador)
	{
		return  contenidos_archivo::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	public static function traerJuegos()
	{
		return  contenidos_juego::all(array('order' => 'orden asc'));
			
	}
	
	
	
	
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////
	
	public function traerPromos($_estado)
	{
		return  contenidos_promo::find('all',array('conditions' => array('estado = ?', $_estado),'order' => 'orden asc'));		
	}
	
	public static function traerImgPpal($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND orden = ?', $_identificador,0)));
	}
	
	public static function traerImgFondo($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND orden = ?', $_identificador,1)));
	}
	
	public static function traerTermino($_identificador)
	{
		return  contenidos_termino::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	
	/**************************************************************************************************/
	public function CapturarAnchoAlto($recurso)											
	{
		$dimension = array();
		
		list($ancho, $alto) = getimagesize($recurso);
		$dimension[] = $ancho;
		$dimension[] = $alto;
		return $dimension;
	}
	
	public static function convertirCaracteres($_cadena)
	{
		return htmlspecialchars_decode(htmlspecialchars(html_entity_decode($_cadena)));
	}
	
	public static function tildesHtml($cadena) 
	{ 
		return str_replace(array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ"),
							array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;","&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),
							$cadena);     
	}
	
	public static function crearTitulo($_titulo)
    {       
		$_titulo = mb_strtolower ($_titulo, 'UTF-8');
		$find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
		$repl = array('a', 'e', 'i', 'o', 'u', 'n');
		$_titulo = str_replace ($find, $repl, $_titulo);
		$find = array(' ', '&', '\r\n', '\n', '+');
		$_titulo = str_replace ($find, '-', $_titulo);
		$find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
		$repl = array('', '-', '');
		$_titulo = preg_replace ($find, $repl, $_titulo);
		return $_titulo;
	
    }
	
	
	public static function crearUrl($_id, $_titulo)
	{
		$_id = (int) $_id;
		$a = array('Ã€', 'Ã�', 'Ã‚', 'Ãƒ', 'Ã„', 'Ã…', 'Ã†', 'Ã‡', 'Ãˆ', 'Ã‰', 'ÃŠ', 'Ã‹', 'ÃŒ', 'Ã�', 'ÃŽ', 'Ã�', 'Ã�', 'Ã‘', 'Ã’', 'Ã“', 'Ã”', 'Ã•', 'Ã–', 'Ã˜', 'Ã™', 'Ãš', 'Ã›', 'Ãœ', 'Ã�', 'ÃŸ', 'Ã ', 'Ã¡', 'Ã¢', 'Ã£', 'Ã¤', 'Ã¥', 'Ã¦', 'Ã§', 'Ã¨', 'Ã©', 'Ãª', 'Ã«', 'Ã¬', 'Ã­', 'Ã®', 'Ã¯', 'Ã±', 'Ã²', 'Ã³', 'Ã´', 'Ãµ', 'Ã¶', 'Ã¸', 'Ã¹', 'Ãº', 'Ã»', 'Ã¼', 'Ã½', 'Ã¿', 'Ä€', 'Ä�', 'Ä‚', 'Äƒ', 'Ä„', 'Ä…', 'Ä†', 'Ä‡', 'Äˆ', 'Ä‰', 'ÄŠ', 'Ä‹', 'ÄŒ', 'Ä�', 'ÄŽ', 'Ä�', 'Ä�', 'Ä‘', 'Ä’', 'Ä“', 'Ä”', 'Ä•', 'Ä–', 'Ä—', 'Ä˜', 'Ä™', 'Äš', 'Ä›', 'Äœ', 'Ä�', 'Äž', 'ÄŸ', 'Ä ', 'Ä¡', 'Ä¢', 'Ä£', 'Ä¤', 'Ä¥', 'Ä¦', 'Ä§', 'Ä¨', 'Ä©', 'Äª', 'Ä«', 'Ä¬', 'Ä­', 'Ä®', 'Ä¯', 'Ä°', 'Ä±', 'Ä²', 'Ä³', 'Ä´', 'Äµ', 'Ä¶', 'Ä·', 'Ä¹', 'Äº', 'Ä»', 'Ä¼', 'Ä½', 'Ä¾', 'Ä¿', 'Å€', 'Å�', 'Å‚', 'Åƒ', 'Å„', 'Å…', 'Å†', 'Å‡', 'Åˆ', 'Å‰', 'ÅŒ', 'Å�', 'ÅŽ', 'Å�', 'Å�', 'Å‘', 'Å’', 'Å“', 'Å”', 'Å•', 'Å–', 'Å—', 'Å˜', 'Å™', 'Åš', 'Å›', 'Åœ', 'Å�', 'Åž', 'ÅŸ', 'Å ', 'Å¡', 'Å¢', 'Å£', 'Å¤', 'Å¥', 'Å¦', 'Å§', 'Å¨', 'Å©', 'Åª', 'Å«', 'Å¬', 'Å­', 'Å®', 'Å¯', 'Å°', 'Å±', 'Å²', 'Å³', 'Å´', 'Åµ', 'Å¶', 'Å·', 'Å¸', 'Å¹', 'Åº', 'Å»', 'Å¼', 'Å½', 'Å¾', 'Å¿', 'Æ’', 'Æ ', 'Æ¡', 'Æ¯', 'Æ°', 'Ç�', 'ÇŽ', 'Ç�', 'Ç�', 'Ç‘', 'Ç’', 'Ç“', 'Ç”', 'Ç•', 'Ç–', 'Ç—', 'Ç˜', 'Ç™', 'Çš', 'Ç›', 'Çœ', 'Çº', 'Ç»', 'Ç¼', 'Ç½', 'Ç¾', 'Ç¿');
	   	$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
	   	$_titulo = str_replace($a, $b, $_titulo);
	   	$_titulo = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), $_titulo));
	   	return $_id . '/' . $_titulo . ".html";
	}
	public static function crearUrlDos($_id, $_titulo, $_lang)
	{
		$_id = (int) $_id;
		$find = array('á', 'é', 'í', 'ó', 'ú', 'ñ','aacute','eacute','iacute','oacute','uacute','ntilde');
		$repl = array('a', 'e', 'i', 'o', 'u', 'n','a', 'e', 'i', 'o', 'u', 'n');
		$_titulo = str_replace ($find, $repl, $_titulo);
		/*$a = array('Ã€', 'Ã�', 'Ã‚', 'Ãƒ', 'Ã„', 'Ã…', 'Ã†', 'Ã‡', 'Ãˆ', 'Ã‰', 'ÃŠ', 'Ã‹', 'ÃŒ', 'Ã�', 'ÃŽ', 'Ã�', 'Ã�', 'Ã‘', 'Ã’', 'Ã“', 'Ã”', 'Ã•', 'Ã–', 'Ã˜', 'Ã™', 'Ãš', 'Ã›', 'Ãœ', 'Ã�', 'ÃŸ', 'Ã ', 'Ã¡', 'Ã¢', 'Ã£', 'Ã¤', 'Ã¥', 'Ã¦', 'Ã§', 'Ã¨', 'Ã©', 'Ãª', 'Ã«', 'Ã¬', 'Ã­', 'Ã®', 'Ã¯', 'Ã±', 'Ã²', 'Ã³', 'Ã´', 'Ãµ', 'Ã¶', 'Ã¸', 'Ã¹', 'Ãº', 'Ã»', 'Ã¼', 'Ã½', 'Ã¿', 'Ä€', 'Ä�', 'Ä‚', 'Äƒ', 'Ä„', 'Ä…', 'Ä†', 'Ä‡', 'Äˆ', 'Ä‰', 'ÄŠ', 'Ä‹', 'ÄŒ', 'Ä�', 'ÄŽ', 'Ä�', 'Ä�', 'Ä‘', 'Ä’', 'Ä“', 'Ä”', 'Ä•', 'Ä–', 'Ä—', 'Ä˜', 'Ä™', 'Äš', 'Ä›', 'Äœ', 'Ä�', 'Äž', 'ÄŸ', 'Ä ', 'Ä¡', 'Ä¢', 'Ä£', 'Ä¤', 'Ä¥', 'Ä¦', 'Ä§', 'Ä¨', 'Ä©', 'Äª', 'Ä«', 'Ä¬', 'Ä­', 'Ä®', 'Ä¯', 'Ä°', 'Ä±', 'Ä²', 'Ä³', 'Ä´', 'Äµ', 'Ä¶', 'Ä·', 'Ä¹', 'Äº', 'Ä»', 'Ä¼', 'Ä½', 'Ä¾', 'Ä¿', 'Å€', 'Å�', 'Å‚', 'Åƒ', 'Å„', 'Å…', 'Å†', 'Å‡', 'Åˆ', 'Å‰', 'ÅŒ', 'Å�', 'ÅŽ', 'Å�', 'Å�', 'Å‘', 'Å’', 'Å“', 'Å”', 'Å•', 'Å–', 'Å—', 'Å˜', 'Å™', 'Åš', 'Å›', 'Åœ', 'Å�', 'Åž', 'ÅŸ', 'Å ', 'Å¡', 'Å¢', 'Å£', 'Å¤', 'Å¥', 'Å¦', 'Å§', 'Å¨', 'Å©', 'Åª', 'Å«', 'Å¬', 'Å­', 'Å®', 'Å¯', 'Å°', 'Å±', 'Å²', 'Å³', 'Å´', 'Åµ', 'Å¶', 'Å·', 'Å¸', 'Å¹', 'Åº', 'Å»', 'Å¼', 'Å½', 'Å¾', 'Å¿', 'Æ’', 'Æ ', 'Æ¡', 'Æ¯', 'Æ°', 'Ç�', 'ÇŽ', 'Ç�', 'Ç�', 'Ç‘', 'Ç’', 'Ç“', 'Ç”', 'Ç•', 'Ç–', 'Ç—', 'Ç˜', 'Ç™', 'Çš', 'Ç›', 'Çœ', 'Çº', 'Ç»', 'Ç¼', 'Ç½', 'Ç¾', 'Ç¿');
	   	$b = array('A', 'A', 'A', 'A', 'A', 'A', 'AE', 'C', 'E', 'E', 'E', 'E', 'I', 'I', 'I', 'I', 'D', 'N', 'O', 'O', 'O', 'O', 'O', 'O', 'U', 'U', 'U', 'U', 'Y', 's', 'a', 'a', 'a', 'a', 'a', 'a', 'ae', 'c', 'e', 'e', 'e', 'e', 'i', 'i', 'i', 'i', 'n', 'o', 'o', 'o', 'o', 'o', 'o', 'u', 'u', 'u', 'u', 'y', 'y', 'A', 'a', 'A', 'a', 'A', 'a', 'C', 'c', 'C', 'c', 'C', 'c', 'C', 'c', 'D', 'd', 'D', 'd', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'E', 'e', 'G', 'g', 'G', 'g', 'G', 'g', 'G', 'g', 'H', 'h', 'H', 'h', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'I', 'i', 'IJ', 'ij', 'J', 'j', 'K', 'k', 'L', 'l', 'L', 'l', 'L', 'l', 'L', 'l', 'l', 'l', 'N', 'n', 'N', 'n', 'N', 'n', 'n', 'O', 'o', 'O', 'o', 'O', 'o', 'OE', 'oe', 'R', 'r', 'R', 'r', 'R', 'r', 'S', 's', 'S', 's', 'S', 's', 'S', 's', 'T', 't', 'T', 't', 'T', 't', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'W', 'w', 'Y', 'y', 'Y', 'Z', 'z', 'Z', 'z', 'Z', 'z', 's', 'f', 'O', 'o', 'U', 'u', 'A', 'a', 'I', 'i', 'O', 'o', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'U', 'u', 'A', 'a', 'AE', 'ae', 'O', 'o');
	   	$_titulo = str_replace($a, $b, $_titulo);*/
	   	$_titulo = strtolower(preg_replace(array('/[^a-zA-Z0-9 -]/', '/[ -]+/', '/^-|-$/'), array('', '-', ''), $_titulo));
	   	return $_id . '/' . $_titulo . '/' . $_lang;
	}
	
	public static function cortarTexto($texto,$tamano,$colilla="...")
	{
		//strip_tags($texto);
		$texto=substr($texto, 0,$tamano);
		$index=strrpos($texto, " ");
		$texto=substr($texto, 0,$index); $texto.=$colilla;
		return $texto;
	} 
	
	public static function limitarTexto($string, $length = 50, $ellipsis = "...")
	{
		$words = explode(' ', $string);
		if (count($words) > $length){
			return implode(' ', array_slice($words, 0, $length)) ." ". $ellipsis;
		}else{
			return $string;
		}
	}
	
	public static function convertirString($_array)
	{
		return implode(",", $_array);
	}
	
	public static function convertirArray($_string)
	{
		return explode(",", $_string);
	}
	
	public static function ultimoOrdenImagen($_identificador)
	{
		//$orden = contenidos_sliders_elemento::find(array('select' => 'max(orden) as orden'));
		$orden = contenidos_imagene::find(array('conditions' => array('identificador = ?', $_identificador),'select' => 'max(orden) as orden'));
		//$orden = contenidos_sliders_elemento::find_by_sql('select max(orden) as orden from contenidos_sliders_elementos where identificador = '.$_identificador);
		return $orden;
	}
	
	public static function convertirMes($_mes,$_lang='1')
	{
		switch($_mes){
			case '01':
				$_mes = ($_lang==1) ? 'Enero' : 'January';
				break;
			case '02':
				$_mes = ($_lang==1) ? 'Febrero' : 'February';
				break;
			case '03':
				$_mes = ($_lang==1) ? 'Marzo' : 'March';
				break;
			case '04':
				$_mes = ($_lang==1) ? 'Abril' : 'April';
				break;
			case '05':
				$_mes = ($_lang==1) ? 'Mayo' : 'May';
				break;
			case '06':
				$_mes = ($_lang==1) ? 'Junio' : 'June';
				break;
			case '07':
				$_mes = ($_lang==1) ? 'Julio' : 'July';
				break;
			case '08':
				$_mes = ($_lang==1) ? 'Agosto' : 'August';
				break;
			case '09':
				$_mes = ($_lang==1) ? 'Septiembre' : 'September';
				break;
			case '10':
				$_mes = ($_lang==1) ? 'Octubre' : 'October';
				break;
			case '11':
				$_mes = ($_lang==1) ? 'Noviembre' : 'November';
				break;
			case '12':
				$_mes = ($_lang==1) ? 'Diciembre' : 'December';
				break;
			
		}
		return $_mes;
	}
	
}