<?php
use Nucleo\Pd\Pd;
use Nucleo\Registro\Registro;
use \Exception as EX;

class admin
{
	
	public function traerLanzamientos()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl ORDER BY cl.id DESC");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}

	public function traerLanzamiento($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl WHERE cl.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarLanzamiento($_id, $_ruta, $_seccion)
	{		
		
		$borrar = contenidos_lanzamiento::find($_id);	
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesAliadas($_img->path, $_ruta, $_seccion);
				$_img->delete();
			}		
			// $this->eliminarImagenesAliadas($dat_por_identificador, $_ruta, $_seccion);
			// $this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
		}
		
		$borrar->delete();
		return true;
		

		
	}


	public function traerTendencias()
	{
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct ORDER BY ct.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerTendencia($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct WHERE ct.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarTendencia($_id, $_ruta, $_ruta_archivos, $_seccion)
	{		
		
		$borrar = contenidos_tendencia::find($_id);	
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesAliadas($_img->path, $_ruta, $_seccion);
				$_img->delete();
			}		
			// $this->eliminarImagenesAliadas($dat_por_identificador, $_ruta, $_seccion);
			// $this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
		}

		$_archBorrar = contenidos_archivo::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($_archBorrar){
			foreach($_archBorrar as $_arch){
				$_archivo = $_ruta_archivos . $_arch->path;
				if(file_exists($_archivo)){
					@unlink($_archivo);
				}
				$_arch->delete();				
			}
		}

		/*$_vidBorrar = contenidos_archivo::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($_archBorrar){
			$_archivo = $_ruta_archivos . $_archBorrar->path;
			if(file_exists($_archivo)){
				@unlink($_archivo);
			}
			$_archBorrar->delete();
		}*/
		
		$borrar->delete();
		return true;		
	}


	public function borrarImgBloque($_id, $_ruta, $_seccion)
	{		
		
		$imgBorrar = contenidos_imagene::find($_id);	
		if($imgBorrar){
			$this->eliminarImagenesAliadas($imgBorrar->path, $_ruta, $_seccion);				
		}		
		
		$imgBorrar->delete();
		return true;		
	}


	public function traerClubaliadas()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_clubaliadas` as cc ORDER BY cc.id DESC");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}

	public function traerClubaliada($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_clubaliadas` as cc WHERE cc.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarClubaliadas($_id, $_ruta, $_seccion)
	{		
		
		$borrar = contenidos_clubaliada::find($_id);	
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				$this->eliminarImagenesAliadas($_img->path, $_ruta, $_seccion);
				// $dat_por_identificador[] = $_img->id;
				$_img->delete();
			}		
			
			// $this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
		}
		
		$borrar->delete();
		return true;
				
	}

	public function traerCapacitacionDestacada()
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc WHERE cc.destacado = 'si' ORDER BY cc.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerCapacitaciones()
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc WHERE cc.destacado = 'no' ORDER BY cc.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerCapacitacion($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_capacitaciones` as cc WHERE cc.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarCapacitacion($_id, $_ruta, $_ruta_archivos, $_seccion)
	{		
		
		$borrar = contenidos_capacitacione::find($_id);	
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesAliadas($_img->path, $_ruta, $_seccion);
				$_img->delete();
			}		
			// $this->eliminarImagenesAliadas($dat_por_identificador, $_ruta, $_seccion);
			// $this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
		}

		$_archBorrar = contenidos_archivo::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($_archBorrar){
			foreach($_archBorrar as $_arch){
				$_archivo = $_ruta_archivos . $_arch->path;
				if(file_exists($_archivo)){
					@unlink($_archivo);
				}
				$_arch->delete();				
			}
		}

		/*$_vidBorrar = contenidos_archivo::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($_archBorrar){
			$_archivo = $_ruta_archivos . $_archBorrar->path;
			if(file_exists($_archivo)){
				@unlink($_archivo);
			}
			$_archBorrar->delete();
		}*/
		
		$borrar->delete();
		
		return true;		
	}

	
	public function traerTutoriales()
	{
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct ORDER BY ct.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerTutorial($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct WHERE ct.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarTutorial($_id, $_ruta, $_seccion)
	{		
		
		$borrar = contenidos_tutoriale::find($_id);	
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesAliadas($_img->path, $_ruta, $_seccion);
				$_img->delete();
			}		
			// $this->eliminarImagenesAliadas($dat_por_identificador, $_ruta, $_seccion);
			// $this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
		}

		/*$_vidBorrar = contenidos_archivo::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($_archBorrar){
			$_archivo = $_ruta_archivos . $_archBorrar->path;
			if(file_exists($_archivo)){
				@unlink($_archivo);
			}
			$_archBorrar->delete();
		}*/
		
		$borrar->delete();
		
		return true;		
	}

	public static function traerCategoriaPorNombre($_nombre)
	{
		return  contenidos_categoria::find(array('conditions' => array('nombre = ?', $_nombre)));
	}

	public static function traerCategoriasStatic()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_categorias` as cc");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}

	public function traerCategorias()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_categorias` as cc");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}

	public function traerTags()
	{
		$_lanz = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tags` as ct");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}


	public static function traerCategoria($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_categorias` as cc WHERE cc.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerClientes()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_clientes` as cl");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}
	
	public  function traerCliente($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_clientes` as cl WHERE cl.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerBuscadorClientes($_valor)
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_clientes` as cl WHERE cl.numero_cliente = '".$_valor."' OR (cl.razon_social LIKE '%".$_valor."%') ORDER BY cl.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public static function traerClientePorUsers($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_clientes` as cl WHERE cl.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerUsersExport()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cu.*,cl.numero_cliente,cl.razon_social FROM `contenidos_users` as cu
											INNER JOIN `contenidos_clientes` as cl
											ON cu.id_cliente = cl.id ORDER BY cu.id DESC");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}

	public function traerUsers()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cu.* FROM `contenidos_users` as cu");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}
	
	public  function traerUser($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cu.* FROM `contenidos_users` as cu WHERE cu.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerBuscadorUsers($_valor)
	{
		//return contenidos_user::find_by_sql('SELECT * FROM contenidos_users WHERE numero_cliente = "'.$_valor.'" OR (razon_social LIKE "%'.$_valor.'%") ORDER BY id DESC');
		$result = Pd::instancia()->prepare("SELECT cu.* FROM `contenidos_users` as cu WHERE cu.nombre LIKE '%".$_valor."%' ORDER BY cu.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}
	
	public function traerBuscadorCapacitacion($_valor)
	{
		//return contenidos_user::find_by_sql('SELECT * FROM contenidos_users WHERE numero_cliente = "'.$_valor.'" OR (razon_social LIKE "%'.$_valor.'%") ORDER BY id DESC');
		$result = Pd::instancia()->prepare("SELECT cp.* FROM `contenidos_capacitaciones` as cp WHERE cp.titulo LIKE '%".$_valor."%' ORDER BY cp.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function traerBuscadorTendencias($_valor)
	{
		//return contenidos_user::find_by_sql('SELECT * FROM contenidos_users WHERE numero_cliente = "'.$_valor.'" OR (razon_social LIKE "%'.$_valor.'%") ORDER BY id DESC');
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tendencias` as ct WHERE ct.titulo LIKE '%".$_valor."%' ORDER BY ct.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}
	
	public function traerBuscadorLanzamientos($_valor)
	{
		//return contenidos_user::find_by_sql('SELECT * FROM contenidos_users WHERE numero_cliente = "'.$_valor.'" OR (razon_social LIKE "%'.$_valor.'%") ORDER BY id DESC');
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_lanzamientos` as cl WHERE cl.titulo LIKE '%".$_valor."%' ORDER BY cl.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}
	public function traerBuscadorLinearios($_valor)
	{
		//return contenidos_user::find_by_sql('SELECT * FROM contenidos_users WHERE numero_cliente = "'.$_valor.'" OR (razon_social LIKE "%'.$_valor.'%") ORDER BY id DESC');
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl WHERE cl.titulo LIKE '%".$_valor."%' ORDER BY cl.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function traerBuscadorEventos($_valor)
	{
		//return contenidos_user::find_by_sql('SELECT * FROM contenidos_users WHERE numero_cliente = "'.$_valor.'" OR (razon_social LIKE "%'.$_valor.'%") ORDER BY id DESC');
		$result = Pd::instancia()->prepare("SELECT ce.* FROM `contenidos_eventos` as ce WHERE ce.titulo LIKE '%".$_valor."%' ORDER BY ce.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function traerBuscadorClubalidas($_valor)
	{
		//return contenidos_user::find_by_sql('SELECT * FROM contenidos_users WHERE numero_cliente = "'.$_valor.'" OR (razon_social LIKE "%'.$_valor.'%") ORDER BY id DESC');
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_clubaliadas` as cc WHERE cc.titulo LIKE '%".$_valor."%' ORDER BY cc.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function traerBuscadorTutoriales($_valor)
	{
		//return contenidos_user::find_by_sql('SELECT * FROM contenidos_users WHERE numero_cliente = "'.$_valor.'" OR (razon_social LIKE "%'.$_valor.'%") ORDER BY id DESC');
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_tutoriales` as ct WHERE ct.titulo LIKE '%".$_valor."%' ORDER BY ct.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function traerBanners()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners` as cb ORDER BY cb.id DESC");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}

	public function traerBannersDos()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners_dos` as cb ORDER BY cb.id DESC");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}
	
	public  function traerBanner($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners` as cb WHERE cb.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public  function traerBannerDos($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners_dos` as cb WHERE cb.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarBanner($_id, $_ruta, $_seccion)
	{		
		
		$borrar = contenidos_banner::find($_id);	
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesAliadas($_img->path, $_ruta, $_seccion);
				$_img->delete();
			}		
			// $this->eliminarImagenesAliadas($dat_por_identificador, $_ruta, $_seccion);
			// $this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
		}
		
		$borrar->delete();		
		return true;		
	}

	public function borrarBannerDos($_id, $_ruta, $_seccion)
	{		
		
		$borrar = contenidos_banners_do::find($_id);	
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesAliadas($_img->path, $_ruta, $_seccion);
				$_img->delete();
			}		
			// $this->eliminarImagenesAliadas($dat_por_identificador, $_ruta, $_seccion);
			// $this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
		}
		
		$borrar->delete();		
		return true;		
	}

	public function traerBuscadorBanner($_valor)
	{
		$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners` as cb WHERE cb.titulo LIKE '%".$_valor."%' ORDER BY cb.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function traerBuscadorBannerDos($_valor)
	{
		$result = Pd::instancia()->prepare("SELECT cb.* FROM `contenidos_banners_dos` as cb WHERE cb.titulo LIKE '%".$_valor."%' ORDER BY cb.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function traerSecciones()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cbs.* FROM `contenidos_banners_secciones` as cbs");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}

	public function traerPosicionesTodas()
	{
		$_lanz = Pd::instancia()->prepare("SELECT cbp.* FROM `contenidos_banners_posiciones` as cbp");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}

	/*public function traerPosiciones($_id)
	{
		$_lanz = Pd::instancia()->prepare("SELECT cbp.posicion FROM `contenidos_banners_posiciones` as cbp WHERE cbp.ids_seccion LIKE '%".$_id."%'");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}*/

	
	public function traerPosiciones($_ids)
	{
		$_query = "SELECT cbp.* FROM `contenidos_banners_posiciones` as cbp WHERE ";

		foreach ($_ids as $val) {
			if ($val === end($_ids)) {
		        $_query .= "cbp.ids_seccion LIKE '%".$val."%'";
		    }else{
		    	$_query .= "cbp.ids_seccion LIKE '%".$val."%' AND ";
		    }
			
		}

		$result = Pd::instancia()->prepare($_query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerPosicionesDos($_ids)
	{
		$_query = "SELECT cbp.* FROM `contenidos_banners_posiciones_dos` as cbp WHERE ";

		foreach ($_ids as $val) {
			if ($val === end($_ids)) {
		        $_query .= "cbp.ids_seccion LIKE '%".$val."%'";
		    }else{
		    	$_query .= "cbp.ids_seccion LIKE '%".$val."%' AND ";
		    }
			
		}

		$result = Pd::instancia()->prepare($_query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	/*public function traerFormatos($_sec, $_pos)
	{
		$_lanz = Pd::instancia()->prepare("SELECT cbf.formato FROM `contenidos_banners_formatos` as cbf WHERE (cbf.ids_seccion LIKE '%".$_sec."%') AND (cbf.ids_posicion LIKE '%".$_pos."%')");
		$_lanz->execute();
		$_lanz = $_lanz->fetchAll(PDO::FETCH_ASSOC);

		return ($_lanz) ? $_lanz : false;
	}*/

	public function traerFormatos($_sec, $_pos)
	{		

		$_query = "SELECT cbf.* FROM `contenidos_banners_formatos` as cbf WHERE ";

		foreach ($_sec as $val) {
			if($val === reset($_sec)) {
		        $_query .= "cbf.ids_seccion LIKE '%".$val."%'";
		    }/*else if($val === end($_sec)) {
		        $_query .= " OR cbf.ids_seccion LIKE '%".$val."%'";
		    }*/else{
		    	$_query .= " OR cbf.ids_seccion LIKE '%".$val."%'";
		    }			
		}

		
		foreach ($_pos as $val) {			
		    if ($val === reset($_pos)) {
		        $_query .= " AND cbf.ids_posicion LIKE '%".$val."%'";
		    }/*else if($val === end($_pos)){
				$_query .= " OR cbf.ids_posicion LIKE '%".$val."%'";
		    }*/else{
		    	$_query .= " OR cbf.ids_posicion LIKE '%".$val."%'";
		    }		    			
		}
		
		//SELECT * FROM `contenidos_banners_formatos` WHERE `ids_seccion` LIKE '%1%' OR `ids_seccion` LIKE '%3%' AND `ids_posicion` LIKE '%2%' OR `ids_posicion` LIKE '%1%' 

		$result = Pd::instancia()->prepare($_query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerFormatosDos($_sec, $_pos)
	{		

		$_query = "SELECT cbf.* FROM `contenidos_banners_formatos_dos` as cbf WHERE ";

		foreach ($_sec as $val) {
			if($val === reset($_sec)) {
		        $_query .= "cbf.ids_seccion LIKE '%".$val."%'";
		    }/*else if($val === end($_sec)) {
		        $_query .= " OR cbf.ids_seccion LIKE '%".$val."%'";
		    }*/else{
		    	$_query .= " OR cbf.ids_seccion LIKE '%".$val."%'";
		    }			
		}

		
		foreach ($_pos as $val) {			
		    if ($val === reset($_pos)) {
		        $_query .= " AND cbf.ids_posicion LIKE '%".$val."%'";
		    }/*else if($val === end($_pos)){
				$_query .= " OR cbf.ids_posicion LIKE '%".$val."%'";
		    }*/else{
		    	$_query .= " OR cbf.ids_posicion LIKE '%".$val."%'";
		    }		    			
		}
		
		//SELECT * FROM `contenidos_banners_formatos` WHERE `ids_seccion` LIKE '%1%' OR `ids_seccion` LIKE '%3%' AND `ids_posicion` LIKE '%2%' OR `ids_posicion` LIKE '%1%' 

		$result = Pd::instancia()->prepare($_query);
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public  function traerFormatoPorId($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cbf.* FROM `contenidos_banners_formatos` as cbf WHERE cbf.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public  function traerFormatoDosPorId($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cbf.* FROM `contenidos_banners_formatos_dos` as cbf WHERE cbf.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	public function eliminarImagenesAliadas($_path, $_ruta, $_seccion)
	{
			
		if(is_readable($_ruta . $_seccion . '/'.$_path)){
			@unlink($_ruta . $_seccion . '/'. $_path);
		}			
		if(is_readable($_ruta  . $_seccion . '/thumb/'. $_path)){
			@unlink($_ruta . $_seccion . '/thumb/'. $_path);
		}
		
	}


	public function traerComentarios()
	{
		$_res = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_comentarios` as cc ORDER BY cc.id ASC");
		$_res->execute();
		$_res = $_res->fetchAll(PDO::FETCH_ASSOC);

		return ($_res) ? $_res : false;
	}

	public static function traerComentario($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_comentarios` as cc WHERE cc.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarComentario($_id)
	{		
		
		$borrar = contenidos_comentario::find($_id);
		if($borrar->id_comentario_padre !=0){
			$borrar->delete();		
			return true;
		}else{
			$replies = contenidos_comentario::find('all',array('conditions' => array('id_comentario_padre = ?', $borrar->id)));
			if($replies){
				foreach($replies as $dat){
					$dat->delete();
				}		
			}

			$borrar->delete();		
			return true;
		}
		
	}



	public static function traerNota($_id, $_seccion)
	{
		$_tabla = 'contenidos_'.$_seccion;
		$result = Pd::instancia()->prepare("SELECT * FROM $_tabla  WHERE id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	public function traerBuscadorComentarios($_valor)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_comentarios` as cc WHERE cc.comentario LIKE '%".$_valor."%' OR 		cc.usuario LIKE '%".$_valor."%' OR cc.seccion LIKE '%".$_valor."%' ORDER BY cc.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function traerBuscadorNotas($_valor)
	{
		$result = Pd::instancia()->prepare("SELECT ct.id,ct.titulo FROM `contenidos_tendencias` as ct WHERE ct.titulo LIKE '%".$_valor."%'");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		$result2 = Pd::instancia()->prepare("SELECT cc.id,cc.titulo FROM `contenidos_capacitaciones` as cc WHERE cc.titulo LIKE '%".$_valor."%'");
		$result2->execute();
		$result2 = $result2->fetchAll(PDO::FETCH_ASSOC);

		$_res= array_merge($result, $result2);

		return ($_res) ? $_res : false;
		
	}

	public function traerBuscadorComentariosNotas($_valor)
	{
		$result = Pd::instancia()->prepare("SELECT cc.* FROM `contenidos_comentarios` as cc WHERE cc.id_nota = :id  ORDER BY cc.id DESC");
		$result->execute(array(":id" => $_valor));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}


	// Juegos


	public function traerChoices()
	{
		$_res = Pd::instancia()->prepare("SELECT cmc.* FROM `contenidos_multiple_choices` as cmc ORDER BY cmc.id DESC");
		$_res->execute();
		$_res = $_res->fetchAll(PDO::FETCH_ASSOC);

		return ($_res) ? $_res : false;
	}

	public function traerChoice($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cmc.* FROM `contenidos_multiple_choices` as cmc WHERE cmc.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerPorTablaJuego($_id, $_tabla)
	{
		$result = Pd::instancia()->prepare("SELECT * FROM `$_tabla` WHERE id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerTiposJuegos()
	{
		$result = Pd::instancia()->prepare("SELECT ctj.* FROM `contenidos_tipo_juegos` as ctj ORDER BY ctj.id ASC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerJuegosPorTipos($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ctj.* FROM `contenidos_juegos` as ctj WHERE ctj.id_tipo_juego = :id ORDER BY ctj.id ASC");
		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerTipoJuego($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ctj.* FROM `contenidos_tipo_juegos` as ctj WHERE ctj.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarChoice($_id)
	{		
		
		$borrar = contenidos_multiple_choice::find($_id);
		if($borrar){
			$borrar_juego = contenidos_juego::find(array('conditions' => array('id_juego = ?', $borrar->id)));
			if($borrar_juego){
				$borrar_juego->delete();	
			}

			$borrar->delete();		
			return true;
		}else{
			return false;
		}
		
	}


	public function traerUsersPorJuego($_id)
	{
		// $result = Pd::instancia()->prepare("SELECT cuj.* FROM `contenidos_users_juegos` as cuj WHERE cuj.id_juego = :id ORDER BY cuj.id DESC");
		$result = Pd::instancia()->prepare("SELECT cuj.*,cu.email FROM `contenidos_users_juegos` as cuj 
											INNER JOIN  contenidos_users as cu
											ON cuj.id_user = cu.id
											WHERE cuj.id_juego = :id 
											ORDER BY cuj.id DESC");	


		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	public function traerNotas()
	{
		$result = Pd::instancia()->prepare("(SELECT ccp.id,ccp.titulo,ccp.categorias,ccp.identificador,'capacitaciones' as tabla FROM `contenidos_capacitaciones` as ccp ORDER BY ccp.id DESC LIMIT 15)
			UNION ALL 
			(SELECT ct.id,ct.titulo,ct.categorias,ct.identificador,'tendencias' as tabla FROM `contenidos_tendencias` as ct ORDER BY ct.id DESC LIMIT 15)
			UNION ALL 
			(SELECT cl.id,cl.titulo,cl.categorias,cl.identificador,'linearios' as tabla FROM `contenidos_linearios` as cl ORDER BY cl.id DESC LIMIT 15)
			UNION ALL 
			(SELECT ctt.id,ctt.titulo,ctt.categorias,ctt.identificador,'tutoriales' as tabla FROM `contenidos_tutoriales` as ctt ORDER BY ctt.id DESC LIMIT 15)
			UNION ALL 
			(SELECT ce.id,ce.titulo,ce.categorias,ce.identificador,'eventos' as tabla FROM `contenidos_eventos` as ce ORDER BY ce.id DESC LIMIT 15)");


		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerNotasLanzamientos()
	{
		$result = Pd::instancia()->prepare("SELECT cl.id,cl.titulo,cl.categorias,cl.identificador,'lanzamientos' as tabla FROM `contenidos_lanzamientos` as cl ORDER BY cl.id DESC LIMIT 50");


		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function armarDestacadoLanzamientos($_data)
	{
		
		
		$result = Pd::instancia()->prepare("TRUNCATE TABLE `contenidos_destacados_lanzamientos`");
		$result->execute();
		foreach ($_data as $key => $val) {
			if($val!=''){
				$_arr = explode('_', $val);
				$_tabla = 'contenidos_'.$_arr[0];
				$_id_nota = $_arr[1];
				$_pos = $key;
				$_date = date('Y-m-d');
				
				$result = Pd::instancia()->prepare("INSERT INTO `contenidos_destacados_lanzamientos` (id_nota,tabla,posicion,fecha) VALUES  (:id_nota, :tabla, :posicion, :fecha)");
				$result->execute(array(":id_nota" => $_id_nota, ":tabla" => $_tabla, ":posicion" => $_pos, ":fecha" => $_date));
			}
			
		
		}
		
		return ($result) ? true : false;
	}




	public function traerDestacadosLanzamientos()
	{
		$result = Pd::instancia()->prepare("SELECT cdl.* FROM `contenidos_destacados_lanzamientos` as cdl ORDER BY cdl.posicion ASC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	public function armarDestacado($_data)
	{
		
		
		$result = Pd::instancia()->prepare("TRUNCATE TABLE `contenidos_destacados`");
		$result->execute();
		foreach ($_data as $key => $val) {
			if($val!=''){
				$_arr = explode('_', $val);
				$_tabla = 'contenidos_'.$_arr[0];
				$_id_nota = $_arr[1];
				$_pos = $key;
				$_date = date('Y-m-d');
				
				$result = Pd::instancia()->prepare("INSERT INTO `contenidos_destacados` (id_nota,tabla,posicion,fecha) VALUES  (:id_nota, :tabla, :posicion, :fecha)");
				$result->execute(array(":id_nota" => $_id_nota, ":tabla" => $_tabla, ":posicion" => $_pos, ":fecha" => $_date));
			}
			
		
		}
		
		return ($result) ? true : false;
	}




	public function traerDestacados()
	{
		$result = Pd::instancia()->prepare("SELECT cd.* FROM `contenidos_destacados` as cd ORDER BY cd.posicion ASC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerLinearios()
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl ORDER BY cl.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerLineario($_id)
	{
		$result = Pd::instancia()->prepare("SELECT cl.* FROM `contenidos_linearios` as cl WHERE cl.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarLineario($_id, $_ruta, $_ruta_archivos, $_seccion)
	{		
		
		$borrar = contenidos_lineario::find($_id);	
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesAliadas($_img->path, $_ruta, $_seccion);
				$_img->delete();
			}		
			// $this->eliminarImagenesAliadas($dat_por_identificador, $_ruta, $_seccion);
			// $this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
		}

		$_archBorrar = contenidos_archivo::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($_archBorrar){
			foreach($_archBorrar as $_arch){
				$_archivo = $_ruta_archivos . $_arch->path;
				if(file_exists($_archivo)){
					@unlink($_archivo);
				}
				$_arch->delete();				
			}
		}
		/*if($_archBorrar){
			$_archivo = $_ruta_archivos . $_archBorrar->path;
			if(file_exists($_archivo)){
				@unlink($_archivo);
			}
			$_archBorrar->delete();
		}*/
		
		$borrar->delete();
		return true;		
	}

	public function traerEventos()
	{
		$result = Pd::instancia()->prepare("SELECT ce.* FROM `contenidos_eventos` as ce ORDER BY ce.id DESC");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerEvento($_id)
	{
		$result = Pd::instancia()->prepare("SELECT ce.* FROM `contenidos_eventos` as ce WHERE ce.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function borrarEvento($_id, $_ruta, $_seccion)
	{		
		
		$borrar = contenidos_evento::find($_id);	
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesAliadas($_img->path, $_ruta, $_seccion);
				$_img->delete();
			}		
		}

		
		
		$borrar->delete();
		
		return true;		
	}


























	/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////



	public static function traerProvPDO()
	{
		$_prov = Pd::instancia()->prepare("SELECT cp.* FROM `contenidos_provincias` as cp");
		$_prov->execute();
		$_prov = $_prov->fetchAll(PDO::FETCH_ASSOC);

		return ($_prov) ? $_prov : false;
	}
	public function traerCatalogos()
	{
		return contenidos_catalogo::all();
	}

	public function traerCatalogo($_id)
	{
		return contenidos_catalogo::find($_id);
	}
	public static function traerCatalogoPorID($_id)
	{
		return contenidos_catalogo::find($_id);
	}
	
	
	public function cambiarEstadoCatalogo($_id, $_val)
	{
		$_estado = contenidos_catalogo::find(array('conditions' => array('id = ?', $_id)));
		$_estado->estado = $_val;
		$_estado->save();
	}
	
	public function borrarCatalogo($_id, $_ruta, $_seccion)
	{		
		$_asig =  contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_id.'",ids_catalogos)');	
		if($_asig){
			return false;
		}else{
			$borrar = contenidos_catalogo::find($_id);	
			$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
			if($imgBorrar){
				foreach($imgBorrar as $_img){
					//$dat_por_identificador[] = $idss->id;
					$_img->delete();
				}		
				//$this->eliminarImagenes($dat_por_identificador, $_ruta, $_seccion);
				$this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
			}
			
			$borrar->delete();
			return true;
		}

		
	}
	
	
	public function traerCatalogoBuscador($_valor, $_estado)
	{
		return contenidos_catalogo::find_by_sql('SELECT * FROM contenidos_catalogos WHERE estado = "'.$_estado.'" AND (titulo LIKE "%'.$_valor.'%") ORDER BY id DESC');
		
	}
	
	
	/*public function borrarDirectorio($carpeta)
    {
      foreach(glob($carpeta . "/*") as $archivos_carpeta){             
        if (is_dir($archivos_carpeta)){
          $this->borrarDirectorio($archivos_carpeta);
        } else {
        unlink($archivos_carpeta);
        }
      }
      rmdir($carpeta);
     }*/




	 
	 
	public function borrarDirectorio($dir) {
		if(!$dh = @opendir($dir)) return;
		while (false !== ($current = readdir($dh))) {
			if($current != '.' && $current != '..') {
				if (!@unlink($dir.'/'.$current)) {
					$this->borrarDirectorio($dir.'/'.$current);
				}
			}       
		}
		closedir($dh);
		@rmdir($dir);
	}
	
		
	public function traerTotems($_super=1)
	{
		return contenidos_totem::find('all',array('conditions' => array('id_supermercado = ?', $_super)));
	}
	
	public function traerTotem($_id)
	{
		return contenidos_totem::find($_id);
	}
	public static function traerTotemPorID($_id)
	{
		return contenidos_totem::find($_id);
	}

	public static function traerTotemPorIdPod($_id)
	{
		$_id = (int) $_id;
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_totems` as ct WHERE ct.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerTotemsPorID($_id)
	{
		//return contenidos_totem::find($_id);
		$_totems =  contenidos_totem::find_by_sql('SELECT * FROM contenidos_totems WHERE id IN ('.$_id.')');
		foreach ($_totems as $totem) {
			$_data[] =$totem->nombre;
		}

		return implode(',',$_data);	
	}
	
	
	public function borrarTotem($_id)
	{
		$_asig =  contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE id_totem = "'.$_id.'"');	
		if($_asig){
			return false;
		}else{
			$borrar = contenidos_totem::find($_id);	
			$borrar->delete();
			return true;
		}	
		
	}

	public function traerFiltroTotemsPod($_super, $_prov, $_tipo_local, $_excluidos)
	{
		$_super = (int) $_super;
		$_prov = array_map('intval', $_prov);
		$_tipo_local = array_map('intval', $_tipo_local);
		$_prov = implode(',', $_prov);		
		$_tipo_local = implode(',', $_tipo_local);
		$result = Pd::instancia()->prepare("SELECT ct.* FROM `contenidos_totems` as ct WHERE ct.id_supermercado = :super AND ct.id_provincia IN ({$_prov}) AND ct.id_tipo_local IN ({$_tipo_local}) AND ct.excluidos = :excluidos ORDER BY ct.nombre ASC");
		$result->execute(array(":super" => $_super, ":excluidos" => $_excluidos));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerFiltroTotems($_super, $_prov, $_tipo_local, $_excluidos)
	{
		$_prov = implode(',', $_prov);
		$_tipo_local = implode(',', $_tipo_local);
		return contenidos_totem::find_by_sql('SELECT * FROM contenidos_totems WHERE id_supermercado = '.$_super.' AND id_provincia IN ('.$_prov.') AND id_tipo_local IN ('.$_tipo_local.') AND excluidos = "'.$_excluidos.'" ORDER BY nombre ASC');
	}
		
	public function traerPromociones($_estado='alta')
	{
		return contenidos_promocione::find('all',array('conditions' => array('estado = ?', $_estado)));
	}
	
	public function traerPromocion($_id)
	{
		return contenidos_promocione::find($_id);
	}
	public static function traerPromocionPorID($_id)
	{
		return contenidos_promocione::find($_id);
	}
	
	public function cambiarEstadoPromociones($_id, $_val)
	{
		$_estado = contenidos_promocione::find(array('conditions' => array('id = ?', $_id)));
		$_estado->estado = $_val;
		$_estado->save();
	}
	
	public function borrarPromocion($_id, $_ruta, $_seccion)
	{		
		$_asig =  contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_id.'",ids_footers)');	
		if($_asig){
			return false;
		}else{
			$borrar = contenidos_promocione::find($_id);	
			$imgBorrar = contenidos_imagene::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
			if($imgBorrar){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesPromos($imgBorrar->path, $_ruta, $_seccion);
				$imgBorrar->delete();
			}
			
			$borrar->delete();
			return true;
		}

	}
	
	
	public function traerPromocionBuscador($_valor, $_estado)
	{
		return contenidos_promocione::find_by_sql('SELECT * FROM contenidos_promociones WHERE estado = "'.$_estado.'" AND (titulo LIKE "%'.$_valor.'%") ORDER BY id DESC');
		
	}
	
	
	
	
	public function traerSupermercados()
	{
		return contenidos_supermercado::all(array('order' => 'nombre asc'));
	}

	public static function traerSuperPorID($_id)
	{
		return contenidos_supermercado::find($_id);
	}
	
	public function traerProvincias()
	{
		return contenidos_provincia::all(array('order' => 'nombre asc'));
		//return contenidos_provincia::find_by_sql("SELECT id,nombre_completo FROM contenidos_candidatos WHERE FIND_IN_SET ('".$_trabajo."',ids_trabajos)");
	}
	public static function traerProvinciaPorIDPod($_id)
	{
		$_id = (int) $_id;
		$result = Pd::instancia()->prepare("SELECT cp.* FROM `contenidos_provincias` as cp WHERE cp.id = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetch(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}
	public static function traerProvinciaPorID($_id)
	{
		return contenidos_provincia::find($_id);
	}

	public static function traerProvPorRegion($_region)
	{
		$_data =  contenidos_provincia::find('all',array('conditions' => array('region = ?', $_region)));
		// $_data = contenidos_provincia::find_by_sql('SELECT id FROM contenidos_provincias WHERE region = "'.$_region.'"');
		if($_data){
			foreach ($_data as $key => $val) {
				$_arr[] = $val->id;
			}
		}

		return $_arr;
		
	}

	public function traerFiltroProv($_prov)
	{
		$_prov = implode(',', $_prov);
		return contenidos_provincia::find_by_sql('SELECT * FROM contenidos_provincias WHERE id IN ('.$_prov.') ORDER BY nombre ASC');
	}
	public function traerTipoLocales()
	{
		return contenidos_tipo_loca::all(array('order' => 'nombre asc'));
	}
	public function traerFondos($_estado='alta')
	{
		return contenidos_fondo::find('all',array('conditions' => array('estado = ?', $_estado)));
	}
	
	public function traerFondo($_id)
	{
		return contenidos_fondo::find($_id);
	}
	public static function traerFondoPorID($_id)
	{
		return contenidos_fondo::find($_id);
	}
	public function traerFondoBuscador($_valor, $_estado)
	{
		return contenidos_fondo::find_by_sql('SELECT * FROM contenidos_fondos WHERE estado = "'.$_estado.'" AND (titulo LIKE "%'.$_valor.'%") ORDER BY id DESC');
		
	}
	public function borrarFondo($_id, $_ruta, $_seccion)
	{		
		$_asig =  contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_id.'",ids_fondos)');	
		if($_asig){
			return false;
		}else{
			$borrar = contenidos_fondo::find($_id);	
			$imgBorrar = contenidos_imagene::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
			if($imgBorrar){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesPromos($imgBorrar->path, $_ruta, $_seccion);
				$imgBorrar->delete();
			}
			
			$borrar->delete();
			return true;
		}


	}
	public function traerVideos($_estado='alta')
	{
		return contenidos_heade::find('all',array('conditions' => array('estado = ?', $_estado)));
	}
	
	public function traerVideo($_id)
	{
		return contenidos_heade::find($_id);
	}
	public static function traerVideoPorID($_id)
	{
		return contenidos_heade::find($_id);
	}
	public function traerVideoBuscador($_valor, $_estado)
	{
		return contenidos_heade::find_by_sql('SELECT * FROM contenidos_header WHERE estado = "'.$_estado.'" AND (titulo LIKE "%'.$_valor.'%") ORDER BY id DESC');
		
	}
	public function borrarVideo($_id, $_ruta)
	{		
		$_asig =  contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_id.'",ids_videos)');	
		if($_asig){
			return false;
		}else{
			$borrar = contenidos_heade::find($_id);	
			$_vidBorrar = contenidos_video::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
			if($_vidBorrar){
				/*$_archivo = $_ruta . $_vidBorrar->path;
				if(file_exists($_archivo)){
					@unlink($_archivo);
				}*/
				$_vidBorrar->delete();
				$this->borrarDirectorio($_ruta.'/vid_'.$borrar->identificador);
			}
			
			$borrar->delete();
			return true;
		}


	}
	/*
	public function borrarVideo($_id, $_ruta)
	{		
		$borrar = contenidos_heade::find($_id);	
		$_vidBorrar = contenidos_video::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($_vidBorrar){			
			$_vidBorrar->delete();
		}
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar){
			foreach($imgBorrar as $_img){
				$_img->delete();
			}		
			$this->borrarDirectorio($_ruta.'/vid_'.$borrar->identificador);
		}
		
		$borrar->delete();
	}
	*/
	public function traerInactivas($_estado='alta')
	{
		return contenidos_pantalla_inactiv::find('all',array('conditions' => array('estado = ?', $_estado)));
	}
	
	public function traerInactiva($_id)
	{
		return contenidos_pantalla_inactiv::find($_id);
	}
	
	public static function traerInactivaPorID($_id)
	{
		return contenidos_pantalla_inactiv::find($_id);
	}
	
	public function traerInactivaBuscador($_valor, $_estado)
	{
		return contenidos_pantalla_inactiv::find_by_sql('SELECT * FROM contenidos_pantalla_inactiva WHERE estado = "'.$_estado.'" AND (titulo LIKE "%'.$_valor.'%") ORDER BY id DESC');
		
	}

	public function borrarInactiva($_id, $_ruta)
	{		
		$_asig =  contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_id.'",ids_inactivas)');	
		if($_asig){
			return false;
		}else{
			$borrar = contenidos_pantalla_inactiv::find($_id);	
			$_vidBorrar = contenidos_video::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
			if($_vidBorrar){
				/*$_archivo = $_ruta . $_vidBorrar->path;
				if(file_exists($_archivo)){
					@unlink($_archivo);
				}*/
				$_vidBorrar->delete();
				$this->borrarDirectorio($_ruta.'/inac_'.$borrar->identificador);
			}
			
			$borrar->delete();
			return true;
		}


	}
	
	
	public function subirVideo($_tipo, $_identificador, $_data, $_nombre, $_ruta)
	{
		//echo "<pre>";print_r($_FILES[$_data]);echo "</pre>";//exit;
		//echo $_FILES[$_data]['name'];exit;
		$_mode = 0777;
		$_nombre = htmlentities($_nombre, ENT_QUOTES);
		$_mime = $this->mimeContentType($_FILES[$_data]['name']);
		//$_nombreArchivo = $this->crearNombre($_nombre);
		$_nombreArchivo = self::cadenaAleatoriaSegura(15);
		if(!$_mime) throw new Exception("ERROR: Archivo Invalido");
		if($_mime[0] != $_tipo) throw new Exception("ERROR: Tipo de Archivo Incorrecto");
		
		$_temp  = $_FILES[$_data]['tmp_name'];
		if(!file_exists($_ruta)){
			mkdir($_ruta, 0777, true);
		}
		$_archivo = $_ruta . $_nombreArchivo . '.' . $_mime[0];
		/*echo "<pre>";print_r($_mime);echo "</pre>";//exit;
		echo $_archivo;*/
		$_vid_ant = self::traerVidId($_identificador);
		if($_vid_ant){
			if(file_exists($_ruta. $_vid_ant->path)){
				@unlink($_ruta. $_vid_ant->path);
			}
		}
		
		if(file_exists($_archivo)){
			@unlink($_archivo);
			@copy($_temp, $_archivo);
			@chmod($_archivo, $_mode);
			$_vid = $this->cargarDataVideo($_nombreArchivo.'.'.$_mime[0], $_nombre,$_identificador);
			return $_vid;
		}else{
			//@copy($_temp, $_archivo);
			move_uploaded_file($_temp,$_archivo);
			@chmod($_archivo, $_mode);
			$_vid = $this->cargarDataVideo($_nombreArchivo.'.'.$_mime[0], $_nombre,$_identificador);
			return $_vid;
		}
		throw new Exception("ERROR: al intentar subir el archivo");
	}
	
	public function editarVideo($_tipo, $_identificador, $_data, $_nombre, $_ruta)
	{
		$_mode = 0777;
		$_nombre = htmlentities($_nombre, ENT_QUOTES);
		$_mime = $this->mimeContentType($_FILES[$_data]['name']);
		//$_nombreArchivo = $this->crearNombre($_nombre);
		$_nombreArchivo = self::cadenaAleatoriaSegura(15);
		if(!$_mime) throw new Exception("ERROR: Archivo Invalido");
		if($_mime[0] != $_tipo) throw new Exception("ERROR: Tipo de Archivo Incorrecto");
		
		$_temp  = $_FILES[$_data]['tmp_name'];
		if(!file_exists($_ruta)){
			mkdir($_ruta, 0777, true);
		}
		$_archivo = $_ruta . $_nombreArchivo . '.' . $_mime[0];
		$_vid_ant = self::traerVidId($_identificador);
		if(file_exists($_ruta. $_vid_ant->path)){
			@unlink($_ruta. $_vid_ant->path);
		}
		if(file_exists($_archivo)){
			@unlink($_archivo);
			@copy($_temp, $_archivo);
			@chmod($_archivo, $_mode);
			$_vid = $this->cargarDataVideo($_nombreArchivo.'.'.$_mime[0], $_nombre,$_identificador);
			return $_vid;
		}else{
			@copy($_temp, $_archivo);
			@chmod($_archivo, $_mode);
			$_vid = $this->cargarDataVideo($_nombreArchivo.'.'.$_mime[0], $_nombre,$_identificador);
			return $_vid;
		}
		throw new Exception("ERROR: al intentar subir el archivo");
	}
	public function cargarDataVideo($_archivo, $_nombre, $_identificador)
	{
		$_nombre = htmlentities($_nombre, ENT_QUOTES);
		$_vid = self::traerVidId($_identificador);
		$_fechaBd = date('Y-m-d');
		if($_vid){
			//$video = new contenidos_video();
			$_vid->nombre = $_nombre;
			//$_vid->identificador = $_identificador;
			$_vid->path = $_archivo;
			//$_vid->orientacion = '';
			//$_vid->orden = 0;
			//$_vid->fecha_alt = date('Y-m-d');
			return ($_vid->save()) ? $_vid->id : false;
		}else{
			$video = new contenidos_video();
			$video->nombre = $_nombre;
			$video->identificador = $_identificador;
			$video->path = $_archivo;
			$video->orientacion = '';
			$video->orden = 0;
			$video->fecha_alt = "$_fechaBd";
			return ($video->save()) ? $video->id : false;
		}
		
		
	}
	public function traerDataVideo($_id)
	{
		return  contenidos_video::find(array('conditions' => array('id = ?', $_id)));
	}
	public static function traerVidId($_identificador)
	{
		return  contenidos_video::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	public static function traerArchivoId($_identificador)
	{
		return  contenidos_archivo::find(array('conditions' => array('identificador = ?', $_identificador)));
	}

	public static function traerArchivosIdentificador($_identificador)
	{
		return  contenidos_archivo::find('all',array('conditions' => array('identificador = ?', $_identificador)));
	}

	public function subirArchivo($_tipo, $_identificador, $_data, $_nombre, $_ruta)
	{
		//echo "<pre>";print_r($_FILES[$_data]);echo "</pre>";//exit;
		//echo $_FILES[$_data]['name'];exit;
		$_mode = 0777;
		$_nombre = htmlentities($_nombre, ENT_QUOTES);
		$_mime = $this->mimeContentType($_FILES[$_data]['name']);
		//$_nombreArchivo = $this->crearNombre($_nombre);
		$_nombreArchivo = self::cadenaAleatoriaSegura(15);
		if(!$_mime) throw new Exception("ERROR: Archivo Invalido");
		if($_mime[0] != $_tipo) throw new Exception("ERROR: Tipo de Archivo Incorrecto");
		
		$_temp  = $_FILES[$_data]['tmp_name'];
		if(!file_exists($_ruta)){
			mkdir($_ruta, 0777, true);
		}
		$_archivo = $_ruta . $_nombreArchivo . '.' . $_mime[0];
		/*echo "<pre>";print_r($_mime);echo "</pre>";//exit;
		echo $_archivo;*/
		/*$_vid_ant = self::traerArchivoId($_identificador);
		if($_vid_ant){
			if(file_exists($_ruta. $_vid_ant->path)){
				@unlink($_ruta. $_vid_ant->path);
			}
		}*/
		
		if(file_exists($_archivo)){
			@unlink($_archivo);
			@copy($_temp, $_archivo);
			@chmod($_archivo, $_mode);
			$_vid = $this->cargarDataArchivo($_nombreArchivo.'.'.$_mime[0], $_tipo, $_nombre,$_identificador);
			return $_vid;
		}else{
			//@copy($_temp, $_archivo);
			move_uploaded_file($_temp,$_archivo);
			@chmod($_archivo, $_mode);
			$_vid = $this->cargarDataArchivo($_nombreArchivo.'.'.$_mime[0], $_tipo, $_nombre,$_identificador);
			return $_vid;
		}
		throw new Exception("ERROR: al intentar subir el archivo");
	}
	

	public function cargarDataArchivo($_archivo, $_formato, $_nombre, $_identificador)
	{
		$_nombre = htmlentities($_nombre, ENT_QUOTES);
		// $_vid = self::traerArchivoId($_identificador);
		// $_fechaBd = date('Y-m-d');
		/*if($_vid){
			//$video = new contenidos_video();
			$_vid->nombre = $_nombre;
			//$_vid->identificador = $_identificador;
			$_vid->path = $_archivo;
			$_vid->formato = $_formato;
			//$_vid->orientacion = '';
			//$_vid->orden = 0;
			//$_vid->fecha_alt = date('Y-m-d');
			return ($_vid->save()) ? $_vid->id : false;
		}else{
			$video = new contenidos_archivo();
			$video->nombre = $_nombre;
			$video->identificador = $_identificador;
			$video->path = $_archivo;
			$video->formato = $_formato;
			$video->orientacion = '';
			$video->orden = 0;
			$video->fecha_alt = "$_fechaBd";
			return ($video->save()) ? $video->id : false;
		}*/
		$video = new contenidos_archivo();
		$video->nombre = $_nombre;
		$video->identificador = $_identificador;
		$video->path = $_archivo;
		$video->formato = $_formato;
		$video->orientacion = '';
		$video->orden = 0;
		$video->fecha_alt = date('Y-m-d');
		return ($video->save()) ? $video->id : false;
		
		
	}

	public function traerDataArchivo($_id)
	{
		return  contenidos_archivo::find(array('conditions' => array('id = ?', $_id)));
	}

	public function borrarArchivo($_id, $_ruta)
	{		
		
		$borrar = contenidos_archivo::find($_id);			
		if($borrar){
			$_archivo = $_ruta . $borrar->path;
			if(file_exists($_archivo)){
				@unlink($_archivo);
			}
			$borrar->delete();
		}
		
		
		return true;		
	}



	public static function traerExcelId($_identificador)
	{
		return  contenidos_archivos_exce::find(array('conditions' => array('identificador = ?', $_identificador)));
	}

	public function subirExcel($_tipo, $_identificador, $_data, $_nombre, $_ruta)
	{
		//echo "<pre>";print_r($_FILES[$_data]);echo "</pre>";//exit;
		//echo $_FILES[$_data]['name'];exit;
		$_mode = 0777;
		$_nombre = htmlentities($_nombre, ENT_QUOTES);
		$_mime = $this->mimeContentType($_FILES[$_data]['name']);
		//$_nombreArchivo = $this->crearNombre($_nombre);
		$_nombreArchivo = self::cadenaAleatoriaSegura(15);
		if(!$_mime) throw new Exception("ERROR: Archivo Invalido");
		if($_mime[0] != $_tipo) throw new Exception("ERROR: Tipo de Archivo Incorrecto");
		
		$_temp  = $_FILES[$_data]['tmp_name'];
		if(!file_exists($_ruta)){
			mkdir($_ruta, 0777, true);
		}
		$_archivo = $_ruta . $_nombreArchivo . '.' . $_mime[0];
		/*echo "<pre>";print_r($_mime);echo "</pre>";//exit;
		echo $_archivo;*/
		$_vid_ant = self::traerExcelId($_identificador);
		if($_vid_ant){
			if(file_exists($_ruta. $_vid_ant->path)){
				@unlink($_ruta. $_vid_ant->path);
			}
		}
		
		if(file_exists($_archivo)){
			@unlink($_archivo);
			@copy($_temp, $_archivo);
			@chmod($_archivo, $_mode);
			$_vid = $this->cargarDataExcel($_nombreArchivo.'.'.$_mime[0], $_nombre,$_identificador);
			return $_vid;
		}else{
			//@copy($_temp, $_archivo);
			move_uploaded_file($_temp,$_archivo);
			@chmod($_archivo, $_mode);
			$_vid = $this->cargarDataExcel($_nombreArchivo.'.'.$_mime[0], $_nombre,$_identificador);
			return $_vid;
		}
		throw new Exception("ERROR: al intentar subir el archivo");
	}
	

	public function cargarDataExcel($_archivo, $_nombre, $_identificador)
	{
		$_nombre = htmlentities($_nombre, ENT_QUOTES);
		// $_vid = self::traerExcelId($_identificador);
		$_vid = contenidos_archivos_exce::find(array('conditions' => array('id = ?', 1)));
		$_fechaBd = date('Y-m-d');
		if($_vid){
			//$video = new contenidos_video();
			$_vid->nombre = $_nombre;
			//$_vid->identificador = $_identificador;
			$_vid->path = $_archivo;
			//$_vid->orientacion = '';
			//$_vid->orden = 0;
			//$_vid->fecha_alt = date('Y-m-d');
			return ($_vid->save()) ? $_vid->id : false;
		}else{
			$video = new contenidos_archivos_exce();
			$video->nombre = $_nombre;
			$video->identificador = $_identificador;
			$video->path = $_archivo;
			$video->orientacion = '';
			$video->orden = 0;
			$video->fecha_alt = "$_fechaBd";
			return ($video->save()) ? $video->id : false;
		}
		
		
	}

	public function traerDataExcel($_id)
	{
		return  contenidos_archivos_exce::find(array('conditions' => array('id = ?', $_id)));
	}














	public function crearNombre($_nombre)
 	{
    	$_nombre = strtolower($_nombre);
    	return str_replace(' ', '_', $_nombre);
    }
    public function mimeContentType($filename) {
    	$_mime = array(
            'txt' 	=> 'text/plain',
            'htm' 	=> 'text/html',
            'html' 	=> 'text/html',
            'php' 	=> 'application/x-httpd-php',
            'css' 	=> 'text/css',
            'js' 	=> 'application/javascript',
            'json' 	=> 'application/json',
            'xml' 	=> 'application/xml',
            'swf' 	=> 'application/x-shockwave-flash',
            'flv' 	=> 'video/x-flv',
            'png' 	=> 'image/png',
            'jpe' 	=> 'image/jpeg',
            'jpeg' 	=> 'image/jpeg',
            'jpg' 	=> 'image/jpeg',
            'gif' 	=> 'image/gif',
            'bmp' 	=> 'image/bmp',
            'ico' 	=> 'image/vnd.microsoft.icon',
            'tiff' 	=> 'image/tiff',
            'tif' 	=> 'image/tiff',
            'svg' 	=> 'image/svg+xml',
            'svgz' 	=> 'image/svg+xml',
            'zip' 	=> 'application/zip',
            'rar' 	=> 'application/x-rar-compressed',
            'exe' 	=> 'application/x-msdownload',
            'msi' 	=> 'application/x-msdownload',
            'cab' 	=> 'application/vnd.ms-cab-compressed',
            'mp3' 	=> 'audio/mpeg', // audio/video
            'mp4' 	=> 'video/mp4',
            'qt' 	=> 'video/quicktime',
            'mov' 	=> 'video/quicktime', // <--
            'pdf' 	=> 'application/pdf',
            'psd' 	=> 'image/vnd.adobe.photoshop',
            'ai' 	=> 'application/postscript',
            'eps' 	=> 'application/postscript',
            'ps' 	=> 'application/postscript',
            'doc' 	=> 'application/msword',
            'rtf' 	=> 'application/rtf',
            'xls' 	=> 'application/vnd.ms-excel',
            'xlsx' 	=> 'application/vnd.ms-excel',
            'ppt' 	=> 'application/vnd.ms-powerpoint',
            'odt' 	=> 'application/vnd.oasis.opendocument.text',
            'ods' 	=> 'application/vnd.oasis.opendocument.spreadsheet',
        );
        $_name = explode('.', $filename);
        $ext = strtolower(array_pop($_name));
        return (array_key_exists($ext, $_mime)) ? array($ext, $_mime[$ext]) : false;
    }

    public function traerBitacoraPorTotem($_id)
    {
    	//return contenidos_bitacor::find('all',array('conditions' => array('id_totem = ?', $_id)));
    	return contenidos_bitacor::find_by_sql('SELECT * FROM contenidos_bitacora WHERE id_totem = '.$_id.' ORDER BY fecha DESC');
    }

 	public function cargarBitacora($_data, $_user, $_accion)
    {
    	$_totems = explode(',', $_data['_totems']);
    	$_fechaBd = date('Y-m-d H:i:s');
    	
    	for ($i=0; $i < count($_totems) ; $i++) {     		
    		$_asig = new contenidos_bitacor();
			$_asig->id_totem = $_totems[$i];			
			/*$_asig->ids_catalogos = $_data['_catalogos'];
			$_asig->ids_videos = $_data['_videos'];
			$_asig->ids_footers = $_data['_footer'];
			$_asig->ids_fondos = $_data['_fondos'];
			$_asig->ids_inactivas = $_data['_inactivas'];*/
			$_asig->ids_catalogos = (isset($_data['_catalogos'])) ? $_data['_catalogos'] : '';
			$_asig->ids_videos = (isset($_data['_videos'])) ? $_data['_videos'] : '';
			$_asig->ids_footers = (isset($_data['_footer'])) ? $_data['_footer'] : '';
			$_asig->ids_fondos = (isset($_data['_fondos'])) ? $_data['_fondos'] : '';
			$_asig->ids_inactivas = (isset($_data['_inactivas'])) ? $_data['_inactivas'] : '';
			$_asig->id_user = $_user;
			$_asig->accion = $_accion;
			$_asig->fecha = "$_fechaBd";
			$_asig->save();
			$_arrays[]= $_asig->id;
    	}
    	foreach (array_keys($_arrays) as $a) {
		    if (!is_int($a)) {
		        return false;
		    }
		}
		return true;
    }

	public function editarBitacora($_data, $_user, $_accion)
    { 
    	$_fechaBd = date('Y-m-d H:i:s');

		$_asig = new contenidos_bitacor();
		$_asig->id_totem = $_data['_id_totem'];		
		$_asig->ids_catalogos = $_data['_catalogos'];
		$_asig->ids_videos = $_data['_videos'];
		$_asig->ids_footers = $_data['_footer'];
		$_asig->ids_fondos = $_data['_fondos'];
		$_asig->ids_inactivas = $_data['_inactivas'];		
		$_asig->id_user = $_user;
		$_asig->accion = $_accion;
		$_asig->fecha = "$_fechaBd";
		return ($_asig->save()) ? true : false;
    }
    
    public function cargarAsignacion($_data, $_user)
    {
    	$_totems = explode(',', $_data['_totems']);
    	$_fechahoy = date('Y-m-d H:i:s');
    	if($_fechahoy >= $_data['_fecha_inicial'] && $_fechahoy <= $_data['_fecha_final']){
			$_estado = 'alta';
			
		}else{
			$_estado = 'baja';			
		}

		$_fechaBd = date('Y-m-d H:i:s');
		$_vigencia_desde = $_data['_fecha_inicial'];
		$_vigencia_hasta = $_data['_fecha_final'];

    	for ($i=0; $i < count($_totems) ; $i++) {     		
    		$_asig = new contenidos_asignacione();
			$_asig->id_totem = $_totems[$i];
			$_asig->ids_catalogos = (isset($_data['_catalogos'])) ? $_data['_catalogos'] : '';
			$_asig->ids_videos = (isset($_data['_videos'])) ? $_data['_videos'] : '';
			$_asig->ids_footers = (isset($_data['_footer'])) ? $_data['_footer'] : '';
			$_asig->ids_fondos = (isset($_data['_fondos'])) ? $_data['_fondos'] : '';
			$_asig->ids_inactivas = (isset($_data['_inactivas'])) ? $_data['_inactivas'] : '';
			$_asig->vigencia_desde = "$_vigencia_desde";
			$_asig->vigencia_hasta = "$_vigencia_hasta";
			$_asig->dias_semana = (isset($_data['_dias_semana'])) ? $_data['_dias_semana'] : '';
			$_asig->id_user_update = '';
			$_asig->id_user_carga = $_user;
			$_asig->update_contenido = '';
			$_asig->estado = $_estado;
			//$_asig->fecha_sincronizacion =  0;
			//$_asig->fecha_upade = 0;
			$_asig->fecha_carga = "$_fechaBd";
			$_asig->save();
			$_arrays[]= $_asig->id;
    	}
    	foreach (array_keys($_arrays) as $a) {
		    if (!is_int($a)) {
		        return false;
		    }
		}
		return true;
    }
    
	public function editarAsignacion($_data, $_user)
    {
    	$_old_asig = contenidos_asignacione::find($_data['_id'])->to_array();
    	$_old = $this->cargarAsignacionUpdate($_old_asig);
    	if($_old==true){   	
	    	//$_totems = explode(',', $_data['_totems']);
	    	$_fechahoy = date('Y-m-d H:i:s');
	    	if($_fechahoy >= $_data['_fecha_inicial'] && $_fechahoy <= $_data['_fecha_final']){
				$_estado = 'alta';
				
			}else{
				$_estado = 'baja';			
			}

			$_fechaBd = date('Y-m-d H:i:s');
			$_vigencia_desde = $_data['_fecha_inicial'];
			$_vigencia_hasta = $_data['_fecha_final'];

			$_asig = contenidos_asignacione::find($_data['_id']);
			//$_asig->id_totem = $_totems[$i];
			/*$_asig->ids_catalogos = $_data['_catalogos'];
			$_asig->ids_videos = $_data['_videos'];
			$_asig->ids_footers = $_data['_footer'];
			$_asig->ids_fondos = $_data['_fondos'];
			$_asig->ids_inactivas = $_data['_inactivas'];*/
			$_asig->ids_catalogos = (isset($_data['_catalogos'])) ? $_data['_catalogos'] : '';
			$_asig->ids_videos = (isset($_data['_videos'])) ? $_data['_videos'] : '';
			$_asig->ids_footers = (isset($_data['_footer'])) ? $_data['_footer'] : '';
			$_asig->ids_fondos = (isset($_data['_fondos'])) ? $_data['_fondos'] : '';
			$_asig->ids_inactivas = (isset($_data['_inactivas'])) ? $_data['_inactivas'] : '';			
			$_asig->vigencia_desde = "$_vigencia_desde";
			$_asig->vigencia_hasta = "$_vigencia_hasta";
			$_asig->dias_semana = (isset($_data['_dias_semana'])) ? $_data['_dias_semana'] : '';
			$_asig->id_user_update = $_user;
			//$_asig->id_user_carga = $_user;
			$_asig->estado = $_estado;
			//$_asig->fecha_sincronizacion =  0;
			$_asig->fecha_upade = "$_fechaBd";
			//$_asig->fecha_carga = date('Y-m-d H:i:s');
			//$_asig->save();
			//$_arrays[]= $_asig->id;
			return ($_asig->save()) ? true : false;
		}
    }
    public function cargarAsignacionUpdate($_data)
    {
    	/*$_totems = explode(',', $_data['_totems']);
    	$_fechahoy = date('Y-m-d H:i:s');
    	if($_fechahoy >= $_data['_fecha_inicial'] && $_fechahoy <= $_data['_fecha_final']){
			$_estado = 'alta';
			
		}else{
			$_estado = 'baja';			
		}*/
    	//for ($i=0; $i < count($_totems) ; $i++) {     
    		$_fechaBd = date('Y-m-d H:i:s');
			$_vigencia_desde = $_data['vigencia_desde'];
			$_vigencia_hasta = $_data['vigencia_hasta'];
			$_fecha_sincronizacion = $_data['fecha_sincronizacion'];
			$_fecha_upade = $_data['fecha_upade'];
			$_fecha_carga = $_data['fecha_carga'];

    		$_asig = new contenidos_asignaciones_updat();
			$_asig->id_asignacion = $_data['id'];
			$_asig->id_totem = $_data['id_totem'];
			$_asig->ids_catalogos = $_data['ids_catalogos'];
			$_asig->ids_videos = $_data['ids_videos'];
			$_asig->ids_footers = $_data['ids_footers'];
			$_asig->ids_fondos = $_data['ids_fondos'];
			$_asig->ids_inactivas = $_data['ids_inactivas'];
			/*$_asig->vigencia_desde = $_data['vigencia_desde'];
			$_asig->vigencia_hasta = $_data['vigencia_hasta'];*/
			$_asig->vigencia_desde = "$_vigencia_desde";
			$_asig->vigencia_hasta = "$_vigencia_hasta";
			$_asig->dias_semana = $_data['dias_semana'];
			$_asig->id_user_update = $_data['id_user_update'];
			$_asig->id_user_carga = $_data['id_user_carga'];
			$_asig->update_contenido = '';
			$_asig->estado = $_data['estado'];
			/*$_asig->fecha_sincronizacion = $_data['fecha_sincronizacion'];
			$_asig->fecha_upade =  $_data['fecha_upade'];
			$_asig->fecha_carga = $_data['fecha_carga'];*/
			$_asig->fecha_sincronizacion = "$_fecha_sincronizacion";
			$_asig->fecha_upade =  "$_fecha_upade";
			$_asig->fecha_carga = "$_fecha_carga";
			//$_asig->save();
			//$_arrays[]= $_asig->id;
    	//}
    	/*foreach (array_keys($_arrays) as $a) {
		    if (!is_int($a)) {
		        return false;
		    }
		}
		return true;*/
		return ($_asig->save()) ? true : false;
    }
    public function desHacerUltimoCambio($_id)
    {
    	$_data = contenidos_asignaciones_updat::find(array('conditions' => array('id_asignacion = ?', $_id),'order' => 'fecha_upade desc'))->to_array();
    	if($_data){

    		$_fechaBd = date('Y-m-d H:i:s');
			$_vigencia_desde = $_data['vigencia_desde'];
			$_vigencia_hasta = $_data['vigencia_hasta'];
			$_fecha_sincronizacion = $_data['fecha_sincronizacion'];
			$_fecha_upade = $_data['fecha_upade'];
			$_fecha_carga = $_data['fecha_carga'];

    		$_asig = contenidos_asignacione::find($_id);
			$_asig->id_totem = $_data['id_totem'];
			$_asig->ids_catalogos = $_data['ids_catalogos'];
			$_asig->ids_videos = $_data['ids_videos'];
			$_asig->ids_footers = $_data['ids_footers'];
			$_asig->ids_fondos = $_data['ids_fondos'];
			$_asig->ids_inactivas = $_data['ids_inactivas'];
			/*$_asig->vigencia_desde = $_data['vigencia_desde'];
			$_asig->vigencia_hasta = $_data['vigencia_hasta'];*/
			$_asig->vigencia_desde = "$_vigencia_desde";
			$_asig->vigencia_hasta = "$_vigencia_hasta";
			$_asig->dias_semana = $_data['dias_semana'];
			$_asig->id_user_update = $_data['id_user_update'];
			$_asig->id_user_carga = $_data['id_user_carga'];
			$_asig->update_contenido = '';
			$_asig->estado = $_data['estado'];
			/*$_asig->fecha_sincronizacion = $_data['fecha_sincronizacion'];
			$_asig->fecha_upade =  $_data['fecha_upade'];
			$_asig->fecha_carga = $_data['fecha_carga'];*/
			$_asig->fecha_sincronizacion = "$_fecha_sincronizacion";
			$_asig->fecha_upade =  "$_fecha_upade";
			$_asig->fecha_carga = "$_fecha_carga";
			if($_asig->save()){
				$_borrar = contenidos_asignaciones_updat::find($_data['id']);
				$_borrar->delete();
				return true;
			}else{
				return false;
			}
    	}else{
    		return false;
    	}
		
		
    }
	
	public function eliminarAsignacion($_id)
    {
    	$_data = contenidos_asignacione::find($_id)->to_array();
    	if($_data){

    		$_fechaBd = date('Y-m-d H:i:s');
			$_vigencia_desde = $_data['vigencia_desde'];
			$_vigencia_hasta = $_data['vigencia_hasta'];
			$_fecha_sincronizacion = $_data['fecha_sincronizacion'];
			$_fecha_upade = $_data['fecha_upade'];
			$_fecha_carga = $_data['fecha_carga'];

    		$_asig = new contenidos_asignaciones_of();
			$_asig->id_totem = $_data['id_totem'];
			$_asig->ids_catalogos = $_data['ids_catalogos'];
			$_asig->ids_videos = $_data['ids_videos'];
			$_asig->ids_footers = $_data['ids_footers'];
			$_asig->ids_fondos = $_data['ids_fondos'];
			$_asig->ids_inactivas = $_data['ids_inactivas'];
			/*$_asig->vigencia_desde = $_data['vigencia_desde'];
			$_asig->vigencia_hasta = $_data['vigencia_hasta'];*/
			$_asig->vigencia_desde = "$_vigencia_desde";
			$_asig->vigencia_hasta = "$_vigencia_hasta";
			$_asig->dias_semana = $_data['dias_semana'];
			$_asig->id_user_update = $_data['id_user_update'];
			$_asig->id_user_carga = $_data['id_user_carga'];
			$_asig->update_contenido = '';
			$_asig->estado = $_data['estado'];
			/*$_asig->fecha_sincronizacion = $_data['fecha_sincronizacion'];
			$_asig->fecha_upade =  $_data['fecha_upade'];
			$_asig->fecha_carga = $_data['fecha_carga'];*/
			$_asig->fecha_sincronizacion = "$_fecha_sincronizacion";
			$_asig->fecha_upade =  "$_fecha_upade";
			$_asig->fecha_carga = "$_fecha_carga";
			if($_asig->save()){
				$_borrar = contenidos_asignacione::find($_id);
				$_borrar->delete();
				return true;
			}else{
				return false;
			}
    	}else{
    		return false;
    	}
		
		
    }


    public function traerAsigModif($_id, $_campo)
	{
		return contenidos_asignacione::find_by_sql("SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ('".$_id."', $_campo)");
	}

	public function traerFiltroTotemsAsigPod($_super, $_prov, $_tipo_local, $_excluidos)
	{
		$_super = (int) $_super;
		$_prov = array_map('intval', $_prov);
		$_tipo_local = array_map('intval', $_tipo_local);
		$_prov = implode(',', $_prov);		
		$_tipo_local = implode(',', $_tipo_local);
		$result = Pd::instancia()->prepare("SELECT ct.id FROM `contenidos_totems` as ct WHERE ct.id_supermercado = :super AND ct.id_provincia IN ({$_prov}) AND ct.id_tipo_local IN ({$_tipo_local}) AND ct.excluidos = :excluidos ORDER BY ct.nombre ASC");
		$result->execute(array(":super" => $_super, ":excluidos" => $_excluidos));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

    public function traerFiltroTotemsAsig($_super, $_prov, $_tipo_local, $_excluidos)
	{
		$_prov = implode(',', $_prov);
		$_tipo_local = implode(',', $_tipo_local);
		return contenidos_totem::find_by_sql('SELECT id FROM contenidos_totems WHERE id_supermercado = '.$_super.' AND id_provincia IN ('.$_prov.') AND id_tipo_local IN ('.$_tipo_local.') AND excluidos = "'.$_excluidos.'" ORDER BY nombre ASC');
	}

	public function traerFiltroAsigPod($_array)
	{
		$_array = array_map('intval', $_array);
		$_ids = implode(',', $_array);	
		$result = Pd::instancia()->prepare("SELECT DISTINCT ca.* FROM `contenidos_asignaciones` as ca WHERE ca.id_totem IN ({$_ids}) GROUP BY ca.id_totem ORDER BY ca.vigencia_desde, ca.vigencia_hasta ASC");
		// $result->execute(array(":ids" => $_ids));
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}
	public function traerFiltroAsig($_array)
	{	
		$_ids = implode(',',$_array);	
		return contenidos_asignacione::find_by_sql('SELECT DISTINCT * FROM contenidos_asignaciones WHERE id_totem IN ('.$_ids.') GROUP BY id_totem ORDER BY vigencia_desde, vigencia_hasta ASC');
	}

	
	public function traerAsigAlta($_data)
	{	
		$_fechahoy = date('Y-m-d H:i:s');
		return contenidos_asignacione::find_by_sql('SELECT DISTINCT * FROM contenidos_asignaciones WHERE id_totem IN ('.$_data['_totems'].') AND (vigencia_desde <= "'.$_fechahoy.'" AND vigencia_hasta >= "'.$_fechahoy.'")');
	}
	public function traerAsigsPorTotem($_id)
	{	
		//$_ids = implode(',',$_array);	
		return contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE id_totem = '.$_id.' ORDER BY vigencia_desde, vigencia_hasta ASC');
	}
	public function traerAsignacion($_id)
	{	
		//$_ids = implode(',',$_array);	
		return contenidos_asignacione::find($_id);
	}
	public static function traerAsignacionUpdatePorIdAsig($_id)
	{	
		//$_ids = implode(',',$_array);	
		return contenidos_asignaciones_updat::find(array('conditions' => array('id_asignacion = ?', $_id)));
	}

	public function traerTotemsAsigPod($_mes)
	{
		 // $_mes = (int) $_mes;
		$result = Pd::instancia()->prepare('SELECT DISTINCT ca.id_totem FROM `contenidos_asignaciones` as ca WHERE :mes BETWEEN DATE_FORMAT(ca.vigencia_desde,"%Y-%m") AND DATE_FORMAT(ca.vigencia_hasta,"%Y-%m") ORDER BY ca.vigencia_desde, ca.vigencia_hasta ASC');
		$result->execute(array("mes" => $_mes));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerTotemsAsig($_mes)
	{	
		return contenidos_asignacione::find_by_sql('SELECT DISTINCT id_totem FROM contenidos_asignaciones WHERE "'.$_mes.'" BETWEEN DATE_FORMAT(vigencia_desde,"%Y-%m") AND DATE_FORMAT(vigencia_hasta,"%Y-%m") ORDER BY vigencia_desde, vigencia_hasta ASC');
		//return contenidos_asignacione::find_by_sql('SELECT DISTINCT id_totem FROM contenidos_asignaciones WHERE MONTH(vigencia_desde) <= '.$_mes.' AND MONTH(vigencia_hasta) >= '.$_mes.' ORDER BY vigencia_desde, vigencia_hasta ASC');
		//return contenidos_asignacione::find_by_sql('SELECT DISTINCT id_totem FROM contenidos_asignaciones WHERE '.$_mes.' BETWEEN EXTRACT(YEAR_MONTH FROM vigencia_desde) AND EXTRACT(YEAR_MONTH FROM vigencia_hasta) ORDER BY vigencia_desde, vigencia_hasta ASC');
		//return contenidos_asignacione::find('all',array('select' => 'DISTINCT id_totem', 'order' => 'vigencia_desde asc'));
	}

	public static function traerAsigsPorTotemStaticPod($_id,$_mes)
	{
		$_id = (int) $_id;
		$result = Pd::instancia()->prepare('SELECT DISTINCT ca.id,ids_catalogos,ids_videos,ids_footers,ids_fondos,ids_inactivas,vigencia_desde,vigencia_hasta,estado,ABS(DATEDIFF(ca.vigencia_desde,ca.vigencia_hasta)) as daydif FROM `contenidos_asignaciones` as ca WHERE ca.id_totem = :id AND (:mes BETWEEN DATE_FORMAT(ca.vigencia_desde,"%Y-%m") AND DATE_FORMAT(ca.vigencia_hasta,"%Y-%m")) ORDER BY ca.vigencia_desde, ca.vigencia_hasta ASC');
		$result->execute(array("id" => $_id, "mes" => $_mes));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public static function traerAsigsPorTotemStatic($_id,$_mes)
	{	
		//return contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE id_totem = '.$_id.' AND (MONTH(vigencia_desde) <= '.$_mes.' AND MONTH(vigencia_hasta) >= '.$_mes.') ORDER BY vigencia_desde, vigencia_hasta ASC');
		return contenidos_asignacione::find_by_sql('SELECT id,ids_catalogos,ids_videos,ids_footers,ids_fondos,ids_inactivas,vigencia_desde,vigencia_hasta,estado,ABS(DATEDIFF(vigencia_desde,vigencia_hasta)) as daydif FROM contenidos_asignaciones WHERE id_totem = '.$_id.' AND ("'.$_mes.'" BETWEEN DATE_FORMAT(vigencia_desde,"%Y-%m") AND DATE_FORMAT(vigencia_hasta,"%Y-%m")) ORDER BY vigencia_desde, vigencia_hasta ASC');
	}


	public function traerTodasAsignaciones()
	{			
		return contenidos_asignacione::all();			
	}

	public function updateEstadoAsig($_id, $_estado)
	{	
		$_asig = contenidos_asignacione::find($_id);		
		$_asig->estado = $_estado;
		
		return ($_asig->save()) ? true : false;
		/*$con = Conexion::con();		
		$result = $con->prepare("UPDATE $_tabla SET estado = :estado WHERE id = :id");
		$result->execute(array(":id" => $_id, ":estado" => $_estado));
		return ($result) ? true : false;*/
	}

	public function traerAsignacionesBaja()
	{	
		//$_ids = implode(',',$_array);	
		return contenidos_asignacione::find('all',array('conditions' => array('estado = ?', 'baja')));
	}

	

	////////////////////////// ver totem ///////////////////////////////////////

	public function traerAsigAltaVerTotem($_id, $_estado)
	{	
		$results = contenidos_asignacione::find('all',array('select' => 'id,id_totem,ids_catalogos,ids_videos,ids_footers,ids_fondos,ids_inactivas','conditions' => array('id_totem = ? AND estado = ?', $_id, 'alta')));
		return $this->armarArray($results);

		
	}

	public function traerAsigAltaVerTotemPdo($_id, $_estado)
	{
		$_id = (int) $_id;
		$result = Pd::instancia()->prepare("SELECT ca.id,ca.id_totem,ca.ids_catalogos,ca.ids_videos,ca.ids_footers,ca.ids_fondos,ca.ids_inactivas FROM `contenidos_asignaciones` as ca WHERE ca.id_totem = :id AND ca.estado = :estado");
		$result->execute(array(":id" => $_id, ":estado" => $_estado));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerAsignaciones($_id)
	{	
		if($_id!=false){
			return contenidos_asignacione::find('all',array('conditions' => array('id_totem = ?', $_id)));
		}else{
			return contenidos_asignacione::all();
		}
		
	}

	public function traerAsignacionesPdo($_id)
	{	
		/*if($_id!=false){
			return contenidos_asignacione::find('all',array('conditions' => array('id_totem = ?', $_id)));
		}else{
			return contenidos_asignacione::all();
		}*/

		$_id = (int) $_id;
		$result = Pd::instancia()->prepare("SELECT ca.* FROM `contenidos_asignaciones` as ca WHERE ca.id_totem = :id");
		$result->execute(array(":id" => $_id));
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
		
	}

	public function updateEstadoAsigPdo($_id, $_estado)
	{	
		/*$_asig = contenidos_asignacione::find($_id);		
		$_asig->estado = $_estado;
		
		return ($_asig->save()) ? true : false;*/
		
		/*$con = Conexion::con();		
		$result = $con->prepare("UPDATE `contenidos_asignaciones` as ca SET ca.estado = :estado WHERE ca.id = :id");
		$result->execute(array(":id" => $_id, ":estado" => $_estado));
		return ($result) ? true : false;*/

		$_id = (int) $_id;
		$result = Pd::instancia()->prepare("UPDATE `contenidos_asignaciones` as ca SET ca.estado = :estado WHERE ca.id = :id");
		$result->execute(array(":id" => $_id, ":estado" => $_estado));
		// $result = $result->fetchAll(PDO::FETCH_ASSOC);
		return ($result) ? true : false;
	}


	public function traerCatalogosVerTotem($_ids)
	{
		$results = contenidos_catalogo::find_by_sql('SELECT  cc.id,cc.titulo,cc.imagenes_orientacion,cc.identificador
														FROM contenidos_catalogos as cc WHERE cc.id IN ('.$_ids.')');
		return $this->armarArray($results);
	}

	public function traerVideosVerTotem($_ids)
	{
		$results = contenidos_video::find_by_sql('SELECT  ch.id,ch.identificador,cv.path 
													FROM contenidos_header as ch
													INNER JOIN  contenidos_videos as cv
													ON ch.identificador = cv.identificador
													WHERE ch.id IN ('.$_ids.')');
		return $this->armarArray($results);
	}
	public function traerFootersVerTotem($_ids)
	{
		$results = contenidos_promocione::find_by_sql('SELECT  cp.id,cp.identificador,ci.path 
													FROM contenidos_promociones as cp
													INNER JOIN  contenidos_imagenes as ci
													ON cp.identificador = ci.identificador
													WHERE cp.id IN ('.$_ids.')');
		return $this->armarArray($results);
	}
	public function traerFondosVerTotem($_ids)
	{
		$results = contenidos_fondo::find_by_sql('SELECT  cf.id,cf.identificador,ci.path 
													FROM contenidos_fondos as cf
													INNER JOIN  contenidos_imagenes as ci
													ON cf.identificador = ci.identificador
													WHERE cf.id IN ('.$_ids.')');
		return $this->armarArray($results);
	}
	
	public function traerInactivasVerTotem($_ids)
	{
		$results = contenidos_pantalla_inactiv::find_by_sql('SELECT  cpi.id,cpi.identificador,cv.path 
																FROM contenidos_pantalla_inactiva as cpi
																INNER JOIN  contenidos_videos as cv
																ON cpi.identificador = cv.identificador
																WHERE cpi.id IN ('.$_ids.')');
		return $this->armarArray($results);
	}



	public function traerCatalogosVerTotemPdo($_ids)
	{
			
		$result = Pd::instancia()->prepare("SELECT cc.id,cc.titulo,cc.imagenes_orientacion,cc.identificador
											 FROM `contenidos_catalogos` as cc 
											 WHERE cc.id IN ({$_ids})");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerVideosVerTotemPdo($_ids)
	{
		/*$results = contenidos_video::find_by_sql('SELECT  ch.id,ch.identificador,cv.path 
													FROM contenidos_header as ch
													INNER JOIN  contenidos_videos as cv
													ON ch.identificador = cv.identificador
													WHERE ch.id IN ('.$_ids.')');
		return $this->armarArray($results);*/

		$result = Pd::instancia()->prepare("SELECT ch.id,ch.identificador,cv.path 
											FROM contenidos_header as ch
											INNER JOIN  contenidos_videos as cv
											ON ch.identificador = cv.identificador
											WHERE ch.id IN ({$_ids})");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}

	public function traerFootersVerTotemPdo($_ids)
	{
		/*$results = contenidos_promocione::find_by_sql('SELECT  cp.id,cp.identificador,ci.path 
													FROM contenidos_promociones as cp
													INNER JOIN  contenidos_imagenes as ci
													ON cp.identificador = ci.identificador
													WHERE cp.id IN ('.$_ids.')');
		return $this->armarArray($results);*/

		$result = Pd::instancia()->prepare("SELECT cp.id,cp.identificador,ci.path 
											FROM contenidos_promociones as cp
											INNER JOIN  contenidos_imagenes as ci
											ON cp.identificador = ci.identificador
											WHERE cp.id IN ({$_ids})");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;


	}


	public function traerFondosVerTotemPdo($_ids)
	{
		/*$results = contenidos_fondo::find_by_sql('SELECT  cf.id,cf.identificador,ci.path 
													FROM contenidos_fondos as cf
													INNER JOIN  contenidos_imagenes as ci
													ON cf.identificador = ci.identificador
													WHERE cf.id IN ('.$_ids.')');
		return $this->armarArray($results);*/

		$result = Pd::instancia()->prepare("SELECT cf.id,cf.identificador,ci.path 
											FROM contenidos_fondos as cf
											INNER JOIN  contenidos_imagenes as ci
											ON cf.identificador = ci.identificador
											WHERE cf.id IN ({$_ids})");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;


	}
	
	public function traerInactivasVerTotemPdo($_ids)
	{
		/*$results = contenidos_pantalla_inactiv::find_by_sql('SELECT  cpi.id,cpi.identificador,cv.path 
																FROM contenidos_pantalla_inactiva as cpi
																INNER JOIN  contenidos_videos as cv
																ON cpi.identificador = cv.identificador
																WHERE cpi.id IN ('.$_ids.')');
		return $this->armarArray($results);*/

		$result = Pd::instancia()->prepare("SELECT cpi.id,cpi.identificador,cv.path 
											FROM contenidos_pantalla_inactiva as cpi
											INNER JOIN  contenidos_videos as cv
											ON cpi.identificador = cv.identificador
											WHERE cpi.id IN ({$_ids})");
		$result->execute();
		$result = $result->fetchAll(PDO::FETCH_ASSOC);

		return ($result) ? $result : false;
	}


	/////////////////////////////////////////////////////////////////////////////////


	public static function convertirMes($_mes,$_lang=1)
	{
		switch($_mes){
			case '1':
				$_mes = ($_lang==1) ? 'Enero' : 'January';
				break;
			case '2':
				$_mes = ($_lang==1) ? 'Febrero' : 'February';
				break;
			case '3':
				$_mes = ($_lang==1) ? 'Marzo' : 'March';
				break;
			case '4':
				$_mes = ($_lang==1) ? 'Abril' : 'April';
				break;
			case '5':
				$_mes = ($_lang==1) ? 'Mayo' : 'May';
				break;
			case '6':
				$_mes = ($_lang==1) ? 'Junio' : 'June';
				break;
			case '7':
				$_mes = ($_lang==1) ? 'Julio' : 'July';
				break;
			case '8':
				$_mes = ($_lang==1) ? 'Agosto' : 'August';
				break;
			case '9':
				$_mes = ($_lang==1) ? 'Septiembre' : 'September';
				break;
			case '10':
				$_mes = ($_lang==1) ? 'Octubre' : 'October';
				break;
			case '11':
				$_mes = ($_lang==1) ? 'Noviembre' : 'November';
				break;
			case '12':
				$_mes = ($_lang==1) ? 'Diciembre' : 'December';
				break;
			
		}
		return $_mes;
	}
	
	public static function convertirFecha($_fecha)
	{
		$fecha = explode('-', $_fecha); 
		$_mes = self::convertirMes($fecha[1]);
		return $fecha[0].' '.$_mes.' de '.$fecha[2];
	}
	
	
	
	
	
	public function armarArray($_val)
	{
		$arrayResult = array_map(function($res){
		  return $res->attributes();
		}, $_val);
		return $arrayResult;
	}


	
	public function traerAreas()
	{
		return contenidos_area::all(array('order' => 'nombre asc'));
	}
	
	public function traerArea($_id)
	{
		return contenidos_area::find($_id);
	}
	
	public function borrarArea($_id, $_ruta, $_seccion)
	{		
		$borrar = contenidos_area::find($_id);	
		//$this->ordenarEquipo($borrar->orden);				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}		
			$this->eliminarImagenes($dat_por_identificador, $_ruta, $_seccion);
		}
		$borrar->delete();
	}
	
	
	public function traerTrabajos($_estado)
	{
		return  contenidos_trabajo::find('all',array('conditions' => array('estado = ?', $_estado),'order' => 'id asc'));
		//return  contenidos_sectore::find('all',array('order' => 'id asc'));
			
	}
	
	public function traerSectores()
	{
		return  contenidos_sectore::find('all',array('order' => 'id asc'));
			
	}
	
	public function traerTrabajo($_id)
	{
		return contenidos_trabajo::find($_id);
	}
	
	public function borrarTrabajo($_id)
	{		
		$borrar = contenidos_trabajo::find($_id);		
		$borrar->delete();
	}
	
	
	
	public function traerLocalidades()
	{
		return contenidos_localidade::all(array('order' => 'nombre asc'));
	}
	
	public function traerLocalidadesPorId($_id)
	{
		return contenidos_localidade::find('all',array('conditions' => array('id_prov = ?', $_id),'select' => 'id_localidad, nombre','order' => 'nombre asc'));
		//Book::find('all', array('select' => 'id, title'));
			
	}
	
	public function traerEstados()
	{
		return contenidos_estado::all(array('order' => 'id asc'));
	}
	
	public function traerCandidatos($_trabajo)
	{
		//return contenidos_candidato::all(array('order' => 'id asc'));
		return contenidos_candidato::find_by_sql("SELECT id,nombre_completo,cv_leido FROM contenidos_candidatos WHERE FIND_IN_SET ('".$_trabajo."',ids_trabajos)");
	}
	
	public function traerCandidato($_id)
	{
		return contenidos_candidato::find($_id);
	}
	
	public function borrarCandidato($_id, $_ruta)
	{		
		$borrar = contenidos_candidato::find($_id);	
		
		if(file_exists($_ruta. $borrar->cv)){
			unlink($_ruta. $borrar->cv);
		}		
			
		$borrar->delete();
	}
	
	public function traerCarreras()
	{
		return contenidos_carrera::all(array('order' => 'nombre asc'));
	}
	
	public function traerNombres($_trabajo)
	{
		//return contenidos_candidato::all(array('select' => 'DISTINCT nombre_completo', 'order' => 'nombre asc'));
		return contenidos_candidato::find_by_sql("SELECT DISTINCT nombre_completo FROM contenidos_candidatos WHERE FIND_IN_SET ('".$_trabajo."',ids_trabajos) ORDER BY nombre ASC");
	}
	
	public function traerEdades($_trabajo)
	{
		//return contenidos_candidato::all(array('select' => 'DISTINCT edad', 'order' => 'edad asc'));
		return contenidos_candidato::find_by_sql("SELECT DISTINCT edad FROM contenidos_candidatos WHERE FIND_IN_SET ('".$_trabajo."',ids_trabajos) ORDER BY edad ASC");
	}
		
	/*
	public function traerFiltro($_nombres,$_provincias,$_localidades,$_carreras,$_edades,$_trabajo)
	{
		$arrayCont = array();
		$_html	='SELECT id,nombre_completo,cv_leido FROM contenidos_candidatos WHERE ';
		if(!empty($_nombres)){
			$a = count($_nombres);
			if($a<=1){
				$_nomb = "'" .$_nombres[0]. "'";
			}else{
				$_nomb ="'" . implode("','", $_nombres) . "'";
			}
			$arrayCont[] = 'nombre_completo IN ('.$_nomb.')';
		}else{
			$_nomb = '';
		}
		
		if(!empty($_provincias)){
			$_prov = implode(',',$_provincias);
			$arrayCont[] = 'provincia IN ('.$_prov.')';
		}else{
			$_prov = 0;
		}
		
		if(!empty($_localidades)){
			$_loc = implode(',',$_localidades);
			$arrayCont[] = 'localidad IN ('.$_loc.')';
		}else{
			$_loc = 0;
		}
		if(!empty($_carreras)){
			$_car = implode(',',$_carreras);
			$arrayCont[] = 'carrera IN ('.$_car.')';
		}else{
			$_car = 0;
		}
		
		if(!empty($_edades)){
			$_ed = implode(',',$_edades);
			$arrayCont[] = 'edad IN ('.$_ed.')';
		}else{
			$_ed = 0;
		}
		
		
		foreach ($arrayCont as $arr){
			$_html .= $arr. " AND ";																									
		}
		
		$_html .= "FIND_IN_SET ('".$_trabajo."',ids_trabajos)";
		
		$_data = contenidos_candidato::find_by_sql($_html);
													
													
		
		return $_data;
	}
	*/
	public static function traerTipoDocPorId($_id)
	{
		return contenidos_tipo_document::find($_id);
	}
	
	public static function traerSexoPorId($_id)
	{
		return contenidos_sex::find($_id);
	}
	
	public static function traerNacPorId($_id)
	{
		return contenidos_nacionalidade::find($_id);
	}
	
	public static function traerProvPorId($_id)
	{
		return contenidos_provincia::find($_id);
	}
	
	public static function traerLocPorId($_id)
	{
		return contenidos_localidade::find(array('conditions' => array('id_localidad = ?', $_id)));
	}
	
	public static function traerUniversidadPorId($_id)
	{
		return contenidos_universidade::find($_id);
	}
	
	public static function traerCarreraPorId($_id)
	{
		return contenidos_carrera::find($_id);
	}
	
	public static function traerCodRefPorId($_id)
	{
		return contenidos_trabajo::find($_id);
	}
	
	public static function traerAreaPorId($_id)
	{
		return contenidos_area::find($_id);
	}
	
	public static function traerSectoresPorId($_id)
	{
		return contenidos_sectore::find($_id);
	}
	
	
	public static function traerTrabajoPorId($_id)
	{
		return contenidos_trabajo::find($_id);
	}
	
	public static function traerDataPorId($_data, $_id)
	{
		if($_data == 'provincias'){
			$_valor = contenidos_provincia::find($_id)->nombre;
			return ucwords(strtolower($_valor));
		}else if($_data == 'localidades'){
			$_valor = contenidos_localidade::find(array('conditions' => array('id_localidad = ?', $_id)))->nombre;
			return ucwords(strtolower($_valor));
		}else if($_data == 'carreras'){
			$_valor = contenidos_carrera::find($_id)->nombre;
			return ucwords(strtolower($_valor));
		}else{
			return $_id;
		}
		
		//return contenidos_provincia::find($_id);
	}
	
	
	
	/*
	
	public function traerDatos($_val)
	{
		if($_val=='peluquerias'){
			return  contenidos_peluqueria::all();
		}else{
			return  contenidos_persona::all();
		}
		
			
	}
	
	public function traerDato($_val, $_id)
	{
		if($_val=='peluquerias'){
			return contenidos_peluqueria::find($_id);
		}else{
			return contenidos_persona::find($_id);
		}
		
	}
	
	
	public function traerPeluquerias()
	{
		return contenidos_peluqueria::all(array('order' => 'nombre asc'));
	}
	
	public static function traerPeluqueriaPorId($_id)
	{
		return contenidos_peluqueria::find($_id);
	}
	
	public function borrarPeluquerias($_id)
	{		
		$borrar = contenidos_peluqueria::find($_id);		
		$borrar->delete();
	}
	
	
	public function borrarPersonas($_id)
	{		
		$borrar = contenidos_persona::find($_id);		
		$borrar->delete();
	}
	*/
	
	/*private function armarArray($_val)
	{
		$arrayResult = array_map(function($res){
		  return $res->attributes();
		}, $_val);
		return $arrayResult;
	}*/

	public function eliminarDirTempImagenPrinc($_dir){

		if(file_exists($_dir)){
			$files = array_diff(scandir($_dir), array('.','..'));

			foreach ($files as $file){
				(is_dir("$_dir/$file")) ? self::eliminarDirTempImagenPrinc("$_dir/$file") : unlink("$_dir/$file"); 
			}
			return rmdir($_dir);
		} 
	}

	public function limpiarDirectorio($_dir, $is_subdir = false)
	{
		if(file_exists($_dir)){
			$files = array_diff(scandir($_dir), array('.','..'));

			foreach ($files as $file){
				(is_dir("$_dir/$file")) ? self::limpiarDirectorio("$_dir/$file", true) : unlink("$_dir/$file"); 
			}
			if($is_subdir != false) return rmdir($_dir);
		} 
	}
	
	public function crearDirTempImagenPrinc($_identificador, $_tipo)
	{
		// ruta del directorio temporal
		$_temp = 'public/img/subidas/temporales/';

		// raiz
		if(!file_exists($_temp . $_identificador)) mkdir($_temp . $_identificador, 0777);

		/*// img principal
		if(!file_exists($_temp . $_identificador . '/principal')) mkdir($_temp . $_identificador . '/principal', 0777);

		// img grande
		if(!file_exists($_temp . $_identificador . '/grande')) mkdir($_temp . $_identificador . '/grande', 0777);

		// img mediana
		if(!file_exists($_temp . $_identificador . '/mediana')) mkdir($_temp . $_identificador . '/mediana', 0777);

		// img chica
		if(!file_exists($_temp . $_identificador . '/chica')) mkdir($_temp . $_identificador . '/chica', 0777);*/

		// img thumb
		if($_tipo != 'lanzamientos' && $_tipo != 'banners' && $_tipo != 'clubalidas'){
			if(!file_exists($_temp . $_identificador . '/thumb')) mkdir($_temp . $_identificador . '/thumb', 0777);
		}
		

		return RAIZ . $_temp . $_identificador;
	}

	public function eliminarImagenOriginal($_ruta, $_path)
	{
		if(file_exists($_ruta . $_path)){
			@unlink($_ruta . $_path);
		}else{
			throw new Exception('ERROR: La ruta es incorrecta!');
		}
	}

	public static function porcentajeXX($_cantidad,$_porciento,$_decimales)
	{
		return number_format($_cantidad*$_porciento/100 ,$_decimales);
	}
	
		
	public function cargarImgDB($_identificador, $_path, $_nombre, $_posicion, $_ruta, $_tipo='')
	{
		//$_categoria_id = (int) $_categoria_id;
		$_nombre = strtolower(trim($_nombre));
		$_nombre = htmlentities($_nombre, ENT_QUOTES | ENT_IGNORE, "UTF-8");
		
		
		$_editar = contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ? AND tipo = ?', $_identificador, $_posicion, $_tipo)));
		
		

		$_fechaBd = date('Y-m-d');
		
		if($_editar){
			
			$_borrarImg = $this->eliminarImagenes($_editar->path, $_ruta, $_posicion);			
			if($_borrarImg==true){			
				$_editar->nombre = $_nombre;
				$_editar->identificador = $_identificador;
				$_editar->path = $_path;
				$_editar->posicion = $_posicion;
				$_editar->tipo = $_tipo;		
				$_editar->orden = 0;							
				$_editar->fecha_alt = "$_fechaBd";
				$_editar->save();
				return $_editar->id;
			}
		}else{
			$imagen = new contenidos_imagene();
			$imagen->nombre = $_nombre;
			$imagen->identificador = $_identificador;
			$imagen->path = $_path;	
			$imagen->posicion = $_posicion;	
			$imagen->tipo = $_tipo;		
			$imagen->orden = 0;		
			$imagen->fecha_alt = "$_fechaBd";
			$imagen->save();	
			return $imagen->id;
		}
	
				
	}

	public function cargarImgDBV2($_identificador, $_path, $_nombre, $_posicion, $_ruta, $_tipo='')
	{
		//$_categoria_id = (int) $_categoria_id;
		$_nombre = strtolower(trim($_nombre));
		$_nombre = htmlentities($_nombre, ENT_QUOTES | ENT_IGNORE, "UTF-8");
		
		/*if($_tipo!=''){
			$_editar = contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ? AND tipo = ?', $_identificador, $_posicion, $_tipo)));
		}else{
			$_editar = contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ?', $_identificador, $_posicion)));
		}
		*/

		$_fechaBd = date('Y-m-d');
		
		
		$imagen = new contenidos_imagene();
		$imagen->nombre = $_nombre;
		$imagen->identificador = $_identificador;
		$imagen->path = $_path;	
		$imagen->posicion = $_posicion;	
		$imagen->tipo = $_tipo;		
		$imagen->orden = 0;		
		$imagen->fecha_alt = "$_fechaBd";
		$imagen->save();	
		return $imagen->id;
	
	
				
	}

	public function cargarImgDBGal($_identificador, $_path, $_nombre, $_posicion, $_ruta, $_tipo='')
	{
		//$_categoria_id = (int) $_categoria_id;
		$_nombre = strtolower(trim($_nombre));
		$_nombre = htmlentities($_nombre, ENT_QUOTES | ENT_IGNORE, "UTF-8");		
		$_fechaBd = date('Y-m-d');		

		$imagen = new contenidos_imagene();
		$imagen->nombre = $_nombre;
		$imagen->identificador = $_identificador;
		$imagen->path = $_path;	
		$imagen->posicion = $_posicion;	
		$imagen->tipo = $_tipo;		
		$imagen->orden = self::ultimoOrdenImg($_identificador)->orden+1;		
		$imagen->fecha_alt = "$_fechaBd";
		$imagen->save();	
		return $imagen->id;
			
		
	}


	public function cargarImgDBGalEditar($_identificador, $_path, $_nombre, $_posicion, $_ruta, $_tipo='')
	{
		//$_categoria_id = (int) $_categoria_id;
		$_nombre = strtolower(trim($_nombre));
		$_nombre = htmlentities($_nombre, ENT_QUOTES | ENT_IGNORE, "UTF-8");		
		$_fechaBd = date('Y-m-d');

		$_editar = contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ? AND tipo = ?', $_identificador, $_posicion, $_tipo)));

		if($_editar){
			
			$_borrarImg = $this->eliminarImagenes($_editar->path, $_ruta, $_posicion);			
			if($_borrarImg==true){			
				$_editar->nombre = $_nombre;
				$_editar->identificador = $_identificador;
				$_editar->path = $_path;
				$_editar->posicion = $_posicion;
				$_editar->tipo = $_tipo;		
				$_editar->orden = self::ultimoOrdenImg($_identificador)->orden+1;								
				$_editar->fecha_alt = "$_fechaBd";
				$_editar->save();

				return $_editar->id;
			}
		}
		
		/*$imagen = new contenidos_imagene();
		//$imagen->categoria_id = $_categoria_id;
		$imagen->nombre = $_nombre;
		$imagen->identificador = $_identificador;
		$imagen->path = $_path;			
		$imagen->posicion = $_pos;
		$imagen->modulo = $_modulo;		
		$imagen->fecha_alt = "$_fechaBd";
		$imagen->save();	
		return $imagen->id;*/
		
		
	}
	
	public function cargarImgDBLote($_identificador, $_path, $_nombre, $_orientacion)
	{
		//$_categoria_id = (int) $_categoria_id;
		$_nombre = strtolower(trim($_nombre));
		$_nombre = htmlentities($_nombre, ENT_QUOTES | ENT_IGNORE, "UTF-8");		
		$_fechaBd = date('Y-m-d');
		
		/*if($_modulo!=0){
			$_editar = contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ? AND modulo = ?', $_identificador, $_pos, $_modulo)));
		}else{
			$_editar = contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ?', $_identificador, $_pos)));
		}
		
		if($_editar){
			
			$_borrarImg = $this->EliminarImagenEnDirectorio($_ruta, $_editar->path, $_seccion);			
			if($_borrarImg==true){			
				$_editar->nombre = $_nombre;
				$_editar->identificador = $_identificador;
				$_editar->path = $_path;
				$_editar->posicion = $_pos;	
				$_editar->modulo = $_modulo;						
				$_editar->fecha_alt = date("Y/m/d");
				$_editar->save();
				return $_editar->id;
			}
		}else{*/
			$imagen = new contenidos_imagene();
			//$imagen->categoria_id = $_categoria_id;
			$imagen->nombre = $_nombre;
			$imagen->identificador = $_identificador;
			$imagen->path = $_path;
			$imagen->orientacion = $_orientacion;		
			$imagen->orden = self::ultimoOrdenImg($_identificador)->orden+1;		
			$imagen->fecha_alt = "$_fechaBd";
			$imagen->save();	
			return $imagen->id;
		//}
		
		
		
		
		
	}
	
	public function cargarImgDBLoteEditar($_identificador, $_path, $_nombre, $_pos, $_seccion, $_ruta, $_modulo=0)
	{
		//$_categoria_id = (int) $_categoria_id;
		$_nombre = strtolower(trim($_nombre));
		$_nombre = htmlentities($_nombre, ENT_QUOTES | ENT_IGNORE, "UTF-8");		
		$_fechaBd = date('Y-m-d');
		
		$imagen = new contenidos_imagene();
		//$imagen->categoria_id = $_categoria_id;
		$imagen->nombre = $_nombre;
		$imagen->identificador = $_identificador;
		$imagen->path = $_path;			
		$imagen->posicion = $_pos;
		$imagen->modulo = $_modulo;		
		$imagen->fecha_alt = "$_fechaBd";
		$imagen->save();	
		return $imagen->id;
		
		
	}
	
	public static function ultimoOrdenImg($_identificador)
	{
		$orden = contenidos_imagene::find(array('conditions' => array('identificador = ?', $_identificador),'select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}

	public static function ultimoOrdenImgDos($_identificador)
	{
		$orden = contenidos_imagenes_do::find(array('conditions' => array('identificador = ?', $_identificador),'select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	

	public function eliminarImagenes($_path, $_ruta, $_seccion)
	{
			
		if(is_readable($_ruta . $_seccion . '/'.$_path)){
			@unlink($_ruta . $_seccion . '/'. $_path);
		}			
		if(is_readable($_ruta  . $_seccion . '/thumb/'. $_path)){
			@unlink($_ruta . $_seccion . '/thumb/'. $_path);
		}
		
		return true;
	}
	
	public function EliminarImagenThumb($_ruta, $_path, $_seccion)
	{		
		//$borrar = contenidos_imagene::find($_id);
		//$borrar = contenidos_imagene::find(array('conditions' => array('id = ?', $_id)));
		
		//if($borrar){
			
			
			if($_seccion == 'promociones' || $_seccion== 'fondos'){
				if(is_readable($_ruta . $_seccion . '/'.$_path)){
					@unlink($_ruta . $_seccion . '/'. $_path);
				}	
			}
			if(is_readable($_ruta  . $_seccion . '/thumb/'. $_path)){
				@unlink($_ruta . $_seccion . '/thumb/'. $_path);
			}
			
			
			
			return true;
		/*}else
			throw new Exception('La imagen no se pudo encontrar');*/	
	}
	
	
	public function eliminarImgGal($_id, $_ruta, $_seccion){
		
		$borrar = contenidos_imagene::find($_id);
		//$borrar = contenidos_imagene::find(array('conditions' => array('id = ?', $_id)));
		
		if($borrar){
			
			// eliminamos la grande
			if(file_exists($_ruta . $_seccion . '/'.$borrar->path)){
				@unlink($_ruta . $_seccion . '/'. $borrar->path);
			}			
			// eliminamos la mediana
			/*if(file_exists($_ruta  . $_seccion . '/thumb/'. $borrar->path)){
				@unlink($_ruta . $_seccion . '/thumb/'. $borrar->path);
			}
			
			if($_seccion=='blog' || $_seccion=='productos'){			
				if(file_exists($_ruta  . $_seccion . '/miniatura/'. $borrar->path)){
					@unlink($_ruta . $_seccion . '/miniatura/'. $borrar->path);
				}
			}*/
			$this->ordenarGaleria($borrar->identificador,$borrar->orden);
			$borrar->delete();
			
			return true;
		}else
			return false;
	}
	public function eliminarImgGalDos($_id, $_ruta, $_seccion){
		
		$borrar = contenidos_imagenes_do::find($_id);
		//$borrar = contenidos_imagene::find(array('conditions' => array('id = ?', $_id)));
		
		if($borrar){
			
			// eliminamos la grande
			if(file_exists($_ruta . $_seccion . '/'.$borrar->path)){
				@unlink($_ruta . $_seccion . '/'. $borrar->path);
			}			
			// eliminamos la mediana
			/*if(file_exists($_ruta  . $_seccion . '/thumb/'. $borrar->path)){
				@unlink($_ruta . $_seccion . '/thumb/'. $borrar->path);
			}
			
			if($_seccion=='blog' || $_seccion=='productos'){			
				if(file_exists($_ruta  . $_seccion . '/miniatura/'. $borrar->path)){
					@unlink($_ruta . $_seccion . '/miniatura/'. $borrar->path);
				}
			}*/
			$this->ordenarGaleriaDos($borrar->identificador,$borrar->orden);
			$borrar->delete();
			
			return true;
		}else
			return false;
	}
	
	public function ordenarGaleria($_identificador,$_orden)
	{
		$arrayOrden = contenidos_imagene::find('all',array('conditions' => array('orden > ? AND identificador = ?', $_orden, $_identificador),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}

	public function ordenarGaleriaDos($_identificador,$_orden)
	{
		$arrayOrden = contenidos_imagenes_do::find('all',array('conditions' => array('orden > ? AND identificador = ?', $_orden, $_identificador),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	
	public function cambiarOrdenGaleria($_id, $_orden)
	{
			
		$act = contenidos_imagene::find(array('conditions' => array('id = ?', $_id)));
		$act->orden = $_orden;
		$act->save();
	}
	
	
	public function eliminarImagenMedio($_id, $_modulo, $_pos, $_ruta, $_seccion)
	{
		$_editar = contenidos_imagene::find(array('conditions' => array('identificador = ? AND modulo = ? AND  posicion = ?', $_id, $_modulo, $_pos)));
		
		if($_editar){
			$_borrarImg = $this->EliminarImagenEnDirectorio($_ruta, $_editar->path, $_seccion);
			/*if($_borrarImg==true){
				return true;
			}*/
			$_editar->delete();
			
		}
		return true;
	}
	
	
	
	
	
	
	public function borrarImagenesModulo($_ruta, $_seccion, $_modulo)
	{		
		//$borrar = contenidos_imagene::find($_id);
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('modulo = ?', $_modulo)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}		
			$this->eliminarImagenes($dat_por_identificador, $_ruta, $_seccion);
		}
		
		
	}
	
	
	
	public static function cadenaAleatoriaSegura($_caracter = 8)
	{      
	    $_chars = 'bcdfghjklmnprstvwxzaeiou0123456789';
	    $_result = '';
	    
	    for($p = 0; $p < $_caracter; $p++){
	        $_result .= ($p%2) ? $_chars[mt_rand(19, 33)] : $_chars[mt_rand(0, 18)];
	    }
	    return $_result;
	}
	
	public function traerDataBasicaImagen($_id)
	{
		return  contenidos_imagene::find(array('conditions' => array('id = ?', $_id)));
	}
	
	public function traerImagenesPorLoteDos($_ids)
	{
		// return contenidos_imagene::find('all', array('conditions' => array('id IN (?)', $_ids)));
		 return contenidos_imagenes_do::find_by_sql('SELECT * FROM contenidos_imagenes_dos WHERE id IN ('.$_ids.') ORDER BY orden ASC');
	}

	public function traerImagenesPorLote($_ids)
	{
		// return contenidos_imagene::find('all', array('conditions' => array('id IN (?)', $_ids)));
		 return contenidos_imagene::find_by_sql('SELECT * FROM contenidos_imagenes WHERE id IN ('.$_ids.') ORDER BY orden ASC');
	}
	
	public static function traerDataGaleriaPorIdentificador($_identificador,$_pos)
	{
		return  contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND posicion = ?', $_identificador, $_pos),'order' => 'orden asc'));
	}
	
	public function traerGaleriaPorIdentificador($_identificador, $_orientacion)
	{
		return contenidos_imagene::find('all',array('select' => 'path', 'conditions' => array('identificador = ? AND orientacion = ?', $_identificador, $_orientacion),'order' => 'orden asc'));
		
	}
	
	public static function traerDataImagen($_id)
	{
		//return  contenidos_imagene::find(array('conditions' => array('id = ?', $_id)));
		return  contenidos_imagene::find(array('conditions' => array('id = ?', $_id)));
	}
	
	public static function traerDataImagenPorIdentificador($_identificador, $_pos)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ?', $_identificador, $_pos)));
	}
	
	public static function traerDataImagenPorIdentificador2($_identificador, $_pos, $_tipo='')
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ? AND tipo = ?', $_identificador, $_pos, $_tipo)));
	}

	public static function traerDataImagenPorIdentificadorDos($_identificador, $_pos, $_tipo='')
	{
		return  contenidos_imagenes_do::find('all',array('conditions' => array('identificador = ? AND posicion = ? AND tipo = ?', $_identificador, $_pos, $_tipo),'order' => 'orden asc'));
	}

	public static function traerDataImagenPorIdentificadorV2($_identificador, $_pos, $_tipo='')
	{
		return  contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND posicion = ? AND tipo = ?', $_identificador, $_pos, $_tipo),'order' => 'orden asc'));
	}

	public static function traerImgOrientacion($_identificador, $_orientacion)
	{
		return  contenidos_imagene::find('all',array('conditions' => array('identificador = ? AND orientacion = ?', $_identificador, $_orientacion),'order' => 'orden asc'));
	}
	
		
	public static function traerImgPromos($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	
	public function traerDatosImagenes()
	{
		return  contenidos_datos_imagene::all();
	}
	
	public function traerDatosImagen($_id)
	{
		return contenidos_datos_imagene::find($_id);
	}
	
	public static function traerImgChica($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	
	
	public function traerImgBuscador($_valor)
	{
		return contenidos_datos_imagene::find_by_sql('SELECT * FROM contenidos_datos_imagenes WHERE (titulo LIKE "%'.$_valor.'%" OR descripcion LIKE "%'.$_valor.'%"  OR dia LIKE "%'.$_valor.'%" ) ORDER BY id DESC');
		
	}
	
	
	public static function formatFechaSql($_fecha)
	{
		$_fecha = explode("-", $_fecha);
		return $_fecha[2] . "-" . $_fecha[1] . "-" . $_fecha[0];
	}

	public static function traerImgPpal($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND orden = ?', $_identificador,0)));
	}
	
	


	////////////////////////////////////////////////////////////////////////////
	
	/*public function traerNoticias()
	{
		return  contenidos_blo::all(array('order' => 'orden asc'));
	}
	
	public function traerNoticia($_id)
	{
		return contenidos_blo::find($_id);
	}
	
	public static function ultimoOrdenBlog()
	{
		$orden = contenidos_blo::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function ordenarBlog($_orden)
	{
		$arrayOrden = contenidos_blo::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	
	public function cambiarOrdenBlog($_id, $_orden)
	{			
		$act = contenidos_blo::find(array('conditions' => array('id = ?', $_id)));
		$act->orden = $_orden;
		$act->save();
	}
	
	public function borrarBlog($_id, $_ruta, $_seccion)
	{		
		$borrar = contenidos_blo::find($_id);	
		$this->ordenarBlog($borrar->orden);				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}		
			$this->eliminarImagenes($dat_por_identificador, $_ruta, $_seccion);
		}
		$borrar->delete();
	}
	
	public function traerUsers($_estado)
	{
		return  contenidos_user::find('all',array('conditions' => array('estado = ?', $_estado),'order' => 'id desc'));
			
	}
	
	public function traerUser($_id)
	{
		return contenidos_user::find($_id);
	}
	
	public function traerProductos($_estado)
	{
		return  contenidos_producto::find('all',array('conditions' => array('estado = ?', $_estado),'order' => 'id desc'));
			
	}
	
	public function traerProducto($_id)
	{
		return contenidos_producto::find($_id);
	}
	
	public function borrarProducto($_id, $_ruta, $_seccion)
	{		
		$borrar = contenidos_producto::find($_id);	
		$this->ordenarProductos($borrar->orden);				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}		
			$this->eliminarImagenes($dat_por_identificador, $_ruta, $_seccion);
		}
		$borrar->delete();
	}
	
	
	public static function ultimoOrdenProductos()
	{
		$orden = contenidos_producto::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function ordenarProductos($_orden)
	{
		$arrayOrden = contenidos_producto::find('all',array('conditions' => array('orden > ? AND estado = ?', $_orden, 1),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	
	public function cambiarOrdenProductos($_id, $_orden)
	{			
		$act = contenidos_producto::find(array('conditions' => array('id = ?', $_id)));
		$act->orden = $_orden;
		$act->save();
	}
	
	public function traerProdAltaBuscador($_valor, $_estado)
	{
		return contenidos_user::find_by_sql('SELECT * FROM contenidos_productos WHERE estado = '.$_estado.' AND (nombre_es LIKE "%'.$_valor.'%" OR nombre_in LIKE "%'.$_valor.'%") ORDER BY id DESC');
		
	}
	
	
	
	
	public function traerGenero()
	{
		return  contenidos_gener::all();
	}
	
	public function traerCategoria()
	{
		return  contenidos_categoria::all();
	}
	
	public function traerTalles()
	{
		return  contenidos_talle::all();
	}
	
	public function traerColores()
	{
		return  contenidos_colore::all();
	}
	
	*/
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
	
	/*public function traerSliders()
	{
		return  contenidos_slider::all(array('order' => 'orden asc'));
	}
	
	public function traerSlider($_id)
	{
		return contenidos_slider::find($_id);
	}
	
	
	public function cambiarOrdenSlider($_id, $_orden)
	{
			
		$act = contenidos_slider::find(array('conditions' => array('id = ?', $_id)));
		$act->orden = $_orden;
		$act->save();
	}
	
	public static function ultimoOrdenSlider()
	{
		$orden = contenidos_slider::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function ordenarSlider($_orden)
	{
		$arrayOrden = contenidos_slider::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	
	public function borrarSlider($_id, $_ruta, $_seccion)
	{		
		$borrar = contenidos_slider::find($_id);
		$this->ordenarSlider($borrar->orden);
				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}		
			$this->eliminarImagenes($dat_por_identificador, $_ruta, $_seccion);
		}
		$borrar->delete();
	}
	
	
	public function traerEquipoCompleto()
	{
		return  contenidos_equip::all(array('order' => 'orden asc'));
	}
	
	public function traerEquipo($_id)
	{
		return contenidos_equip::find($_id);
	}
	
	
	public function cambiarOrdenEquipo($_id, $_orden)
	{
			
		$act = contenidos_equip::find(array('conditions' => array('id = ?', $_id)));
		$act->orden = $_orden;
		$act->save();
	}
	
	public static function ultimoOrdenEquipo()
	{
		$orden = contenidos_equip::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function ordenarEquipo($_orden)
	{
		$arrayOrden = contenidos_equip::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	
	public function borrarEquipo($_id, $_ruta, $_seccion)
	{		
		$borrar = contenidos_equip::find($_id);	
		$this->ordenarEquipo($borrar->orden);				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}		
			$this->eliminarImagenes($dat_por_identificador, $_ruta, $_seccion);
		}
		$borrar->delete();
	}
	
	
	public function traerSoluciones()
	{
		return  contenidos_solucione::all(array('order' => 'orden asc'));
	}
	
	public function traerSolucion($_id)
	{
		return contenidos_solucione::find($_id);
	}
	
	public static function traerEstadosSoluciones($_identificador, $modulo)
	{
		return  contenidos_estados_solucione::find(array('conditions' => array('id_solucion = ? AND modulo = ?', $_identificador, $modulo)));
	}
	
	public static function traerModulosSolucion($_identificador, $modulo)
	{
		$_dato = contenidos_modulos_solucione::find(array('conditions' => array('identificador = ? AND modulo = ?', $_identificador, $modulo)));
		return (isset($_dato)) ? $_dato : '';
	}
	
	
	public static function ultimoOrdenSoluciones()
	{
		$orden = contenidos_solucione::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function cambiarOrdenSoluciones($_id, $_orden)
	{
			
		$act = contenidos_solucione::find(array('conditions' => array('id = ?', $_id)));
		$act->orden = $_orden;
		$act->save();
	}
	
	public function ordenarSoluciones($_orden)
	{
		$arrayOrden = contenidos_solucione::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	
	public function borrarSoluciones($_id, $_ruta, $_seccion)
	{		
		$borrar = contenidos_solucione::find($_id);	
		$this->ordenarSoluciones($borrar->orden);				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}		
			$this->eliminarImagenes($dat_por_identificador, $_ruta, $_seccion);
		}
		
		$borrar_modulos = contenidos_modulos_solucione::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($borrar_modulos && is_array($borrar_modulos)){
			foreach($borrar_modulos as $mod){
				$mod->delete();
			}		
		}
		
		$borrar_estados = contenidos_estados_solucione::find('all',array('conditions' => array('id_solucion = ?', $borrar->identificador)));
		if($borrar_estados && is_array($borrar_estados)){
			foreach($borrar_estados as $est){
				$est->delete();
			}		
		}
		$borrar->delete();
	}
	
	
	
	
	
	
	
	public function traerCasos()
	{
		return  contenidos_caso::all(array('order' => 'orden asc'));
	}
	
	public function traerCaso($_id)
	{
		return contenidos_caso::find($_id);
	}
	
	public static function ultimoOrdenCasos()
	{
		$orden = contenidos_caso::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function cambiarOrdenCasos($_id, $_orden)
	{			
		$act = contenidos_caso::find(array('conditions' => array('id = ?', $_id)));
		$act->orden = $_orden;
		$act->save();
	}
	
	public function ordenarCasos($_orden)
	{
		$arrayOrden = contenidos_caso::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	
	public function borrarCasos($_id, $_ruta, $_seccion)
	{		
		$borrar = contenidos_caso::find($_id);
		$_borrar_medios = contenidos_medio::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($_borrar_medios){
			foreach($_borrar_medios as $b){
				$b->delete();	
			}
		}
		
		$this->ordenarCasos($borrar->orden);				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}		
			$this->eliminarImagenes($dat_por_identificador, $_ruta, $_seccion);
		}
		$borrar->delete();
	}
	
	public static function ultimoOrdenMedios($_identificador)
	{
		$orden = contenidos_medio::find(array('conditions' => array('identificador = ?', $_identificador),'select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function traerMedios($_identificador)
	{
		return  contenidos_medio::find('all',array('conditions' => array('identificador = ?', $_identificador),'order' => 'orden asc'));
	}
	
	
	public function borrarMedio($_id, $_ruta, $_seccion)
	{		
		$borrar = contenidos_medio::find($_id);	
		
		if($borrar->orden == 1){
			$_pos = 5;		
		} else if($borrar->orden == 2){
			$_pos = 6;		
		}else if($borrar->orden == 3){
			$_pos = 7;		
		}		
		
			
		
		$_eliminar_img = $this->eliminarImagenMedio($borrar->identificador, 0, $_pos, $_ruta, $_seccion);			
		if($_eliminar_img==true){
			$this->ordenarMedios($borrar->identificador, $borrar->orden);
		}
		
		$borrar->delete();
	}
	
	
	public function ordenarMedios($_id, $_orden)
	{
		$_arrayOrden = contenidos_medio::find('all',array('conditions' => array('identificador = ?  AND orden > ?',$_id, $_orden),'order' => 'orden asc'));
		if($_arrayOrden && is_array($_arrayOrden)){
			for($i=0;$i<count($_arrayOrden);$i++){
				$this->ordenarImgMedios($_id, $_arrayOrden[$i]->orden);
				//$dat_por_identificador[] = $arrayOrden[$i]->orden;
				$_arrayOrden[$i]->orden = $_arrayOrden[$i]->orden-1;
				$_arrayOrden[$i]->save();		
			}
		}
		
		
	}
	
	public function ordenarImgMedios($_id, $_orden)
	{
		if($_orden == 1){
			$_pos = 5;		
		} else if($_orden == 2){
			$_pos = 6;		
		}else if($_orden == 3){
			$_pos = 7;		
		}	
		
		$orden = contenidos_imagene::find(array('conditions' => array('identificador = ? AND posicion = ?', $_id, $_pos)));
		if($orden){
			$orden->posicion = $orden->posicion-1;
			$orden->save();		
		}
	}
	
	
	
	
	*/
	
	
	
	
	
	
	
	
	
	
	
	
	///////////////////////////////////////////////////////////////////////////////
	
	
	
	/*
	
	public function traerBeneficios($_estado)
	{
		return  contenidos_beneficio::find('all',array('conditions' => array('estado = ?', $_estado),'order' => 'orden asc'));
			
	}
	
	public function traerBeneficio($_id)
	{
		return contenidos_beneficio::find($_id);
	}
	
	public static function ultimoOrdenBeneficios()
	{
		$orden = contenidos_beneficio::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function ordenarBeneficios($_orden)
	{
		$arrayOrden = contenidos_beneficio::find('all',array('conditions' => array('orden > ? AND estado = ?', $_orden,2),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	
	
	
	public function cambiarOrdenBeneficios($_id, $_orden)
	{
			
		$act = contenidos_beneficio::find($_id);
		$act->orden = $_orden;
		$act->save();
	}
	
	public function traerManuales()
	{
		return  contenidos_manuale::all(array('order' => 'orden asc'));
			
	}
	
	public function traerManual($_id)
	{
		return contenidos_manuale::find($_id);
	}
	
	public static function ultimoOrdenManuales()
	{
		$orden = contenidos_manuale::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function ordenarManuales($_orden)
	{
		$arrayOrden = contenidos_manuale::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	
	public function cambiarOrdenManuales($_id, $_orden)
	{
			
		$act = contenidos_manuale::find($_id);
		$act->orden = $_orden;
		$act->save();
	}
	
	public static function traerArchivo($_identificador)
	{
		return  contenidos_archivo::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	
	public function borrarManual($_id, $_ruta)
	{		
		$borrar = contenidos_manuale::find($_id);		
		self::ordenarManual($borrar->orden);
		
	
				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}
		}
		$this->eliminarImagenes($dat_por_identificador, $_ruta, 'manuales');
		
		$borrar->delete();
	}
	
	public static function ordenarManual($_orden)
	{
		$arrayOrden = contenidos_manuale::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	
	}
	
	
	public function traerCapacitaciones()
	{
		return  contenidos_capacitacione::all(array('order' => 'orden asc'));
			
	}
	
	public function traerCapacitacion($_id)
	{
		return contenidos_capacitacione::find($_id);
	}
	
	public static function ultimoOrdenCapacitacion()
	{
		$orden = contenidos_capacitacione::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function ordenarCapacitaciones($_orden)
	{
		$arrayOrden = contenidos_capacitacione::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	public function cambiarOrdenCapacitaciones($_id, $_orden)
	{
			
		$act = contenidos_capacitacione::find($_id);
		$act->orden = $_orden;
		$act->save();
	}
	
	public function borrarCapacitacion($_id, $_ruta)
	{		
		$borrar = contenidos_capacitacione::find($_id);		
		self::ordenarCapacitacion($borrar->orden);		
				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}
		}
		$this->eliminarImagenes($dat_por_identificador, $_ruta, 'capacitaciones');
		
		$borrar->delete();
	}
	
	public static function ordenarCapacitacion($_orden)
	{
		$arrayOrden = contenidos_capacitacione::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	
	}
	
	public function traerDestacados()
	{
		return  contenidos_destacado::all(array('order' => 'orden asc'));
			
	}
	
	public function traerDestacado($_id)
	{
		return contenidos_destacado::find($_id);
	}
	
	public static function ultimoOrdenDestacado()
	{
		$orden = contenidos_destacado::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	public function ordenarDestacados($_orden)
	{
		$arrayOrden = contenidos_destacado::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}
	public function cambiarOrdenDestacados($_id, $_orden)
	{
			
		$act = contenidos_destacado::find($_id);
		$act->orden = $_orden;
		$act->save();
	}
	
	public function borrarDestacado($_id, $_ruta)
	{		
		$borrar = contenidos_destacado::find($_id);		
		self::ordenarDest($borrar->orden);		
				
		$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
		if($imgBorrar && is_array($imgBorrar)){
			foreach($imgBorrar as $idss){
				$dat_por_identificador[] = $idss->id;
			}
		}
		$this->eliminarImagenes($dat_por_identificador, $_ruta, 'destacados');
		
		$borrar->delete();
	}
	
	public static function ordenarDest($_orden)
	{
		$arrayOrden = contenidos_destacado::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	
	}
	
	public function traerDestacadosModulos()
	{
		return  contenidos_destacados_modulo::all(array('order' => 'id desc'));
			
	}
	
	public function traerDestacadoModulo($_id)
	{
		return contenidos_destacados_modulo::find($_id);
	}
	
	public function traerDestacadoModuloActivo($_modulo)
	{
		return contenidos_destacados_modulo::find(array('conditions' => array('modulo = ?', $_modulo)));
	}
	
	public static function ordenarDestModulo($_orden)
	{
		$arrayOrden = contenidos_destacados_modulo::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	
	}
	
	
	public function traerUserAltaBuscador($_valor, $_estado)
	{
		return contenidos_user::find_by_sql('SELECT * FROM contenidos_users WHERE estado = '.$_estado.' AND (nombre LIKE "%'.$_valor.'%" OR nombre_serv_tecnico LIKE "%'.$_valor.'%"  OR localidad LIKE "%'.$_valor.'%" OR provincia LIKE "%'.$_valor.'%" OR telefono LIKE "%'.$_valor.'%" OR email LIKE "%'.$_valor.'%" OR num_credencial LIKE "%'.$_valor.'%") ORDER BY id DESC');
		
	}
	
	public function traerJuegos()
	{
		return  contenidos_juego::all(array('order' => 'orden asc'));
			
	}
	
	public function traerJuego($_id)
	{
		return contenidos_juego::find($_id);
	}
	
	public function cambiarOrdenJuegos($_id, $_orden)
	{
			
		$act = contenidos_juego::find($_id);
		$act->orden = $_orden;
		$act->save();
	}
	
	public static function ultimoOrdenJuegos()
	{
		$orden = contenidos_juego::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	
	public function borrarJuegos($_id)
	{		
		$borrar = contenidos_juego::find($_id);		
		self::ordenarJuego($borrar->orden);
		$borrar->delete();
	}
	
	public static function ordenarJuego($_orden)
	{
		$arrayOrden = contenidos_juego::find('all',array('conditions' => array('orden > ?', $_orden),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	
	}
	
	*/
	
	//////////////////////////////////////////////////////////////////////////////////////////
	
	/*public function traerPromos($_estado)
	{
		if($_estado==1){
			return  contenidos_promo::find('all',array('conditions' => array('estado = ?', $_estado),'order' => 'orden asc'));
		}else{
			return  contenidos_promo::find('all',array('conditions' => array('estado = ?', $_estado),'order' => 'id desc'));		
		}
			
	}
	
	public function traerPromo($_id)
	{
		return contenidos_promo::find($_id);
	}
	
	
	
		
	public static function traerImgFondo($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ? AND orden = ?', $_identificador,1)));
	}
	
	public static function traerTermino($_identificador)
	{
		return  contenidos_termino::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	
	public static function excelRegistros($_id,$_ruta)
	{
		$registros = contenidos_promo::find($_id);
		$participantes  = contenidos_participante::find('all',array('conditions' => array('id_promo = ?', $registros->id)));
		$contenido ='';
		if($participantes){
			
			
			foreach($participantes as $regs){
				$contenido .= html_entity_decode(self::convertirCaracteres(self::tildesHtml(ucwords(strtolower(Encriptar::decrypt($registros->titulo))))));
				$contenido .= "\t".self::convertirCaracteres(self::tildesHtml(ucwords(strtolower(Encriptar::decrypt($registros->descripcion)))));
				$contenido .= "\t".self::convertirCaracteres(self::tildesHtml(ucwords(strtolower(Encriptar::decrypt($regs->nombre)))));
				$contenido .= "\t".self::convertirCaracteres(self::tildesHtml(ucwords(strtolower(Encriptar::decrypt($regs->apellido)))));
				$contenido .= "\t".Encriptar::decrypt($regs->dni);
				$contenido .= "\t".self::convertirCaracteres(self::tildesHtml(ucwords(strtolower(Encriptar::decrypt($regs->localidad)))));
				$contenido .= "\t".self::convertirCaracteres(self::tildesHtml(ucwords(strtolower(Encriptar::decrypt($regs->provincia)))));
				$contenido .= "\t".Encriptar::decrypt($regs->telefono);
				$contenido .= "\t".Encriptar::decrypt($regs->email);
				$contenido .= "\t".Encriptar::decrypt($regs->factura);
				$contenido .= "\t".self::convertirCaracteres(self::tildesHtml(ucwords(strtolower(Encriptar::decrypt($regs->compra)))))."\n";
			}
			
			$cabecera="TITULO\tDESCRIPCION\tNOMBRE\tAPELLIDO\tDNI\tLOCALIDAD\tPROVINCIA\tTELEFONO\tEMAIL\tNUMERO FACTURA\tDONDE REALIZASTE TU COMPRA\n";
			
		}else{
			$contenido .= self::convertirCaracteres(self::tildesHtml(ucwords(strtolower(Encriptar::decrypt($registros->titulo)))));
			$contenido .= "\t".self::convertirCaracteres(self::tildesHtml(ucwords(strtolower(Encriptar::decrypt($registros->descripcion)))))."\n";
			
			$cabecera="TITULO\tDESCRIPCION\n";
		}
		
		
		$nombre = "registros_promos_". $_id .".xls";
		$url = $_ruta.$nombre;
		//$url = $this->_conf['base_url']."public/descargas/".$nombre;
	
		$p=fopen("$url","w");
		if($p){			
			fwrite($p,$cabecera);
			fwrite($p,$contenido);
		}	
		fclose($p);
		
		return $nombre;
	}
	
	
	
	
	
	public function ordenarPromos($_orden)
	{
		$arrayOrden = contenidos_promo::find('all',array('conditions' => array('orden > ? AND estado = ?', $_orden,1),'order' => 'orden asc'));
		for($i=0;$i<count($arrayOrden);$i++){
			$arrayOrden[$i]->orden = $arrayOrden[$i]->orden-1;
			$arrayOrden[$i]->save();		
		}
	}*/
	
	//////////////////////////////////////////////////////////////////////////////
	///////////////////////////////////////////////////////////////////////////
	
	
	
	public static function traerImgPorId($_id)
	{
		$_id = (int) $_id;
		return contenidos_imagene::find($_id);
	}
	
	public function traerImgPorIdentificador($_identificador)
	{
		return  contenidos_imagene::find(array('conditions' => array('identificador = ?', $_identificador)));
	}
	
	public static function convertirCaracteres($_cadena)
	{
		return htmlspecialchars_decode(html_entity_decode($_cadena));
	}
	
	public static function convertirCaracteres2($_cadena)
	{
		return htmlspecialchars(html_entity_decode($_cadena));
	}
	
	public static function convertirCaracteres3($_cadena)
	{
		$_cadena = htmlspecialchars_decode($_cadena);
		$_cadena = html_entity_decode($_cadena);
		return utf8_decode($_cadena);
	}
	public static function convertirCaracteres4($_cadena)
	{
		$_cadena = htmlspecialchars_decode($_cadena);
		$_cadena = html_entity_decode($_cadena);
		return $_cadena;
	}
	
	
	public static function ultimoOrden()
	{
		$orden = contenidos_noticia::find(array('select' => 'max(orden) as orden'));
		if (isset($orden)){
			return $orden;
		}else{
			return 0;
		}
	}
	
	
	public function imagenesVinculadasPorIdentificador($_identificador)
	{		
		// Traemos las imagenes por Identificador
		$por_identificador = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $_identificador)));
		
		// inicializamos los arreglos 
		$dat_por_identificador = array();	
		// Filtramos Ids por Identificadores
		if($por_identificador && is_array($por_identificador)){
			foreach($por_identificador as $idss){
				$dat_por_identificador[] = $idss->id;
			}
		}
		
		if(!empty($dat_por_identificador)){
			$ids = implode($dat_por_identificador,',');
		}else{
			$ids = '';	
		}
		return $ids;
	}
	
	public function traerImgPorIds($_ids)
	{
		//$dat_por_identificador = array();	
		return $ids= explode(',',$_ids);
	}
	
	public function eliminarArchivos($_elementos, $_ruta)
	{
		$_elemento = contenidos_archivo::find($_elementos); 
		
		if(is_readable($_ruta. $_elemento->path)){
			@unlink($_ruta. $_elemento->path);
		}
		$_elemento->delete();	
	}
	
	
	
	
	public function traerImagenes($_identificador)
	{
		$por_identificador = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $_identificador),'order' => 'orden asc'));
		
		return $por_identificador;
		
	}
	
	public static function quitarTags($cadena)
	{
		return str_replace(array('<p>','</p>'),array('',''),$cadena);
	}
	
	public static function tildesHtml($cadena) 
	{ 
		return str_replace(array("á","é","í","ó","ú","ñ","Á","É","Í","Ó","Ú","Ñ"),
							array("&aacute;","&eacute;","&iacute;","&oacute;","&uacute;","&ntilde;","&Aacute;","&Eacute;","&Iacute;","&Oacute;","&Uacute;","&Ntilde;"),
							$cadena);     
	}
	
	public static function cortarTexto($texto,$tamano,$colilla="...")
	{
		$texto=substr($texto, 0,$tamano);
		$index=strrpos($texto, " ");
		$texto=substr($texto, 0,$index); $texto.=$colilla;
		return $texto;
	} 
	
	
	public static function CapturarAnchoAlto($recurso)											
	{
		$dimension = array();
		
		list($ancho, $alto) = getimagesize($recurso);
		$dimension[] = $ancho;
		$dimension[] = $alto;
		return $dimension;
	}
	
	/*public static function crearTitulo($_titulo)
    {
        $_titulo = strtolower($_titulo);
        return str_replace(" ","-",$_titulo);
    }*/

    public static function crearTitulo($_titulo)
    {       
		$_titulo = mb_strtolower ($_titulo, 'UTF-8');
		$find = array('á', 'é', 'í', 'ó', 'ú', 'ñ');
		$repl = array('a', 'e', 'i', 'o', 'u', 'n');
		$_titulo = str_replace ($find, $repl, $_titulo);
		$find = array(' ', '&', '\r\n', '\n', '+');
		$_titulo = str_replace ($find, '-', $_titulo);
		$find = array('/[^a-z0-9\-<>]/', '/[\-]+/', '/<[^>]*>/');
		$repl = array('', '', '');
		$_titulo = preg_replace ($find, $repl, $_titulo);
		return $_titulo;
	
    }


    //nuevo borrar contenidos baja mas de 60 dias

    public function borrarVariosCatalogo($_id, $_ruta, $_seccion, $_array_bajas)
	{	

		$_ids = explode(',', $_id);
		$ids_bajas = implode(',', $_array_bajas);
		for ($i=0; $i < count($_ids); $i++) { 
			$_asig =  contenidos_asignacione::find_by_sql('SELECT id FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_ids[$i].'",ids_catalogos) AND id NOT IN ('.$ids_bajas.')');
			if($_asig){

				$_asig = $this->armarArray($_asig);
		    	for ($k=0; $k < count($_asig); $k++) { 
		    		$_asig_id[] = $_asig[$k]['id'];
		    	}	
			
				$_res = array_intersect($_asig_id, $_array_bajas);
				if($_res){

					for ($w=0; $w < count($_res); $w++) { 
						$borrar = contenidos_catalogo::find($_ids[$i]);	
						$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
						if($imgBorrar){
							foreach($imgBorrar as $_img){
								$_img->delete();
							}		
							$this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
						}
						
						$borrar->delete();
						return true;
					}
					

				}else{
					return false;
				}
				
			}else{

				$borrar = contenidos_catalogo::find($_ids[$i]);	
				$imgBorrar = contenidos_imagene::find('all',array('conditions' => array('identificador = ?', $borrar->identificador)));
				if($imgBorrar){
					foreach($imgBorrar as $_img){
						$_img->delete();
					}		
					$this->borrarDirectorio($_ruta.$_seccion.'/cat_'.$borrar->identificador);
				}
				
				$borrar->delete();
				return true;

			}
		}

		

		
	}


	public function borrarVariosVideos($_id, $_ruta, $_array_bajas)
	{

		$_ids = explode(',', $_id);
		$ids_bajas = implode(',', $_array_bajas);
		for ($i=0; $i < count($_ids); $i++) { 
			$_asig =  contenidos_asignacione::find_by_sql('SELECT id FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_ids[$i].'",ids_videos) AND id NOT IN ('.$ids_bajas.')');
			if($_asig){
				$_asig = $this->armarArray($_asig);
		    	for ($k=0; $k < count($_asig); $k++) { 
		    		$_asig_id[] = $_asig[$k]['id'];
		    	}	
			
				$_res = array_intersect($_asig_id, $_array_bajas);
				if($_res){

					for ($w=0; $w < count($_res); $w++) { 
						
						$borrar = contenidos_heade::find($_ids[$i]);	
						$_vidBorrar = contenidos_video::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
						if($_vidBorrar){				
							$_vidBorrar->delete();
							$this->borrarDirectorio($_ruta.'/vid_'.$borrar->identificador);
						}
						
						$borrar->delete();
						return true;
					}
					

				}else{
					return false;
				}
				
			}else{

				$borrar = contenidos_heade::find($_ids[$i]);	
				$_vidBorrar = contenidos_video::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
				if($_vidBorrar){				
					$_vidBorrar->delete();
					$this->borrarDirectorio($_ruta.'/vid_'.$borrar->identificador);
				}
				
				$borrar->delete();
				return true;

			}
		}

		


	}

	public function borrarVariosInactivas($_id, $_ruta, $_array_bajas)
	{		

		$_ids = explode(',', $_id);
		$ids_bajas = implode(',', $_array_bajas);
		for ($i=0; $i < count($_ids); $i++) { 
			$_asig =  contenidos_asignacione::find_by_sql('SELECT id FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_ids[$i].'",ids_inactivas) AND id NOT IN ('.$ids_bajas.')');
			if($_asig){
				$_asig = $this->armarArray($_asig);
		    	for ($k=0; $k < count($_asig); $k++) { 
		    		$_asig_id[] = $_asig[$k]['id'];
		    	}	
			
				$_res = array_intersect($_asig_id, $_array_bajas);
				if($_res){

					for ($w=0; $w < count($_res); $w++) { 						

						$borrar = contenidos_pantalla_inactiv::find($_ids[$i]);	
						$_vidBorrar = contenidos_video::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
						if($_vidBorrar){				
							$_vidBorrar->delete();
							$this->borrarDirectorio($_ruta.'/inac_'.$borrar->identificador);
						}
						
						$borrar->delete();
						return true;
					}
					

				}else{
					return false;
				}
				
			}else{
				
				$borrar = contenidos_pantalla_inactiv::find($_ids[$i]);	
				$_vidBorrar = contenidos_video::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
				if($_vidBorrar){				
					$_vidBorrar->delete();
					$this->borrarDirectorio($_ruta.'/inac_'.$borrar->identificador);
				}
				
				$borrar->delete();
				return true;

			}
		}


		/*$_asig =  contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_id.'",ids_inactivas)');	
		if($_asig){
			return false;
		}else{
			$borrar = contenidos_pantalla_inactiv::find($_id);	
			$_vidBorrar = contenidos_video::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
			if($_vidBorrar){				
				$_vidBorrar->delete();
				$this->borrarDirectorio($_ruta.'/inac_'.$borrar->identificador);
			}
			
			$borrar->delete();
			return true;
		}*/


	}

	public function borrarVariosFooters($_id, $_ruta, $_seccion, $_array_bajas)
	{		

		$_ids = explode(',', $_id);
		$ids_bajas = implode(',', $_array_bajas);
		for ($i=0; $i < count($_ids); $i++) { 
			$_asig =  contenidos_asignacione::find_by_sql('SELECT id FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_ids[$i].'",ids_footers) AND id NOT IN ('.$ids_bajas.')');
			if($_asig){
				$_asig = $this->armarArray($_asig);
		    	for ($k=0; $k < count($_asig); $k++) { 
		    		$_asig_id[] = $_asig[$k]['id'];
		    	}	
			
				$_res = array_intersect($_asig_id, $_array_bajas);
				if($_res){

					for ($w=0; $w < count($_res); $w++) { 

						$borrar = contenidos_promocione::find($_ids[$i]);	
						$imgBorrar = contenidos_imagene::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
						if($imgBorrar){
							$this->eliminarImagenesPromos($imgBorrar->path, $_ruta, $_seccion);
							$imgBorrar->delete();
						}
						
						$borrar->delete();
						return true;
					}
					

				}else{
					return false;
				}
				
			}else{
				

				$borrar = contenidos_promocione::find($_ids[$i]);	
				$imgBorrar = contenidos_imagene::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
				if($imgBorrar){
					$this->eliminarImagenesPromos($imgBorrar->path, $_ruta, $_seccion);
					$imgBorrar->delete();
				}
				
				$borrar->delete();
				return true;

			}
		}


		/*$_asig =  contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_id.'",ids_footers)');	
		if($_asig){
			return false;
		}else{
			$borrar = contenidos_promocione::find($_id);	
			$imgBorrar = contenidos_imagene::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
			if($imgBorrar){
				$this->eliminarImagenesPromos($imgBorrar->path, $_ruta, $_seccion);
				$imgBorrar->delete();
			}
			
			$borrar->delete();
			return true;
		}*/

	}

	public function borrarVariosFondos($_id, $_ruta, $_seccion, $_array_bajas)
	{		
		$_ids = explode(',', $_id);
		$ids_bajas = implode(',', $_array_bajas);
		for ($i=0; $i < count($_ids); $i++) { 
			$_asig =  contenidos_asignacione::find_by_sql('SELECT id FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_ids[$i].'",ids_fondos) AND id NOT IN ('.$ids_bajas.')');
			if($_asig){
				$_asig = $this->armarArray($_asig);
		    	for ($k=0; $k < count($_asig); $k++) { 
		    		$_asig_id[] = $_asig[$k]['id'];
		    	}	
			
				$_res = array_intersect($_asig_id, $_array_bajas);
				if($_res){

					for ($w=0; $w < count($_res); $w++) { 

						$borrar = contenidos_fondo::find($_ids[$i]);	
						$imgBorrar = contenidos_imagene::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
						if($imgBorrar){
							$this->eliminarImagenesPromos($imgBorrar->path, $_ruta, $_seccion);
							$imgBorrar->delete();
						}
						
						$borrar->delete();
						return true;
					}
					

				}else{
					return false;
				}
				
			}else{
				

				$borrar = contenidos_fondo::find($_ids[$i]);	
				$imgBorrar = contenidos_imagene::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
				if($imgBorrar){
					$this->eliminarImagenesPromos($imgBorrar->path, $_ruta, $_seccion);
					$imgBorrar->delete();
				}
				
				$borrar->delete();
				return true;

			}
		}




		/*$_asig =  contenidos_asignacione::find_by_sql('SELECT * FROM contenidos_asignaciones WHERE FIND_IN_SET ("'.$_id.'",ids_fondos)');	
		if($_asig){
			return false;
		}else{
			$borrar = contenidos_fondo::find($_ids[$i]);	
			$imgBorrar = contenidos_imagene::find(array('conditions' => array('identificador = ?', $borrar->identificador)));
			if($imgBorrar){
				//$dat_por_identificador[] = $idss->id;
				$this->eliminarImagenesPromos($imgBorrar->path, $_ruta, $_seccion);
				$imgBorrar->delete();
			}
			
			$borrar->delete();
			return true;
		}*/


	}

	public static function array_msort($array, $cols)
	{
	    $colarr = array();
	    foreach ($cols as $col => $order) {
	        $colarr[$col] = array();
	        foreach ($array as $k => $row) { $colarr[$col]['_'.$k] = strtolower($row[$col]); }
	    }
	    $eval = 'array_multisort(';
	    foreach ($cols as $col => $order) {
	        $eval .= '$colarr[\''.$col.'\'],'.$order.',';
	    }
	    $eval = substr($eval,0,-1).');';
	    eval($eval);
	    $ret = array();
	    foreach ($colarr as $col => $arr) {
	        foreach ($arr as $k => $v) {
	            $k = substr($k,1);
	            if (!isset($ret[$k])) $ret[$k] = $array[$k];
	            $ret[$k][$col] = $array[$k][$col];
	        }
	    }
	    return $ret;

	}

	
}