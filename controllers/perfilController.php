<?php

use Nucleo\Controller\Controller;

class perfilController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('class.home');		
		$this->homeGestion = new home();

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
			
				
    }
    
   		
	
	public function index()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }
        
       $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $this->_view->user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front'));
        $this->_view->cliente = home::traerClientePorUsers($this->_view->user['id_cliente']);
        $this->_view->user['numero_cliente'] = $this->_view->cliente['numero_cliente'];
        $this->_view->user['razon_social'] = $this->_view->cliente['razon_social'];
        $_total = $this->homeGestion->traerUserPuntosTotales($this->_sess->get('id_usuario_front'));         
        $this->_view->total = $_total['puntos_totales'] - $_total['puntos_canjeados'];   

        if(!$this->_sess->get('carga_user')){
        	if($this->_view->user['identificador']!=''){
        		$this->_sess->set('carga_user', $this->_view->user['identificador']);
        	}else{
        		$this->_sess->set('carga_user', rand((int)1135687452,(int)999999999));
        	}
			
		}        	
		

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index','perfil', 'default');
    }

    public function puntos()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }
        
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $this->_view->puntos = $this->homeGestion->traerUserPuntos($this->_sess->get('id_usuario_front'));
        $this->_view->puntos_canjeados = $this->homeGestion->traerUserPuntosCanjeados($this->_sess->get('id_usuario_front'));
		// $this->_view->total = $this->homeGestion->traerUserPuntosTotales($this->_sess->get('id_usuario_front'));    
		$_total = $this->homeGestion->traerUserPuntosTotales($this->_sess->get('id_usuario_front'));         
        $this->_view->total = $_total['puntos_totales'] - $_total['puntos_canjeados'];   

		//  echo "<pre>";print_r($this->_view->puntos_canjeados);exit;

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('puntos','perfil', 'default');
    }


    public function editar()
    {

    	if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }

    	if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					// echo "<pre>";print_r($this->_view->data);exit;

					if(!validador::getPostParam('nombre')){
						/*$this->_view->_error = 'Debe introducir un numero de cliente valido';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'Debe introducir un nombre';
						exit;
					}


					// if(!validador::getAlphaNum('usuario')){
					if(!validador::validarEmail($this->_view->data['email'])){
						/*$this->_view->_error = 'Debe introducir un email valido';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'Debe introducir un email valido';
						exit;
					}
					
					//if(!validador::getSql('pass',$this->_conf['baseDatos'])){
					if(!validador::getAlphaNum('password')){					
						/*$this->_view->_error = 'Debe introducir su password';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'Debe introducir su contraseña';
						exit;
					}


					if($this->_view->data['password']!= $this->_view->data['repetir_password']){					
						/*$this->_view->_error = 'Debe introducir su password';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'Contraseña y repetir contraseña no coinciden';
						exit;
					}
					
					
					
					$cat = contenidos_user::find($this->_sess->get('id_usuario_front'));
					// $cat->numero_cliente = $this->_view->data['numero_cliente'];
					// $cat->razon_social = $this->_xss->xss_clean(validador::getTexto('razon_social'));
					// $cat->ciudad = $this->_xss->xss_clean(validador::getTexto('ciudad'));
					// $cat->provincia = $this->_xss->xss_clean(validador::getTexto('provincia'));
					$cat->nombre = $this->_xss->xss_clean(validador::getTexto('nombre'));
					$cat->email = $this->_view->data['email'];
					// $cat->password = $this->_view->data['password'];	
					$cat->password = Hash::getHash('md5', $this->_xss->xss_clean($this->_view->data['password']), $this->_conf['hash_key']);				
					// $cat->fecha_nacimiento = $this->_view->data['fecha_nacimiento'];	
					$cat->identificador = $this->_sess->get('carga_user');				
					$cat->save();
								
					

					 $this->_sess->destroy('carga_user');	
					 $this->_sess->destroy('img_id_user');	
					 $this->_sess->destroy('img_error_user');				
					// $this->redireccionar('administrador/users');
					echo 'ok';
					exit;
					
												
					
				}

			}else{
				// $this->redireccionar('error/access/404');
				echo 'Hubo un error, vuelva a intentarlo mas tarde.';
				exit;
			}
		}

    }



    public function cargImg($_seccion)
	{
		if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }

		if (!empty($_FILES)) {

			$filename = strtolower($_FILES['file']['name']);
			$whitelist = array('jpg', 'png', 'gif', 'jpeg');
			// $whitelist = array('mp4','avi','mpeg');  
			$backlist = array('php', 'php3', 'php4', 'phtml','exe','bat','js');
			$tmp = explode('.', $filename);
			$file_extension = end($tmp); 
			if(!in_array($file_extension, $whitelist)){
				$this->_sess->set('img_error_user', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}
			if(in_array($file_extension, $backlist)){
				$this->_sess->set('img_error_user', 'archivo invalido');
			    echo 'archivo invalido';
			    exit();
			}

			if($this->_sess->get('img_id_user')) $this->_sess->destroy('img_id_user');
			
			$_info = pathinfo($_FILES['file']['name']);
			$_nombre_archivo =  basename($_FILES['file']['name'], '.' . $_info['extension']);
			$_nombre_interno = admin::cadenaAleatoriaSegura(15);
			$size = getimagesize($_FILES['file']['tmp_name']);
			
			
			$foo = new upload($_FILES['file']);
			$foo->file_new_name_body 		= $_nombre_interno;
			//$foo->image_convert 			= 'jpg';
			$foo->image_resize          	= true;
			$foo->jpeg_quality          	= 100;
			$foo->png_compression       	= 10;
			/*$foo->image_ratio_fill      	= true;
			$foo->image_ratio_y         	= true;*/
			$foo->image_ratio_crop			= true;				
			$foo->image_x = $this->_conf['formatos_img'][$_seccion]['ancho'];
			$foo->image_y = $this->_conf['formatos_img'][$_seccion]['alto'];
			
			$foo->process($this->_conf['ruta_img_cargadas'] . $_seccion . DS);			
			
			
			if($foo->processed){							
				
								
				$_nombre = (isset($_POST['imagen_principal_nombre']) && $_POST['imagen_principal_nombre'] != '') ? $_POST['imagen_principal_nombre'] : $_FILES['file']['name'];

				// Base Datos				
				$_imagen = $this->homeGestion->cargarImgDB($this->_sess->get('carga_user'), $foo->file_dst_name, $_nombre, $_seccion, $this->_conf['ruta_img_cargadas']);
				
				
				if($_imagen) $this->_sess->set('img_id_user', $_imagen);
				exit;
			}
		}
	}
	
	

	public function traerImagenfinal($_seccion)
	{	
		if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }


		if($this->_sess->get('img_error_user')){
			echo 'archivo invalido';
			$this->_sess->destroy('img_error_user');
			exit();
		}

		
		$_data_img = $this->homeGestion->traerDataBasicaImagen($this->_sess->get('img_id_user'));
		
		// Imagen principal
		$_img = $this->_conf['ruta_img_cargadas'] . $_seccion . "/" . $_data_img->path;
		
		if(!file_exists($_img)) exit("Ha ocurrido un error. La imágen no existe o se ha corrompido.");
		
		$cat = contenidos_user::find($this->_sess->get('id_usuario_front'));			
		$cat->identificador = $this->_sess->get('carga_user');				
		$cat->save();
		
	    // echo "<div id=\"" . $this->_sess->get('img_id_user') . "\">
	    //     <img src=\"" . $this->_conf['base_url'] . "public/img/subidas/".$_seccion . "/". $_data_img->path . "\" class=\"img-thumbnail\">
	    // </div>";		

	    echo $this->_conf['base_url'] . "public/img/subidas/".$_seccion . "/". $_data_img->path;
		
		$this->_sess->destroy('img_id_user');
		
		exit();
	}
	
	
	
}


?>