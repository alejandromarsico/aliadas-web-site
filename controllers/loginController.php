<?php

use Nucleo\Controller\Controller;

class loginController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
			
				
    }
    
   		
	
	/*public function index()
	{	
		$this->redireccionar('usuarios/login');		
    }*/
	

	public function index()
    {
        // if($this->_sess->get('autenticado_front')){
            
        // }
        //$this->redireccionar('home');
       /*$_datos = home::traerBuscadorCategorias(4);
       echo "<pre>";print_r($_datos);echo "</pre>";exit;*/
		

		 $this->_view->titulo = 'Iniciar Sesion';
        $this->_view->renderizar('index','login', 'login');
    }

    public function login()
    {

    	if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
			
				if(validador::getInt('enviar') == 1){
					
					
					$this->_view->datos = $_POST;
					
					// if(!validador::getAlphaNum('usuario')){
					if(!validador::validarEmail($this->_view->datos['email'])){
						// $this->_view->_error = 'Debe introducir un email valido';
						/*$this->_view->renderizar('index','login');
						exit;*/
						echo 'Debe introducir un email valido';
						exit;
					}
					
					//if(!validador::getSql('pass',$this->_conf['baseDatos'])){
					if(!validador::getAlphaNum('pass')){					
						/*$this->_view->_error = 'Debe introducir su password';
						$this->_view->renderizar('index','login', 'login'); 
						exit;*/
						echo 'Debe introducir su password';
						exit;
					}
									
					
					
					// 'find' si se busca un solo registro, 'all' si se busca solo 1
					$row = contenidos_user::find(array(
											'conditions' => array(
															'email = ? AND password = ?', 
															// $this->_xss->xss_clean(validador::getAlphaNum('usuario')), 
															$this->_xss->xss_clean($this->_view->datos['email']),
															Hash::getHash('md5', $this->_xss->xss_clean(validador::getPostParam('pass')), $this->_conf['hash_key'])
															// $this->_xss->xss_clean(validador::getPostParam('pass'))
															)
												)
										);
					
					
					if(!$row){
						/*$this->_view->_error = 'Usuario y/o password incorrectos';
						$this->_view->renderizar('index','login', 'login');
						exit;*/
						echo 'Usuario y/o password incorrectos';
						exit;
					}
					
										
					$this->_sess->set('autenticado_front', true);
					$this->_sess->set('usuario_front', $row->nombre);
					$this->_sess->set('id_usuario_front', $row->id);

					echo 'ok';
					exit;
					// $this->redireccionar('home');
				}

			}else{
				//$this->redireccionar('error/access/404');
				// $this->_view->_error = 'Hubo un error, vuelva a intentarlo mas tarde.';
				echo 'Hubo un error, vuelva a intentarlo mas tarde.';
				exit;
			}
		}

    }
	

	public function salir()
    {
        $this->_sess->destroy('autenticado_front');
        $this->_sess->destroy('id_usuario_front');
        $this->_sess->destroy('usuario_front');
        $this->_sess->destroy('carga_user');
        $this->_sess->destroy('img_id_user');
        $this->_sess->destroy('img_error_user');
        $this->redireccionar();
    }
}
	
	

?>