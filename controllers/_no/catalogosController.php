<?php

use Nucleo\Controller\Controller;

class catalogosController extends Controller
{
	public $homeGestion;
	//public $_cache;
	
    public function __construct()
	{
        parent::__construct();
		
		$this->getLibrary('class.home');		
		$this->homeGestion = new home();
		
		$this->getLibrary('class.validador');
		
				
    }
    
   		
	
	public function index()
	{			
	
		/*$this->_view->titulo = 'Aca Salud';	
		$this->_view->renderizar('index', 'catalogos', 'default');*/
		
    }
	
	
	public function descargarCatalogo()
	{

		if(!isset($_POST['token'])){
			//return false;
			$jsondata['envio'] = false;
			echo json_encode($jsondata);
    		exit;			
		}else{
			//echo "<pre>";print_r($_POST);exit;
			$_cat = $_POST['token'];
			
			//traer id totem con el token
			$_totem = $this->homeGestion->traerTotemPorToken($_cat)->id;
			
			//ejecutar cron para actualizar estado asignaciones
			$_cron = $this->ejecutarCron($_totem);
			
			//echo "<pre>";print_r($_data);
			//echo $_totem;exit;
			
			//array data asignaciones alta
			$_asig = $this->homeGestion->traerAsigAlta($_totem);

			if(!empty($_asig)){

				//echo "<pre>";print_r($_asig);echo "</pre>";exit;
				
				//comparar data en totem con data en servidor, si hay dif descargar la diferencia
				if(isset($_POST['json']) && $_POST['json']!='nada'){
					
					$_json = $_POST['json'];
					$_dataJson = json_decode($_json,true);

										
					//traer todos los contenidos
					//convertir array de datos server a formato optimo para comparacion
					foreach ($_asig as $dat) {
						$_cata[]=$dat['ids_catalogos'];						
					}
					$_cata = implode(',', $_cata);
					$_cata = explode(',',$_cata);
					$_cata = array_unique($_cata);
					$_cata = array_filter($_cata);
					$_cata = implode(',', $_cata);
					$_idss['catalogos']= $_cata;
					
					//videos
					foreach ($_asig as $dat) {
						$_vid[]=$dat['ids_videos'];						
					}
					$_vid = implode(',', $_vid);
					$_vid = explode(',',$_vid);
					$_vid = array_unique($_vid);
					$_vid = array_filter($_vid);
					$_vid = implode(',', $_vid);
					$_idss['videos']= $_vid;
					
					//footers
					foreach ($_asig as $dat) {
						$_fot[]=$dat['ids_footers'];						
					}
					$_fot = implode(',', $_fot);
					$_fot = explode(',',$_fot);
					$_fot = array_unique($_fot);
					$_fot = array_filter($_fot);
					$_fot = implode(',', $_fot);
					$_idss['footers']= $_fot;
					
					//fondos
					foreach ($_asig as $dat) {
						$_fon[]=$dat['ids_fondos'];						
					}
					$_fon = implode(',', $_fon);
					$_fon = explode(',',$_fon);
					$_fon = array_unique($_fon);
					$_fon = array_filter($_fon);
					$_fon = implode(',', $_fon);
					$_idss['fondos']= $_fon;

					//pantalla inactiva
					foreach ($_asig as $dat) {
						$_inac[]=$dat['ids_inactivas'];						
					}
					$_inac = implode(',', $_inac);
					$_inac = explode(',',$_inac);
					$_inac = array_unique($_inac);
					$_inac = array_filter($_inac);
					$_inac = implode(',', $_inac);
					$_idss['inactivas']= $_inac;

					//echo "<pre>";print_r($_idss);echo "</pre>";exit;

					foreach ($_idss as $k => $v) {
						$_ids_final[$k]= explode(',', $v);						
					}

					foreach ($_dataJson['ids'] as $k => $v) {
						$_json_final[$k]= explode(',', $v);						
					}
					
					

					//chequear si hay dif de array osea contenido a descargar
					foreach ($_ids_final as $k => $v) {						
						foreach($v as $x){
							if($x==''){
								$b1[$k][0] = $x;
							}else{
								$b1[$k][$x] = $x;
							}
						    
						}
					}

					foreach ($_json_final as $k => $v) {						
						foreach($v as $x){
						    $b2[$k][$x] = $x;
						}
					}
					
					/*echo "<pre>";print_r($_ids_final);echo "</pre>";//exit;
					echo "<pre>";print_r($_dataJson);echo "</pre>";exit;*/

					//$res = home::diferenciaArrayMultidimensional($_ids_final, $_json_final);
					$res = home::diferenciaArrayMultidimensional($b1, $b2);

					$_asig_modif = $this->homeGestion->traerAsigModif($_totem);

					if($_asig_modif){
						foreach ($_asig_modif as $dat) {
							$_modiff[] = $dat['update_contenido'];						
						}
						$_modif = implode(',',$_modiff);
						$_modif = explode(',', $_modif);
						$_modif = array_unique($_modif);	
						

						foreach ($_modif as $datos) {
							$d = explode('_', $datos);
							switch ($d[0]) {
							    case 'c':
							        $_ids_final_modif['catalogos'][$d[1]]= $d[1];
							        break;
							    case 'v':
							        $_ids_final_modif['videos'][$d[1]]= $d[1];
							        break;							 
							    case 'foo':
							        $_ids_final_modif['footers'][$d[1]]= $d[1];
							        break;
							    case 'fo':
							        $_ids_final_modif['fondos'][$d[1]]= $d[1];
							        break;
							    case 'i':
							        $_ids_final_modif['inactivas'][$d[1]]= $d[1];
							        break;						   
							
							}					
						}

						foreach ($_ids_final_modif as $k => $v) {						
							foreach($v as $x){
								if($x==''){
									$b3[$k][0] = $x;
								}else{
									$b3[$k][$x] = $x;
								}
							    
							}
						}

												
						if(!empty($res)){
							/*$res_modif = home::diferenciaArrayMultidimensional($b3, $res);
							if(!empty($res_modif)){
								$res[] = $res_modif;
							}*/

							$resultado = array_merge_recursive($res, $b3);
							foreach ($resultado as $k => $v) {						
								foreach($v as $x){
									if($x==''){
										$b4[$k][0] = $x;
									}else{
										$b4[$k][$x] = $x;
									}								    
								}
							}

							$res = $b4;

						}else{
							$res = $b3;
						}

					}

					//borrar flag de base
					$_edit = contenidos_asignacione::find('all',array('conditions' => array('id_totem = ? AND estado = ? AND update_contenido != ?', $_totem, 'alta', '')));
					if($_edit){
						foreach ($_edit as $ed) {
							$ed->update_contenido = '';
							$ed->save();
						}
						

					}

					// echo "<pre>";print_r($b1);echo "</pre>";//exit;
					// echo "<pre>";print_r($b2);echo "</pre>";//exit;					
					
					// echo "<pre>";print_r($b3);echo "</pre>";//exit;
					// echo "<pre>";print_r($res);echo "</pre>";exit;
					//echo "<pre>";print_r($b4);echo "</pre>";exit;

					if(empty($res)){

						$res_inverso = home::diferenciaArrayMultidimensional($b2, $b1);
						
						if(empty($res_inverso)){
						
							$jsondata['envio'] = 'no update';
							echo json_encode($jsondata);
			    			exit;
						
						}else{

							foreach ($_idss as $k => $v) {
								if($k=='catalogos'){
									if($v){
										$_ids_json['catalogos']= $v;
										$_catalogos = $this->homeGestion->traerCatalogos($v);
										if($_catalogos){
											$_data_json['catalogos'] = $_catalogos;
										}
									}
								}

								if($k=='videos'){
									if($v){
										$_ids_json['videos']= $v;
										$_videos = $this->homeGestion->traerVideos($v);
										if($_videos){
											$_data_json['videos'] = $_videos;
										}
									}
								}

								if($k=='footers'){
									if($v){
										$_ids_json['footers']= $v;
										$_footers = $this->homeGestion->traerFooters($v);
										if($_footers){
											$_data_json['footers'] = $_footers;
										}
									}
								}

								if($k=='fondos'){
									if($v){
										$_ids_json['fondos']= $v;
										$_fondos = $this->homeGestion->traerFondos($v);
										if($_fondos){
											$_data_json['fondos'] = $_fondos;
										}
									}
								}

								if($k=='inactivas'){
									if($v){
										$_ids_json['inactivas']= $v;
										$_inactivas = $this->homeGestion->traerInactivas($v);
										if($_inactivas){
											$_data_json['inactivas'] = $_inactivas;
										}
									}
								}
							}

							$_data_json_final['ids'] = $_ids_json;
							$_data_json_final['contenidos'] = $_data_json;

							$_archivo = $this->copiarArchivosJson($_cat, $_data_json_final);
							if($_archivo==true){

								//echo "<pre>";print_r($_ids);echo "</pre>";//exit;
								//echo "<pre>";print_r($_data);echo "</pre>";exit;
								
								//comprimir archivos
								$_ruta_descarga = $this->_conf['ruta_archivos'].'descarga_'.$_cat. DS;
								if(file_exists($_ruta_descarga)){
									$this->homeGestion->borrarDir($_ruta_descarga);
								}
								$_ar = $this->generarZip($_cat, 'data');
								if($_ar==false){
									//return false;
								    $jsondata['envio'] = false;
									echo json_encode($jsondata);
		    						exit;
								}else{
									//return 'descarga_'.$_cat;
									$jsondata['envio'] = 'descarga_'.$_cat;
									echo json_encode($jsondata);
									exit;
								}

							
							}else{
								$jsondata['envio'] = false;
								echo json_encode($jsondata);
				    			exit;
							}



						}
						

					}else{


						//update asignaciones con fecha de sincronizacion						
						/*$_key= key($res);
						$_value = reset($res[$_key]);
						$_update = $this->homeGestion->selectSincroAsig($_totem, $_key, $_value);*/						
						$_cambio = array();
						foreach ($res as $k => $v) {
							foreach ($res[$k] as $w) {
								if($w!=''){
									$_update = $this->homeGestion->selectSincroAsig($_totem, $k, $w);
									$_cambio[] = 'si';
								}else{
									$_cambio[] = 'no';
								}
							}
						}



						$_dev = array_search('si',$_cambio);

						/*echo $_dev;
						echo "<pre>";print_r($res);echo "</pre>";//exit;
						echo "<pre>";print_r($_cambio);echo "</pre>";exit;*/

						if($_dev===false){
							
							/*echo $_dev;
							echo "<pre>";print_r($_idss);echo "</pre>";exit;*/


							foreach ($_idss as $k => $v) {
								if($k=='catalogos'){
									if($v){
										$_ids_json['catalogos']= $v;
										$_catalogos = $this->homeGestion->traerCatalogos($v);
										if($_catalogos){
											$_data_json['catalogos'] = $_catalogos;
										}
									}
								}

								if($k=='videos'){
									if($v){
										$_ids_json['videos']= $v;
										$_videos = $this->homeGestion->traerVideos($v);
										if($_videos){
											$_data_json['videos'] = $_videos;
										}
									}
								}

								if($k=='footers'){
									if($v){
										$_ids_json['footers']= $v;
										$_footers = $this->homeGestion->traerFooters($v);
										if($_footers){
											$_data_json['footers'] = $_footers;
										}
									}
								}

								if($k=='fondos'){
									if($v){
										$_ids_json['fondos']= $v;
										$_fondos = $this->homeGestion->traerFondos($v);
										if($_fondos){
											$_data_json['fondos'] = $_fondos;
										}
									}
								}

								if($k=='inactivas'){
									if($v){
										$_ids_json['inactivas']= $v;
										$_inactivas = $this->homeGestion->traerInactivas($v);
										if($_inactivas){
											$_data_json['inactivas'] = $_inactivas;
										}
									}
								}
							}

							$_data_json_final['ids'] = $_ids_json;
							$_data_json_final['contenidos'] = $_data_json;

							$_archivo = $this->copiarArchivosJson($_cat, $_data_json_final);
							if($_archivo==true){

								//echo "<pre>";print_r($_ids);echo "</pre>";//exit;
								//echo "<pre>";print_r($_data);echo "</pre>";exit;
								
								//comprimir archivos
								$_ruta_descarga = $this->_conf['ruta_archivos'].'descarga_'.$_cat. DS;
								if(file_exists($_ruta_descarga)){
									$this->homeGestion->borrarDir($_ruta_descarga);
								}
								$_ar = $this->generarZip($_cat, 'data');
								if($_ar==false){
									//return false;
								    $jsondata['envio'] = false;
									echo json_encode($jsondata);
		    						exit;
								}else{
									//return 'descarga_'.$_cat;
									$jsondata['envio'] = 'descarga_'.$_cat;
									echo json_encode($jsondata);
									exit;
								}

							
							}else{
								//return false;
								$jsondata['envio'] = false;
								echo json_encode($jsondata);
				    			exit;
							}



							/*$jsondata['envio'] = 'no update';
							echo json_encode($jsondata);
			    			exit;*/
						}
						//echo "<pre>";print_r($_update);echo "</pre>";//exit;
						//echo "<pre>";print_r($res);echo "</pre>";exit;

						
						//armar array de contenidos a descargar						
						foreach ($res as $k => $v) {
							if($k=='catalogos'){
								$_cata = implode(',', $v);
								if($_cata){
									// echo "alog: ".$_cata;
									// exit;
									$_ids['catalogos']= $_cata;
									$_catalogos = $this->homeGestion->traerCatalogos($_cata);
									if($_catalogos){
										$_data['catalogos'] = $_catalogos;
									}
								}
									
							}

							if($k=='videos'){
								$_cata = implode(',', $v);
								if($_cata){
								$_ids['videos']= $_cata;
									$_videos = $this->homeGestion->traerVideos($_cata);
									if($_videos){
										$_data['videos'] = $_videos;
									}	
								}
							}

							if($k=='footers'){
								$_cata = implode(',', $v);
								if($_cata!=''){
									$_ids['footers']= $_cata;
									$_footers = $this->homeGestion->traerFooters($_cata);
									if($_footers){
										$_data['footers'] = $_footers;
									}
								}
							}

							if($k=='fondos'){
								$_cata = implode(',', $v);
								if($_cata){
									$_ids['fondos']= $_cata;
									$_fondos = $this->homeGestion->traerFondos($_cata);
									if($_fondos){
										$_data['fondos'] = $_fondos;
									}
								}
							}

							if($k=='inactivas'){
								$_cata = implode(',', $v);
								if($_cata){
									$_ids['inactivas']= $_cata;
									$_inactivas = $this->homeGestion->traerInactivas($_cata);
									if($_inactivas){
										$_data['inactivas'] = $_inactivas;
									}
								}
							}
						}

						/*echo "<pre>";print_r($_ids);echo "</pre>";//exit;
						echo "<pre>";print_r($_data);echo "</pre>";exit;*/

						//echo "<pre>";print_r($_ids_final);echo "</pre>";exit;

						//ids y datos finales para data.json
						foreach ($_ids_final as $k => $v) {
							if($k=='catalogos'){
								$_cata = implode(',', $v);
								if($_cata){
									$_ids_json['catalogos']= $_cata;
									$_catalogos = $this->homeGestion->traerCatalogos($_cata);
									if($_catalogos){
										$_data_json['catalogos'] = $_catalogos;
									}
								}
							}

							if($k=='videos'){
								$_cata = implode(',', $v);
								if($_cata){
									$_ids_json['videos']= $_cata;
									$_videos = $this->homeGestion->traerVideos($_cata);
									if($_videos){
										$_data_json['videos'] = $_videos;
									}
								}
							}

							if($k=='footers'){
								$_cata = implode(',', $v);
								if($_cata){
									$_ids_json['footers']= $_cata;
									$_footers = $this->homeGestion->traerFooters($_cata);
									if($_footers){
										$_data_json['footers'] = $_footers;
									}
								}
							}

							if($k=='fondos'){
								$_cata = implode(',', $v);
								if($_cata){
									$_ids_json['fondos']= $_cata;
									$_fondos = $this->homeGestion->traerFondos($_cata);
									if($_fondos){
										$_data_json['fondos'] = $_fondos;
									}
								}
							}

							if($k=='inactivas'){
								$_cata = implode(',', $v);
								if($_cata){
									$_ids_json['inactivas']= $_cata;
									$_inactivas = $this->homeGestion->traerInactivas($_cata);
									if($_inactivas){
										$_data_json['inactivas'] = $_inactivas;
									}
								}
							}
						}

						$_data_json_final['ids'] = $_ids_json;
						$_data_json_final['contenidos'] = $_data_json;


						/*echo "<pre>";print_r($_json_final);echo "</pre>";//exit;
						echo "<pre>";print_r($_ids_json);echo "</pre>";//exit;
						echo "<pre>";print_r($_dataJson);echo "</pre>";exit;*/


						
						//copiar archivos para descargar
						$_archivo = $this->copiarArchivos($_data, $_ids, $_cat, $_data_json_final);
						if($_archivo==true){

							//echo "<pre>";print_r($_ids);echo "</pre>";//exit;
							//echo "<pre>";print_r($_data);echo "</pre>";exit;
							
							//comprimir archivos
							$_ruta_descarga = $this->_conf['ruta_archivos'].'descarga_'.$_cat. DS;
							if(file_exists($_ruta_descarga)){
								$this->homeGestion->borrarDir($_ruta_descarga);
							}
							foreach ($_data as $key => $value) {
								if ($value === end($_data)) {
									$_ar = $this->generarZip($_cat, $key);
									$_comp[]= $_ar;
							        $_ar = $this->generarZip($_cat, 'data');
									$_comp[]= $_ar;
							    }else{
							    	$_ar = $this->generarZip($_cat, $key);
									$_comp[]= $_ar;
							    }								
								
							}
							
							if (in_array(false, $_comp)) {
							    //return false;
							    $jsondata['envio'] = false;
								echo json_encode($jsondata);
	    						exit;
							}else{
								//return 'descarga_'.$_cat;
								$jsondata['envio'] = 'descarga_'.$_cat;
								echo json_encode($jsondata);
								exit;
							}

							
						
						}else{
							//return false;
							$jsondata['envio'] = false;
							echo json_encode($jsondata);
			    			exit;
						}

					}

				


				
				}else{
					


					//traer todos los contenidos

					//update asignaciones con fecha de sincronizacion
					foreach ($_asig as $up) {
						
						$_up = contenidos_asignacione::find(array('conditions' => array('id = ?', $up['id'])));
						if($_up){
							$_fechaBd = date('Y-m-d H:i:s');
							$_up->fecha_sincronizacion = "$_fechaBd";
							$_up->save();
						}			
							
					}


					//arma array de contenidos a descargar
					
					//catalogos
					foreach ($_asig as $dat) {
						$_cata[]=$dat['ids_catalogos'];						
					}
					$_cata = implode(',', $_cata);
					$_cata = explode(',',$_cata);
					$_cata = array_unique($_cata);
					$_cata = array_filter($_cata);
					$_cata = implode(',', $_cata);
					$_ids['catalogos']= $_cata;
					if($_cata){
						$_catalogos = $this->homeGestion->traerCatalogos($_cata);
						if($_catalogos){
							$_data['catalogos'] = $_catalogos;
						}
					}

					
					//videos
					foreach ($_asig as $dat) {
						$_vid[]=$dat['ids_videos'];						
					}
					$_vid = implode(',', $_vid);
					$_vid = explode(',',$_vid);
					$_vid = array_unique($_vid);
					$_vid = array_filter($_vid);
					$_vid = implode(',', $_vid);
					$_ids['videos']= $_vid;
					if($_vid){
						$_videos = $this->homeGestion->traerVideos($_vid);
						if($_videos){
							$_data['videos'] = $_videos;
						}
					}
					
					//footers
					foreach ($_asig as $dat) {
						$_fot[]=$dat['ids_footers'];						
					}
					$_fot = implode(',', $_fot);
					$_fot = explode(',',$_fot);
					$_fot = array_unique($_fot);
					$_fot = array_filter($_fot);
					$_fot = implode(',', $_fot);
					$_ids['footers']= $_fot;
					if($_fot){
						$_footers = $this->homeGestion->traerFooters($_fot);
						if($_footers){
							$_data['footers'] = $_footers;
						}
					}
					
					//fondos
					foreach ($_asig as $dat) {
						$_fon[]=$dat['ids_fondos'];						
					}
					$_fon = implode(',', $_fon);
					$_fon = explode(',',$_fon);
					$_fon = array_unique($_fon);
					$_fon = array_filter($_fon);
					$_fon = implode(',', $_fon);
					$_ids['fondos']= $_fon;
					if($_fon){
						$_fondos = $this->homeGestion->traerFondos($_fon);
						if($_fondos){
							$_data['fondos'] = $_fondos;
						}
					}

					//pantalla inactiva
					foreach ($_asig as $dat) {
						$_inac[]=$dat['ids_inactivas'];						
					}
					$_inac = implode(',', $_inac);
					$_inac = explode(',',$_inac);
					$_inac = array_unique($_inac);
					$_inac = array_filter($_inac);
					$_inac = implode(',', $_inac);
					$_ids['inactivas']= $_inac;
					if($_inac){
						$_inactivas = $this->homeGestion->traerInactivas($_inac);
						if($_inactivas){
							$_data['inactivas'] = $_inactivas;
						}
					}

					
					$_data_json_final['ids'] = $_ids;
					$_data_json_final['contenidos'] = $_data;
					
					//echo "<pre>";print_r($_ids);echo "</pre>";//exit;
					//echo "<pre>";print_r($_data);echo "</pre>";exit;

					//copiar archivos para descargar
					$_archivo = $this->copiarArchivos($_data, $_ids, $_cat, $_data_json_final);
					if($_archivo==true){

						//comprimir archivos
						$_ruta_descarga = $this->_conf['ruta_archivos'].'descarga_'.$_cat. DS;
						if(file_exists($_ruta_descarga)){
							$this->homeGestion->borrarDir($_ruta_descarga);
						}
						foreach ($_data as $key => $value) {
							if ($value === end($_data)) {
								$_ar = $this->generarZip($_cat, $key);
								$_comp[]= $_ar;
						        $_ar = $this->generarZip($_cat, 'data');
								$_comp[]= $_ar;
						    }else{
						    	$_ar = $this->generarZip($_cat, $key);
								$_comp[]= $_ar;
						    }
														
						}
						
						if (in_array(false, $_comp)) {
						    //return false;
						    $jsondata['envio'] = false;
							echo json_encode($jsondata);
    						exit;
						}else{
							//return 'descarga_'.$_cat;
							$jsondata['envio'] = 'descarga_'.$_cat;
							echo json_encode($jsondata);
							exit;
						}

						
					
					}else{
						//return false;
						$jsondata['envio'] = false;
						echo json_encode($jsondata);
		    			exit;
					}
					
				}
			}else{
				//return false;
				$jsondata['envio'] = 'no asignaciones';
				echo json_encode($jsondata);
    			exit;
			}
			
			//echo $_cata;exit;
			//echo "<pre>";print_r($_ids);echo "</pre>";//exit;
			//echo "<pre>";print_r($_data);echo "</pre>";exit;		

			
			/*$this->_view->titulo = 'catalogos';
			$this->_view->renderizar('index', 'catalogos', 'default');*/
			
			
		}
	}
	
	public function copiarArchivos($_data='', $_ids='', $_cat='', $_json_final='')
	{
		$_directorio = $this->_conf['ruta_archivos_descargas']. 'descarga_'.$_cat;
				
		if(file_exists($_directorio)){
			$this->homeGestion->borrarDir($_directorio);
		}
		
		if(!file_exists($_directorio)){
			mkdir($_directorio, 0777, true);
			
			if(file_exists($_directorio)){				

				mkdir($_directorio.'/data', 0777, true);
				if (file_exists($_directorio.'/data')){
					
					// crear json final				
					$_data_final_json = json_encode($_json_final);
					$fp = fopen($_directorio."/data/data.json","w+"); 
					if($fp == false) { 
					   	die("No se ha podido crear el archivo."); 
					   	fclose($fp);
					}else{
						fwrite($fp, $_data_final_json);
						fclose($fp);
					}
				}				

				
				

				//copiar footers
				if (array_key_exists("footers",$_data)){
					mkdir($_directorio.'/footers', 0777, true);
					if (file_exists($_directorio.'/footers')){
					   foreach($_data['footers'] as $prom){
							copy($this->_conf['ruta_img_cargadas'].'promociones/'.$prom['path'], $_directorio.'/footers/'.$prom['path']);
						}
					   
					}	
				}		
				

				//copiar videos
				if (array_key_exists("videos",$_data)){
					mkdir($_directorio.'/videos', 0777, true);
					if (file_exists($_directorio.'/videos')){
					   foreach($_data['videos'] as $vid){
							//copy($this->_conf['ruta_videos'].$vid['path'], $_directorio.'/videos/'.$vid['path']);
					   		$this->homeGestion->copiarDirectorios2($this->_conf['ruta_videos'].'vid_'.$vid['identificador'], $_directorio.'/videos/vid_'.$vid['identificador'],true);
						}
					   
					}
				}

				//copiar fondos
				if (array_key_exists("fondos",$_data)){
					mkdir($_directorio.'/fondos', 0777, true);
					if (file_exists($_directorio.'/fondos')){
					   foreach($_data['fondos'] as $fondos){
							copy($this->_conf['ruta_img_cargadas'].'fondos/'.$fondos['path'], $_directorio.'/fondos/'.$fondos['path']);
						}
					   
					}
				}
						
				//copiar catalogos	
				if (array_key_exists("catalogos",$_data)){				
					mkdir($_directorio.'/catalogos', 0777, true);
					if (file_exists($_directorio.'/catalogos')){
					   	foreach($_data['catalogos'] as $cat){
							$this->homeGestion->copiarDirectorios2($this->_conf['ruta_img_cargadas'].'catalogos/cat_'.$cat['identificador'], $_directorio.'/catalogos/cat_'.$cat['identificador'],true);
						}
						 //rename($_directorio.'/catalogos/cat_'.$cat['identificador'].'/thu', 'path/to/file/newname.ext');
						
						//recorrer directorio y renombrar img segun orden
					   	foreach($_data['catalogos'] as $cat){

					   		$directory = $_directorio.'/catalogos/cat_'.$cat['identificador'];
							$dirint = dir($directory);
							while (($archivo = $dirint->read()) !== false){
								if (preg_match( '/\.(?:jpg|png|gif)$/i', $archivo)){
									$_nuevo_nombre = home::cambiarNombreImg($cat['identificador'], $archivo);
									$_ext = explode('.', $archivo);
									rename($directory.'/'.$archivo, $directory.'/cat_'.$_nuevo_nombre->orden.'.'.$_ext[1]);
								}
							}
							$dirint->close();

					   	}

					}
				}

				//copiar pantalla inactiva
				if (array_key_exists("inactivas",$_data)){
					mkdir($_directorio.'/inactivas', 0777, true);
					if (file_exists($_directorio.'/inactivas')){
					   foreach($_data['inactivas'] as $inac){
							//copy($this->_conf['ruta_videos'].$vid['path'], $_directorio.'/videos/'.$vid['path']);
					   		$this->homeGestion->copiarDirectorios2($this->_conf['ruta_videos'].'inac_'.$inac['identificador'], $_directorio.'/inactivas/inac_'.$inac['identificador'],true);
						}
					   
					}
				}


			}
			
			
			//echo "ok";
			
			return true;
			
		}
	}

	public function copiarArchivosJson($_cat='', $_json_final='')
	{
		$_directorio = $this->_conf['ruta_archivos_descargas']. 'descarga_'.$_cat;
				
		if(file_exists($_directorio)){
			$this->homeGestion->borrarDir($_directorio);
		}
		
		if(!file_exists($_directorio)){
			mkdir($_directorio, 0777, true);
			
			if(file_exists($_directorio)){				

				mkdir($_directorio.'/data', 0777, true);
				if (file_exists($_directorio.'/data')){
					
					// crear json final				
					$_data_final_json = json_encode($_json_final);
					$fp = fopen($_directorio."/data/data.json","w+"); 
					if($fp == false) { 
					   	die("No se ha podido crear el archivo."); 
					   	fclose($fp);
					}else{
						fwrite($fp, $_data_final_json);
						fclose($fp);
					}
				}				
	
			}
			
			
			//echo "ok";
			
			return true;
			
		}
	}
	
	public function procesarCache(){
		cache::procesarCache(RAIZ.'nucleo/');
	}
	
	public function generarZip($_cat, $_folder)
	{
		
		$zip = new ZipArchive();
		// directorio a comprimir
		// la barra inclinada al final es importante
		// la ruta debe ser relativa no absoluta			
		$dir = 'public' . DS . 'descargas' . DS .'descarga_'.$_cat. DS . $_folder . DS;
		//ruta donde guardar los archivos zip, ya debe existir
		$rutaFinal = $this->_conf['ruta_archivos'].'descarga_'.$_cat. DS;

		/*if(file_exists($rutaFinal)){
			$this->homeGestion->borrarDir($rutaFinal);
		}*/

		if(!file_exists($rutaFinal)){
		  mkdir($rutaFinal, 0777, true);
		}
		//$archivoZip = 'descarga_'.$_cat.'.zip';
		$archivoZip = $_folder.'.zip';
		if($zip->open($archivoZip, ZIPARCHIVE::CREATE) === true) {
			$this->agregarZip($dir, $zip);
			$zip->close();
			//Muevo el archivo a una ruta
			//donde no se mezcle los zip con los demas archivos
			rename($archivoZip, $rutaFinal.$archivoZip);
			//Hasta aqui el archivo zip ya esta creado
			//Verifico si el archivo ha sido creado
			if(file_exists($rutaFinal . $archivoZip)) {
				//echo "Proceso Finalizado!!";
				//return $archivoZip;
				return true;
			}else{
				//echo "Error, archivo zip no ha sido creado!!";
				return false;
			}
		}else{
			return false;
		}
	}
	
	public function agregarZip($dir, $zip) {
	  
		//verificamos si $dir es un directorio
		if (is_dir($dir)) {
			//abrimos el directorio y lo asignamos a $da
			if ($da = opendir($dir)) {
			  //leemos del directorio hasta que termine
			  while (($archivo = readdir($da)) !== false) {
				/*Si es un directorio imprimimos la ruta
				 * y llamamos recursivamente esta función
				 * para que verifique dentro del nuevo directorio
				 * por mas directorios o archivos
				 */
				if (is_dir($dir . $archivo) && $archivo != "." && $archivo != "..") {
				 // echo "<strong>Creando directorio: $dir$archivo</strong><br/>";
				  $this->agregarZip($dir . $archivo . "/", $zip);
				  /*si encuentra un archivo imprimimos la ruta donde se encuentra
				   * y agregamos el archivo al zip junto con su ruta 
				   */
				} else if(is_file($dir . $archivo) && $archivo != "." && $archivo != "..") {
				 // echo "Agregando archivo: $dir$archivo <br/>";
				  $zip->addFile($dir . $archivo, $dir . $archivo);
				}
			  }
			  //cerramos el directorio abierto en el momento
			  closedir($da);
			}
		}
	}
	
	public function ejecutarCron($_id=false)
	{			
	
		$asignaciones = $this->homeGestion->traerAsignaciones($_id);
		$_fechahoy = date('Y-m-d H:i:s');
		$_dia = jddayofweek (cal_to_jd(CAL_GREGORIAN, date("m"),date("d"), date("Y")) , 0);
		$_array_dias = array('Domingo', 'Lunes','Martes', 'Miércoles', 'Jueves', 'Viernes', 'Sábado');
		$_dia_semana = $_array_dias[$_dia];

		foreach($asignaciones as $asig){
			if($_fechahoy >= $asig->vigencia_desde->format('Y-m-d H:i:s') && $_fechahoy <= $asig->vigencia_hasta->format('Y-m-d H:i:s')){
				
				if($asig->dias_semana!=''){
					$_dias = explode(',', $asig->dias_semana);
					if(in_array($_dia_semana, $_dias)){
						$this->homeGestion->updateEstadoAsig($asig->id,'alta');
					}else{
						$this->homeGestion->updateEstadoAsig($asig->id,'baja');
					}
				}else{
					if($asig->estado!='alta'){
						$this->homeGestion->updateEstadoAsig($asig->id,'alta');
					}
				}
				

				
				
			}else{
				//echo "fuera de vigencia";
				if($asig->estado!='baja'){
					$this->homeGestion->updateEstadoAsig($asig->id,'baja');
				}
				
			}
		}
		
    }	
	
	
}
?>