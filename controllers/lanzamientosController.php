<?php

use Nucleo\Controller\Controller;

class lanzamientosController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('class.home');		
		$this->homeGestion = new home();

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();

        $this->cantidadRegistros = 6;	
        $this->filtros =  array(1,2,3,4);		
				
    }
    
   		
	
	public function _index()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        // $this->_sess->destroy('_cat_lanz');

        // $this->_view->datos = $this->homeGestion->traerLanzamientos($this->cantidadRegistros);
        // $this->_view->cantReg = home::contarRegistrosLanz() / $this->cantidadRegistros;
         // $this->_sess->set('_cat_lanz',$this->filtros);
         $_SESSION['_cat_lanz'] = $this->filtros;

        $this->_view->datos = $this->homeGestion->traerLanzamientos();
        // $this->_view->datos = $this->homeGestion->traerLanzamientos($this->cantidadRegistros, $_SESSION['_cat_lanz']);
        $this->_view->cantReg = home::contarRegistrosLanz($_SESSION['_cat_lanz']) / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg);    
        $this->_view->id_nota = '';
		$this->_view->seccion = 'lanzamientos';

        $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social']; 
            	
		 // echo "<pre>";print_r($this->_view->datos);exit;

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index','lanzamientos', 'default');
    }

    public function index()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        // $this->_sess->destroy('_cat_lanz');

        // $this->_view->datos = $this->homeGestion->traerLanzamientos($this->cantidadRegistros);
        // $this->_view->cantReg = home::contarRegistrosLanz() / $this->cantidadRegistros;
         // $this->_sess->set('_cat_lanz',$this->filtros);
         $_SESSION['_cat_lanz'] = $this->filtros;

        $this->_view->datos = $this->homeGestion->traerLanzamientos();
        // $this->_view->datos = $this->homeGestion->traerLanzamientos($this->cantidadRegistros, $_SESSION['_cat_lanz']);
        $this->_view->cantReg = home::contarRegistrosLanz($_SESSION['_cat_lanz']) / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg);    
        $this->_view->id_nota = '';
        $this->_view->seccion = 'lanzamientos';

        // $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        // $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social']; 

        $this->_view->banners_top_dos = $this->homeGestion->traerBannerPorSeccionDos(2, 1);
        if($this->_view->banners_top_dos){
            for ($i=0; $i < count($this->_view->banners_top_dos); $i++) {         
                $this->_view->banners_top_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_top_dos[$i]['link']));
            }
        } 
                
         // echo "<pre>";print_r($this->_view->datos);exit;

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index2','lanzamientos', 'default');
    }


    public function paginacion()
    {

        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 

                $_pag = $_POST['p'];
                // $_data = $this->homeGestion->paginadorLanz($_pag, $this->cantidadRegistros); 
                $_data = $this->homeGestion->paginadorLanz($_pag, $this->cantidadRegistros, $_SESSION['_cat_lanz']);      
                $proy='';
                
                foreach($_data as $datos){
                            
                    /*$proy .= '<div id="item_'.$datos['id'].'" class="forum-item">
                                <div class="row">
                                    <div class="col-md-4">';
                                        $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'lanzamientos');
                                        if($_img !=''){
                                        $proy .= '<img src="'.$this->_conf['base_url'] . 'public/img/subidas/lanzamientos/'. $_img->path .'" width="300">';
                                        }                                        
                                        $_cat = explode(',', $datos['categorias']);
                                        $_arr_cat = array();
                                        foreach ($_cat as $val) {
                                          $_arr = home::traerCategoria($val);
                                          $_arr_cat[] = $_arr['nombre']; 
                                        }
                                        $_cate = implode(', ', $_arr_cat);                                        
                                        $proy .= '<br><small><b>'.$_cate.'</b></small>
                                        <br>
                                        <small>Codigo:'.$datos['codigo'].'</small>
                                        <h4><strong>'.admin::convertirCaracteres($datos['titulo']).'</strong></h4>                              
                                        '.admin::convertirCaracteres($datos['descripcion']).'
                                    </div>
                                </div>
                            </div>';*/
                            

                        $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'lanzamientos');
                        if($_img !=''){                            
                            $_url_img = $this->_conf['base_url'] . 'public/img/subidas/lanzamientos/'. $_img->path;
                        }

                        if($datos['link']==''){
                            preg_match('/src="([^"]+)"/', $datos['video'], $match);
                            $_url_vid = $match[1];
                        }

                        $_cat=array();
                        $_cat = explode(',', $datos['categorias']);       
                        $_arr_cat=array();
                        $_arr_label=array();
                        foreach ($_cat as $val) {
                          $_arr = home::traerCategoria($val);
                          $_arr_cat[] = $_arr['nombre']; 
                          $_arr_label[] = $_arr['clase']; 
                        }
                        $_cate = implode(', ', $_arr_cat);
                       

                        $proy .= ' <article class="grid-item '.$_arr_label[0].'">
                                <div class="img" style="background-image: url('.$_url_img.')"></div>
                                <div class="copy">
                                    <div class="close"><i class="fa fa-times" aria-hidden="true"></i></div>
                                    <h4>'.$_cate.'</h4>
                                    <h3>'.home::convertirCaracteres($datos['titulo']).'</h3>
                                    <div class="content">
                                        <p>'.home::limitarTexto(home::convertirCaracteres(strip_tags($datos['descripcion'])),150).'</p>';
                                        if($datos['link']==''){
                                            $proy .= ' <a href="javascript:void(0);" rel="'.$_url_vid.'" class="ver-video">Ver Video</a>';
                                        } else{
                                            $proy .= '<a href="'.$datos['link'].'" target="_blank">Ir al Site</a>';
                                        }
                        $proy .= '</div>
                                    <div class="preview">
                                        <p>'.home::limitarTexto(home::convertirCaracteres(strip_tags($datos['bajada'])),150).'</p>
                                        <a href="#" class="conoce">Conocé más</a>
                                    </div>
                                </div>
                            </article>';
                }

              
                echo $proy;
                exit;
            }
        }
            
        
        
    }

    public function traerNotasFiltradasCat()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
                
                
                // $this->_sess->set('_cat_linearios', $_POST['valor']);
                if (in_array($_POST['valor'], $_SESSION['_cat_lanz'])) {
                    $_borrar = array_keys($_SESSION['_cat_lanz'],$_POST['valor']);
                    unset($_SESSION['_cat_lanz'][$_borrar[0]]);
                }else{
                    $_SESSION['_cat_lanz'][]=$_POST['valor'];
                }

                 // echo "<pre>";print_r($_SESSION['_cat_lanz']);echo "</pre>";exit;
                
                
                $_data = $this->homeGestion->traerLanzamientosPorCat($this->cantidadRegistros, $_SESSION['_cat_lanz']);               

                $_cantReg = home::contarRegistrosLanz($_SESSION['_cat_lanz']) / $this->cantidadRegistros;
                $jsondata['cantReg'] = ceil($_cantReg); ; 

               // $_data = $this->homeGestion->traerTendenciasPorCat($_POST['valor']);
                if($_data){

                    $jsondata['html'] ='';
                    foreach ($_data as $datos) {
                        /*$jsondata['html'] .='<div class="forum-item"  style="float: left;margin-right: 10px;">
                                    <div class="row">
                                        <div class="col-md-12">';                   
                                            
                                            $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'linearios');
                                            if($_img !=''){
                                                $jsondata['html']  .=' <img src="'. $this->_conf['base_url'] . 'public/img/subidas/linearios/thumb/'. $_img->path .'" >';
                                            }                                            
                                            $_cat = explode(',', $datos['categorias']); 
                                            $_arr_cat = array();                                       
                                            foreach ($_cat as $val) {
                                              $_arr = home::traerCategoria($val);
                                              $_arr_cat[] = $_arr['nombre']; 
                                            }
                                            $_cate = implode(', ', $_arr_cat);
                                            
                                            $jsondata['html']  .='<br><small><b>'.$_cate.'</b></small>
                                            <h4><strong>'.home::convertirCaracteres($datos['titulo']).'</strong></h4>  
                                            <!-- <p>'.home::convertirCaracteres($datos['bajada']).'</p>  -->  
                                            <a class="btn btn-warning" href="'. $this->_conf['url_enlace'].'linearios/detalle/'. home::crearUrl($datos['id'],$datos['titulo']).'">ver</a>                                                                     
                                        </div>                                      
                                    </div>
                                </div>';*/

                        $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'lanzamientos');
                        if($_img !=''){                            
                            $_url_img = $this->_conf['base_url'] . 'public/img/subidas/lanzamientos/'. $_img->path;
                        }

                        if($datos['link']==''){
                            preg_match('/src="([^"]+)"/', $datos['video'], $match);
                            $_url_vid = $match[1];
                        }

                        $_cat=array();
                        $_cat = explode(',', $datos['categorias']);       
                        $_arr_cat=array();
                        $_arr_label=array();
                        foreach ($_cat as $val) {
                          $_arr = home::traerCategoria($val);
                          $_arr_cat[] = $_arr['nombre']; 
                          $_arr_label[] = $_arr['clase']; 
                        }
                        $_cate = implode(', ', $_arr_cat);
                       

                        $jsondata['html'] .= '<article class="grid-item '.$_arr_label[0].'">
                                <div class="img" style="background-image: url('.$_url_img.')"></div>
                                <div class="copy">
                                    <div class="close"><i class="fa fa-times" aria-hidden="true"></i></div>
                                    <h4>'.$_cate.'</h4>
                                    <h3>'.home::convertirCaracteres($datos['titulo']).'</h3>
                                    <div class="content">
                                        <p>'.home::limitarTexto(home::convertirCaracteres(strip_tags($datos['descripcion'])),150).'</p>';
                                        if($datos['link']==''){
                                            $jsondata['html'] .= ' <a href="javascript:void(0);" rel="'.$_url_vid.'" class="ver-video">Ver Video</a>';
                                        } else{
                                            $jsondata['html'] .= '<a href="'.$datos['link'].'" target="_blank">Ir al Site</a>';
                                        }
                        $jsondata['html'] .= '</div>
                                    <div class="preview">
                                        <p>'.home::limitarTexto(home::convertirCaracteres(strip_tags($datos['bajada'])),150).'</p>
                                        <a href="#" class="conoce">Conocé más</a>
                                    </div>
                                </div>
                            </article>';
                    }
                    

                }else{
                     $jsondata['html'] ='No hay contenidos';
                }               

               

                echo json_encode($jsondata);
                exit;

            }

        }

    }

   


    /*public function detalle($_id, $_titulo)
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }
        
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $_id = (int) $_id;

        $this->_view->datos = $this->homeGestion->traerCapacitacion($_id);
		$_juego = $this->homeGestion->traerJuegoPorTipoId($this->_view->datos['id_juego'], $this->_view->datos['id_tipo_juego']); 
		$this->_view->juego = $this->homeGestion->traerJuegoPorTabla($_juego['id_juego'], $_juego['tabla_juegos']);  
		$preg = base64_decode($this->_view->juego['preguntas']);
	    $this->_view->juego['_preg'] = unserialize($preg); 
	    $resp = base64_decode($this->_view->juego['respuestas']);
	    $this->_view->juego['_resp']  = unserialize($resp);  
	    $_resp_correctas = base64_decode($this->_view->juego['respuesta_correcta']);
	    $this->_view->juego['_resp_correctas'] = unserialize($_resp_correctas);
	    $puntos = base64_decode($this->_view->juego['puntos']);
	    $this->_view->juego['_puntos'] = unserialize($puntos); 

		 // echo "<pre>";print_r($this->_view->datos);echo "</pre>";
		 // echo "<pre>";print_r($_juego);echo "</pre>";
		 echo "<pre>";print_r($this->_view->juego);exit;

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('detalle','lanzamientos');
    }*/

	
}


?>