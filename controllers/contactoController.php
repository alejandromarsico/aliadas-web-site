<?php

use Nucleo\Controller\Controller;

class contactoController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('class.home');		
		$this->homeGestion = new home();

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();

       $this->getLibrary('PHPMailerAutoload');
        $this->envioMail = new PHPMailer(); 
			
				
    }
    
   	
    public function index()
    {

        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }


        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

                // echo "<pre>";print_r($_POST);exit;

                /*if(!$this->_publicGestion->hash_equals(hash_hmac('sha256', $_SERVER['REQUEST_URI'], $this->_sess->get('_csrf')), validador::getPostParam('_token'))) exit('Error de autentificacion 2');*/
                
                if(!validador::getTexto('nombre')) exit("Debe completar el campo nombre");
                if(!validador::getTexto('numero_cliente')) exit("Debe completar el campo numero de cliente ");
                if(!validador::numerico($_POST['numero_cliente'])) exit("Debe introducir un numero de cliente válido");
                if(!validador::getTexto('email')) exit("Debe completar el campo email");
                if(!validador::validarEmail($_POST['email'])) exit("Debe introducir un email válido");
                if(!validador::getTexto('nombre_farmacia')) exit("Debe completar el campo nombre de farmacia");
                if(!validador::getTexto('consulta')) exit("Debe completar el campo consulta");                
                
                
                // mail admin
                $_body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <title>Aliadas</title>
                        </head>                 
                        <body>
                        <p>Nombre: '.home::convertirCaracteres($_POST['nombre']).'</p>
                        <p>Numero de cliente: '.$_POST['numero_cliente'].'</p>
                        <p>Email: '.$_POST['email'].'</p>
                        <p>Nombre de farmacia: '.home::convertirCaracteres($_POST['nombre_farmacia']).'</p>
                        <p>Consulta: '.$_POST['consulta'].'</p>
                        </body>
                        </html>';
                
                // $this->envioMail->IsSMTP();
                // $this->envioMail->SMTPAuth = true;
                // $this->envioMail->Host = "smtphub.cencosud.cl";
                // $this->envioMail->Username = "_MailCarteleria"; 
                // $this->envioMail->Password = "a23uj8rs"; 
                // $this->envioMail->Port = 25;     
                $this->envioMail->IsSMTP();
                $this->envioMail->SMTPAuth = true;
                $this->envioMail->Host = "mail.alejandromarsico.com.ar";
                $this->envioMail->Username = "_mainaccount@alejandromarsico.com.ar"; 
                $this->envioMail->Password = "alem1984"; 
                $this->envioMail->Port = 465;
                $this->envioMail->SMTPSecure = 'ssl';
                               
                $this->envioMail->From ='info@aliadas.com.ar';
                $this->envioMail->FromName ='Aliadas';
                $this->envioMail->Subject = 'Formulario contacto';               
                $this->envioMail->Body = $_body;                            
                // $this->envioMail->AddAddress('lucianodirisio@gmail.com');
                $this->envioMail->AddAddress('aliadas@delsud.com.ar');
                $this->envioMail->IsHTML(true); 
                
                $exito = $this->envioMail->Send();
                
                $intentos=1;
                
                while ((!$exito) && ($intentos < 3)) {
                    sleep(5);           
                    $exito = $this->envioMail->Send();              
                    $intentos=$intentos+1;          
                }
                
                if(!$exito) {           
                    echo "Problemas enviando correo electrónico a ".$this->envioMail->ErrorInfo;
                    exit;               
                }

                 echo "ok";
                exit;

            }else exit('Error');
        }
       
        // $this->_view->titulo = 'Aliadas';
        // $this->_view->renderizar('index','contacto');
    }
	


   

   


	
}


?>