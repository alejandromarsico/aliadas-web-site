<?php

use Nucleo\Controller\Controller;

class capacitacionesController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('class.home');		
		$this->homeGestion = new home();

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
			
		$this->cantidadRegistros = 6;
        $this->filtros =  array(1,2,3,4); 		
    }
    
   		
	
	public function _index()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }
        
       // $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>";

       $_SESSION['_cat_capacitaciones'] = $this->filtros; 

        $this->_view->datos = $this->homeGestion->traerCapacitaciones();
        // $this->_view->datos = $this->homeGestion->traerCapacitaciones($this->cantidadRegistros, $_SESSION['_cat_capacitaciones']);
        $this->_view->cantReg = home::contarRegistrosCapacitaciones($_SESSION['_cat_capacitaciones']) / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
		$this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front'));
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];  
            	
		//echo "<pre>";print_r($this->_view->banners_top);exit;      	
		

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index','capacitaciones', 'default');
    }

    public function index()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }
        
       // $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>";

       $_SESSION['_cat_capacitaciones'] = $this->filtros; 

        $this->_view->datos = $this->homeGestion->traerCapacitaciones();
        // $this->_view->datos = $this->homeGestion->traerCapacitaciones($this->cantidadRegistros, $_SESSION['_cat_capacitaciones']);
        $this->_view->cantReg = home::contarRegistrosCapacitaciones($_SESSION['_cat_capacitaciones']) / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
        // $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        // $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front'));
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];  

        $this->_view->banners_top_dos = $this->homeGestion->traerBannerPorSeccionDos(2, 1);
        if($this->_view->banners_top_dos){
            for ($i=0; $i < count($this->_view->banners_top_dos); $i++) {         
                $this->_view->banners_top_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_top_dos[$i]['link']));
            }
        } 
                
        //echo "<pre>";print_r($this->_view->banners_top);exit;         
        

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index2','capacitaciones', 'default');
    }


    public function paginacion()
    {

        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 

                $_pag = $_POST['p'];
                $_data = $this->homeGestion->paginadorCapacitaciones($_pag, $this->cantidadRegistros, $_SESSION['_cat_capacitaciones']);      
                $proy='';
                
                foreach($_data as $datos){

                    $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'capacitaciones');
                    if($_img !=''){                            
                        $_url_img = $this->_conf['base_url'] . 'public/img/subidas/capacitaciones/thumb/'. $_img->path;
                    }
                   
                    $_cat=array();
                    $_cat = explode(',', $datos['categorias']);       
                    $_arr_cat=array();
                    $_arr_label=array();
                    foreach ($_cat as $val) {
                      $_arr = home::traerCategoria($val);
                      $_arr_cat[] = $_arr['nombre']; 
                      $_arr_label[] = $_arr['clase']; 
                    }
                    $_cate = implode(', ', $_arr_cat);
                    $_fecha = explode('-', $datos['fecha']);
                    $_fecha = $_fecha[2].' '. home::convertirMes($_fecha[1]).' de '.$_fecha[0];

                    $proy .= '<a href="'.$this->_conf['url_enlace'].'capacitaciones/detalle/'.home::crearUrl($datos['id'],$datos['titulo']).'" class="grid-item '.$_arr_label[0].'">
                                <div class="img" style="background-image: url('.$_url_img.')"></div>
                                <h2>'.home::convertirCaracteres($datos['titulo']).'</h2>
                                <span><i class="fa fa-calendar-o" aria-hidden="true"></i>  '.$_fecha.'  |  '.$_arr_label[0].'</span>
                                <p>'.home::limitarTexto(home::convertirCaracteres(strip_tags($datos['bajada'])),50).'</p>
                            </a>'; 
                            
                   /* $proy .= '<div class="forum-item" style="float: left; margin-right: 10px;">
                                <div class="row">
                                    <div class="col-md-10">';
                                        $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'capacitaciones');
                                        if($_img !=''){
                                        $proy .= '<img src="'.$this->_conf['base_url'] . 'public/img/subidas/capacitaciones/thumb/'. $_img->path .'" />';
                                        }                                        
                                        $_cat = explode(',', $datos['categorias']);
                                        $_arr_cat = array();
                                        foreach ($_cat as $val) {
                                          $_arr = home::traerCategoria($val);
                                          $_arr_cat[] = $_arr['nombre']; 
                                        }
                                        $_cate = implode(', ', $_arr_cat);                                        
                                        $proy .= '<br><small><b>'.$_cate.'</b></small>
                                        <br>
                                        <h4><strong>'.admin::convertirCaracteres($datos['titulo']).'</strong></h4>                              
                                    </div>
                                </div>
                            </div>';*/
                            
                }

                echo $proy;
                exit;
            }
        }
            
        
    }

    public function traerNotasFiltradasCat()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
                
                
               /* $this->_sess->set('_cat_tendencias', $_POST['valor']);

                
                if(empty($this->_sess->get('_tipo_tendencias')) || !$this->_sess->get('_tipo_tendencias')){
                     $_data = $this->homeGestion->traerTendenciasPorCat($this->cantidadRegistros, $_POST['valor']);
                } else{
                     $_data = $this->homeGestion->traerTendenciasPorCat($this->cantidadRegistros, $_POST['valor'],$this->_sess->get('_tipo_tendencias'));
                }*/

                if (in_array($_POST['valor'], $_SESSION['_cat_capacitaciones'])) {
                    $_borrar = array_keys($_SESSION['_cat_capacitaciones'],$_POST['valor']);
                    unset($_SESSION['_cat_capacitaciones'][$_borrar[0]]);
                }else{
                    $_SESSION['_cat_capacitaciones'][]=$_POST['valor'];
                }

                $_data = $this->homeGestion->traerCapacitacionesPorCat($this->cantidadRegistros, $_SESSION['_cat_capacitaciones']); 

                $_cantReg = home::contarRegistrosCapacitaciones($_SESSION['_cat_capacitaciones']) / $this->cantidadRegistros;
                $jsondata['cantReg'] = ceil($_cantReg); ; 

               // $_data = $this->homeGestion->traerTendenciasPorCat($_POST['valor']);
                if($_data){

                    $jsondata['html'] ='';
                    foreach ($_data as $datos) {

                        $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'capacitaciones');
                        if($_img !=''){                            
                            $_url_img = $this->_conf['base_url'] . 'public/img/subidas/capacitaciones/thumb/'. $_img->path;
                        }
                       
                        $_cat=array();
                        $_cat = explode(',', $datos['categorias']);       
                        $_arr_cat=array();
                        $_arr_label=array();
                        foreach ($_cat as $val) {
                          $_arr = home::traerCategoria($val);
                          $_arr_cat[] = $_arr['nombre']; 
                          $_arr_label[] = $_arr['clase']; 
                        }
                        $_cate = implode(', ', $_arr_cat);
                        $_fecha = explode('-', $datos['fecha']);
                        $_fecha = $_fecha[2].' '. home::convertirMes($_fecha[1]).' de '.$_fecha[0];

                        $jsondata['html'] .= '<a href="'.$this->_conf['url_enlace'].'capacitaciones/detalle/'.home::crearUrl($datos['id'],$datos['titulo']).'" class="grid-item '.$_arr_label[0].'">
                                    <div class="img" style="background-image: url('.$_url_img.')"></div>
                                    <h2>'.home::convertirCaracteres($datos['titulo']).'</h2>
                                    <span><i class="fa fa-calendar-o" aria-hidden="true"></i>  '.$_fecha.'  |  '.$_cate.'</span>
                                    <p>'.home::limitarTexto(home::convertirCaracteres(strip_tags($datos['bajada'])),50).'</p>
                                </a>'; 



                    }
                    

                }else{
                     $_html='No hay contenidos';
                }               

               

                echo json_encode($jsondata);
                exit;

            }

        }

    }


    public function _detalle($_id, $_titulo)
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }
        
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $_id = (int) $_id;

        $this->_view->id_nota = $_id;
        $this->_view->seccion = 'capacitaciones';

        $this->_view->datos = $this->homeGestion->traerCapacitacion($_id);
        $this->_view->relacionadas = $this->homeGestion->traerCapacitacionesRelacionadas($_id);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];
       

        $_juego = $this->homeGestion->traerJuegoPorTipoId($this->_view->datos['id_juego'], $this->_view->datos['id_tipo_juego']); 

        if($_juego){
            $this->_view->juego = $this->homeGestion->traerJuegoPorTabla($_juego['id_juego'], $_juego['tabla_juegos']);  
            $this->_view->tabla_juego = $_juego['tabla_juegos'];
            $preg = base64_decode($this->_view->juego['preguntas']);
            $this->_view->juego['_preg'] = unserialize($preg); 
            $resp = base64_decode($this->_view->juego['respuestas']);
            $this->_view->juego['_resp']  = unserialize($resp);  
            $_resp_correctas = base64_decode($this->_view->juego['respuesta_correcta']);
            $this->_view->juego['_resp_correctas'] = unserialize($_resp_correctas);
            $puntos = base64_decode($this->_view->juego['puntos']);
            $this->_view->juego['_puntos'] = unserialize($puntos);  
        }else{
           $this->_view->juego ='';
        }

        if($this->_view->datos['id_juego'] != 0){
            $_jugar = $this->homeGestion->checkUserPlay($this->_view->data_user['id'], $this->_view->datos['id_juego']);
            if(!$_jugar){
                $this->_view->data_user['jugar'] = 'si';
            }else{
                $this->_view->data_user['jugar'] = 'no';
            }
        }else{
            $this->_view->data_user['jugar'] = 'no';
        }
        

        $_cat = explode(',', $this->_view->datos['categorias']);        
        foreach ($_cat as $val) {
          $_arr = home::traerCategoria($val);
          $_arr_cat[] = $_arr['nombre']; 
        }
        $this->_view->categorias = implode(', ', $_arr_cat);

        if($this->_view->datos['tags']!=''){
            $this->_view->tags = explode(',', $this->_view->datos['tags']);  
        }

        $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(3, 1);
        $this->_view->banners_lateral = $this->homeGestion->traerBannerPorSeccion(3, 3);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(3, 2);
                
       // echo "<pre>";print_r($this->_view->banners_lateral);exit;
       // echo "<pre>";print_r($this->_view->datos);echo "</pre>";exit;
         // echo "<pre>";print_r($_juego);echo "</pre>";exit;
        // echo "<pre>";print_r($this->_view->data_user);exit;

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('detalle','capacitaciones', 'default');
    }


    public function detalle($_id, $_titulo)
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }
        
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $_id = (int) $_id;

        $this->_view->id_nota = $_id;
		$this->_view->seccion = 'capacitaciones';

        $this->_view->datos = $this->homeGestion->traerCapacitacion($_id);
        $this->_view->relacionadas = $this->homeGestion->traerCapacitacionesRelacionadas($_id);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];
       

		$_juego = $this->homeGestion->traerJuegoPorTipoId($this->_view->datos['id_juego'], $this->_view->datos['id_tipo_juego']); 

        if($_juego){
            $this->_view->juego = $this->homeGestion->traerJuegoPorTabla($_juego['id_juego'], $_juego['tabla_juegos']);  
            $this->_view->tabla_juego = $_juego['tabla_juegos'];
            $preg = base64_decode($this->_view->juego['preguntas']);
            $this->_view->juego['_preg'] = unserialize($preg); 
            $resp = base64_decode($this->_view->juego['respuestas']);
            $this->_view->juego['_resp']  = unserialize($resp);  
            $_resp_correctas = base64_decode($this->_view->juego['respuesta_correcta']);
            $this->_view->juego['_resp_correctas'] = unserialize($_resp_correctas);
            $puntos = base64_decode($this->_view->juego['puntos']);
            $this->_view->juego['_puntos'] = unserialize($puntos);  
        }else{
           $this->_view->juego ='';
        }

        if($this->_view->datos['id_juego'] != 0){
            $_jugar = $this->homeGestion->checkUserPlay($this->_view->data_user['id'], $this->_view->datos['id_juego']);
            if(!$_jugar){
                $this->_view->data_user['jugar'] = 'si';
            }else{
                $this->_view->data_user['jugar'] = 'no';
            }
        }else{
            $this->_view->data_user['jugar'] = 'no';
        }
		

	    $_cat = explode(',', $this->_view->datos['categorias']);        
        foreach ($_cat as $val) {
          $_arr = home::traerCategoria($val);
          $_arr_cat[] = $_arr['nombre']; 
        }
        $this->_view->categorias = implode(', ', $_arr_cat);

        if($this->_view->datos['tags']!=''){
        	$this->_view->tags = explode(',', $this->_view->datos['tags']);  
    	}

    	// $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(3, 1);
        // $this->_view->banners_lateral = $this->homeGestion->traerBannerPorSeccion(3, 3);
        // $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(3, 2);

        $this->_view->banners_top_dos = $this->homeGestion->traerBannerPorSeccionDos(3, 1);
        if($this->_view->banners_top_dos){
            for ($i=0; $i < count($this->_view->banners_top_dos); $i++) {         
                $this->_view->banners_top_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_top_dos[$i]['link']));
            }
        }
        
        $this->_view->banners_middle_dos = $this->homeGestion->traerBannerPorSeccionDos(3, 2);
        if($this->_view->banners_middle_dos){
             for ($i=0; $i < count($this->_view->banners_middle_dos); $i++) {         
                $this->_view->banners_middle_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_middle_dos[$i]['link']));
            }
        }
        
        $this->_view->banners_bottom_dos = $this->homeGestion->traerBannerPorSeccionDos(3, 3);
        if($this->_view->banners_bottom_dos){
             for ($i=0; $i < count($this->_view->banners_bottom_dos); $i++) {         
                $this->_view->banners_bottom_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_bottom_dos[$i]['link']));
            }
        }
                
       // echo "<pre>";print_r($this->_view->banners_lateral);exit;
	   // echo "<pre>";print_r($this->_view->datos);echo "</pre>";exit;
		 // echo "<pre>";print_r($_juego);echo "</pre>";exit;
		// echo "<pre>";print_r($this->_view->data_user);exit;

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('detalle2','capacitaciones', 'default');
    }


    public function finalizarJuego()
    {
    	if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }

        if($_POST){

        	if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){


        		$this->_view->data = $_POST;


                $_jugo = $this->homeGestion->checkUserPlay($this->_sess->get('id_usuario_front'), $this->_view->data['id_juego']);
                if($_jugo){
                    
                    echo "yajugo";
                    exit;

                }else{                

            		$cat = new contenidos_users_juego();
    				$cat->id_user = $this->_sess->get('id_usuario_front');
    				$cat->id_juego = $this->_view->data['id_juego'];
    				$cat->id_tipo_juego = $this->_view->data['tipo_juego'];
    				$cat->tabla_juegos = $this->_view->data['tabla_juego'];
    				$cat->puntos = $this->_view->data['puntos'];
    				$cat->fecha = date('Y-m-d');
    				$cat->save();

    				echo "ok";
    				exit;

                }

        	}

        }

    }


  /*  public function editar()
    {

    	if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }

    	if($_POST){
			
			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
				if($_POST['envio01'] == 1){
					
					$this->_view->data = $_POST;
					
				
					// echo "<pre>";print_r($this->_view->data);exit;

					
					
					
					$cat = contenidos_user::find($this->_sess->get('id_usuario_front'));
					$cat->numero_cliente = $this->_view->data['numero_cliente'];
					$cat->razon_social = $this->_xss->xss_clean(validador::getTexto('razon_social'));;
					$cat->ciudad = $this->_xss->xss_clean(validador::getTexto('ciudad'));;
					$cat->provincia = $this->_xss->xss_clean(validador::getTexto('provincia'));;
					$cat->email = $this->_view->data['email'];
					$cat->password = $this->_view->data['password'];
					$cat->fecha_nacimiento = $this->_view->data['fecha_nacimiento'];	
					$cat->identificador = $this->_sess->get('carga_user');				
					$cat->save();
								
					

					 $this->_sess->destroy('carga_user');	
					 $this->_sess->destroy('img_id_user');	
					 $this->_sess->destroy('img_error_user');				
					// $this->redireccionar('administrador/users');
					echo 'ok';
					exit;
					
												
					
				}

			}else{
				// $this->redireccionar('error/access/404');
				echo 'Hubo un error, vuelva a intentarlo mas tarde.';
				exit;
			}
		}

    }*/

public function login()
    {

    	if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
			
				if(validador::getInt('enviar') == 1){
					
					
					$this->_view->datos = $_POST;
					
					// if(!validador::getAlphaNum('usuario')){
					if(!validador::validarEmail($this->_view->datos['email'])){
						// $this->_view->_error = 'Debe introducir un email valido';
						/*$this->_view->renderizar('index','login');
						exit;*/
						echo 'Debe introducir un email valido';
						exit;
					}
					
					//if(!validador::getSql('pass',$this->_conf['baseDatos'])){
					if(!validador::getAlphaNum('pass')){					
						/*$this->_view->_error = 'Debe introducir su password';
						$this->_view->renderizar('index','login', 'login'); 
						exit;*/
						echo 'Debe introducir su password';
						exit;
					}
									
					
					
					// 'find' si se busca un solo registro, 'all' si se busca solo 1
					$row = contenidos_user::find(array(
											'conditions' => array(
															'email = ? AND password = ?', 
															// $this->_xss->xss_clean(validador::getAlphaNum('usuario')), 
															$this->_xss->xss_clean($this->_view->datos['email']),
															Hash::getHash('md5', $this->_xss->xss_clean(validador::getPostParam('pass')), $this->_conf['hash_key'])
															// $this->_xss->xss_clean(validador::getPostParam('pass'))
															)
												)
										);
					
					
					if(!$row){
						/*$this->_view->_error = 'Usuario y/o password incorrectos';
						$this->_view->renderizar('index','login', 'login');
						exit;*/
						echo 'Usuario y/o password incorrectos';
						exit;
					}
					
										
					$this->_sess->set('autenticado_front', true);
					$this->_sess->set('usuario_front', $row->nombre);
					$this->_sess->set('id_usuario_front', $row->id);

					echo 'ok';
					exit;
					// $this->redireccionar('home');
				}

			}else{
				//$this->redireccionar('error/access/404');
				// $this->_view->_error = 'Hubo un error, vuelva a intentarlo mas tarde.';
				echo 'Hubo un error, vuelva a intentarlo mas tarde.';
				exit;
			}
		}

    }

	
}


?>