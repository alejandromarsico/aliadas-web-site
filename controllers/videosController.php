<?php

use Nucleo\Controller\Controller;

class videosController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('class.home');		
		$this->homeGestion = new home();

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
			
		$this->cantidadRegistros = 8;	

       
    }
    
   		
	
	public function _index()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // echo 'cat: '.$this->_sess->get('_cat_videos');
       //  echo 'tipo: '.$this->_sess->get('_tipo_tendencias');         

         // $this->_sess->destroy('_cat_videos');

         // echo "<pre>";print_r($_SESSION);echo "</pre>";

        $this->_view->datos = $this->homeGestion->traerVideos();
        // $this->_view->datos = $this->homeGestion->traerVideos($this->cantidadRegistros);
        $this->_view->cantReg = home::contarRegistrosVideos() / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
        $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front'));  
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];
                
       // echo "<pre>";print_r($this->_view->banners_top);exit;

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index','videos', 'default');
    }

    public function index()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // echo 'cat: '.$this->_sess->get('_cat_videos');
       //  echo 'tipo: '.$this->_sess->get('_tipo_tendencias');         

         // $this->_sess->destroy('_cat_videos');

         // echo "<pre>";print_r($_SESSION);echo "</pre>";

        $this->_view->datos = $this->homeGestion->traerVideos();
        // $this->_view->datos = $this->homeGestion->traerVideos($this->cantidadRegistros);
        $this->_view->cantReg = home::contarRegistrosVideos() / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
        // $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        // $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front'));  
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];

        $this->_view->banners_top_dos = $this->homeGestion->traerBannerPorSeccionDos(2, 1);
        if($this->_view->banners_top_dos){
            for ($i=0; $i < count($this->_view->banners_top_dos); $i++) {         
                $this->_view->banners_top_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_top_dos[$i]['link']));
            }
        } 
                
       // echo "<pre>";print_r($this->_view->banners_top);exit;

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index2','videos', 'default');
    }



    public function paginacion()
    {

        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 

                // echo "<pre>";print_r($_SESSION);echo "</pre>";

                $_pag = $_POST['p'];
                $_data = $this->homeGestion->paginadorVideos($_pag, $this->cantidadRegistros);      
                $proy='';
                
                foreach($_data as $datos){

                    $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'tutoriales');
                    if($_img !=''){                            
                        $_url_img = $this->_conf['base_url'] . 'public/img/subidas/tutoriales/thumb/'. $_img->path;
                    }
                   
                    $_cat=array();
                    $_cat = explode(',', $datos['categorias']);       
                    $_arr_cat=array();
                    $_arr_label=array();
                    foreach ($_cat as $val) {
                      $_arr = home::traerCategoria($val);
                      $_arr_cat[] = $_arr['nombre']; 
                      $_arr_label[] = $_arr['clase']; 
                    }
                    $_cate = implode(', ', $_arr_cat);
                    $_fecha = explode('-', $datos['fecha']);
                    $_fecha = $_fecha[2].' '. home::convertirMes($_fecha[1]).' de '.$_fecha[0];

                    $proy .= '<a href="'.$this->_conf['url_enlace'].'videos/detalle/'.home::crearUrl($datos['id'],$datos['titulo']).'" class="grid-item '.$_arr_label[0].'">
                                <div class="img" style="background-image: url('.$_url_img.')">
                                    <img src="'.$this->_conf['base_url'].'views/layout/default/img/ico_play.png" alt="Video">
                                </div>
                                <span>'.$_arr_label[0].'</span>
                                <h2>'.home::convertirCaracteres($datos['titulo']).'</h2>
                            </a>'; 
                            
                   /* $proy .= '<div class="forum-item" style="float: left;margin-right: 10px;">
                                <div class="row">
                                    <div class="col-md-12">';
                                        $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'tutoriales');
                                        if($_img !=''){
                                        $proy .= '<img src="'.$this->_conf['base_url'] . 'public/img/subidas/tutoriales/thumb/'. $_img->path .'" />';
                                        }                                        
                                        $_cat = explode(',', $datos['categorias']);
                                        $_arr_cat = array();
                                        foreach ($_cat as $val) {
                                          $_arr = home::traerCategoria($val);
                                          $_arr_cat[] = $_arr['nombre']; 
                                        }
                                        $_cate = implode(', ', $_arr_cat);                                        
                                        $proy .= '<br><small><b>'.$_cate.'</b></small>
                                        <h4><strong>'.admin::convertirCaracteres($datos['titulo']).'</strong></h4> 
                                        <a class="btn btn-warning" href="'.$this->_conf['url_enlace'].'videos/detalle/'.home::crearUrl($datos['id'],$datos['titulo']).'">
                                               ver
                                        </a>                               
                                    </div>
                                </div>
                            </div>';*/
                            
                }

                echo $proy;
                exit;
            }
        }
            
        
    }

    public function _detalle($_id, $_titulo)
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $_id = (int) $_id;

        $this->_view->id_nota = $_id;
        $this->_view->seccion = 'videos';

        $this->_view->datos = $this->homeGestion->traerVideo($_id);
        $this->_view->relacionadas = $this->homeGestion->traerVideosRelacionadas($_id);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];
        
        $_cat = explode(',', $this->_view->datos['categorias']);        
        foreach ($_cat as $val) {
          $_arr = home::traerCategoria($val);
          $_arr_cat[] = $_arr['nombre']; 
        }
        $this->_view->categorias = implode(', ', $_arr_cat);

        if($this->_view->datos['tags']!=''){
            $this->_view->tags = explode(',', $this->_view->datos['tags']);  
        }

        $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(3, 1);
        $this->_view->banners_lateral = $this->homeGestion->traerBannerPorSeccion(3, 3);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(3, 2);
        // $this->_view->categorias = home::traerCategorias();
                
        // echo "<pre>";print_r($this->_view->banners_lateral);exit;
        // echo "<pre>";print_r($this->_view->datos);echo "</pre>";
        // echo "<pre>";print_r($_juego);echo "</pre>";
        // echo "<pre>";print_r($this->_view->juego);exit;

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('detalle','videos', 'default');
    }

    public function detalle($_id, $_titulo)
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $_id = (int) $_id;

        $this->_view->id_nota = $_id;
		$this->_view->seccion = 'videos';

        $this->_view->datos = $this->homeGestion->traerVideo($_id);
        $this->_view->relacionadas = $this->homeGestion->traerVideosRelacionadas($_id);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];
		
	    $_cat = explode(',', $this->_view->datos['categorias']);        
        foreach ($_cat as $val) {
          $_arr = home::traerCategoria($val);
          $_arr_cat[] = $_arr['nombre']; 
        }
        $this->_view->categorias = implode(', ', $_arr_cat);

        if($this->_view->datos['tags']!=''){
        	$this->_view->tags = explode(',', $this->_view->datos['tags']);  
    	}

        // $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(3, 1);
        // $this->_view->banners_lateral = $this->homeGestion->traerBannerPorSeccion(3, 3);
        // $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(3, 2);
        // $this->_view->categorias = home::traerCategorias();

        $this->_view->banners_top_dos = $this->homeGestion->traerBannerPorSeccionDos(3, 1);
        if($this->_view->banners_top_dos){
            for ($i=0; $i < count($this->_view->banners_top_dos); $i++) {         
                $this->_view->banners_top_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_top_dos[$i]['link']));
            }
        }
        
        $this->_view->banners_middle_dos = $this->homeGestion->traerBannerPorSeccionDos(3, 2);
        if($this->_view->banners_middle_dos){
             for ($i=0; $i < count($this->_view->banners_middle_dos); $i++) {         
                $this->_view->banners_middle_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_middle_dos[$i]['link']));
            }
        }
        
        $this->_view->banners_bottom_dos = $this->homeGestion->traerBannerPorSeccionDos(3, 3);
        if($this->_view->banners_bottom_dos){
             for ($i=0; $i < count($this->_view->banners_bottom_dos); $i++) {         
                $this->_view->banners_bottom_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_bottom_dos[$i]['link']));
            }
        }
                
        // echo "<pre>";print_r($this->_view->banners_lateral);exit;
		// echo "<pre>";print_r($this->_view->datos);echo "</pre>";
		// echo "<pre>";print_r($_juego);echo "</pre>";
		// echo "<pre>";print_r($this->_view->juego);exit;

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('detalle2','videos', 'default');
    }




   /* public function traerNotasFiltradasCat()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
                
                
                $this->_sess->set('_cat_videos', $_POST['valor']);

                // echo "<pre>";print_r($_SESSION);echo "</pre>";
                
                $_data = $this->homeGestion->traerVideosPorCat($this->cantidadRegistros, $_POST['valor']);

                $_cantReg = home::contarRegistrosVideos($this->_sess->get('_cat_videos')) / $this->cantidadRegistros;
                $jsondata['cantReg'] = ceil($_cantReg); ; 

                if($_data){

                    $jsondata['html'] ='';
                    foreach ($_data as $datos) {
                        $jsondata['html'] .='<div class="forum-item"  style="float: left;margin-right: 10px;">
                                    <div class="row">
                                        <div class="col-md-12">';                   
                                            
                                            $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'tutoriales');
                                            if($_img !=''){
                                                $jsondata['html']  .=' <img src="'. $this->_conf['base_url'] . 'public/img/subidas/tutoriales/thumb/'. $_img->path .'" >';
                                            }                                            
                                            $_cat = explode(',', $datos['categorias']); 
                                            $_arr_cat = array();                                       
                                            foreach ($_cat as $val) {
                                              $_arr = home::traerCategoria($val);
                                              $_arr_cat[] = $_arr['nombre']; 
                                            }
                                            $_cate = implode(', ', $_arr_cat);                                            
                                            $jsondata['html']  .='<br><small><b>'.$_cate.'</b></small>
                                            <h4><strong>'.home::convertirCaracteres($datos['titulo']).'</strong></h4>  
                                            <!-- <p>'.home::convertirCaracteres($datos['bajada']).'</p>  -->  
                                            <a class="btn btn-warning" href="'. $this->_conf['url_enlace'].'videos/detalle/'. home::crearUrl($datos['id'],$datos['titulo']).'">ver</a>                                                                     
                                        </div>                                      
                                    </div>
                                </div>';
                    }
                    

                }else{
                     $_html='No hay contenidos';
                }               

               

                echo json_encode($jsondata);
                exit;

            }

        }

    }
*/

 

	
}


?>