<?php

use Nucleo\Controller\Controller;

class clubaliadasController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('class.home');		
		$this->homeGestion = new home();

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();

        $this->getLibrary('PHPMailerAutoload');
        $this->envioMail = new PHPMailer();	

        $this->cantidadRegistros = 4;   		
				
    }
    
   	
    public function _index()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }
        
       // $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 
        $this->_view->orden = 'desc';
        $this->_view->datos = $this->homeGestion->traerClubaliadas();
        // $this->_view->datos = $this->homeGestion->traerClubaliadas($this->cantidadRegistros);
        $this->_view->cantReg = home::contarRegistrosClubaliadas() / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
        $this->_view->id_nota = '';
        $this->_view->seccion = 'clubaliadas';
        $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $_total = $this->homeGestion->traerUserPuntosTotales($this->_sess->get('id_usuario_front'));
        $this->_view->total = $_total['puntos_totales'] - $_total['puntos_canjeados'];
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social']; 

        // echo "<pre>";print_r($this->_view->banners_top);exit;        
        

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index','clubaliadas', 'default');
    }
	
	public function index()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }
        
       // $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 
        $this->_view->orden = 'desc';
        $this->_view->datos = $this->homeGestion->traerClubaliadas();
        // $this->_view->datos = $this->homeGestion->traerClubaliadas($this->cantidadRegistros);
        $this->_view->cantReg = home::contarRegistrosClubaliadas() / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
        $this->_view->id_nota = '';
		$this->_view->seccion = 'clubaliadas';
        // $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        // $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $_total = $this->homeGestion->traerUserPuntosTotales($this->_sess->get('id_usuario_front'));
        $this->_view->total = $_total['puntos_totales'] - $_total['puntos_canjeados'];
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social']; 

        $this->_view->banners_top_dos = $this->homeGestion->traerBannerPorSeccionDos(2, 1);
        if($this->_view->banners_top_dos){
            for ($i=0; $i < count($this->_view->banners_top_dos); $i++) {         
                $this->_view->banners_top_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_top_dos[$i]['link']));
            }
        } 

        // echo "<pre>";print_r($this->_view->banners_top);exit;     	
		

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index2','clubaliadas', 'default');
    }

    public function ordenar($_val=false)
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }
        
       // $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 
        $this->_view->orden = ($_val!=false) ? $_val: 'desc';

        $this->_view->datos = $this->homeGestion->traerClubaliadasOrden($this->_view->orden);
        // $this->_view->datos = $this->homeGestion->traerClubaliadas($this->cantidadRegistros);
        $this->_view->cantReg = home::contarRegistrosClubaliadas() / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
        $this->_view->id_nota = '';
        $this->_view->seccion = 'clubaliadas';
        $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $_total = $this->homeGestion->traerUserPuntosTotales($this->_sess->get('id_usuario_front'));
        $this->_view->total = $_total['puntos_totales'] - $_total['puntos_canjeados'];
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front'));  
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];

        // echo "<pre>";print_r($this->_view->banners_top);exit;        
        

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index','clubaliadas', 'default');
    }

    public function paginacion()
    {

        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 

                $_pag = $_POST['p'];
                $_data = $this->homeGestion->paginadorClubaliadas($_pag, $this->cantidadRegistros); 
                $_total = $this->homeGestion->traerUserPuntosTotales($this->_sess->get('id_usuario_front'));
                $this->_view->total = $_total['puntos_totales'] - $_total['puntos_canjeados'];
     
                $proy='';
                
                foreach($_data as $datos){

                    $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'clubaliadas');
                    if($_img !=''){                            
                        $_url_img = $this->_conf['base_url'] . 'public/img/subidas/clubaliadas/'. $_img->path;
                    }                   
                    
                  
                    $proy .= '<div class="canjes">
                                    <div class="img" style="background-image: url('.$_url_img.')">
                                        <span class="label">'.$datos['puntos'].' PTS.</span>
                                    </div>
                                    <h2>'.home::convertirCaracteres($datos['titulo']).'</h2>';
                                    if($datos['puntos'] <= $this->_view->total){
                                        $proy .= '<a href="javascript:void(0);" onclick="canjear('.$datos['puntos'].', '.$datos['id'].');">
                                           canjear
                                        </a>';
                                    }
                    $proy .= '</div>';    

                    /*$proy .= '<div id="item_'.$datos['id'].'" class="forum-item" style="float: left; margin-right: 10px;">
                                <div class="row">
                                    <div class="col-md-10">'; 

                                        $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'clubaliadas');
                                        if($_img !=''){
                                        $proy .= '<img src="'. $this->_conf['base_url'] . 'public/img/subidas/clubaliadas/'. $_img->path.'" width="300">';
                                        }
                                        $proy .= '<br>
                                        <small>Puntos: '.$datos['puntos'].'</small>
                                        <br>
                                        <small>Monto: '.$datos['monto'].'</small>
                                        <h4><strong>'.admin::convertirCaracteres($datos['titulo']).'</strong></h4>';                              
                                        admin::convertirCaracteres($datos['descripcion']);

                                        if($datos['puntos'] <= $this->_view->total){
                                        $proy .= '<a class="btn btn-warning" href="javascript:void(0);" onclick="canjear('.$datos['puntos'].', '.$datos['id'].');">
                                               Canjear
                                            </a>';
                                        }

                                    $proy .= '</div>                                   
                                </div>
                            </div>';*/

                            
                    
                            
                }

                echo $proy;
                exit;
            }
        }
            
        
    }

    public function canjear()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar('login');
        }

        $_data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $_cliente = home::traerClientePorUsers($_data_user['id_cliente']);
        $_data_user['numero_cliente'] = $_cliente['numero_cliente'];  

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 

                $_puntos = $_POST['puntos'];
               // $_total = $_POST['total'];
                $_id_clubaliadas = $_POST['club'];
                // $_puntos_totales = $_total - $_puntos;

                $cat = new contenidos_users_puntos_canjeado();
                $cat->id_user = $this->_sess->get('id_usuario_front');
                $cat->id_clubaliadas = $_id_clubaliadas;
                $cat->puntos = $_puntos;   
                $cat->fecha = date('Y-m-d');
                $cat->save();

                //  echo "ok";
                // exit;


                $_prod = $this->homeGestion->traerClubaliada($_id_clubaliadas);
                $_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front'));
                $_total = $this->homeGestion->traerUserPuntosTotales($this->_sess->get('id_usuario_front'));
                $_punto_total = $_total['puntos_totales'] - $_total['puntos_canjeados'];

                // mail admin
                $_body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <title>Aliadas</title>
                        </head>                 
                        <body style="font-family:Arial,sans-serif;">
                        <p>El usuario <strong>'.$this->_sess->get('usuario_front').' ('.$_user['email'].')</strong>, n&uacute;mero de cliente: <strong>'. $_data_user['numero_cliente'] .'</strong>, ha canjeado puntos en el producto: <strong>'.$_prod['titulo'].' (SAP: '.strip_tags($_prod['descripcion']).')</strong></p>
                        <p>Acaba de canjear: '.$_puntos.' puntos</p>
                        <p>Sus puntos restantes son: <strong>'.$_punto_total .' puntos.</strong></p>
                        </body>
                        </html>';
                
                $this->envioMail->IsSMTP();
                $this->envioMail->SMTPAuth = true;
                $this->envioMail->Host = "mail.alejandromarsico.com.ar";
                $this->envioMail->Username = "_mainaccount@alejandromarsico.com.ar"; 
                $this->envioMail->Password = "alem1984"; 
                $this->envioMail->Port = 465;
                $this->envioMail->SMTPSecure = 'ssl';

                $this->envioMail->From ='info@aliadas.com.ar';
                $this->envioMail->FromName ='Aliadas';
                $this->envioMail->Subject = 'Aviso de canje de puntos';               
                $this->envioMail->Body = $_body;
                $this->envioMail->AddAddress('aliadas@delsud.com.ar');        
                $this->envioMail->addBCC('aliadas@gmail.com');         
                //$this->envioMail->addBCC('marsicoalejandro@hotmail.com');  

                $this->envioMail->IsHTML(true); 
                
                $exito = $this->envioMail->Send();
                
                $intentos=1;
                
                while ((!$exito) && ($intentos < 3)) {
                    sleep(5);           
                    $exito = $this->envioMail->Send();              
                    $intentos=$intentos+1;          
                }
                
                if(!$exito) {           
                    echo "Problemas enviando correo electrónico a ".$this->envioMail->ErrorInfo;
                    exit;               
                }


                //mail usuario
                $_body_u='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
                        <html xmlns="http://www.w3.org/1999/xhtml">
                        <head>
                        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
                        <title>Aliadas</title>
                        </head>                 
                        <body style="font-family:Arial,sans-serif;">
                        <p><strong>Hola '.$this->_sess->get('usuario_front').'</strong></p>
                        <p>Acabas de canjear '.$_puntos.' puntos por una OhGiftcard de <strong>'.$_prod['titulo'].'</strong>. El viernes te estará llegando tu e-giftcard a este mail desde una casilla de OhGiftcard. ¡Tenés cientos de locales adheridos donde usarlas! Una vez que recibas tu giftcard, podés ingresar a <a href="https://www.ohgiftcard.com.ar/" target="_blank">https://www.ohgiftcard.com.ar/</a> y consultar todas las marcas adheridas pegando el código de tu tarjeta en la barra de menú, a tu derecha.</p>
                        <p>¡No te olvides que las giftcard tiene vencimiento! Revisá bien este dato cuando la recibas para no perder tu premio. En caso que no te llegue el mail con tu canje, escribinos a <a href="aliadas@delsud.com.ar" target="_blank">aliadas@delsud.com.ar</a></p>
                        <p>Sus puntos restantes son: <strong>'.$_punto_total .'</strong></p>
                        <p>¡Gracias por seguir acompañándonos y ser parte de esta comunidad!</p>
                        <p>Saludos</p>
                        <p><strong>El equipo Alidas</strong></p>

                        <i><strong>Por consultas escribinos a: <a href="aliadas@delsud.com.ar" target="_blank">aliadas@delsud.com.ar</a></strong></i>
 
                        </body>
                        </html>';
                
                $this->envioMail->IsSMTP();
                $this->envioMail->SMTPAuth = true;
                $this->envioMail->Host = "mail.alejandromarsico.com.ar";
                $this->envioMail->Username = "_mainaccount@alejandromarsico.com.ar"; 
                $this->envioMail->Password = "alem1984"; 
                $this->envioMail->Port = 465;
                $this->envioMail->SMTPSecure = 'ssl';

                $this->envioMail->From ='aliadas@delsud.com.ar';
                $this->envioMail->FromName ='Aliadas';
                $this->envioMail->Subject = 'Aviso de canje de puntos';               
                $this->envioMail->Body = $_body_u;
                $this->envioMail->AddAddress($_user['email']);           

                $this->envioMail->IsHTML(true); 
                
                $exito = $this->envioMail->Send();
                
                $intentos=1;
                
                while ((!$exito) && ($intentos < 3)) {
                    sleep(5);           
                    $exito = $this->envioMail->Send();              
                    $intentos=$intentos+1;          
                }
                
                if(!$exito) {           
                    echo "Problemas enviando correo electrónico a ".$this->envioMail->ErrorInfo;
                    exit;               
                }

                            
                echo "ok";
                exit;

            }
        }

    }

    /*public function detalle($_id, $_titulo)
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }
        
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $_id = (int) $_id;

        $this->_view->datos = $this->homeGestion->traerCapacitacion($_id);
		$_juego = $this->homeGestion->traerJuegoPorTipoId($this->_view->datos['id_juego'], $this->_view->datos['id_tipo_juego']); 
		$this->_view->juego = $this->homeGestion->traerJuegoPorTabla($_juego['id_juego'], $_juego['tabla_juegos']);  
		$preg = base64_decode($this->_view->juego['preguntas']);
	    $this->_view->juego['_preg'] = unserialize($preg); 
	    $resp = base64_decode($this->_view->juego['respuestas']);
	    $this->_view->juego['_resp']  = unserialize($resp);  
	    $_resp_correctas = base64_decode($this->_view->juego['respuesta_correcta']);
	    $this->_view->juego['_resp_correctas'] = unserialize($_resp_correctas);
	    $puntos = base64_decode($this->_view->juego['puntos']);
	    $this->_view->juego['_puntos'] = unserialize($puntos); 

		 // echo "<pre>";print_r($this->_view->datos);echo "</pre>";
		 // echo "<pre>";print_r($_juego);echo "</pre>";
		 echo "<pre>";print_r($this->_view->juego);exit;

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('detalle','clubaliadas');
    }*/

public function login()
    {

    	if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
			
				if(validador::getInt('enviar') == 1){
					
					
					$this->_view->datos = $_POST;
					
					// if(!validador::getAlphaNum('usuario')){
					if(!validador::validarEmail($this->_view->datos['email'])){
						// $this->_view->_error = 'Debe introducir un email valido';
						/*$this->_view->renderizar('index','login');
						exit;*/
						echo 'Debe introducir un email valido';
						exit;
					}
					
					//if(!validador::getSql('pass',$this->_conf['baseDatos'])){
					if(!validador::getAlphaNum('pass')){					
						/*$this->_view->_error = 'Debe introducir su password';
						$this->_view->renderizar('index','login', 'login'); 
						exit;*/
						echo 'Debe introducir su password';
						exit;
					}
									
					
					
					// 'find' si se busca un solo registro, 'all' si se busca solo 1
					$row = contenidos_user::find(array(
											'conditions' => array(
															'email = ? AND password = ?', 
															// $this->_xss->xss_clean(validador::getAlphaNum('usuario')), 
															$this->_xss->xss_clean($this->_view->datos['email']),
															Hash::getHash('md5', $this->_xss->xss_clean(validador::getPostParam('pass')), $this->_conf['hash_key'])
															// $this->_xss->xss_clean(validador::getPostParam('pass'))
															)
												)
										);
					
					
					if(!$row){
						/*$this->_view->_error = 'Usuario y/o password incorrectos';
						$this->_view->renderizar('index','login', 'login');
						exit;*/
						echo 'Usuario y/o password incorrectos';
						exit;
					}
					
										
					$this->_sess->set('autenticado_front', true);
					$this->_sess->set('usuario_front', $row->nombre);
					$this->_sess->set('id_usuario_front', $row->id);

					echo 'ok';
					exit;
					// $this->redireccionar('home');
				}

			}else{
				//$this->redireccionar('error/access/404');
				// $this->_view->_error = 'Hubo un error, vuelva a intentarlo mas tarde.';
				echo 'Hubo un error, vuelva a intentarlo mas tarde.';
				exit;
			}
		}

    }


	
}


?>