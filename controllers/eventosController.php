<?php

use Nucleo\Controller\Controller;

class eventosController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('class.home');		
		$this->homeGestion = new home();

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();
			
		$this->cantidadRegistros = 6;	
        $this->filtros =  array(1,2,3,4);   
       
    }
    
   	public function index()
    {
    	// if(!$this->_sess->get('autenticado_front')){
     //        $this->redireccionar();
     //    }
    }
	
	/*public function index()
    {
        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }
        
       // echo 'cat: '.$this->_sess->get('_cat_tendencias');
       //  echo 'tipo: '.$this->_sess->get('_tipo_tendencias');         

         // $this->_sess->destroy('_cat_tendencias');
         // $this->_sess->destroy('_tipo_tendencias');

         // echo "<pre>";print_r($_SESSION);echo "</pre>";
        $_SESSION['_cat_tendencias'] = $this->filtros;

        $this->_view->datos = $this->homeGestion->traerTendencias();
        // $this->_view->datos = $this->homeGestion->traerTendencias($this->cantidadRegistros, $_SESSION['_cat_tendencias']);
        $this->_view->cantReg = home::contarRegistrosTendencias($_SESSION['_cat_tendencias']) / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
        $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front'));  
                
        // echo "<pre>";print_r($this->_view->banners_bottom);exit;

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index','tendencias', 'default');
    }


    public function paginacion()
    {

        if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 

                // echo "<pre>";print_r($_SESSION);echo "</pre>";

                $_pag = $_POST['p'];
                $_data = $this->homeGestion->paginadorTendencias($_pag, $this->cantidadRegistros, $_SESSION['_cat_tendencias']);      
                $proy='';
                
                foreach($_data as $datos){

                    $_img = home::traerDataImagenPorIdentificador($datos['identificador'],'tendencias');
                    if($_img !=''){                            
                        $_url_img = $this->_conf['base_url'] . 'public/img/subidas/tendencias/thumb/'. $_img->path;
                    }
                   
                    $_cat=array();
                    $_cat = explode(',', $datos['categorias']);       
                    $_arr_cat=array();
                    $_arr_label=array();
                    foreach ($_cat as $val) {
                      $_arr = home::traerCategoria($val);
                      $_arr_cat[] = $_arr['nombre']; 
                      $_arr_label[] = $_arr['clase']; 
                    }
                    $_cate = implode(', ', $_arr_cat);
                    $_fecha = explode('-', $datos['fecha']);
                    $_fecha = $_fecha[2].' '. home::convertirMes($_fecha[1]).' de '.$_fecha[0];

                    $proy .= '<a href="'.$this->_conf['url_enlace'].'tendencias/detalle/'.home::crearUrl($datos['id'],$datos['titulo']).'" class="grid-item '.$_arr_label[0].'">
                                <div class="img" style="background-image: url('.$_url_img.')"></div>
                                <h2>'.home::convertirCaracteres($datos['titulo']).'</h2>
                                <span><i class="fa fa-calendar-o" aria-hidden="true"></i>  '.$_fecha.'  |  '.$_arr_label[0].'</span>
                                <p>'.home::limitarTexto(home::convertirCaracteres(strip_tags($datos['bajada'])),50).'</p>
                            </a>';                   

                            
                
                            
                }

                echo $proy;
                exit;
            }
        }
            
        
    }*/

    

    public function _detalle($_id, $_titulo)
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $_id = (int) $_id;

        $this->_view->id_nota = $_id;
		$this->_view->seccion = 'eventos';

        // $_list = $this->homeGestion->traerListaComentarios($this->_view->seccion, $this->_view->id_nota);
        // echo"<pre>";print_r($_list);exit;

        $this->_view->datos = $this->homeGestion->traerEvento($_id);
        $this->_view->relacionadas = $this->homeGestion->traerEventosRelacionadas($_id);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];
		
	    if($this->_view->datos['categorias']!=''){
            $_cat = explode(',', $this->_view->datos['categorias']);        
            foreach ($_cat as $val) {
              $_arr = home::traerCategoria($val);
              $_arr_cat[] = $_arr['nombre']; 
            }
            $this->_view->categorias = implode(', ', $_arr_cat);
        }else{
            $this->_view->categorias = '';
        }

        if($this->_view->datos['tags']!=''){
        	$this->_view->tags = explode(',', $this->_view->datos['tags']);  
    	}

        $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(3, 1);
        $this->_view->banners_lateral = $this->homeGestion->traerBannerPorSeccion(3, 3);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(3, 2);
        // $this->_view->categorias = home::traerCategorias();
                
        // echo "<pre>";print_r($this->_view->banners_bottom);exit;
		// echo "<pre>";print_r($this->_view->datos);echo "</pre>";
		// echo "<pre>";print_r($_juego);echo "</pre>";
		// echo "<pre>";print_r($this->_view->datos);exit;

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('detalle','eventos', 'default');
    }


    public function detalle($_id, $_titulo)
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        $_id = (int) $_id;

        $this->_view->id_nota = $_id;
        $this->_view->seccion = 'eventos';

        // $_list = $this->homeGestion->traerListaComentarios($this->_view->seccion, $this->_view->id_nota);
        // echo"<pre>";print_r($_list);exit;

        $this->_view->datos = $this->homeGestion->traerEvento($_id);
        $this->_view->relacionadas = $this->homeGestion->traerEventosRelacionadas($_id);
        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];
        
        if($this->_view->datos['categorias']!=''){
            $_cat = explode(',', $this->_view->datos['categorias']);        
            foreach ($_cat as $val) {
              $_arr = home::traerCategoria($val);
              $_arr_cat[] = $_arr['nombre']; 
            }
            $this->_view->categorias = implode(', ', $_arr_cat);
        }else{
            $this->_view->categorias = '';
        }

        if($this->_view->datos['tags']!=''){
            $this->_view->tags = explode(',', $this->_view->datos['tags']);  
        }

        // $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(3, 1);
        // $this->_view->banners_lateral = $this->homeGestion->traerBannerPorSeccion(3, 3);
        // $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(3, 2);
        // $this->_view->categorias = home::traerCategorias();

        $this->_view->banners_top_dos = $this->homeGestion->traerBannerPorSeccionDos(3, 1);
        if($this->_view->banners_top_dos){
            for ($i=0; $i < count($this->_view->banners_top_dos); $i++) {         
                $this->_view->banners_top_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_top_dos[$i]['link']));
            }
        }
        
        $this->_view->banners_middle_dos = $this->homeGestion->traerBannerPorSeccionDos(3, 2);
        if($this->_view->banners_middle_dos){
             for ($i=0; $i < count($this->_view->banners_middle_dos); $i++) {         
                $this->_view->banners_middle_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_middle_dos[$i]['link']));
            }
        }
        
        $this->_view->banners_bottom_dos = $this->homeGestion->traerBannerPorSeccionDos(3, 3);
        if($this->_view->banners_bottom_dos){
             for ($i=0; $i < count($this->_view->banners_bottom_dos); $i++) {         
                $this->_view->banners_bottom_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_bottom_dos[$i]['link']));
            }
        }
                
        // echo "<pre>";print_r($this->_view->banners_bottom);exit;
        // echo "<pre>";print_r($this->_view->datos);echo "</pre>";
        // echo "<pre>";print_r($_juego);echo "</pre>";
        // echo "<pre>";print_r($this->_view->datos);exit;

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('detalle2','eventos', 'default');
    }


	
}


?>