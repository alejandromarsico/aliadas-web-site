<?php

use Nucleo\Controller\Controller;

class homeController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();

		$this->getLibrary('class.home');		
		$this->homeGestion = new home();	

        $this->cantidadRegistros = 8;   		
				
    }
    
   		
	
	/*public function index()
	{	
		$this->redireccionar('usuarios/login');		
    }*/
	

	public function __index()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
        $this->_view->destacados = $this->homeGestion->traerDestacados();
        $this->_view->novedades = $this->homeGestion->traerNovedades($this->cantidadRegistros );
        $this->_view->cantReg = home::contarRegistrosNovedades() / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
        // $this->_view->lanzamientos = $this->homeGestion->traerLanzamientosHome(12);
        $this->_view->lanzamientos = $this->homeGestion->traerLanzamientosHome2();
        $this->_view->eventos = $this->homeGestion->traerEventos();
        $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccionRandom(1, 1);
        $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(1, 2);

        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];

       
        // $_ten = home::traerDestPorTabla('tendencias');
        // $_ten = ($_ten!='') ? implode(',', $_ten) : '';

        // echo $_ten;
       
       // echo implode(',', $_cap);
        //echo "<pre>";print_r($_SESSION);echo "</pre>";
		
         // echo "<pre>";print_r($this->_view->banners_bottom_dos);exit;
		

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index','home', 'default');
    }


    public function index()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
        $this->_view->destacados = $this->homeGestion->traerDestacados();
        $this->_view->novedades = $this->homeGestion->traerNovedades($this->cantidadRegistros );
        $this->_view->cantReg = home::contarRegistrosNovedades() / $this->cantidadRegistros;
        $this->_view->cantReg = ceil($this->_view->cantReg); 
        // $this->_view->lanzamientos = $this->homeGestion->traerLanzamientosHome(12);
        $this->_view->lanzamientos = $this->homeGestion->traerLanzamientosHome2();
        $this->_view->eventos = $this->homeGestion->traerEventos();
        // $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccionRandom(1, 1);
        // $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(1, 2);

        $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
        $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
        $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
        $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social'];

        $this->_view->banners_top_dos = $this->homeGestion->traerBannerPorSeccionDos(1, 1);
        if($this->_view->banners_top_dos){
            for ($i=0; $i < count($this->_view->banners_top_dos); $i++) {         
                $this->_view->banners_top_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_top_dos[$i]['link']));
            }
        }
        
        $this->_view->banners_middle_dos = $this->homeGestion->traerBannerPorSeccionDos(1, 2);
        if($this->_view->banners_middle_dos){
             for ($i=0; $i < count($this->_view->banners_middle_dos); $i++) {         
                $this->_view->banners_middle_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_middle_dos[$i]['link']));
            }
        }
        
        $this->_view->banners_bottom_dos = $this->homeGestion->traerBannerPorSeccionDos(1, 3);
        if($this->_view->banners_bottom_dos){
             for ($i=0; $i < count($this->_view->banners_bottom_dos); $i++) {         
                $this->_view->banners_bottom_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_bottom_dos[$i]['link']));
            }
        }

        $this->_view->banners_modal = $this->homeGestion->traerBannerPorSeccionDos(1, 4);
        if($this->_view->banners_modal){
             for ($i=0; $i < count($this->_view->banners_modal); $i++) {         
                $this->_view->banners_modal[$i]['link'] = unserialize(base64_decode($this->_view->banners_modal[$i]['link']));
            }
        }
        // $_ten = home::traerDestPorTabla('tendencias');
        // $_ten = ($_ten!='') ? implode(',', $_ten) : '';

        // echo $_ten;
       
       // echo implode(',', $_cap);
        //echo "<pre>";print_r($_SESSION);echo "</pre>";
        
         // echo "<pre>";print_r($this->_view->banners_modal);exit;
        

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index2','home', 'default');
    }

    public function paginacion()
    {

        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 

                $_pag = $_POST['p'];
                $_data = $this->homeGestion->paginadorNovedades($_pag, $this->cantidadRegistros);      
                $proy='';
                
                foreach($_data as $datos){
                            
                   /* $proy .= '<div class="forum-item" style="float: left; margin-right: 10px;">
                                <div class="row">
                                    <div class="col-md-10">';

                                        $_img = home::traerDataImagenPorIdentificador($datos['identificador'],$datos['tabla']);
                                        if($_img !=''){
                                            if($datos['tabla']!='lanzamientos'){
                                                $proy .= '<img src="'. $this->_conf['base_url'] . 'public/img/subidas/'.$datos['tabla'].'/thumb/'. $_img->path.'" width="300">';
                                            }else{
                                                $proy .= '<img src="'. $this->_conf['base_url'] . 'public/img/subidas/'.$datos['tabla'].'/'. $_img->path.'" width="300">';
                                            }
                                        
                                        }
                                        
                                        $_cat = explode(',', $datos['categorias']);                                       
                                        $_arr_cat=array();
                                        foreach ($_cat as $val) {
                                          $_arr = home::traerCategoria($val);
                                          $_arr_cat[] = $_arr['nombre']; 
                                        }
                                        $_cate = implode(', ', $_arr_cat);
                                        
                                       $proy .= '<br><small>'.ucfirst($datos['tabla']).'</small>
                                        <br>
                                        <small><b>'.$_cate.'</b></small>

                                        <h4><strong>'.home::convertirCaracteres($datos['titulo']).'</strong></h4>  
                                        <a class="btn btn-warning" href="'. $this->_conf['url_enlace'].$datos['tabla'].'/detalle/'.home::crearUrl($datos['id'],$datos['titulo']).'">
                                               ver
                                        </a>                              
                                    </div>
                                </div>
                            </div>';*/

                    // $_img = home::traerDataImagenPorIdentificador($datos['identificador'],$datos['tabla']);
                    if($datos['tabla'] == 'tendencias'){
                        $_img = home::traerDataImagenPorIdentificador2($datos['identificador'],$datos['tabla']);
                    }else{
                        $_img = home::traerDataImagenPorIdentificador($datos['identificador'],$datos['tabla']);
                    }
                    if($_img !=''){

                         if(isset($datos['notas_viejas']) && $datos['notas_viejas'] != 'si'){
                            if($datos['tabla'] != 'lanzamientos'){  
                                $_url_img = $this->_conf['base_url'] . "public/img/subidas/".$datos['tabla']."/thumb/". $_img->path;
                            }else{
                                $_url_img = $this->_conf['base_url'] . "public/img/subidas/".$datos['tabla']."/". $_img->path; 
                            }
                        } else{

                            if($datos['tabla'] == 'lanzamientos'){  

                                if(file_exists($this->_conf['ruta_img_cargadas'].$datos['tabla'] . "/" . $_img->path)){
                                    $_url_img = $this->_conf['base_url'] . "public/img/subidas/".$datos['tabla']."/". $_img->path; 
                                }else{
                                    $_url_img = $this->_conf['base_url'] . "public/img/subidas/anteriores/images/". $_img->path;   
                                }


                            }else{
                                if(file_exists($this->_conf['ruta_img_cargadas'].$datos['tabla'] . "/thumb/" . $_img->path)){
                                    $_url_img = $this->_conf['base_url'] . "public/img/subidas/".$datos['tabla']."/thumb/". $_img->path;
                                }else{
                                    $_url_img = $this->_conf['base_url'] . "public/img/subidas/anteriores/images/". $_img->path;   
                                }
                            }                
                            
                        }



                        /*if($datos['tabla']!='lanzamientos'){
                            $_url_img = $this->_conf['base_url'] . 'public/img/subidas/'.$datos['tabla'].'/thumb/'. $_img->path;
                        }else{
                            $_url_img = $this->_conf['base_url'] . 'public/img/subidas/'.$datos['tabla'].'/'. $_img->path;
                        }*/
                    
                    }

                    $_cat=array();
                    $_cat = explode(',', $datos['categorias']);       
                    $_arr_cat=array();
                    $_arr_label=array();
                    foreach ($_cat as $val) {
                      $_arr = home::traerCategoria($val);
                      $_arr_cat[] = $_arr['nombre']; 
                      $_arr_label[] = $_arr['clase']; 
                    }
                    $_cate = implode(', ', $_arr_cat);
                    if($datos['tabla'] != 'lanzamientos'){
                        $_link = $this->_conf['url_enlace'].$datos['tabla'].'/detalle/'.home::crearUrl($datos['id'],$datos['titulo']);
                    }else{
                        $_link ='#';
                    }

                    $_fecha = explode('-', $datos['fecha']);
                    $_fecha = $_fecha[2].' '. home::convertirMes($_fecha[1]).' de '.$_fecha[0];

                    $proy .= '<a href="'.$_link.'" class="'.$_arr_label[0].'">
                                <div class="img" style="background-image: url('.$_url_img.')"></div>
                                <h2>'.home::convertirCaracteres($datos['titulo']).'</h2>
                                <span><i class="fa fa-calendar-o" aria-hidden="true"></i> '.$_fecha.'  |  '.$_cate.'</span>
                                <p>'.home::cortarTexto(home::convertirCaracteres(strip_tags($datos['bajada'])),150).'</p>
                            </a>';

                            
                }

                echo $proy;
                exit;
            }
        }
            
        
    }

    public function traerNotasCat()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){


                //$this->_view->data = $_POST;

                
                $_data = $this->homeGestion->traerBuscadorCategorias($_POST['valor']);
               /* if($_data){

                    $_html='';
                    foreach ($_data as $key => $val) {
                        $_html .= '<h4>'.$key.'</h4>';
                        for ($i=0; $i < count($val); $i++) { 
                        	$_dat = $val[$i]['tabla'].'_'.$val[$i]['id'];
                            $_html .= '<a class="bt_preview" href="javascript:void(0);" onclick="cat_prev(\''.$_dat.'\')">'.$val[$i]['titulo'].'</a><br>';
                        }
                        $_html .= '<a href="'.$this->_conf['base_url'].$key.'">ver mas</a>';

                    }
                    $_html .='<div id="contenido_cat_preview"></div>';
                }else{
                     $_html='No hay contenidos';
                }               
*/

                // echo "<pre>";print_r($_data);exit;

                if($_data){

                    $_html='';
                    foreach ($_data as $key => $val) {
                        $_html .= '<div class="box">';
                        if($key=='lanzamientos'){
                            $_html .= '<h4>'.ucfirst($key).'</h4>';
                        }
                        
                        for ($i=0; $i < count($val); $i++) { 
                            $_dat = $val[$i]['tabla'].'_'.$val[$i]['id'];
                            if($key=='lanzamientos'){
                                 $_html .= '<a class="bt_preview" href="javascript:void(0);">'.$val[$i]['titulo'].'</a>';
                            }else{
                               $_html .= '<a class="bt_preview" href="javascript:void(0);" onclick="cat_prev(\''.$_dat.'\')">'.$val[$i]['titulo'].'</a>'; 
                            }
                        }
                        if($key=='lanzamientos'){
                            $_html .= '<a class="btn-mas" href="'.$this->_conf['base_url'].$key.'">Ver más</a>';
                        }
                        $_html .='</div>';

                    }



                     $_html .='<div id="contenido_cat_preview" class="mid"></div>';
                }else{
                     $_html='No hay contenidos';
                }               

               /*<div class="box">
              <h4>Lanzamientos</h4>

              <a href="#">GIESSO ESENCIA HOMBRE</a>
              <a href="#">We Rock! FOR MEN</a>
              <a href="#">Fragancias Rihanna</a>
              <a href="#">Shakira Dream</a>

              <a href="#" class="btn-mas">Ver más ></a>

            </div>*/
               

                echo $_html;
                exit;

            }

        }

    }

	
    public function traerNotasCatPreview()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }

        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){


               // $this->_view->data = $_POST;

                $val = home::traerNota($_POST['id'], $_POST['tabla']);

                // echo "<pre>";print_r($val);exit;

                if($val){

                    $_html='';
                    // foreach ($_data as $val) {

                    // $_img = home::traerDataImagenPorIdentificador($val['identificador'],$_POST['tabla']);
                    if($_POST['tabla'] == 'tendencias'){
                        $_img = home::traerDataImagenPorIdentificador2($val['identificador'],$_POST['tabla']);
                    }else{
                        $_img = home::traerDataImagenPorIdentificador($val['identificador'],$_POST['tabla']);
                    }
                    if($_img !=''){
                        if($_POST['tabla']!= 'lanzamientos'){
                            $_html .= '<div class="image_news" style="background-image: url('.$this->_conf['base_url'] . 'public/img/subidas/'.$_POST['tabla'].'/thumb/'. $_img->path.')"></div>';
                            // $_html .= '<img src="'.$this->_conf['base_url'] . 'public/img/subidas/'.$_POST['tabla'].'/thumb/'. $_img->path.'">';
                        }else{
                            $_html .= '<div class="image_news" style="background-image: url('.$this->_conf['base_url'] . 'public/img/subidas/'.$_POST['tabla'].'/'. $_img->path.')"></div>';
                            // $_html .= '<img src="'.$this->_conf['base_url'] . 'public/img/subidas/'.$_POST['tabla'].'/'. $_img->path.'">';
                        }
                        
                    }  
                    $_html .= '<div class="copy">';                     
                    
                    $_cat = explode(',', $val['categorias']);                        
                    $_arr_cat = array();
                    foreach ($_cat as $value) {
                      $_arr = home::traerCategoria($value);
                      $_arr_cat[] = $_arr['nombre']; 
                    }
                    $_cate = implode(', ', $_arr_cat);
                    
                   $_html .= '<h3>'.home::convertirCaracteres($val['titulo']).'</h3>'; 
                   $_html .= '<h5>'.$_cate.'</h5>';

                   if($_POST['tabla']!= 'lanzamientos'){
                            $_html .= '<p>'.home::cortarTexto(home::convertirCaracteres(strip_tags($val['bajada'])),150).'</p>';                            
                        }else{
                            $_html .= '<p>'.home::cortarTexto(home::convertirCaracteres(strip_tags($val['descripcion'])),150).'</p>';
                        } 
                    
                    if($_POST['tabla']== 'tutoriales'){
                        $_html .= '<a class="btn-mas" href="'. $this->_conf['url_enlace'].'videos/detalle/'.home::crearUrl($val['id'],$val['titulo']).'">continuar leyendo</a>';
                    }elseif($_POST['tabla']!= 'lanzamientos'){
                        $_html .= '<a class="btn-mas" href="'. $this->_conf['url_enlace'].$_POST['tabla'].'/detalle/'.home::crearUrl($val['id'],$val['titulo']).'">continuar leyendo</a>';
                    }else{
                        $_html .= '<a class="btn-mas" href="'. $this->_conf['url_enlace'].$_POST['tabla'].'/#item_'.$val['id'].'">continuar leyendo</a>';
                    }
                           
                    $_html .= '</div>'; 


                }else{
                     $_html='No hay contenidos';
                }               

               /*<div class="image_news" style="background-image: url(<?php echo $_params['ruta_img']?>img-menu@2x.jpg)"></div>
            <div class="copy">
              <h3>Fragancias: ¿qué se espera  para este otoño?</h3>
              <h5>TENDENCIA fragancia</h5>
              <p>Todavía hay muchas mujeres que permanecen fieles a uno o dos perfumes durante todo el año y pasan por alto el cambio estacional. Un buen asesoramiento puede</p>
              <a href="#" class="btn-mas">Continuar leyendo ></a>
            </div>*/

                echo $_html;
                exit;

            }

        }

    }
	
	
}
?>