<?php

use Nucleo\Controller\Controller;

class errorController extends Controller
{
    public function __construct()
    {
        parent::__construct();
		$this->getLibrary('class.validador');
    }
    
    public function index()
    {        
        $this->redireccionar('usuarios/login/cerrar'); 
    }
    
    public function access($codigo = false)
    {        
        $this->redireccionar('usuarios/login/cerrar'); 
    }
    
    private function _getError($codigo = false)
    {
        if($codigo){
            $codigo = validador::filtrarInt($codigo);
            if(is_int($codigo))
                $codigo = $codigo;
        }else{
            $codigo = 'default';
        }        
        
        $error['default'] = 'Ha ocurrido un error y la página no puede mostrarse';
        $error['5050'] = 'Acceso restringido!';
        $error['8080'] = 'Tiempo de la sesion agotado';
		$error['9090'] = 'ERROR: Falta un parametro en la URL';
        
        if(array_key_exists($codigo, $error)){
            return $error[$codigo];
        }else{
            return $error['default'];
        }
    }
}