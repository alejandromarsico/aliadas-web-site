<?php

use Nucleo\Controller\Controller;

class tagsController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('class.home');		
		$this->homeGestion = new home();

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();

        // $this->cantidadRegistros = 6;  
			
				
    }
    
   		
	
	public function _index()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        // $this->_view->datos = $this->homeGestion->traertags();


        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 

                // echo "<pre>";print_r($_POST);exit;
            
                $_val = $_POST['buscador'];
                
                $_datos = $this->homeGestion->traerBuscadorTags(ucwords(strtolower($_val)));
                $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
                $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
                $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
                $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
                $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
                $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social']; 
                // $this->_view->datos = $this->homeGestion->traerClubaliadas($this->cantidadRegistros);
                // $this->_view->cantReg = home::contarRegistrosClubaliadas() / $this->cantidadRegistros;
                // $this->_view->cantReg = ceil($this->_view->cantReg); 
                
                // echo "<pre>";print_r($_datos);exit;

                if($_datos ){
                    $_data=array();
                    for ($i=0; $i < count($_datos) ; $i++) { 
                       $_data[$i] = home::traerNota($_datos[$i]['id'], $_datos[$i]['tabla']);
                       $_data[$i]['tabla'] = $_datos[$i]['tabla'];
                       // $this->_view->datos = array_merge($_datos[$i], $_data)
                    }
                  
                    $this->_view->datos = $_data;


                }else{
                    $this->_view->datos = '';
                }
                
                
                 // echo "<pre>";print_r($_datos);exit;

                /*echo "<pre>";print_r($this->_view->users);echo"</pre>";*/
                
                /*$_html = '';
                foreach($_datos as $data){ 

                    if($data['tabla'] == 'tendencias' || $data['tabla'] == 'capacitaciones'){
                        $_link = '<a class="btn btn-warning btn-round" href="'.$this->_conf['url_enlace'].$data['tabla'].'/detalle/'. home::crearUrl($data['id'],$data['titulo']).'">ver</a>&nbsp;&nbsp;';
                    }else{
                        $_link = '<a class="btn btn-warning btn-round" href="'.$this->_conf['url_enlace'].$data['tabla'].'/#item_'.$data['id'].'">ver</a>&nbsp;&nbsp;';
                    }
                    
                    $_html .='<div class="forum-item">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h4>'.$data['titulo'].'</h4>
                                    </div>
                                    <div class="col-md-2 forum-info">
                                        <div class="tooltip-demo pull-right">'.$_link.'</div>
                                    </div>
                                </div>
                            </div>';
                    
            
                }           
                
                echo $_html;
                exit;*/

            }else{
                $this->redireccionar('error/access/404');
                // echo "error de busqueda";
                // exit;
            }
            
        } 
       	
		

		$this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index','tags', 'default');
    }


    public function index()
    {
        // if(!$this->_sess->get('autenticado_front')){
        //     $this->redireccionar();
        // }
        
       // $this->_view->setCssPlugin(array('dropzone.min'));
       // echo "<pre>";print_r($_SESSION);echo "</pre>"; 

        // $this->_view->datos = $this->homeGestion->traertags();


        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 

                // echo "<pre>";print_r($_POST);exit;
            
                $_val = $_POST['buscador'];
                
                $_datos = $this->homeGestion->traerBuscadorTags(ucwords(strtolower($_val)));
                // $this->_view->banners_top = $this->homeGestion->traerBannerPorSeccion(2, 1);
                // $this->_view->banners_bottom = $this->homeGestion->traerBannerPorSeccion(2, 2);
                $this->_view->data_user = $this->homeGestion->traerUser($this->_sess->get('id_usuario_front')); 
                $this->_view->cliente = home::traerClientePorUsers($this->_view->data_user['id_cliente']);
                $this->_view->data_user['numero_cliente'] = $this->_view->cliente['numero_cliente'];  
                $this->_view->data_user['razon_social'] = $this->_view->cliente['razon_social']; 
                // $this->_view->datos = $this->homeGestion->traerClubaliadas($this->cantidadRegistros);
                // $this->_view->cantReg = home::contarRegistrosClubaliadas() / $this->cantidadRegistros;
                // $this->_view->cantReg = ceil($this->_view->cantReg); 

                $this->_view->banners_top_dos = $this->homeGestion->traerBannerPorSeccionDos(2, 1);
                if($this->_view->banners_top_dos){
                    for ($i=0; $i < count($this->_view->banners_top_dos); $i++) {         
                        $this->_view->banners_top_dos[$i]['link'] = unserialize(base64_decode($this->_view->banners_top_dos[$i]['link']));
                    }
                } 
                
                // echo "<pre>";print_r($_datos);exit;

                if($_datos ){
                    $_data=array();
                    for ($i=0; $i < count($_datos) ; $i++) { 
                       $_data[$i] = home::traerNota($_datos[$i]['id'], $_datos[$i]['tabla']);
                       $_data[$i]['tabla'] = $_datos[$i]['tabla'];
                       // $this->_view->datos = array_merge($_datos[$i], $_data)
                    }
                  
                    $this->_view->datos = $_data;


                }else{
                    $this->_view->datos = '';
                }
                
                
                 // echo "<pre>";print_r($_datos);exit;

                /*echo "<pre>";print_r($this->_view->users);echo"</pre>";*/
                
                /*$_html = '';
                foreach($_datos as $data){ 

                    if($data['tabla'] == 'tendencias' || $data['tabla'] == 'capacitaciones'){
                        $_link = '<a class="btn btn-warning btn-round" href="'.$this->_conf['url_enlace'].$data['tabla'].'/detalle/'. home::crearUrl($data['id'],$data['titulo']).'">ver</a>&nbsp;&nbsp;';
                    }else{
                        $_link = '<a class="btn btn-warning btn-round" href="'.$this->_conf['url_enlace'].$data['tabla'].'/#item_'.$data['id'].'">ver</a>&nbsp;&nbsp;';
                    }
                    
                    $_html .='<div class="forum-item">
                                <div class="row">
                                    <div class="col-md-10">
                                        <h4>'.$data['titulo'].'</h4>
                                    </div>
                                    <div class="col-md-2 forum-info">
                                        <div class="tooltip-demo pull-right">'.$_link.'</div>
                                    </div>
                                </div>
                            </div>';
                    
            
                }           
                
                echo $_html;
                exit;*/

            }else{
                $this->redireccionar('error/access/404');
                // echo "error de busqueda";
                // exit;
            }
            
        } 
        
        

        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('index2','tags', 'default');
    }

    public function traerTags()
    {
        /*if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }*/
        
        if($_POST){

            if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){ 
            
                $_val = $_POST['key'];
                $_prod ='';
                $_prod  = $this->homeGestion->traerTags($_val);       

                 // echo "<pre>";print_r($_prod);exit;      
                if($_prod){
                
                    $_html = '';
                    foreach($_prod as $prod){ 
                        
                        $_html .= '<div><a class="suggest-element" data="'.admin::convertirCaracteres($prod['nombre']).'" id="'.$prod['id'].'">'.admin::convertirCaracteres($prod['nombre']).'</a></div>';
                
                    }           
                }else{
                    $_html = 'No hay resultados';
                }
                
                echo $_html;
                exit;

            }else{
                $this->redireccionar('error/access/404');
            }
            
        }
    }

    public function buscar($_valor)
    {
        /*if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }*/
        

        
        $this->_view->datos = $this->homeGestion->traerBuscadorTags(ucwords(strtolower($_valor)));        
        
        
        $this->_view->titulo = 'Aliadas';
        $this->_view->renderizar('buscar','tags', 'default');

        
            
        
    }

   


	
}


?>