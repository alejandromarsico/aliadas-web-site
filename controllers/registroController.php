<?php

use Nucleo\Controller\Controller;

class registroController extends Controller
{
		
    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('AntiXSS');
		$this->_xss = new AntiXSS();

		$this->getLibrary('PHPMailerAutoload');
        $this->envioMail = new PHPMailer();				
				
    }
    
   		
	
	/*public function index()
	{	
		$this->redireccionar('usuarios/login');		
    }*/
	

	public function index()
    {
        if($this->_sess->get('autenticado_front')){
            $this->redireccionar('home');
        }
        
        $this->_view->titulo = 'Resgistro';
        //echo "<pre>";print_r($_SESSION);echo "</pre>";
		
		if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
			
				if(validador::getInt('enviar') == 1){
					
					
					$this->_view->datos = $_POST;


					
					if(!validador::numerico($this->_view->datos['numero_cliente'])){
						$this->_view->_error = 'Debe introducir un numero de cliente valido';
						$this->_view->renderizar('index','registro', 'login');
						exit;
					}

					// if(!validador::getAlphaNum('usuario')){
					if(!validador::validarEmail($this->_view->datos['email'])){
						$this->_view->_error = 'Debe introducir un email valido';
						$this->_view->renderizar('index','registro', 'login');
						exit;
					}
					
					//if(!validador::getSql('pass',$this->_conf['baseDatos'])){
					if(!validador::getAlphaNum('pass')){					
						$this->_view->_error = 'Debe introducir su password';
						$this->_view->renderizar('index','registro', 'login');
						exit;
					}
									
					
					
					// 'find' si se busca un solo registro, 'all' si se busca solo 1
					$row = contenidos_user::find(array('conditions' => array('numero_cliente = ?', $this->_view->datos['numero_cliente'])));
										
					if(!$row){
						$this->_view->_error = 'El numero de cliente no existe';
						$this->_view->renderizar('index','registro', 'login');
						exit;
					}else{

						$row->email = $this->_xss->xss_clean($this->_view->datos['email']);
						// $row->password = $this->_xss->xss_clean($this->_view->datos['pass']);
						$row->password = Hash::getHash('md5', $this->_xss->xss_clean($this->_view->datos['pass']), $this->_conf['hash_key']);
						$row->save();

						$this->_sess->set('autenticado_front', true);
						$this->_sess->set('usuario_front', $row->nombre);
						$this->_sess->set('id_usuario_front', $row->id);
					
						$this->redireccionar('home');


					}
					
										
					
				}

			}else{
				//$this->redireccionar('error/access/404');
				$this->_view->_error = 'Hubo un error, vuelva a intentarlo mas tarde.';
				// $this->_view->renderizar('index','login');
				// exit;
			}
		}

        $this->_view->renderizar('index','registro', 'login');
    }
    


    public function traerNumeroCliente()
    {

    	if($_POST){

    		if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){

	    		$_cliente = $_POST['cliente'];

	    		$row = contenidos_cliente::find(array('conditions' => array('numero_cliente = ?', $_cliente)));
	    		if($row){
	    			/*$roww = contenidos_user::find(array('conditions' => array('id_cliente = ?', $row->id)));
	    			if($roww){
	    				if($roww->password!=''){
		    				$jsondata['status'] = 'existe';
		    				echo json_encode($jsondata);
							exit;
		    			}else{
		    				$jsondata['status'] = 'ok';
		    				$jsondata['nombre'] = $roww->nombre;
		    				echo json_encode($jsondata);
							exit;
		    			}
	    			}else{
	    				$jsondata['status'] = 'ok';
	    				$jsondata['nombre'] = '';
	    				echo json_encode($jsondata);
						exit;
	    			}*/
	    			
	    			$jsondata['status'] = 'ok';
    				$jsondata['nombre'] = '';
    				echo json_encode($jsondata);
					exit;

					
				}else{
					$jsondata['status'] = 'no';
					echo json_encode($jsondata);
					exit;
				}
			}

    	}

    }

    public function registrarDatos()
    {
    	if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
			
				if(validador::getInt('enviar') == 1){
					
					
					$this->_view->datos = $_POST;

					// echo "<pre>";print_r($_POST);exit;
					
					if(!validador::numerico($this->_view->datos['numero_cliente'])){
						/*$this->_view->_error = 'Debe introducir un numero de cliente valido';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'Debe introducir un numero de cliente valido';
						exit;
					}

					if(!validador::getPostParam('name')){
						/*$this->_view->_error = 'Debe introducir un numero de cliente valido';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'Debe introducir un nombre';
						exit;
					}


					// if(!validador::getAlphaNum('usuario')){
					if(!validador::validarEmail($this->_view->datos['email'])){
						/*$this->_view->_error = 'Debe introducir un email valido';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'Debe introducir un email valido';
						exit;
					}
					
					//if(!validador::getSql('pass',$this->_conf['baseDatos'])){
					if(!validador::getAlphaNum('pass')){					
						/*$this->_view->_error = 'Debe introducir su password';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'Debe introducir su contraseña';
						exit;
					}


					if($this->_view->datos['pass']!= $this->_view->datos['repeat_pass']){					
						/*$this->_view->_error = 'Debe introducir su password';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'Contraseña y repetir contraseña no coinciden';
						exit;
					}
									
					
					
					$row = contenidos_cliente::find(array('conditions' => array('numero_cliente = ?', $this->_view->datos['numero_cliente'])));
										
					if(!$row){
						/*$this->_view->_error = 'El numero de cliente no existe';
						$this->_view->renderizar('index','registro');
						exit;*/

						echo 'El numero de cliente no existe';
						exit;

					}else{

						$roww = contenidos_user::find(array('conditions' => array('nombre = ? AND email = ?', $this->_xss->xss_clean(validador::getTexto('name')), $this->_xss->xss_clean(validador::getTexto('email')))));
		    			if($roww){
		    				
		    				echo 'Ya hay un usuario registrado con este email y nombre';
							exit;

		    			}else{

		    				$_mes = ($this->_view->datos['month']<10) ? '0'.$this->_view->datos['month']: $this->_view->datos['month'];
							$_dia = ($this->_view->datos['day']<10) ? '0'.$this->_view->datos['day']: $this->_view->datos['day'];

							$user = new contenidos_user();
							$user->id_cliente = $row->id;
							$user->nombre = $this->_xss->xss_clean(validador::getTexto('name'));						
							$user->email = $this->_xss->xss_clean($this->_view->datos['email']);							
							// $user->password = $this->_xss->xss_clean($this->_view->datos['pass']);
							$user->password = Hash::getHash('md5', $this->_xss->xss_clean($this->_view->datos['pass']), $this->_conf['hash_key']);							
							$user->fecha_nacimiento = $_dia.'/'.$_mes.'/'.$this->_view->datos['year'];
							$user->fecha = date('Y-m-d');
							$user->save();

							$this->_sess->set('autenticado_front', true);
							$this->_sess->set('usuario_front', $user->nombre);
							$this->_sess->set('id_usuario_front', $user->id);

							echo 'ok';
							exit;
		    				
		    			}

						
					


					}
					
										
					
				}

			}else{
				//$this->redireccionar('error/access/404');
				$this->_view->_error = 'Hubo un error, vuelva a intentarlo mas tarde.';
				// $this->_view->renderizar('index','login');
				// exit;
			}
		}

    }

    public function recuperar()
    {
        if($this->_sess->get('autenticado_front')){
            $this->redireccionar('home');
        }
        
      
		$this->_view->titulo = 'Recuperar contraseña';
        $this->_view->renderizar('recuperar','registro', 'login');
    }
	
	public function recuperarPass()
    {

    	if($_POST){

			if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
			
			
				if(validador::getInt('enviar') == 1){
					
					
					$this->_view->datos = $_POST;
					
					// if(!validador::getAlphaNum('usuario')){
					if(!validador::validarEmail($this->_view->datos['email'])){						
						echo 'Debe introducir un email valido';
						exit;
					}
													
					
					
					// 'find' si se busca un solo registro, 'all' si se busca solo 1
					$row = contenidos_user::find(array('conditions' => array('email = ?', $this->_xss->xss_clean($this->_view->datos['email']))));					
					
					if(!$row){						
						echo 'El usuario no esta registrado';
						exit;
					}else{

						// $_token = md5(uniqid($this->_view->datos['email'], true));
						$_pretoken = uniqid();
						$_token = Hash::getHash('md5', $_pretoken, $this->_conf['hash_key']);
						$reg = contenidos_user::find(array('conditions' => array('password = ?', $_token)));
						if(!$reg){
							$row->password = Hash::getHash('md5', $_token, $this->_conf['hash_key']);
							$row->save();


							// mail admin
				            $_body='<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Transitional//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-transitional.dtd">
				                    <html xmlns="http://www.w3.org/1999/xhtml">
				                    <head>
				                    <meta http-equiv="Content-Type" content="text/html; charset=UTF-8" />
				                    <title>Aliadas</title>
				                    </head>                 
				                    <body>
				                    <p><strong>ALIADAS - recuperar de contraseña -</strong></p>
				                    <p>Su nueva contraseña es <strong>'.$_token.'</strong></p>
				                    </body>
				                    </html>';
				            
				            // $this->envioMail->IsSMTP();
				            // $this->envioMail->SMTPAuth = true;
				            // $this->envioMail->Host = "smtphub.cencosud.cl";
				            // $this->envioMail->Username = "_MailCarteleria"; 
				            // $this->envioMail->Password = "a23uj8rs"; 
				            // $this->envioMail->Port = 25;    
				            $this->envioMail->IsSMTP();
			                $this->envioMail->SMTPAuth = true;
			                $this->envioMail->Host = "mail.alejandromarsico.com.ar";
			                $this->envioMail->Username = "_mainaccount@alejandromarsico.com.ar"; 
			                $this->envioMail->Password = "alem1984"; 
			                $this->envioMail->Port = 465;
			                $this->envioMail->SMTPSecure = 'ssl';

				            $this->envioMail->From ='info@aliadas.com.ar';
				            $this->envioMail->FromName ='Aliadas';
				            $this->envioMail->Subject = 'Recuperar '.utf8_decode('contraseña');               
				            $this->envioMail->Body = $_body;
				            $this->envioMail->AddAddress($this->_view->datos['email']);            
				            //$this->envioMail->AddAddress('alejandromarsico@gmail.com');
				            $this->envioMail->IsHTML(true); 
				            
				            $exito = $this->envioMail->Send();
				            
				            $intentos=1;
				            
				            while ((!$exito) && ($intentos < 3)) {
				                sleep(5);           
				                $exito = $this->envioMail->Send();              
				                $intentos=$intentos+1;          
				            }
				            
				            if(!$exito) {           
				                echo "Problemas enviando correo electrónico a ".$this->envioMail->ErrorInfo;
				                exit;               
				            }

				            echo 'ok';
							exit;

						}else{
							echo 'Hubo un error, vuelva a intentarlo.';
							exit;
						}	


					}
					
					
				}

			}else{				
				echo 'Hubo un error, vuelva a intentarlo mas tarde.';
				exit;
			}
		}

    }
	
	
}
?>