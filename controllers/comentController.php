<?php

use Nucleo\Controller\Controller;

class comentController extends Controller
{
	public $homeGestion;

    public function __construct()
	{
        parent::__construct();

        $this->getLibrary('class.validador');	

        $this->getLibrary('class.home');		
		$this->homeGestion = new home();			
				
    }
    
   		
	
	public function index()
	{	
		if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }
        
		
		$this->_view->id_nota = '';
		$this->_view->seccion = 'lanzamientos';

		$this->_view->titulo = 'comentarios';
        $this->_view->renderizar('index', 'coment', 'default');		
    }
	
	public function coment_list()
	{			

		if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }
        
        if($_POST){

	        if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
				
				$_list = $this->homeGestion->traerListaComentarios($_POST['seccion'], $_POST['nota']);

				// echo"<pre>";print_r($_list);exit;

				echo json_encode($_list);
				exit;

	       	}
       	}
		
		
		

		/*require_once ("db.php");

		$sql = "SELECT * FROM tbl_comment ORDER BY parent_comment_id asc, comment_id asc";

		$result = mysqli_query($conn, $sql);
		$record_set = array();
		while ($row = mysqli_fetch_assoc($result)) {
		    array_push($record_set, $row);
		}
		mysqli_free_result($result);

		mysqli_close($conn);
		echo json_encode($record_set);*/

    }
	
	public function coment_add()
	{	

		if(!$this->_sess->get('autenticado_front')){
            $this->redireccionar();
        }
        
		if($_POST){

	        if(validador::getPostParam('_csrf') == $this->_sess->get('_csrf')){
				
				$commentId = $_POST['comment_id'];
				$comment =  $_POST['comment'];
				$user =  $_POST['name'];
				$id_user =  $_POST['id_user'];
				$id_nota =  $_POST['id_nota'];
				$seccion =  $_POST['seccion'];
				$date = date('Y-m-d H:i:s');


				$_add = $this->homeGestion->addComentarios($commentId, $comment, $user, $id_user, $id_nota, $seccion, $date);
				echo $_add;
				exit;

	       	}
       	}
		// require_once ("db.php");
		/*$commentId = $_POST['comment_id'];
		$comment =  $_POST['comment'];
		$commentSenderName =  $_POST['name'];
		$date = date('Y-m-d H:i:s');*/

		// echo"<pre>";print_r($_POST);exit;

		
		
		/*$cat = new contenidos_comentario();
		$cat->id_comentario_padre = $commentId;		
		$cat->comentario = $comment;
		$cat->usuario = $commentSenderName;
		$cat->fecha = $date;
		$cat->save();


		echo true;
		exit;*/

		/*$sql = "INSERT INTO tbl_comment(parent_comment_id,comment,comment_sender_name,date) VALUES ('" . $commentId . "','" . $comment . "','" . $commentSenderName . "','" . $date . "')";

		$result = mysqli_query($conn, $sql);

		if (! $result) {
		    $result = mysqli_error($conn);
		}
		echo $result;	*/
    }



    /*public function comentar2()
	{	
		
		//$_asig = $this->homeGestion->traerAsigAlta($_totem);

		$this->_view->titulo = 'comentarios';
        $this->_view->renderizar('index2', 'coment');		
    }


    public function comentar()
    {
    	if ($_POST) {
			$autor_id = $_POST['autor_id'];
			$post_id = $_POST['post_id'];
			$comentario = $_POST['comentario'];
			$comentario = trim($comentario);
			$comentario = htmlentities($comentario);
			$date = date('Y-m-d H:i:s');
			
			// $tabla = "wk_comentarios";
			// $consulta = mysql_query("INSERT INTO $tabla (autor_id,post_id,comentario,fecha) VALUES('$autor_id','$post_id','$comentario',NOW())");

			$consulta = $this->homeGestion->addComentarios2($autor_id, $post_id, $comentario, $date);

			if ($consulta) {
				echo $autor_id." dijo: <br> ".$comentario;
				exit();
			}

		}
    }*/
}

?>