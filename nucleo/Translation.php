<?php

namespace Nucleo\Translation;

class Translation
{
	public function establecerIdioma()
	{
		// primero, buscamos la cookie del idioma
		if(isset($_COOKIE['idioma'])){
			return $_COOKIE['idioma'];
		}else{
			setcookie( "idioma", 1, time() + (60 * 60 * 24 * 30), "/");
			return (isset($_COOKIE['idioma'])) ? $_COOKIE['idioma'] : 1;
		}
	}	
}