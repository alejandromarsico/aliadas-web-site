<?php

namespace Nucleo\Configuracion;

require 'configuracion' . DS . 'baseDatos' . DS . 'baseDatos.php';
require 'configuracion' . DS . 'traslate' . DS . 'traslate.php';
require 'configuracion' . DS . 'widgets' . DS . 'widgets.php';
require 'configuracion' . DS . 'baseUrl' . DS . 'baseUrl.php';
require 'configuracion' . DS . 'urlEnlace' . DS . 'urlEnlace.php';

class Configuracion
{	
	public function __construct() {
		
		//$this->_lang = $_lang;
		/*if(!$this->_sess->get('lang')){
			$this->_sess->set('lang', 'es');
		}*/
	
	}
	
	public static function datos()
	{
		return array(
			// Sitio
			// Version Apache mínima requerida
			'version_php'				=> '5.3',
			
			// Raiz real del sitio
			'raiz'						=> RAIZ,
			// Definicion de modulos.
			// Aqui se colocan los modulos activos actuales de la aplicacion
			'modulos'					=> array('administrador','usuarios'),
			
			// Definimos la url que sera la base del ruteo en la aplicacion
			'base_url' 					=> \BaseUrl\BaseUrl::data(),
			
			// Definimos la url que servira de enlace en las vistas
			'url_enlace' 				=> \UrlEnlace\UrlEnlace::data(),
			
			// Extension que se transfiere a los archivos finales
			'extension'					=> '.html',
			
			'errores'					=> array(
											'acceso' => array(
															'autenticado' => '5050', // acceso autenticado
															'nivel' => '6060', // acceso de nivel
															'extricto' => '7070',
															'tiempo' => '8080',
														),
											),
			
			// Se define el controlador por defecto de la aplicacion.
			'default_controller'	 	=> 'index',
			
			// Se define el Layout que se cargara por defecto.
			'layout_principal'			=> 'default',
											
			// Se definen los niveles de usuario y sus layout correspondientes
			'roles'					=> array(
										 1 => 'admin',
										 2 => 'admin',
										 3 => 'admin'
										),
			
			// Se establece el nombre, slogan y la compania de la aplicacion
			'app_nombre'				=> '',
			'app_slogan'				=> 'Administrador',
			'app_compania'				=> '',
			
			// Idioma por default
			'idioma'					=> 1,
			
			// Tiempo por defecto de la sesiones
			'tiempo_sesion' 			=> '60',
			
			// Hash Key para la encriptacion en md5. Se puede modificar.
			'hash_key'					=> '4f6a6d832be79',
			
			
			// Idioma
			'traslation' 				=> \Traslate\Traslate::data(),
			
			
			
			// cache de elementos
			'ruta_cache'				=> array(
											'noticias' => 'tmp/cache/noticias/',
											'slider' => 'tmp/cache/slider/',
											'home' => 'tmp/cache/home/',
											'widgets' => array(
															'menus' => 'tmp/cache/widgets/menus/',	
												),
											),
			
			// Accesos de la base de datos primaria	
			'baseDatos'				    =>	\BaseDatos\BaseDatos::data(),
			
			// Directorios de modelos
			'directorio_modelos'		=> 'modelos', 
			
			// Datos para la construccion del sitio			
			// peso imagenes
			'peso_maximo_imagenes' 			=> '2', //MB

			'peso_maximo_videos' 			=> '24', //MB

			'peso_maximo_archivos' 			=> '10', //MB
									
			// Tamaño de imagenes cargadas
			'formatos_img'				=> array(
											'lanzamientos' => array(
																'ancho' => 796,
																'alto' 	=> 796,
															),
											'tendencias' => array(
																'ancho' => 1024,
																'alto' 	=> 576,
															),
											'tendencias_thumb' => array(
																'ancho' => 796,
																'alto' 	=> 448,
															),
											'tendencias_desktop' => array(
																'ancho' => 1024,
																'alto' 	=> 576,
															),
											'tendencias_mobile' => array(
																'ancho' => 500,
																'alto' 	=> 500,
															),
											'clubaliadas' => array(
																'ancho' => 500,
																'alto' 	=> 500,
															),
											'capacitaciones' => array(
																'ancho' => 1024,
																'alto' 	=> 576,
															),
											'capacitaciones_thumb' => array(
																'ancho' => 796,
																'alto' 	=> 448,
															),
											'tutoriales' => array(
																'ancho' => 1024,
																'alto' 	=> 576,
															),
											'tutoriales_thumb' => array(
																'ancho' => 796,
																'alto' 	=> 448,
															),
											'linearios' => array(
																'ancho' => 1024,
																'alto' 	=> 576,
															),
											'linearios_thumb' => array(
																'ancho' => 796,
																'alto' 	=> 448,
															),
											'eventos' => array(
																'ancho' => 1024,
																'alto' 	=> 576,
															),
											'eventos_thumb' => array(
																'ancho' => 796,
																'alto' 	=> 448,
															),
											'banner_960x90' => array(
																'ancho' => 960,
																'alto' 	=> 90,
															),
											'banner_720x90' => array(
																'ancho' => 720,
																'alto' 	=> 200,
															),
											'banner_728x90' => array(
																'ancho' => 728,
																'alto' 	=> 90,
															),
											'banner_250x600' => array(
																'ancho' => 250,
																'alto' 	=> 600,
															),
											'banner_250x400' => array(
																'ancho' => 250,
																'alto' 	=> 400,
															),
											'banner_250x300' => array(
																'ancho' => 250,
																'alto' 	=> 300,
															),
											'banner_250x250' => array(
																'ancho' => 250,
																'alto' 	=> 250,
															),
											'banner_800x600' => array(
																'ancho' => 800,
																'alto' 	=> 200,
															),
											'user' => array(
																'ancho' => 200,
																'alto' 	=> 200,
															),
											/*'ancho_minimo_catalogos' => array(
																'ancho' => 800,
																'alto' 	=> 600,
															),
											'catalogos_horizontal' => array(
																'ancho' => 800,
																'alto' 	=> 450,
															),
											'catalogos_vertical' => array(
																'ancho' => 800,
																'alto' 	=> 1052,
															),
											'catalogos_cuadrado' => array(
																'ancho' => 800,
																'alto' 	=> 800,
															),
											'catalogos_thumb' => array(
																'ancho' => 300,
																'alto' 	=> 398,
															),
											'videos' => array(
																'ancho' => 300,
																'alto' 	=> 398,
															),
											'promociones' => array(
																'ancho' => 1080,
																'alto' 	=> 275,
															),
											'promociones_thumb' => array(
																'ancho' => 400,
																'alto' 	=> 102,
															),
											'fondos_thumb' => array(
																'ancho' => 200,
																'alto' 	=> 200,
															),
											'fondos' => array(
																'ancho' => 800,
																'alto' 	=> 800,
															),*/
											
											),	
											
			// En este archivo se colocan las imagenes subidas
			// por defecto. Se puede cambiar, por supuesto
			'ruta_img_cargadas'			=> RAIZ . 'public' . DS . 'img' . DS . 'subidas' . DS,
			
			'ruta_archivos_cargados'	=> RAIZ . 'public' . DS . 'manuales' . DS,
			
			'ruta_archivos_descargas'	=> RAIZ . 'public' . DS . 'descargas' . DS,
			
			'ruta_archivos'				=> RAIZ . 'public' . DS . 'files' . DS,
			
			'ruta_videos'				=> RAIZ . 'public' . DS . 'videos' . DS,
			
			'ruta_swf_cargados'			=> RAIZ . 'public' . DS . 'swf' . DS,
			
			'rango_pag'					=> array(
											'home' => 10,
										),
			// Widgets
			'widgets'					=> \Widgets\Widgets::data(),
			);	
	}
}